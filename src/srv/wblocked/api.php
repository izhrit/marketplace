<? header("HTTP/1.1 503 Service Unavailable"); ?>
<html>
	<head>
		<title>marketplace ПАУ - технический перерыв</title>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />
	</head>

	<style>
div.cpw-marketplace-ui-content
{
	display: block;
	width: 800px;
	margin:0 auto;
	text-align: center;
	margin-top: 40px;
}
	</style>
	<body class="cpw-ama">
		<div class="cpw-marketplace-ui-content">
			<h1>
				<p>marketplace ПАУ</p> 
				<p>
					находится в состоянии технического обслуживания<br/>
					и недоступна для использования.
				</p>
				
			</h1>
			<p>
				<b>Прикладной программный интерфейс также недоступен.</b>
			</p>
			Приносим свои извинения за доставленные неудобства.
		</div>
	</body>
</html>
