<?
$title= isset($title) ? $title : 'REST методы для marketplace ПАУ';
if ($trace_methods)
	write_to_log('   undefined action!');
?>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />
		<title><?= $title ?></title>
	</head>
	<body style="width:800px;margin:60px auto;text-align:center;">
		<h1><?= $title ?></h1>
		<p style="color:red;">
			Параметр действия неопределён
		</p>
		<? if (isset($doc_link)) : ?>
		<p>
			Документацию на API можно почитать <a href="<?= $doc_link ?>">здесь</a>. 
			Там же можно потестировать методы
		</p>
		<? endif; ?>
	</body>
</html>