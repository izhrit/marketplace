﻿<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';


$fields= "
	id_Asset
	,Name Name
";
$from_where= "from asset ";
$filter_rule_builders= array(
	'Name'=>'std_filter_rule_builder'
);
execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);
