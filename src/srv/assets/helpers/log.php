<?

function write_to_log_string($txt)
{
	global $log_file_name;
	file_put_contents($log_file_name,date('m/d/Y h:i:s a: ', time()).$txt."\r\n",FILE_APPEND);
	return $txt;
}

function write_to_log($data)
{
	return write_to_log_string(is_string($data) ? $data : print_r($data,true));
}

