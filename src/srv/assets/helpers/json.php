<?

function pretty_print_json( $json )
{
	$result = '';
	$level = 0;
	$in_quotes = false;
	$in_escape = false;
	$ends_line_level = NULL;
	$json_length = strlen( $json );

	for( $i = 0; $i < $json_length; $i++ )
	{
		$char = $json[$i];
		$new_line_level = NULL;
		$post = "";
		if( $ends_line_level !== NULL )
		{
			$new_line_level = $ends_line_level;
			$ends_line_level = NULL;
		}
		if     ( $in_escape )    { $in_escape = false; }
		else if( $char === '"' ) { $in_quotes = !$in_quotes; }
		else if( ! $in_quotes )
		{
			switch( $char )
			{
				case '}':
				case ']':
					$level--;
					$ends_line_level = NULL;
					$new_line_level = $level;
					break;
				case '{':
				case '[': $level++; case ',': $ends_line_level = $level; break;
				case ':': $post = " "; break;
				case " ":
				case "\t":
				case "\n":
				case "\r":
					$char = "";
					$ends_line_level = $new_line_level;
					$new_line_level = NULL;
					break;
			}
		}
		else if ( $char === '\\' )           { $in_escape = true; }
		if      ( $new_line_level !== NULL ) { $result .= "\r\n".str_repeat( "\t", $new_line_level ); }
		$result .= $char.$post;
	}
	return $result;
}

function fix_readable_utf8($json_txt)
{
	return preg_replace_callback('/\\\\u(\w{4})/', function ($matches) {return html_entity_decode('&#x' . $matches[1] . ';', ENT_COMPAT, 'UTF-8');}, $json_txt);
}

function nice_json_encode($obj)
{
	$json_txt= json_encode($obj);
	global $use_pretty_json_print;
	if ($use_pretty_json_print)
		$json_txt= pretty_print_json($json_txt);
	return fix_readable_utf8($json_txt);
}
