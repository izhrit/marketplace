<?php

function options_OPENSSL_RAW_DATA()
{
	$options= 0;
	if (version_compare(PHP_VERSION, '5.4.0') >= 0)
	{
		$options= OPENSSL_RAW_DATA;
	}
	else
	{
		$options= 'OPENSSL_RAW_DATA';
	}
	return $options;
}

function sync_encrypt($txt,$options)
{
	return openssl_encrypt($txt, "DES-EDE3", $options['key'], options_OPENSSL_RAW_DATA(), $options['iv']);
}

function sync_decrypt($txt,$options)
{
	return openssl_decrypt($txt, "DES-EDE3", $options['key'], options_OPENSSL_RAW_DATA(), $options['iv']);
}
