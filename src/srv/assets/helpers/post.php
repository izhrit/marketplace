<?php

function stripslashes_deep($value)
{
    $value = is_array($value) ?
                array_map('stripslashes_deep', $value) :
                stripslashes($value);

    return $value;
}

function safe_POST($value)
{
	return stripslashes_deep($value);
}
