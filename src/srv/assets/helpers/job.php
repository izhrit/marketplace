<?php

function echo_job_exception($ex)
{
	$ex_class= get_class($ex);
	$ex_Message= $ex->getMessage();
	echo "\r\nException occurred: $ex_class - $ex_Message\r\n";
}

function execute_job_part($exec,$title)
{
	echo "-------------------------------------------------------------------------------- { \r\n";
	echo job_time(). " $title\r\n";
	try
	{
		$exec();
	}
	catch (Exception $ex)
	{
		echo_job_exception($ex);
	}
	echo job_time(). " done ($title)\r\n";
	echo "-------------------------------------------------------------------------------- } \r\n";
}

function execute_job_parts($parts)
{
	foreach ($parts as $part)
	{
		execute_job_part($part['exec'],$part['title']);
	}
}

function execute_job_parts_locked_by_pid_file($parts,$pid_file_path)
{
	if (file_exists($pid_file_path))
	{
		echo "\r\n\r\np";
		echo job_time(). " pid file exists! ($pid_file_path)";
		echo "\r\n\r\n";
		exit();
	}

	file_put_contents($pid_file_path,date_format(date_create(),'Y-m-d\TH:i:s'));

	try
	{
		echo "******************************************************************************** { \r\n";
		execute_job_parts($parts);
		echo "******************************************************************************** } \r\n";
	}
	catch (Exception $ex)
	{
		unlink($pid_file_path);
		throw $ex;
	}
	unlink($pid_file_path);
}

function safe_execute_job_parts_locked_by_pid_file($parts,$pid_file_path)
{
	try
	{
		global $echo_errors, $do_not_write_errors_to_log;
		$echo_errors= true;
		$do_not_write_errors_to_log= true;
		execute_job_parts_locked_by_pid_file($parts,$pid_file_path);
	}
	catch (Exception $ex)
	{
		echo_job_exception($ex);
		throw $exception;
	}
}