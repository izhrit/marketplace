<?php

class XMLParser
{
	function internal_tag_open($xml_parser, $name, $attributes, $depth) {}
	function internal_cdata($xml_parser, $cdata, $depth) {}
	function internal_tag_close($xml_parser, $name, $depth) {}

	function bind($xml_parser)
	{
		xml_set_object($xml_parser, $this);
		xml_set_element_handler($xml_parser, "tag_open", "tag_close");
		xml_set_character_data_handler($xml_parser, "cdata");
	}
}

class XMLParser_echor extends XMLParser
{
	private $depth= 0;
	function internal_tag_open($xml_parser, $name, $attributes, $depth)
	{
		echo "$depth-".$name."\r\n";
		print_r($attributes);
	}
	function internal_cdata($xml_parser, $cdata, $depth)
	{
		print_r($cdata);
	}
}

class Field_parser extends XMLParser
{
	private $depth= 0;
	private $index_in_path= null;
	public $value= null;

	function __construct($path)
	{
		$this->path= $path;
	}

	function internal_tag_open($xml_parser, $name, $attributes, $depth)
	{
		$path_length= count($this->path);
		if ($depth<$path_length && $name==mb_strtoupper($this->path[$depth]))
		{
			if (null===$this->index_in_path)
			{
				if (0==$depth)
					$this->index_in_path= 0;
			}
			else if ($depth==$this->index_in_path+1)
			{
				$this->index_in_path++;
			}
		}
		if ($depth==$this->index_in_path && $this->index_in_path==$path_length-2)
		{
			$last_name= $this->path[$path_length-1];
			if ('@'==substr($last_name,0,1))
			{
				$attrname= substr($last_name,1);
				if (isset($attributes[$attrname]))
					$this->value= $attributes[$attrname];
			}
		}
	}

	function internal_cdata($xml_parser, $cdata, $depth) 
	{
		if ($this->index_in_path==count($this->path)-1)
		{
			$this->value= $cdata;
		}
	}

	function internal_tag_close($xml_parser, $name, $depth)
	{
		if ($depth==$this->index_in_path+1)
			$this->index_in_path--;
	}
}

class XMLParserArray extends XMLParser
{
	private $depth= 0;
	function __construct($parsers)
	{
		$this->depth= 0;
		$this->parsers= $parsers;
	}

	function tag_open($xml_parser, $name, $attributes) 
	{
		foreach ($this->parsers as $parser)
		{
			$parser->internal_tag_open($xml_parser, $name, $attributes, $this->depth);
		}
		$this->depth++;
	}

	function cdata($xml_parser, $cdata) 
	{
		foreach ($this->parsers as $field_parser)
		{
			$field_parser->internal_cdata($xml_parser, $cdata, $this->depth);
		}
	}

	function tag_close($xml_parser, $name) 
	{
		foreach ($this->parsers as $field_parser)
		{
			$field_parser->internal_tag_close($xml_parser, $name, $this->depth);
		}
		$this->depth--;
	}
}

function any_not_null_value($parsers)
{
	foreach ($parsers as $parser)
	{
		if (null!=$parser->value)
			return $parser->value;
	}
	return null;
}

class AFields_parser_combination
{
	protected $parsers= null;

	public function get_parsers() { return $this->parsers; }
	public function any_not_null_value() { return any_not_null_value($this->parsers); }

	public function __construct($aparsers)
	{
		$this->parsers= $aparsers;
	}
}

class AFields_parser_named_combination extends AFields_parser_combination
{
	protected $named_parsers= null;
	protected $parsers= null;

	public function __construct($parser_assoc_array)
	{
		$this->parsers= array_values($parser_assoc_array);
		$this->named_parsers= (object)$parser_assoc_array;
	}
}

function parse_fields($xml_text,$field_parsers)
{
	$parser = new XMLParserArray($field_parsers);

	$xml_parser = xml_parser_create();
	$parser->bind($xml_parser);
	if (!xml_parse($xml_parser, $xml_text, /* is_final= */TRUE))
	{
		$ex_text= sprintf("������ XML: %s �� ������ %d",
						xml_error_string(xml_get_error_code($xml_parser)),
						xml_get_current_line_number($xml_parser));
		xml_parser_free($xml_parser);
		throw new Exception($ex_text);
	}
	xml_parser_free($xml_parser);
}
