<?

function mysqli_stmt_get_result_without_mysqlnd( $Statement )
{
	$RESULT = array();
	$Statement->store_result();
	for ( $i = 0; $i < $Statement->num_rows; $i++ )
	{
		$Metadata = $Statement->result_metadata();
		$PARAMS = array();
		while ( $Field = $Metadata->fetch_field() )
		{
			$PARAMS[] = &$RESULT[ $i ][ $Field->name ];
		}
		call_user_func_array( array( $Statement, 'bind_result' ), $PARAMS );
		$Statement->fetch();
		$RESULT[$i]= (object)$RESULT[$i];
	}
	return $RESULT;
}

class Reusable_mysql_connection
{
	private $db_link= null;

	function get_db_link()
	{
		return $this->db_link;
	}

	function __construct($options= null)
	{
		global $default_db_options;

		if (!isset($options) || null==$options)
			$options= $default_db_options;

		$db_link = mysqli_connect($options->host, $options->user, $options->password, $options->dbname);

		if (!$db_link)
			die('Ошибка соединения: ' . mysql_error());

		if (!mysqli_set_charset($db_link, $options->charset))
			die('Ошибка соединения при установке кодировки: ' . mysql_error());

		$this->db_link= $db_link;
	}

	function __destruct()
	{
		if (null!=$this->db_link)
		{
			mysqli_close($this->db_link);
			$this->db_link= null;
			// write_to_log('mysqli_close ..');
		}
	}

	function result_with_found_rows($stmt)
	{
		$rows= get_query_result_rows($stmt,$this->db_link);
		$found_rows_result = mysqli_query($this->db_link, "select found_rows();");
		$found_rows_result_row= $found_rows_result->fetch_assoc();
		$num_rows = $found_rows_result_row['found_rows()'];
		return array('rows'=>$rows,'found_rows'=>$num_rows);
	}

	function write_to_log($txt_query,$parameters,$error=null)
	{
		if (count($parameters)>1)
		{
			$spec= $parameters[0];
			for ($i= 0; $i<strlen($spec); $i++)
			{
				$c= $spec[$i];
				if ('b'==$c && ($i+1)<count($parameters))
				{
					if (null!=$parameters[$i+1])
					{
						$l= strlen($parameters[$i+1]);
						$parameters[$i+1]= "!!! some not null value with length=$l !!!";
					}
				}
			}
		}
		$txt_parameters= print_r($parameters,true);
		$message= "exception for query: \"$txt_query\"
parameters: $txt_parameters";
		if (isset($error) && null!=$error)
			$message.= "
error:$error";
		write_to_log($message);
		global $echo_errors;
		if ($echo_errors)
			echo $message."\r\n";
	}

	function execute_query_internal($txt_query,$parameters,$return_result)
	{
		$query_result= null;
		try
		{
			$stmt= mysqli_stmt_init($this->db_link);
			if (!mysqli_stmt_prepare($stmt, $txt_query))
				throw new Exception("can not prepare query!");

			prepare_stmt_parameters($parameters,$stmt);
			if (!mysqli_stmt_execute($stmt))
				throw new Exception("can not execute query!");

			if (true===$return_result)
			{
				$query_result= get_query_result_rows($stmt,$this->db_link);
			}
			else if (false==$return_result)
			{
			}
			else if (3==$return_result)
			{
				$query_result= mysqli_insert_id($this->db_link);
			}
			else if (4==$return_result)
			{
				return $this->result_with_found_rows($stmt);
			}
			else if (5==$return_result)
			{
				return mysqli_stmt_affected_rows($stmt);
			}
			else if (isset($return_result))
			{
				write_to_log($return_result);
			}
			else
			{
				write_to_log('!isset(return_result)');
			}
		}
		catch (mysqli_sql_exception $ex)
		{
			$this->write_to_log($txt_query,$parameters);
			throw $ex;
		}
		catch (Exception $ex)
		{
			$this->write_to_log($txt_query,$parameters,mysqli_error($this->db_link));
			throw $ex;
		}
		if (false!=$return_result)
			return $query_result;
	}

	function execute_query($txt_query,$parameters)
	{
		return $this->execute_query_internal($txt_query,$parameters,true);
	}

	function execute_query_get_last_insert_id($txt_query,$parameters)
	{
		return $this->execute_query_internal($txt_query,$parameters,3);
	}

	function execute_query_get_found_rows($txt_query,$parameters)
	{
		return $this->execute_query_internal($txt_query,$parameters,4);
	}

	function execute_query_get_affected_rows($txt_query,$parameters)
	{
		return $this->execute_query_internal($txt_query,$parameters,5);
	}

	function execute_query_no_result($txt_query,$parameters= array())
	{
		$this->execute_query_internal($txt_query,$parameters,false);
	}

	function begin_transaction()
	{
		mysqli_query($this->db_link, "START TRANSACTION");
	}

	function commit()
	{
		mysqli_query($this->db_link, "COMMIT");
	}

	function rollback()
	{
		mysqli_query($this->db_link, "ROLLBACK");
	}
}

global $default_mysql_connection;
$default_mysql_connection= null;

function default_dbconnect()
{
	global $default_mysql_connection;

	if (null==$default_mysql_connection)
		$default_mysql_connection= new Reusable_mysql_connection();

	return $default_mysql_connection;
}

function prepare_stmt_parameters($p,$stmt)
{
	$parameters_len= count($p);
	if ($parameters_len>1)
	{
		$blobs= array();
		$types= $p[0];
		for ($i= 0; $i<$parameters_len-1; $i++)
		{
			if ('b'==substr($types,$i,1))
			{
				$blobs[$i]= $p[$i+1];
				$p[$i+1]= null;
			}
		}
		switch ($parameters_len)
		{
			case 0: break;
			case 1: break;
			case 2: mysqli_stmt_bind_param($stmt, $p[0], $p[1]); break;
			case 3: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2]); break;
			case 4: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3]); break;
			case 5: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4]); break;
			case 6: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5]); break;
			case 7: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6]); break;
			case 8: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7]); break;
			case 9: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8]); break;
			case 10: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9]); break;
			case 11: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10]); break;
			case 12: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11]); break;
			case 13: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12]); break;
			case 14: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13]); break;
			case 15: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14]); break;
			case 16: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15]); break;
			case 17: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16]); break;
			case 18: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17]); break;
			case 19: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18]); break;
			case 20: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19]); break;
			case 21: mysqli_stmt_bind_param($stmt, $p[0], $p[1], $p[2], $p[3], $p[4], $p[5], $p[6], $p[7], $p[8], $p[9], $p[10], $p[11], $p[12], $p[13], $p[14], $p[15], $p[16], $p[17], $p[18], $p[19], $p[20]); break;
		}
		foreach ($blobs as $i => $blob)
		{
			mysqli_stmt_send_long_data($stmt,$i,$blob);
		}
	}
}

function get_query_result_rows($stmt,$connection)
{
	$query_result= array();
	$result = mysqli_stmt_get_result_without_mysqlnd($stmt);
	while($connection->more_results() && $connection->next_result())
		$connection->store_result();
	while ($row = array_shift($result))
	{
		$query_result[]= $row;
	}
	return $query_result;
}

function execute_query_internal($txt_query,$parameters,$return_result)
{
	default_dbconnect()->execute_query_internal($txt_query,$parameters,$return_result);
}

function execute_query($txt_query,$parameters)
{
	return default_dbconnect()->execute_query_internal($txt_query,$parameters,true);
}

function execute_query_get_last_insert_id($txt_query,$parameters)
{
	return default_dbconnect()->execute_query_internal($txt_query,$parameters,3);
}

function execute_query_get_found_rows($txt_query,$parameters)
{
	return default_dbconnect()->execute_query_internal($txt_query,$parameters,4);
}

function execute_query_get_affected_rows($txt_query,$parameters)
{
	return default_dbconnect()->execute_query_internal($txt_query,$parameters,5);
}

function execute_query_no_result($txt_query,$parameters= array())
{
	default_dbconnect()->execute_query_internal($txt_query,$parameters,false);
}


