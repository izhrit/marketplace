<?

function zipFileErrMsg($errno)
{
	// using constant name as a string to make this function PHP4 compatible
	$zipFileFunctionsErrors = array(
	  'ZIPARCHIVE::ER_MULTIDISK' => 'Multi-disk zip archives not supported.',
	  'ZIPARCHIVE::ER_RENAME' => 'Renaming temporary file failed.',
	  'ZIPARCHIVE::ER_CLOSE' => 'Closing zip archive failed',
	  'ZIPARCHIVE::ER_SEEK' => 'Seek error',
	  'ZIPARCHIVE::ER_READ' => 'Read error',
	  'ZIPARCHIVE::ER_WRITE' => 'Write error',
	  'ZIPARCHIVE::ER_CRC' => 'CRC error',
	  'ZIPARCHIVE::ER_ZIPCLOSED' => 'Containing zip archive was closed',
	  'ZIPARCHIVE::ER_NOENT' => 'No such file.',
	  'ZIPARCHIVE::ER_EXISTS' => 'File already exists',
	  'ZIPARCHIVE::ER_OPEN' => 'Can\'t open file',
	  'ZIPARCHIVE::ER_TMPOPEN' => 'Failure to create temporary file.',
	  'ZIPARCHIVE::ER_ZLIB' => 'Zlib error',
	  'ZIPARCHIVE::ER_MEMORY' => 'Memory allocation failure',
	  'ZIPARCHIVE::ER_CHANGED' => 'Entry has been changed',
	  'ZIPARCHIVE::ER_COMPNOTSUPP' => 'Compression method not supported.',
	  'ZIPARCHIVE::ER_EOF' => 'Premature EOF',
	  'ZIPARCHIVE::ER_INVAL' => 'Invalid argument',
	  'ZIPARCHIVE::ER_NOZIP' => 'Not a zip archive',
	  'ZIPARCHIVE::ER_INTERNAL' => 'Internal error',
	  'ZIPARCHIVE::ER_INCONS' => 'Zip archive inconsistent',
	  'ZIPARCHIVE::ER_REMOVE' => 'Can\'t remove file',
	  'ZIPARCHIVE::ER_DELETED' => 'Entry has been deleted',
	);
	$errmsg = 'unknown';
	foreach ($zipFileFunctionsErrors as $constName => $errorMessage) {
		if (defined($constName) and constant($constName) === $errno) {
			return 'Zip File Function error: '.$errorMessage;
		}
	}
	return 'Zip File Function error: unknown';
}

function ReadZippedFile($zip_file_name,$file_name_in_zip)
{
	$contents= null;
	$zip = zip_open($zip_file_name);
	if (!is_resource($zip))
	{
		echo "\r\n";
		echo "zip_file_name=$zip_file_name<br/>\r\n";
		echo zipFileErrMsg($zip)."<br/>\r\n";
		echo 'filesize='.filesize($zip_file_name)."<br/>\r\n";
		echo 'md5='.md5_file($zip_file_name)."<br/>\r\n";
	}
	else if ($zip)
	{
		while ($zip_entry = zip_read($zip))
		{
			$zip_entry_name= zip_entry_name($zip_entry);
			if ($file_name_in_zip==$zip_entry_name)
			{
				if (zip_entry_open($zip, $zip_entry))
				{
					$contents = zip_entry_read($zip_entry, 32505856);
					zip_entry_close($zip_entry);
				}
				break;
			}
		}
		zip_close($zip);
	}
	else // if (null==$contents)
	{
		$zip_path= 'zip://'.$zip_file_name.'#'.$file_name_in_zip;
		$contents= file_get_contents($zip_path);
	}
	return $contents;
}
