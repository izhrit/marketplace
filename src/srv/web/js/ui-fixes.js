﻿$(function ()
{
	// fix for problem of select2 on jquery.ui.dialog, see https://stackoverflow.com/questions/19787982/select2-plugin-and-jquery-ui-modal-dialogs
	$.ui.dialog.prototype._allowInteraction = function(e)
	{
		return !!$(e.target).closest('.ui-dialog, .ui-datepicker, .select2-drop').length;
	};
});
