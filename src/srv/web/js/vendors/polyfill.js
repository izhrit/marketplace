/* Polyfill service v3.96.0
 * For detailed credits and licence information see https://github.com/financial-times/polyfill-service.
 * 
 * Features requested: Intl.~locale.ru
 * 
 * - _ESAbstract.Call, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyDescriptor", "_ESAbstract.ToPropertyKey", "_ESAbstract.ToString", "_ESAbstract.ToPrimitive", "_ESAbstract.OrdinaryToPrimitive", "Intl.~locale.ru")
 * - _ESAbstract.CreateDataProperty, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "_ESAbstract.CreateIterResultObject", "Intl.~locale.ru")
 * - _ESAbstract.CreateMethodProperty, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyNames", "Array.prototype.includes", "Intl.~locale.ru")
 * - _ESAbstract.Get, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyDescriptor", "_ESAbstract.ToPropertyKey", "_ESAbstract.ToString", "_ESAbstract.ToPrimitive", "_ESAbstract.OrdinaryToPrimitive", "Intl.~locale.ru")
 * - _ESAbstract.HasOwnProperty, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyDescriptor", "Intl.~locale.ru")
 * - _ESAbstract.IsArray, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "WeakMap", "Intl.~locale.ru")
 * - _ESAbstract.IsCallable, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyDescriptor", "_ESAbstract.ToPropertyKey", "_ESAbstract.ToString", "_ESAbstract.ToPrimitive", "_ESAbstract.OrdinaryToPrimitive", "Intl.~locale.ru")
 * - _ESAbstract.SameValueNonNumber, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "_ESAbstract.SameValueZero", "Intl.~locale.ru")
 * - _ESAbstract.ToBoolean, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "_ESAbstract.IteratorStep", "_ESAbstract.IteratorComplete", "Intl.~locale.ru")
 * - _ESAbstract.ToInteger, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyNames", "Array.prototype.includes", "_ESAbstract.ToLength", "Intl.~locale.ru")
 * - _ESAbstract.ToLength, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyNames", "Array.prototype.includes", "Intl.~locale.ru")
 * - _ESAbstract.ToObject, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyNames", "Array.prototype.includes", "Intl.~locale.ru")
 * - _ESAbstract.GetV, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "_ESAbstract.IteratorClose", "_ESAbstract.GetMethod", "Intl.~locale.ru")
 * - _ESAbstract.GetMethod, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyDescriptor", "_ESAbstract.ToPropertyKey", "_ESAbstract.ToString", "_ESAbstract.ToPrimitive", "Intl.~locale.ru")
 * - _ESAbstract.Type, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyDescriptor", "_ESAbstract.ToPropertyKey", "_ESAbstract.ToString", "_ESAbstract.ToPrimitive", "_ESAbstract.OrdinaryToPrimitive", "Intl.~locale.ru")
 * - _ESAbstract.CreateIterResultObject, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Intl.~locale.ru")
 * - _ESAbstract.GetPrototypeFromConstructor, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "_ESAbstract.OrdinaryCreateFromConstructor", "Intl.~locale.ru")
 * - _ESAbstract.OrdinaryCreateFromConstructor, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Intl.~locale.ru")
 * - _ESAbstract.IteratorClose, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Intl.~locale.ru")
 * - _ESAbstract.IteratorComplete, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "_ESAbstract.IteratorStep", "Intl.~locale.ru")
 * - _ESAbstract.IteratorNext, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "_ESAbstract.IteratorStep", "Intl.~locale.ru")
 * - _ESAbstract.IteratorStep, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Intl.~locale.ru")
 * - _ESAbstract.IteratorValue, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Intl.~locale.ru")
 * - _ESAbstract.OrdinaryToPrimitive, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyDescriptor", "_ESAbstract.ToPropertyKey", "_ESAbstract.ToString", "_ESAbstract.ToPrimitive", "Intl.~locale.ru")
 * - _ESAbstract.SameValue, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "WeakMap", "Intl.~locale.ru")
 * - _ESAbstract.SameValueZero, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyNames", "Array.prototype.includes", "Intl.~locale.ru")
 * - _ESAbstract.ToPrimitive, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyDescriptor", "_ESAbstract.ToPropertyKey", "_ESAbstract.ToString", "Intl.~locale.ru")
 * - _ESAbstract.ToString, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyDescriptor", "_ESAbstract.ToPropertyKey", "Intl.~locale.ru")
 * - _ESAbstract.ToPropertyKey, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyDescriptor", "Intl.~locale.ru")
 * - Array.prototype.includes, License: MIT (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyNames", "Intl.~locale.ru")
 * - Intl.getCanonicalLocales, License: MIT (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Intl.PluralRules", "Intl.~locale.ru")
 * - Object.getOwnPropertyDescriptor, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Intl.~locale.ru")
 * - Object.keys, License: MIT (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Object.getOwnPropertyNames", "Intl.~locale.ru")
 * - Object.getOwnPropertyNames, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol", "Intl.~locale.ru")
 * - Symbol, License: MIT (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Symbol.species", "Intl.~locale.ru")
 * - Symbol.iterator, License: MIT (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "_ESAbstract.GetIterator", "Intl.~locale.ru")
 * - _ESAbstract.GetIterator, License: CC0 (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Intl.~locale.ru")
 * - Symbol.species, License: MIT (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Set", "Intl.~locale.ru")
 * - Set, License: CC0 (required by "Intl.DateTimeFormat.~locale.ru", "Intl.DateTimeFormat", "Intl.~locale.ru")
 * - WeakMap, License: MIT (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Intl.PluralRules", "Intl.~locale.ru")
 * - Intl.PluralRules, License: MIT (required by "Intl.NumberFormat.~locale.ru", "Intl.NumberFormat", "Intl.~locale.ru")
 * - Intl.NumberFormat, License: MIT (required by "Intl.DateTimeFormat.~locale.ru", "Intl.DateTimeFormat", "Intl.~locale.ru")
 * - Intl.DateTimeFormat, License: MIT (required by "Intl.DateTimeFormat.~locale.ru", "Intl.~locale.ru")
 * - Intl.DateTimeFormat.~locale.ru, License: MIT (required by "Intl.~locale.ru")
 * - Intl.NumberFormat.~locale.ru, License: MIT (required by "Intl.~locale.ru")
 * - Intl.PluralRules.~locale.ru, License: MIT (required by "Intl.~locale.ru") */

(function(self, undefined) {

// _ESAbstract.Call
/* global IsCallable */
// 7.3.12. Call ( F, V [ , argumentsList ] )
function Call(F, V /* [, argumentsList] */) { // eslint-disable-line no-unused-vars
	// 1. If argumentsList is not present, set argumentsList to a new empty List.
	var argumentsList = arguments.length > 2 ? arguments[2] : [];
	// 2. If IsCallable(F) is false, throw a TypeError exception.
	if (IsCallable(F) === false) {
		throw new TypeError(Object.prototype.toString.call(F) + 'is not a function.');
	}
	// 3. Return ? F.[[Call]](V, argumentsList).
	return F.apply(V, argumentsList);
}

// _ESAbstract.CreateDataProperty
// 7.3.4. CreateDataProperty ( O, P, V )
// NOTE
// This abstract operation creates a property whose attributes are set to the same defaults used for properties created by the ECMAScript language assignment operator.
// Normally, the property will not already exist. If it does exist and is not configurable or if O is not extensible, [[DefineOwnProperty]] will return false.
function CreateDataProperty(O, P, V) { // eslint-disable-line no-unused-vars
	// 1. Assert: Type(O) is Object.
	// 2. Assert: IsPropertyKey(P) is true.
	// 3. Let newDesc be the PropertyDescriptor{ [[Value]]: V, [[Writable]]: true, [[Enumerable]]: true, [[Configurable]]: true }.
	var newDesc = {
		value: V,
		writable: true,
		enumerable: true,
		configurable: true
	};
	// 4. Return ? O.[[DefineOwnProperty]](P, newDesc).
	try {
		Object.defineProperty(O, P, newDesc);
		return true;
	} catch (e) {
		return false;
	}
}

// _ESAbstract.CreateMethodProperty
// 7.3.5. CreateMethodProperty ( O, P, V )
function CreateMethodProperty(O, P, V) { // eslint-disable-line no-unused-vars
	// 1. Assert: Type(O) is Object.
	// 2. Assert: IsPropertyKey(P) is true.
	// 3. Let newDesc be the PropertyDescriptor{[[Value]]: V, [[Writable]]: true, [[Enumerable]]: false, [[Configurable]]: true}.
	var newDesc = {
		value: V,
		writable: true,
		enumerable: false,
		configurable: true
	};
	// 4. Return ? O.[[DefineOwnProperty]](P, newDesc).
	Object.defineProperty(O, P, newDesc);
}

// _ESAbstract.Get
// 7.3.1. Get ( O, P )
function Get(O, P) { // eslint-disable-line no-unused-vars
	// 1. Assert: Type(O) is Object.
	// 2. Assert: IsPropertyKey(P) is true.
	// 3. Return ? O.[[Get]](P, O).
	return O[P];
}

// _ESAbstract.HasOwnProperty
// 7.3.11 HasOwnProperty (O, P)
function HasOwnProperty(o, p) { // eslint-disable-line no-unused-vars
	// 1. Assert: Type(O) is Object.
	// 2. Assert: IsPropertyKey(P) is true.
	// 3. Let desc be ? O.[[GetOwnProperty]](P).
	// 4. If desc is undefined, return false.
	// 5. Return true.
	// Polyfill.io - As we expect user agents to support ES3 fully we can skip the above steps and use Object.prototype.hasOwnProperty to do them for us.
	return Object.prototype.hasOwnProperty.call(o, p);
}

// _ESAbstract.IsArray
// 7.2.2. IsArray ( argument )
function IsArray(argument) { // eslint-disable-line no-unused-vars
	// 1. If Type(argument) is not Object, return false.
	// 2. If argument is an Array exotic object, return true.
	// 3. If argument is a Proxy exotic object, then
		// a. If argument.[[ProxyHandler]] is null, throw a TypeError exception.
		// b. Let target be argument.[[ProxyTarget]].
		// c. Return ? IsArray(target).
	// 4. Return false.

	// Polyfill.io - We can skip all the above steps and check the string returned from Object.prototype.toString().
	return Object.prototype.toString.call(argument) === '[object Array]';
}

// _ESAbstract.IsCallable
// 7.2.3. IsCallable ( argument )
function IsCallable(argument) { // eslint-disable-line no-unused-vars
	// 1. If Type(argument) is not Object, return false.
	// 2. If argument has a [[Call]] internal method, return true.
	// 3. Return false.

	// Polyfill.io - Only function objects have a [[Call]] internal method. This means we can simplify this function to check that the argument has a type of function.
	return typeof argument === 'function';
}

// _ESAbstract.SameValueNonNumber
// 7.2.12. SameValueNonNumber ( x, y )
function SameValueNonNumber(x, y) { // eslint-disable-line no-unused-vars
	// 1. Assert: Type(x) is not Number.
	// 2. Assert: Type(x) is the same as Type(y).
	// 3. If Type(x) is Undefined, return true.
	// 4. If Type(x) is Null, return true.
	// 5. If Type(x) is String, then
		// a. If x and y are exactly the same sequence of code units (same length and same code units at corresponding indices), return true; otherwise, return false.
	// 6. If Type(x) is Boolean, then
		// a. If x and y are both true or both false, return true; otherwise, return false.
	// 7. If Type(x) is Symbol, then
		// a. If x and y are both the same Symbol value, return true; otherwise, return false.
	// 8. If x and y are the same Object value, return true. Otherwise, return false.

	// Polyfill.io - We can skip all above steps because the === operator does it all for us.
	return x === y;
}

// _ESAbstract.ToBoolean
// 7.1.2. ToBoolean ( argument )
// The abstract operation ToBoolean converts argument to a value of type Boolean according to Table 9:
/*
--------------------------------------------------------------------------------------------------------------
| Argument Type | Result                                                                                     |
--------------------------------------------------------------------------------------------------------------
| Undefined     | Return false.                                                                              |
| Null          | Return false.                                                                              |
| Boolean       | Return argument.                                                                           |
| Number        | If argument is +0, -0, or NaN, return false; otherwise return true.                        |
| String        | If argument is the empty String (its length is zero), return false; otherwise return true. |
| Symbol        | Return true.                                                                               |
| Object        | Return true.                                                                               |
--------------------------------------------------------------------------------------------------------------
*/
function ToBoolean(argument) { // eslint-disable-line no-unused-vars
	return Boolean(argument);
}

// _ESAbstract.ToInteger
// 7.1.4. ToInteger ( argument )
function ToInteger(argument) { // eslint-disable-line no-unused-vars
	// 1. Let number be ? ToNumber(argument).
	var number = Number(argument);
	// 2. If number is NaN, return +0.
	if (isNaN(number)) {
		return 0;
	}
	// 3. If number is +0, -0, +∞, or -∞, return number.
	if (1/number === Infinity || 1/number === -Infinity || number === Infinity || number === -Infinity) {
		return number;
	}
	// 4. Return the number value that is the same sign as number and whose magnitude is floor(abs(number)).
	return ((number < 0) ? -1 : 1) * Math.floor(Math.abs(number));
}

// _ESAbstract.ToLength
/* global ToInteger */
// 7.1.15. ToLength ( argument )
function ToLength(argument) { // eslint-disable-line no-unused-vars
	// 1. Let len be ? ToInteger(argument).
	var len = ToInteger(argument);
	// 2. If len ≤ +0, return +0.
	if (len <= 0) {
		return 0;
	}
	// 3. Return min(len, 253-1).
	return Math.min(len, Math.pow(2, 53) -1);
}

// _ESAbstract.ToObject
// 7.1.13 ToObject ( argument )
// The abstract operation ToObject converts argument to a value of type Object according to Table 12:
// Table 12: ToObject Conversions
/*
|----------------------------------------------------------------------------------------------------------------------------------------------------|
| Argument Type | Result                                                                                                                             |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
| Undefined     | Throw a TypeError exception.                                                                                                       |
| Null          | Throw a TypeError exception.                                                                                                       |
| Boolean       | Return a new Boolean object whose [[BooleanData]] internal slot is set to argument. See 19.3 for a description of Boolean objects. |
| Number        | Return a new Number object whose [[NumberData]] internal slot is set to argument. See 20.1 for a description of Number objects.    |
| String        | Return a new String object whose [[StringData]] internal slot is set to argument. See 21.1 for a description of String objects.    |
| Symbol        | Return a new Symbol object whose [[SymbolData]] internal slot is set to argument. See 19.4 for a description of Symbol objects.    |
| Object        | Return argument.                                                                                                                   |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
*/
function ToObject(argument) { // eslint-disable-line no-unused-vars
	if (argument === null || argument === undefined) {
		throw TypeError();
	}
  return Object(argument);
}

// _ESAbstract.GetV
/* global ToObject */
// 7.3.2 GetV (V, P)
function GetV(v, p) { // eslint-disable-line no-unused-vars
	// 1. Assert: IsPropertyKey(P) is true.
	// 2. Let O be ? ToObject(V).
	var o = ToObject(v);
	// 3. Return ? O.[[Get]](P, V).
	return o[p];
}

// _ESAbstract.GetMethod
/* global GetV, IsCallable */
// 7.3.9. GetMethod ( V, P )
function GetMethod(V, P) { // eslint-disable-line no-unused-vars
	// 1. Assert: IsPropertyKey(P) is true.
	// 2. Let func be ? GetV(V, P).
	var func = GetV(V, P);
	// 3. If func is either undefined or null, return undefined.
	if (func === null || func === undefined) {
		return undefined;
	}
	// 4. If IsCallable(func) is false, throw a TypeError exception.
	if (IsCallable(func) === false) {
		throw new TypeError('Method not callable: ' + P);
	}
	// 5. Return func.
	return func;
}

// _ESAbstract.Type
// "Type(x)" is used as shorthand for "the type of x"...
function Type(x) { // eslint-disable-line no-unused-vars
	switch (typeof x) {
		case 'undefined':
			return 'undefined';
		case 'boolean':
			return 'boolean';
		case 'number':
			return 'number';
		case 'string':
			return 'string';
		case 'symbol':
			return 'symbol';
		default:
			// typeof null is 'object'
			if (x === null) return 'null';
			// Polyfill.io - This is here because a Symbol polyfill will have a typeof `object`.
			if ('Symbol' in self && (x instanceof self.Symbol || x.constructor === self.Symbol)) return 'symbol';
			return 'object';
	}
}

// _ESAbstract.CreateIterResultObject
/* global Type, CreateDataProperty */
// 7.4.7. CreateIterResultObject ( value, done )
function CreateIterResultObject(value, done) { // eslint-disable-line no-unused-vars
	// 1. Assert: Type(done) is Boolean.
	if (Type(done) !== 'boolean') {
		throw new Error();
	}
	// 2. Let obj be ObjectCreate(%ObjectPrototype%).
	var obj = {};
	// 3. Perform CreateDataProperty(obj, "value", value).
	CreateDataProperty(obj, "value", value);
	// 4. Perform CreateDataProperty(obj, "done", done).
	CreateDataProperty(obj, "done", done);
	// 5. Return obj.
	return obj;
}

// _ESAbstract.GetPrototypeFromConstructor
/* global Get, Type */
// 9.1.14. GetPrototypeFromConstructor ( constructor, intrinsicDefaultProto )
function GetPrototypeFromConstructor(constructor, intrinsicDefaultProto) { // eslint-disable-line no-unused-vars
	// 1. Assert: intrinsicDefaultProto is a String value that is this specification's name of an intrinsic object. The corresponding object must be an intrinsic that is intended to be used as the [[Prototype]] value of an object.
	// 2. Assert: IsCallable(constructor) is true.
	// 3. Let proto be ? Get(constructor, "prototype").
	var proto = Get(constructor, "prototype");
	// 4. If Type(proto) is not Object, then
	if (Type(proto) !== 'object') {
		// a. Let realm be ? GetFunctionRealm(constructor).
		// b. Set proto to realm's intrinsic object named intrinsicDefaultProto.
		proto = intrinsicDefaultProto;
	}
	// 5. Return proto.
	return proto;
}

// _ESAbstract.OrdinaryCreateFromConstructor
/* global GetPrototypeFromConstructor */
// 9.1.13. OrdinaryCreateFromConstructor ( constructor, intrinsicDefaultProto [ , internalSlotsList ] )
function OrdinaryCreateFromConstructor(constructor, intrinsicDefaultProto) { // eslint-disable-line no-unused-vars
	var internalSlotsList = arguments[2] || {};
	// 1. Assert: intrinsicDefaultProto is a String value that is this specification's name of an intrinsic object.
	// The corresponding object must be an intrinsic that is intended to be used as the[[Prototype]] value of an object.

	// 2. Let proto be ? GetPrototypeFromConstructor(constructor, intrinsicDefaultProto).
	var proto = GetPrototypeFromConstructor(constructor, intrinsicDefaultProto);

	// 3. Return ObjectCreate(proto, internalSlotsList).
	// Polyfill.io - We do not pass internalSlotsList to Object.create because Object.create does not use the default ordinary object definitions specified in 9.1.
	var obj = Object.create(proto);
	for (var name in internalSlotsList) {
		if (Object.prototype.hasOwnProperty.call(internalSlotsList, name)) {
			Object.defineProperty(obj, name, {
				configurable: true,
				enumerable: false,
				writable: true,
				value: internalSlotsList[name]
			});
		}
	}
	return obj;
}

// _ESAbstract.IteratorClose
/* global GetMethod, Type, Call */
// 7.4.6. IteratorClose ( iteratorRecord, completion )
function IteratorClose(iteratorRecord, completion) { // eslint-disable-line no-unused-vars
	// 1. Assert: Type(iteratorRecord.[[Iterator]]) is Object.
	if (Type(iteratorRecord['[[Iterator]]']) !== 'object') {
		throw new Error(Object.prototype.toString.call(iteratorRecord['[[Iterator]]']) + 'is not an Object.');
	}
	// 2. Assert: completion is a Completion Record.
	// Polyfill.io - Ignoring this step as there is no way to check if something is a Completion Record in userland JavaScript.

	// 3. Let iterator be iteratorRecord.[[Iterator]].
	var iterator = iteratorRecord['[[Iterator]]'];
	// 4. Let return be ? GetMethod(iterator, "return").
	// Polyfill.io - We name it  returnMethod because return is a keyword and can not be used as an identifier (E.G. variable name, function name etc).
	var returnMethod = GetMethod(iterator, "return");
	// 5. If return is undefined, return Completion(completion).
	if (returnMethod === undefined) {
		return completion;
	}
	// 6. Let innerResult be Call(return, iterator, « »).
	try {
		var innerResult = Call(returnMethod, iterator);
	} catch (error) {
		var innerException = error;
	}
	// 7. If completion.[[Type]] is throw, return Completion(completion).
	if (completion) {
		return completion;
	}
	// 8. If innerResult.[[Type]] is throw, return Completion(innerResult).
	if (innerException) {
		throw innerException;
	}
	// 9. If Type(innerResult.[[Value]]) is not Object, throw a TypeError exception.
	if (Type(innerResult) !== 'object') {
		throw new TypeError("Iterator's return method returned a non-object.");
	}
	// 10. Return Completion(completion).
	return completion;
}

// _ESAbstract.IteratorComplete
/* global Type, ToBoolean, Get */
// 7.4.3 IteratorComplete ( iterResult )
function IteratorComplete(iterResult) { // eslint-disable-line no-unused-vars
	// 1. Assert: Type(iterResult) is Object.
	if (Type(iterResult) !== 'object') {
		throw new Error(Object.prototype.toString.call(iterResult) + 'is not an Object.');
	}
	// 2. Return ToBoolean(? Get(iterResult, "done")).
	return ToBoolean(Get(iterResult, "done"));
}

// _ESAbstract.IteratorNext
/* global Call, Type */
// 7.4.2. IteratorNext ( iteratorRecord [ , value ] )
function IteratorNext(iteratorRecord /* [, value] */) { // eslint-disable-line no-unused-vars
	// 1. If value is not present, then
	if (arguments.length < 2) {
		// a. Let result be ? Call(iteratorRecord.[[NextMethod]], iteratorRecord.[[Iterator]], « »).
		var result = Call(iteratorRecord['[[NextMethod]]'], iteratorRecord['[[Iterator]]']);
	// 2. Else,
	} else {
		// a. Let result be ? Call(iteratorRecord.[[NextMethod]], iteratorRecord.[[Iterator]], « value »).
		result = Call(iteratorRecord['[[NextMethod]]'], iteratorRecord['[[Iterator]]'], [arguments[1]]);
	}
	// 3. If Type(result) is not Object, throw a TypeError exception.
	if (Type(result) !== 'object') {
		throw new TypeError('bad iterator');
	}
	// 4. Return result.
	return result;
}

// _ESAbstract.IteratorStep
/* global IteratorNext, IteratorComplete */
// 7.4.5. IteratorStep ( iteratorRecord )
function IteratorStep(iteratorRecord) { // eslint-disable-line no-unused-vars
	// 1. Let result be ? IteratorNext(iteratorRecord).
	var result = IteratorNext(iteratorRecord);
	// 2. Let done be ? IteratorComplete(result).
	var done = IteratorComplete(result);
	// 3. If done is true, return false.
	if (done === true) {
		return false;
	}
	// 4. Return result.
	return result;
}

// _ESAbstract.IteratorValue
/* global Type, Get */
// 7.4.4 IteratorValue ( iterResult )
function IteratorValue(iterResult) { // eslint-disable-line no-unused-vars
	// Assert: Type(iterResult) is Object.
	if (Type(iterResult) !== 'object') {
		throw new Error(Object.prototype.toString.call(iterResult) + 'is not an Object.');
	}
	// Return ? Get(iterResult, "value").
	return Get(iterResult, "value");
}

// _ESAbstract.OrdinaryToPrimitive
/* global Get, IsCallable, Call, Type */
// 7.1.1.1. OrdinaryToPrimitive ( O, hint )
function OrdinaryToPrimitive(O, hint) { // eslint-disable-line no-unused-vars
	// 1. Assert: Type(O) is Object.
	// 2. Assert: Type(hint) is String and its value is either "string" or "number".
	// 3. If hint is "string", then
	if (hint === 'string') {
		// a. Let methodNames be « "toString", "valueOf" ».
		var methodNames = ['toString', 'valueOf'];
		// 4. Else,
	} else {
		// a. Let methodNames be « "valueOf", "toString" ».
		methodNames = ['valueOf', 'toString'];
	}
	// 5. For each name in methodNames in List order, do
	for (var i = 0; i < methodNames.length; ++i) {
		var name = methodNames[i];
		// a. Let method be ? Get(O, name).
		var method = Get(O, name);
		// b. If IsCallable(method) is true, then
		if (IsCallable(method)) {
			// i. Let result be ? Call(method, O).
			var result = Call(method, O);
			// ii. If Type(result) is not Object, return result.
			if (Type(result) !== 'object') {
				return result;
			}
		}
	}
	// 6. Throw a TypeError exception.
	throw new TypeError('Cannot convert to primitive.');
}

// _ESAbstract.SameValue
/* global Type, SameValueNonNumber */
// 7.2.10. SameValue ( x, y )
function SameValue(x, y) { // eslint-disable-line no-unused-vars
	// 1. If Type(x) is different from Type(y), return false.
	if (Type(x) !== Type(y)) {
		return false;
	}
	// 2. If Type(x) is Number, then
	if (Type(x) === 'number') {
		// a. If x is NaN and y is NaN, return true.
		if (isNaN(x) && isNaN(y)) {
			return true;
		}
		// Polyfill.io - 0 === -0 is true, but they are not the same value.
		// b. If x is +0 and y is -0, return false.
		// c. If x is -0 and y is +0, return false.
		if (x === 0 && y === 0 && 1/x !== 1/y) {
			return false;
		}
		// d. If x is the same Number value as y, return true.
		if (x === y) {
			return true;
		}
		// e. Return false.
		return false;
	}
	// 3. Return SameValueNonNumber(x, y).
	return SameValueNonNumber(x, y);
}

// _ESAbstract.SameValueZero
/* global Type, SameValueNonNumber */
// 7.2.11. SameValueZero ( x, y )
function SameValueZero (x, y) { // eslint-disable-line no-unused-vars
	// 1. If Type(x) is different from Type(y), return false.
	if (Type(x) !== Type(y)) {
		return false;
	}
	// 2. If Type(x) is Number, then
	if (Type(x) === 'number') {
		// a. If x is NaN and y is NaN, return true.
		if (isNaN(x) && isNaN(y)) {
			return true;
		}
		// b. If x is +0 and y is -0, return true.
		if (1/x === Infinity && 1/y === -Infinity) {
			return true;
		}
		// c. If x is -0 and y is +0, return true.
		if (1/x === -Infinity && 1/y === Infinity) {
			return true;
		}
		// d. If x is the same Number value as y, return true.
		if (x === y) {
			return true;
		}
		// e. Return false.
		return false;
	}
	// 3. Return SameValueNonNumber(x, y).
	return SameValueNonNumber(x, y);
}

// _ESAbstract.ToPrimitive
/* global Type, GetMethod, Call, OrdinaryToPrimitive */
// 7.1.1. ToPrimitive ( input [ , PreferredType ] )
function ToPrimitive(input /* [, PreferredType] */) { // eslint-disable-line no-unused-vars
	var PreferredType = arguments.length > 1 ? arguments[1] : undefined;
	// 1. Assert: input is an ECMAScript language value.
	// 2. If Type(input) is Object, then
	if (Type(input) === 'object') {
		// a. If PreferredType is not present, let hint be "default".
		if (arguments.length < 2) {
			var hint = 'default';
			// b. Else if PreferredType is hint String, let hint be "string".
		} else if (PreferredType === String) {
			hint = 'string';
			// c. Else PreferredType is hint Number, let hint be "number".
		} else if (PreferredType === Number) {
			hint = 'number';
		}
		// d. Let exoticToPrim be ? GetMethod(input, @@toPrimitive).
		var exoticToPrim = typeof self.Symbol === 'function' && typeof self.Symbol.toPrimitive === 'symbol' ? GetMethod(input, self.Symbol.toPrimitive) : undefined;
		// e. If exoticToPrim is not undefined, then
		if (exoticToPrim !== undefined) {
			// i. Let result be ? Call(exoticToPrim, input, « hint »).
			var result = Call(exoticToPrim, input, [hint]);
			// ii. If Type(result) is not Object, return result.
			if (Type(result) !== 'object') {
				return result;
			}
			// iii. Throw a TypeError exception.
			throw new TypeError('Cannot convert exotic object to primitive.');
		}
		// f. If hint is "default", set hint to "number".
		if (hint === 'default') {
			hint = 'number';
		}
		// g. Return ? OrdinaryToPrimitive(input, hint).
		return OrdinaryToPrimitive(input, hint);
	}
	// 3. Return input
	return input;
}
// _ESAbstract.ToString
/* global Type, ToPrimitive */
// 7.1.12. ToString ( argument )
// The abstract operation ToString converts argument to a value of type String according to Table 11:
// Table 11: ToString Conversions
/*
|---------------|--------------------------------------------------------|
| Argument Type | Result                                                 |
|---------------|--------------------------------------------------------|
| Undefined     | Return "undefined".                                    |
|---------------|--------------------------------------------------------|
| Null	        | Return "null".                                         |
|---------------|--------------------------------------------------------|
| Boolean       | If argument is true, return "true".                    |
|               | If argument is false, return "false".                  |
|---------------|--------------------------------------------------------|
| Number        | Return NumberToString(argument).                       |
|---------------|--------------------------------------------------------|
| String        | Return argument.                                       |
|---------------|--------------------------------------------------------|
| Symbol        | Throw a TypeError exception.                           |
|---------------|--------------------------------------------------------|
| Object        | Apply the following steps:                             |
|               | Let primValue be ? ToPrimitive(argument, hint String). |
|               | Return ? ToString(primValue).                          |
|---------------|--------------------------------------------------------|
*/
function ToString(argument) { // eslint-disable-line no-unused-vars
	switch(Type(argument)) {
		case 'symbol':
			throw new TypeError('Cannot convert a Symbol value to a string');
		case 'object':
			var primValue = ToPrimitive(argument, String);
			return ToString(primValue);
		default:
			return String(argument);
	}
}

// _ESAbstract.ToPropertyKey
/* globals ToPrimitive, Type, ToString */
// 7.1.14. ToPropertyKey ( argument )
function ToPropertyKey(argument) { // eslint-disable-line no-unused-vars
	// 1. Let key be ? ToPrimitive(argument, hint String).
	var key = ToPrimitive(argument, String);
	// 2. If Type(key) is Symbol, then
	if (Type(key) === 'symbol') {
		// a. Return key.
		return key;
	}
	// 3. Return ! ToString(key).
	return ToString(key);
}

// Array.prototype.includes
/* global CreateMethodProperty, Get, SameValueZero, ToInteger, ToLength, ToObject, ToString */
// 22.1.3.11. Array.prototype.includes ( searchElement [ , fromIndex ] )
CreateMethodProperty(Array.prototype, 'includes', function includes(searchElement /* [ , fromIndex ] */) {
	'use strict';
	// 1. Let O be ? ToObject(this value).
	var O = ToObject(this);
	// 2. Let len be ? ToLength(? Get(O, "length")).
	var len = ToLength(Get(O, "length"));
	// 3. If len is 0, return false.
	if (len === 0) {
		return false;
	}
	// 4. Let n be ? ToInteger(fromIndex). (If fromIndex is undefined, this step produces the value 0.)
	var n = ToInteger(arguments[1]);
	// 5. If n ≥ 0, then
	if (n >= 0) {
		// a. Let k be n.
		var k = n;
		// 6. Else n < 0,
	} else {
		// a. Let k be len + n.
		k = len + n;
		// b. If k < 0, let k be 0.
		if (k < 0) {
			k = 0;
		}
	}
	// 7. Repeat, while k < len
	while (k < len) {
		// a. Let elementK be the result of ? Get(O, ! ToString(k)).
		var elementK = Get(O, ToString(k));
		// b. If SameValueZero(searchElement, elementK) is true, return true.
		if (SameValueZero(searchElement, elementK)) {
			return true;
		}
		// c. Increase k by 1.
		k = k + 1;
	}
	// 8. Return false.
	return false;
});

// Intl.getCanonicalLocales
(function (factory) {
    typeof define === 'function' && define.amd ? define(factory) :
    factory();
}((function () { 'use strict';

    var __assign = (undefined && undefined.__assign) || function () {
        __assign = Object.assign || function(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                    t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    var ALPHANUM_1_8 = /^[a-z0-9]{1,8}$/i;
    var ALPHANUM_2_8 = /^[a-z0-9]{2,8}$/i;
    var ALPHANUM_3_8 = /^[a-z0-9]{3,8}$/i;
    var KEY_REGEX = /^[a-z0-9][a-z]$/i;
    var TYPE_REGEX = /^[a-z0-9]{3,8}$/i;
    var ALPHA_4 = /^[a-z]{4}$/i;
    // alphanum-[tTuUxX]
    var OTHER_EXTENSION_TYPE = /^[0-9a-svwyz]$/i;
    var UNICODE_REGION_SUBTAG_REGEX = /^([a-z]{2}|[0-9]{3})$/i;
    var UNICODE_VARIANT_SUBTAG_REGEX = /^([a-z0-9]{5,8}|[0-9][a-z0-9]{3})$/i;
    var UNICODE_LANGUAGE_SUBTAG_REGEX = /^([a-z]{2,3}|[a-z]{5,8})$/i;
    var TKEY_REGEX = /^[a-z][0-9]$/i;
    var SEPARATOR = '-';
    function isUnicodeLanguageSubtag(lang) {
        return UNICODE_LANGUAGE_SUBTAG_REGEX.test(lang);
    }
    function isUnicodeRegionSubtag(region) {
        return UNICODE_REGION_SUBTAG_REGEX.test(region);
    }
    function isUnicodeScriptSubtag(script) {
        return ALPHA_4.test(script);
    }
    function isUnicodeVariantSubtag(variant) {
        return UNICODE_VARIANT_SUBTAG_REGEX.test(variant);
    }
    function parseUnicodeLanguageId(chunks) {
        if (typeof chunks === 'string') {
            chunks = chunks.split(SEPARATOR);
        }
        var lang = chunks.shift();
        if (!lang) {
            throw new RangeError('Missing unicode_language_subtag');
        }
        if (lang === 'root') {
            return { lang: 'root', variants: [] };
        }
        // unicode_language_subtag
        if (!isUnicodeLanguageSubtag(lang)) {
            throw new RangeError('Malformed unicode_language_subtag');
        }
        var script;
        // unicode_script_subtag
        if (isUnicodeScriptSubtag(chunks[0])) {
            script = chunks.shift();
        }
        var region;
        // unicode_region_subtag
        if (isUnicodeRegionSubtag(chunks[0])) {
            region = chunks.shift();
        }
        var variants = {};
        while (chunks.length && isUnicodeVariantSubtag(chunks[0])) {
            var variant = chunks.shift();
            if (variant in variants) {
                throw new RangeError("Duplicate variant \"" + variant + "\"");
            }
            variants[variant] = 1;
        }
        return {
            lang: lang,
            script: script,
            region: region,
            variants: Object.keys(variants),
        };
    }
    function parseUnicodeExtension(chunks) {
        var keywords = [];
        var keyword;
        while (chunks.length && (keyword = parseKeyword(chunks))) {
            keywords.push(keyword);
        }
        if (keywords.length) {
            return {
                type: 'u',
                keywords: keywords,
                attributes: [],
            };
        }
        // Mix of attributes & keywords
        // Check for attributes first
        var attributes = [];
        while (chunks.length && ALPHANUM_3_8.test(chunks[0])) {
            attributes.push(chunks.shift());
        }
        while (chunks.length && (keyword = parseKeyword(chunks))) {
            keywords.push(keyword);
        }
        if (keywords.length || attributes.length) {
            return {
                type: 'u',
                attributes: attributes,
                keywords: keywords,
            };
        }
        throw new RangeError('Malformed unicode_extension');
    }
    function parseKeyword(chunks) {
        var key;
        if (!KEY_REGEX.test(chunks[0])) {
            return;
        }
        key = chunks.shift();
        var type = [];
        while (chunks.length && TYPE_REGEX.test(chunks[0])) {
            type.push(chunks.shift());
        }
        var value = '';
        if (type.length) {
            value = type.join(SEPARATOR);
        }
        return [key, value];
    }
    function parseTransformedExtension(chunks) {
        var lang;
        try {
            lang = parseUnicodeLanguageId(chunks);
        }
        catch (e) {
            // Try just parsing tfield
        }
        var fields = [];
        while (chunks.length && TKEY_REGEX.test(chunks[0])) {
            var key = chunks.shift();
            var value = [];
            while (chunks.length && ALPHANUM_3_8.test(chunks[0])) {
                value.push(chunks.shift());
            }
            if (!value.length) {
                throw new RangeError("Missing tvalue for tkey \"" + key + "\"");
            }
            fields.push([key, value.join(SEPARATOR)]);
        }
        if (fields.length) {
            return {
                type: 't',
                fields: fields,
                lang: lang,
            };
        }
        throw new RangeError('Malformed transformed_extension');
    }
    function parsePuExtension(chunks) {
        var exts = [];
        while (chunks.length && ALPHANUM_1_8.test(chunks[0])) {
            exts.push(chunks.shift());
        }
        if (exts.length) {
            return {
                type: 'x',
                value: exts.join(SEPARATOR),
            };
        }
        throw new RangeError('Malformed private_use_extension');
    }
    function parseOtherExtensionValue(chunks) {
        var exts = [];
        while (chunks.length && ALPHANUM_2_8.test(chunks[0])) {
            exts.push(chunks.shift());
        }
        if (exts.length) {
            return exts.join(SEPARATOR);
        }
        return '';
    }
    function parseExtensions(chunks) {
        if (!chunks.length) {
            return { extensions: [] };
        }
        var extensions = [];
        var unicodeExtension;
        var transformedExtension;
        var puExtension;
        var otherExtensionMap = {};
        do {
            var type = chunks.shift();
            switch (type) {
                case 'u':
                case 'U':
                    if (unicodeExtension) {
                        throw new RangeError('There can only be 1 -u- extension');
                    }
                    unicodeExtension = parseUnicodeExtension(chunks);
                    extensions.push(unicodeExtension);
                    break;
                case 't':
                case 'T':
                    if (transformedExtension) {
                        throw new RangeError('There can only be 1 -t- extension');
                    }
                    transformedExtension = parseTransformedExtension(chunks);
                    extensions.push(transformedExtension);
                    break;
                case 'x':
                case 'X':
                    if (puExtension) {
                        throw new RangeError('There can only be 1 -x- extension');
                    }
                    puExtension = parsePuExtension(chunks);
                    extensions.push(puExtension);
                    break;
                default:
                    if (!OTHER_EXTENSION_TYPE.test(type)) {
                        throw new RangeError('Malformed extension type');
                    }
                    if (type in otherExtensionMap) {
                        throw new RangeError("There can only be 1 -" + type + "- extension");
                    }
                    var extension = {
                        type: type,
                        value: parseOtherExtensionValue(chunks),
                    };
                    otherExtensionMap[extension.type] = extension;
                    extensions.push(extension);
                    break;
            }
        } while (chunks.length);
        return { extensions: extensions };
    }
    function parseUnicodeLocaleId(locale) {
        var chunks = locale.split(SEPARATOR);
        var lang = parseUnicodeLanguageId(chunks);
        return __assign({ lang: lang }, parseExtensions(chunks));
    }

    var __spreadArrays = (undefined && undefined.__spreadArrays) || function () {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };
    function emitUnicodeLanguageId(lang) {
        if (!lang) {
            return '';
        }
        return __spreadArrays([lang.lang, lang.script, lang.region], (lang.variants || [])).filter(Boolean)
            .join('-');
    }
    function emitUnicodeLocaleId(_a) {
        var lang = _a.lang, extensions = _a.extensions;
        var chunks = [emitUnicodeLanguageId(lang)];
        for (var _i = 0, extensions_1 = extensions; _i < extensions_1.length; _i++) {
            var ext = extensions_1[_i];
            chunks.push(ext.type);
            switch (ext.type) {
                case 'u':
                    chunks.push.apply(chunks, __spreadArrays(ext.attributes, ext.keywords.reduce(function (all, kv) { return all.concat(kv); }, [])));
                    break;
                case 't':
                    chunks.push.apply(chunks, __spreadArrays([emitUnicodeLanguageId(ext.lang)], ext.fields.reduce(function (all, kv) { return all.concat(kv); }, [])));
                    break;
                default:
                    chunks.push(ext.value);
                    break;
            }
        }
        return chunks.filter(Boolean).join('-');
    }

    /* @generated */
    // prettier-ignore  
    var languageAlias = { "aa-SAAHO": "ssy", "aam": "aas", "aar": "aa", "abk": "ab", "adp": "dz", "afr": "af", "aju": "jrb", "aka": "ak", "alb": "sq", "als": "sq", "amh": "am", "ara": "ar", "arb": "ar", "arg": "an", "arm": "hy", "art-lojban": "jbo", "asd": "snz", "asm": "as", "aue": "ktz", "ava": "av", "ave": "ae", "aym": "ay", "ayr": "ay", "ayx": "nun", "az-AZ": "az-Latn-AZ", "aze": "az", "azj": "az", "bak": "ba", "bam": "bm", "baq": "eu", "bcc": "bal", "bcl": "bik", "bel": "be", "ben": "bn", "bgm": "bcg", "bh": "bho", "bih": "bho", "bis": "bi", "bjd": "drl", "bod": "bo", "bos": "bs", "bre": "br", "bs-BA": "bs-Latn-BA", "bul": "bg", "bur": "my", "bxk": "luy", "bxr": "bua", "cat": "ca", "ccq": "rki", "cel-gaulish": "xtg-x-cel-gaulish", "ces": "cs", "cha": "ch", "che": "ce", "chi": "zh", "chu": "cu", "chv": "cv", "cjr": "mom", "cka": "cmr", "cld": "syr", "cmk": "xch", "cmn": "zh", "cnr": "sr-ME", "cor": "kw", "cos": "co", "coy": "pij", "cqu": "quh", "cre": "cr", "cwd": "cr", "cym": "cy", "cze": "cs", "dan": "da", "deu": "de", "dgo": "doi", "dhd": "mwr", "dik": "din", "diq": "zza", "dit": "dif", "div": "dv", "drh": "mn", "drw": "fa-af", "dut": "nl", "dzo": "dz", "ekk": "et", "ell": "el", "emk": "man", "eng": "en", "epo": "eo", "esk": "ik", "est": "et", "eus": "eu", "ewe": "ee", "fao": "fo", "fas": "fa", "fat": "ak", "fij": "fj", "fin": "fi", "fra": "fr", "fre": "fr", "fry": "fy", "fuc": "ff", "ful": "ff", "gav": "dev", "gaz": "om", "gbo": "grb", "geo": "ka", "ger": "de", "gfx": "vaj", "ggn": "gvr", "gla": "gd", "gle": "ga", "glg": "gl", "glv": "gv", "gno": "gon", "gre": "el", "grn": "gn", "gti": "nyc", "gug": "gn", "guj": "gu", "guv": "duz", "gya": "gba", "ha-Latn-GH": "ha-GH", "ha-Latn-NE": "ha-NE", "ha-Latn-NG": "ha-NG", "hat": "ht", "hau": "ha", "hbs": "sr-Latn", "hdn": "hai", "hea": "hmn", "heb": "he", "her": "hz", "him": "srx", "hin": "hi", "hmo": "ho", "hrr": "jal", "hrv": "hr", "hun": "hu", "hye": "hy", "i-ami": "ami", "i-bnn": "bnn", "i-hak": "hak", "i-klingon": "tlh", "i-lux": "lb", "i-navajo": "nv", "i-pwn": "pwn", "i-tao": "tao", "i-tay": "tay", "i-tsu": "tsu", "i-default": "en-x-i-default", "i-enochian": "und-x-i-enochian", "i-mingo": "see-x-i-mingo", "ibi": "opa", "ibo": "ig", "ice": "is", "ido": "io", "iii": "ii", "ike": "iu", "iku": "iu", "ile": "ie", "ilw": "gal", "in": "id", "ina": "ia", "ind": "id", "ipk": "ik", "isl": "is", "ita": "it", "iw": "he", "jav": "jv", "jeg": "oyb", "ji": "yi", "jpn": "ja", "jw": "jv", "kal": "kl", "kan": "kn", "kas": "ks", "kat": "ka", "kau": "kr", "kaz": "kk", "kgc": "tdf", "kgh": "kml", "khk": "mn", "khm": "km", "kik": "ki", "kin": "rw", "kir": "ky", "kk-Cyrl-KZ": "kk-KZ", "kmr": "ku", "knc": "kr", "kng": "kg", "knn": "kok", "koj": "kwv", "kom": "kv", "kon": "kg", "kor": "ko", "kpv": "kv", "krm": "bmf", "ks-Arab-IN": "ks-IN", "ktr": "dtp", "kua": "kj", "kur": "ku", "kvs": "gdj", "kwq": "yam", "kxe": "tvd", "ky-Cyrl-KG": "ky-KG", "kzj": "dtp", "kzt": "dtp", "lao": "lo", "lat": "la", "lav": "lv", "lbk": "bnc", "lii": "raq", "lim": "li", "lin": "ln", "lit": "lt", "llo": "ngt", "lmm": "rmx", "ltz": "lb", "lub": "lu", "lug": "lg", "lvs": "lv", "mac": "mk", "mah": "mh", "mal": "ml", "mao": "mi", "mar": "mr", "may": "ms", "meg": "cir", "mhr": "chm", "mkd": "mk", "mlg": "mg", "mlt": "mt", "mn-Cyrl-MN": "mn-MN", "mnk": "man", "mo": "ro", "mol": "ro", "mon": "mn", "mri": "mi", "ms-Latn-BN": "ms-BN", "ms-Latn-MY": "ms-MY", "ms-Latn-SG": "ms-SG", "msa": "ms", "mst": "mry", "mup": "raj", "mwj": "vaj", "mya": "my", "myd": "aog", "myt": "mry", "nad": "xny", "nau": "na", "nav": "nv", "nbl": "nr", "ncp": "kdz", "nde": "nd", "ndo": "ng", "nep": "ne", "nld": "nl", "nno": "nn", "nns": "nbr", "nnx": "ngv", "no": "nb", "no-bok": "nb", "no-BOKMAL": "nb", "no-nyn": "nn", "no-NYNORSK": "nn", "nob": "nb", "nor": "nb", "npi": "ne", "nts": "pij", "nya": "ny", "oci": "oc", "ojg": "oj", "oji": "oj", "ori": "or", "orm": "om", "ory": "or", "oss": "os", "oun": "vaj", "pa-IN": "pa-Guru-IN", "pa-PK": "pa-Arab-PK", "pan": "pa", "pbu": "ps", "pcr": "adx", "per": "fa", "pes": "fa", "pli": "pi", "plt": "mg", "pmc": "huw", "pmu": "phr", "pnb": "lah", "pol": "pl", "por": "pt", "ppa": "bfy", "ppr": "lcq", "prs": "fa-AF", "pry": "prt", "pus": "ps", "puz": "pub", "que": "qu", "quz": "qu", "rmy": "rom", "roh": "rm", "ron": "ro", "rum": "ro", "run": "rn", "rus": "ru", "sag": "sg", "san": "sa", "sca": "hle", "scc": "sr", "scr": "hr", "sgn-BE-FR": "sfb", "sgn-BE-NL": "vgt", "sgn-CH-DE": "sgg", "sh": "sr-Latn", "shi-MA": "shi-Tfng-MA", "sin": "si", "skk": "oyb", "slk": "sk", "slo": "sk", "slv": "sl", "sme": "se", "smo": "sm", "sna": "sn", "snd": "sd", "som": "so", "sot": "st", "spa": "es", "spy": "kln", "sqi": "sq", "sr-BA": "sr-Cyrl-BA", "sr-ME": "sr-Latn-ME", "sr-RS": "sr-Cyrl-RS", "sr-XK": "sr-Cyrl-XK", "src": "sc", "srd": "sc", "srp": "sr", "ssw": "ss", "sun": "su", "swa": "sw", "swc": "sw-CD", "swe": "sv", "swh": "sw", "tah": "ty", "tam": "ta", "tat": "tt", "tdu": "dtp", "tel": "te", "tgk": "tg", "tgl": "fil", "tha": "th", "thc": "tpo", "thx": "oyb", "tib": "bo", "tie": "ras", "tir": "ti", "tkk": "twm", "tl": "fil", "tlw": "weo", "tmp": "tyj", "tne": "kak", "tnf": "fa-af", "ton": "to", "tsf": "taj", "tsn": "tn", "tso": "ts", "ttq": "tmh", "tuk": "tk", "tur": "tr", "tw": "ak", "twi": "ak", "tzm-Latn-MA": "tzm-MA", "ug-Arab-CN": "ug-CN", "uig": "ug", "ukr": "uk", "umu": "del", "uok": "ema", "urd": "ur", "uz-AF": "uz-Arab-AF", "uz-UZ": "uz-Latn-UZ", "uzb": "uz", "uzn": "uz", "vai-LR": "vai-Vaii-LR", "ven": "ve", "vie": "vi", "vol": "vo", "wel": "cy", "wln": "wa", "wol": "wo", "xba": "cax", "xho": "xh", "xia": "acn", "xkh": "waw", "xpe": "kpe", "xsj": "suj", "xsl": "den", "ybd": "rki", "ydd": "yi", "yid": "yi", "yma": "lrr", "ymt": "mtm", "yor": "yo", "yos": "zom", "yue-CN": "yue-Hans-CN", "yue-HK": "yue-Hant-HK", "yuu": "yug", "zai": "zap", "zh-CN": "zh-Hans-CN", "zh-guoyu": "zh", "zh-hakka": "hak", "zh-HK": "zh-Hant-HK", "zh-min-nan": "nan", "zh-MO": "zh-Hant-MO", "zh-SG": "zh-Hans-SG", "zh-TW": "zh-Hant-TW", "zh-xiang": "hsn", "zh-min": "nan-x-zh-min", "zha": "za", "zho": "zh", "zsm": "ms", "zul": "zu", "zyb": "za" };
    var territoryAlias = { "100": "BG", "104": "MM", "108": "BI", "112": "BY", "116": "KH", "120": "CM", "124": "CA", "132": "CV", "136": "KY", "140": "CF", "144": "LK", "148": "TD", "152": "CL", "156": "CN", "158": "TW", "162": "CX", "166": "CC", "170": "CO", "172": "RU AM AZ BY GE KG KZ MD TJ TM UA UZ", "174": "KM", "175": "YT", "178": "CG", "180": "CD", "184": "CK", "188": "CR", "191": "HR", "192": "CU", "196": "CY", "200": "CZ SK", "203": "CZ", "204": "BJ", "208": "DK", "212": "DM", "214": "DO", "218": "EC", "222": "SV", "226": "GQ", "230": "ET", "231": "ET", "232": "ER", "233": "EE", "234": "FO", "238": "FK", "239": "GS", "242": "FJ", "246": "FI", "248": "AX", "249": "FR", "250": "FR", "254": "GF", "258": "PF", "260": "TF", "262": "DJ", "266": "GA", "268": "GE", "270": "GM", "275": "PS", "276": "DE", "278": "DE", "280": "DE", "288": "GH", "292": "GI", "296": "KI", "300": "GR", "304": "GL", "308": "GD", "312": "GP", "316": "GU", "320": "GT", "324": "GN", "328": "GY", "332": "HT", "334": "HM", "336": "VA", "340": "HN", "344": "HK", "348": "HU", "352": "IS", "356": "IN", "360": "ID", "364": "IR", "368": "IQ", "372": "IE", "376": "IL", "380": "IT", "384": "CI", "388": "JM", "392": "JP", "398": "KZ", "400": "JO", "404": "KE", "408": "KP", "410": "KR", "414": "KW", "417": "KG", "418": "LA", "422": "LB", "426": "LS", "428": "LV", "430": "LR", "434": "LY", "438": "LI", "440": "LT", "442": "LU", "446": "MO", "450": "MG", "454": "MW", "458": "MY", "462": "MV", "466": "ML", "470": "MT", "474": "MQ", "478": "MR", "480": "MU", "484": "MX", "492": "MC", "496": "MN", "498": "MD", "499": "ME", "500": "MS", "504": "MA", "508": "MZ", "512": "OM", "516": "NA", "520": "NR", "524": "NP", "528": "NL", "530": "CW SX BQ", "531": "CW", "532": "CW SX BQ", "533": "AW", "534": "SX", "535": "BQ", "536": "SA IQ", "540": "NC", "548": "VU", "554": "NZ", "558": "NI", "562": "NE", "566": "NG", "570": "NU", "574": "NF", "578": "NO", "580": "MP", "581": "UM", "582": "FM MH MP PW", "583": "FM", "584": "MH", "585": "PW", "586": "PK", "591": "PA", "598": "PG", "600": "PY", "604": "PE", "608": "PH", "612": "PN", "616": "PL", "620": "PT", "624": "GW", "626": "TL", "630": "PR", "634": "QA", "638": "RE", "642": "RO", "643": "RU", "646": "RW", "652": "BL", "654": "SH", "659": "KN", "660": "AI", "662": "LC", "663": "MF", "666": "PM", "670": "VC", "674": "SM", "678": "ST", "682": "SA", "686": "SN", "688": "RS", "690": "SC", "694": "SL", "702": "SG", "703": "SK", "704": "VN", "705": "SI", "706": "SO", "710": "ZA", "716": "ZW", "720": "YE", "724": "ES", "728": "SS", "729": "SD", "732": "EH", "736": "SD", "740": "SR", "744": "SJ", "748": "SZ", "752": "SE", "756": "CH", "760": "SY", "762": "TJ", "764": "TH", "768": "TG", "772": "TK", "776": "TO", "780": "TT", "784": "AE", "788": "TN", "792": "TR", "795": "TM", "796": "TC", "798": "TV", "800": "UG", "804": "UA", "807": "MK", "810": "RU AM AZ BY EE GE KZ KG LV LT MD TJ TM UA UZ", "818": "EG", "826": "GB", "830": "JE GG", "831": "GG", "832": "JE", "833": "IM", "834": "TZ", "840": "US", "850": "VI", "854": "BF", "858": "UY", "860": "UZ", "862": "VE", "876": "WF", "882": "WS", "886": "YE", "887": "YE", "890": "RS ME SI HR MK BA", "891": "RS ME", "894": "ZM", "958": "AA", "959": "QM", "960": "QN", "962": "QP", "963": "QQ", "964": "QR", "965": "QS", "966": "QT", "967": "EU", "968": "QV", "969": "QW", "970": "QX", "971": "QY", "972": "QZ", "973": "XA", "974": "XB", "975": "XC", "976": "XD", "977": "XE", "978": "XF", "979": "XG", "980": "XH", "981": "XI", "982": "XJ", "983": "XK", "984": "XL", "985": "XM", "986": "XN", "987": "XO", "988": "XP", "989": "XQ", "990": "XR", "991": "XS", "992": "XT", "993": "XU", "994": "XV", "995": "XW", "996": "XX", "997": "XY", "998": "XZ", "999": "ZZ", "004": "AF", "008": "AL", "010": "AQ", "012": "DZ", "016": "AS", "020": "AD", "024": "AO", "028": "AG", "031": "AZ", "032": "AR", "036": "AU", "040": "AT", "044": "BS", "048": "BH", "050": "BD", "051": "AM", "052": "BB", "056": "BE", "060": "BM", "062": "034 143", "064": "BT", "068": "BO", "070": "BA", "072": "BW", "074": "BV", "076": "BR", "084": "BZ", "086": "IO", "090": "SB", "092": "VG", "096": "BN", "AAA": "AA", "ABW": "AW", "AFG": "AF", "AGO": "AO", "AIA": "AI", "ALA": "AX", "ALB": "AL", "AN": "CW SX BQ", "AND": "AD", "ANT": "CW SX BQ", "ARE": "AE", "ARG": "AR", "ARM": "AM", "ASC": "AC", "ASM": "AS", "ATA": "AQ", "ATF": "TF", "ATG": "AG", "AUS": "AU", "AUT": "AT", "AZE": "AZ", "BDI": "BI", "BEL": "BE", "BEN": "BJ", "BES": "BQ", "BFA": "BF", "BGD": "BD", "BGR": "BG", "BHR": "BH", "BHS": "BS", "BIH": "BA", "BLM": "BL", "BLR": "BY", "BLZ": "BZ", "BMU": "BM", "BOL": "BO", "BRA": "BR", "BRB": "BB", "BRN": "BN", "BTN": "BT", "BU": "MM", "BUR": "MM", "BVT": "BV", "BWA": "BW", "CAF": "CF", "CAN": "CA", "CCK": "CC", "CHE": "CH", "CHL": "CL", "CHN": "CN", "CIV": "CI", "CMR": "CM", "COD": "CD", "COG": "CG", "COK": "CK", "COL": "CO", "COM": "KM", "CPT": "CP", "CPV": "CV", "CRI": "CR", "CS": "RS ME", "CT": "KI", "CUB": "CU", "CUW": "CW", "CXR": "CX", "CYM": "KY", "CYP": "CY", "CZE": "CZ", "DD": "DE", "DDR": "DE", "DEU": "DE", "DGA": "DG", "DJI": "DJ", "DMA": "DM", "DNK": "DK", "DOM": "DO", "DY": "BJ", "DZA": "DZ", "ECU": "EC", "EGY": "EG", "ERI": "ER", "ESH": "EH", "ESP": "ES", "EST": "EE", "ETH": "ET", "FIN": "FI", "FJI": "FJ", "FLK": "FK", "FQ": "AQ TF", "FRA": "FR", "FRO": "FO", "FSM": "FM", "FX": "FR", "FXX": "FR", "GAB": "GA", "GBR": "GB", "GEO": "GE", "GGY": "GG", "GHA": "GH", "GIB": "GI", "GIN": "GN", "GLP": "GP", "GMB": "GM", "GNB": "GW", "GNQ": "GQ", "GRC": "GR", "GRD": "GD", "GRL": "GL", "GTM": "GT", "GUF": "GF", "GUM": "GU", "GUY": "GY", "HKG": "HK", "HMD": "HM", "HND": "HN", "HRV": "HR", "HTI": "HT", "HUN": "HU", "HV": "BF", "IDN": "ID", "IMN": "IM", "IND": "IN", "IOT": "IO", "IRL": "IE", "IRN": "IR", "IRQ": "IQ", "ISL": "IS", "ISR": "IL", "ITA": "IT", "JAM": "JM", "JEY": "JE", "JOR": "JO", "JPN": "JP", "JT": "UM", "KAZ": "KZ", "KEN": "KE", "KGZ": "KG", "KHM": "KH", "KIR": "KI", "KNA": "KN", "KOR": "KR", "KWT": "KW", "LAO": "LA", "LBN": "LB", "LBR": "LR", "LBY": "LY", "LCA": "LC", "LIE": "LI", "LKA": "LK", "LSO": "LS", "LTU": "LT", "LUX": "LU", "LVA": "LV", "MAC": "MO", "MAF": "MF", "MAR": "MA", "MCO": "MC", "MDA": "MD", "MDG": "MG", "MDV": "MV", "MEX": "MX", "MHL": "MH", "MI": "UM", "MKD": "MK", "MLI": "ML", "MLT": "MT", "MMR": "MM", "MNE": "ME", "MNG": "MN", "MNP": "MP", "MOZ": "MZ", "MRT": "MR", "MSR": "MS", "MTQ": "MQ", "MUS": "MU", "MWI": "MW", "MYS": "MY", "MYT": "YT", "NAM": "NA", "NCL": "NC", "NER": "NE", "NFK": "NF", "NGA": "NG", "NH": "VU", "NIC": "NI", "NIU": "NU", "NLD": "NL", "NOR": "NO", "NPL": "NP", "NQ": "AQ", "NRU": "NR", "NT": "SA IQ", "NTZ": "SA IQ", "NZL": "NZ", "OMN": "OM", "PAK": "PK", "PAN": "PA", "PC": "FM MH MP PW", "PCN": "PN", "PER": "PE", "PHL": "PH", "PLW": "PW", "PNG": "PG", "POL": "PL", "PRI": "PR", "PRK": "KP", "PRT": "PT", "PRY": "PY", "PSE": "PS", "PU": "UM", "PYF": "PF", "PZ": "PA", "QAT": "QA", "QMM": "QM", "QNN": "QN", "QPP": "QP", "QQQ": "QQ", "QRR": "QR", "QSS": "QS", "QTT": "QT", "QU": "EU", "QUU": "EU", "QVV": "QV", "QWW": "QW", "QXX": "QX", "QYY": "QY", "QZZ": "QZ", "REU": "RE", "RH": "ZW", "ROU": "RO", "RUS": "RU", "RWA": "RW", "SAU": "SA", "SCG": "RS ME", "SDN": "SD", "SEN": "SN", "SGP": "SG", "SGS": "GS", "SHN": "SH", "SJM": "SJ", "SLB": "SB", "SLE": "SL", "SLV": "SV", "SMR": "SM", "SOM": "SO", "SPM": "PM", "SRB": "RS", "SSD": "SS", "STP": "ST", "SU": "RU AM AZ BY EE GE KZ KG LV LT MD TJ TM UA UZ", "SUN": "RU AM AZ BY EE GE KZ KG LV LT MD TJ TM UA UZ", "SUR": "SR", "SVK": "SK", "SVN": "SI", "SWE": "SE", "SWZ": "SZ", "SXM": "SX", "SYC": "SC", "SYR": "SY", "TAA": "TA", "TCA": "TC", "TCD": "TD", "TGO": "TG", "THA": "TH", "TJK": "TJ", "TKL": "TK", "TKM": "TM", "TLS": "TL", "TMP": "TL", "TON": "TO", "TP": "TL", "TTO": "TT", "TUN": "TN", "TUR": "TR", "TUV": "TV", "TWN": "TW", "TZA": "TZ", "UGA": "UG", "UK": "GB", "UKR": "UA", "UMI": "UM", "URY": "UY", "USA": "US", "UZB": "UZ", "VAT": "VA", "VCT": "VC", "VD": "VN", "VEN": "VE", "VGB": "VG", "VIR": "VI", "VNM": "VN", "VUT": "VU", "WK": "UM", "WLF": "WF", "WSM": "WS", "XAA": "XA", "XBB": "XB", "XCC": "XC", "XDD": "XD", "XEE": "XE", "XFF": "XF", "XGG": "XG", "XHH": "XH", "XII": "XI", "XJJ": "XJ", "XKK": "XK", "XLL": "XL", "XMM": "XM", "XNN": "XN", "XOO": "XO", "XPP": "XP", "XQQ": "XQ", "XRR": "XR", "XSS": "XS", "XTT": "XT", "XUU": "XU", "XVV": "XV", "XWW": "XW", "XXX": "XX", "XYY": "XY", "XZZ": "XZ", "YD": "YE", "YEM": "YE", "YMD": "YE", "YU": "RS ME", "YUG": "RS ME", "ZAF": "ZA", "ZAR": "CD", "ZMB": "ZM", "ZR": "CD", "ZWE": "ZW", "ZZZ": "ZZ" };
    var scriptAlias = { "Qaai": "Zinh" };
    var variantAlias = { "AALAND": "AX", "arevela": "hy", "arevmda": "hyw", "heploc": "alalc97", "HEPLOC": "ALALC97", "POLYTONI": "POLYTON" };

    var supplemental = {
    	version: {
    		_unicodeVersion: "12.1.0",
    		_cldrVersion: "36"
    	},
    	likelySubtags: {
    		aa: "aa-Latn-ET",
    		aai: "aai-Latn-ZZ",
    		aak: "aak-Latn-ZZ",
    		aau: "aau-Latn-ZZ",
    		ab: "ab-Cyrl-GE",
    		abi: "abi-Latn-ZZ",
    		abq: "abq-Cyrl-ZZ",
    		abr: "abr-Latn-GH",
    		abt: "abt-Latn-ZZ",
    		aby: "aby-Latn-ZZ",
    		acd: "acd-Latn-ZZ",
    		ace: "ace-Latn-ID",
    		ach: "ach-Latn-UG",
    		ada: "ada-Latn-GH",
    		ade: "ade-Latn-ZZ",
    		adj: "adj-Latn-ZZ",
    		adp: "adp-Tibt-BT",
    		ady: "ady-Cyrl-RU",
    		adz: "adz-Latn-ZZ",
    		ae: "ae-Avst-IR",
    		aeb: "aeb-Arab-TN",
    		aey: "aey-Latn-ZZ",
    		af: "af-Latn-ZA",
    		agc: "agc-Latn-ZZ",
    		agd: "agd-Latn-ZZ",
    		agg: "agg-Latn-ZZ",
    		agm: "agm-Latn-ZZ",
    		ago: "ago-Latn-ZZ",
    		agq: "agq-Latn-CM",
    		aha: "aha-Latn-ZZ",
    		ahl: "ahl-Latn-ZZ",
    		aho: "aho-Ahom-IN",
    		ajg: "ajg-Latn-ZZ",
    		ak: "ak-Latn-GH",
    		akk: "akk-Xsux-IQ",
    		ala: "ala-Latn-ZZ",
    		ali: "ali-Latn-ZZ",
    		aln: "aln-Latn-XK",
    		alt: "alt-Cyrl-RU",
    		am: "am-Ethi-ET",
    		amm: "amm-Latn-ZZ",
    		amn: "amn-Latn-ZZ",
    		amo: "amo-Latn-NG",
    		amp: "amp-Latn-ZZ",
    		an: "an-Latn-ES",
    		anc: "anc-Latn-ZZ",
    		ank: "ank-Latn-ZZ",
    		ann: "ann-Latn-ZZ",
    		any: "any-Latn-ZZ",
    		aoj: "aoj-Latn-ZZ",
    		aom: "aom-Latn-ZZ",
    		aoz: "aoz-Latn-ID",
    		apc: "apc-Arab-ZZ",
    		apd: "apd-Arab-TG",
    		ape: "ape-Latn-ZZ",
    		apr: "apr-Latn-ZZ",
    		aps: "aps-Latn-ZZ",
    		apz: "apz-Latn-ZZ",
    		ar: "ar-Arab-EG",
    		arc: "arc-Armi-IR",
    		"arc-Nbat": "arc-Nbat-JO",
    		"arc-Palm": "arc-Palm-SY",
    		arh: "arh-Latn-ZZ",
    		arn: "arn-Latn-CL",
    		aro: "aro-Latn-BO",
    		arq: "arq-Arab-DZ",
    		ars: "ars-Arab-SA",
    		ary: "ary-Arab-MA",
    		arz: "arz-Arab-EG",
    		as: "as-Beng-IN",
    		asa: "asa-Latn-TZ",
    		ase: "ase-Sgnw-US",
    		asg: "asg-Latn-ZZ",
    		aso: "aso-Latn-ZZ",
    		ast: "ast-Latn-ES",
    		ata: "ata-Latn-ZZ",
    		atg: "atg-Latn-ZZ",
    		atj: "atj-Latn-CA",
    		auy: "auy-Latn-ZZ",
    		av: "av-Cyrl-RU",
    		avl: "avl-Arab-ZZ",
    		avn: "avn-Latn-ZZ",
    		avt: "avt-Latn-ZZ",
    		avu: "avu-Latn-ZZ",
    		awa: "awa-Deva-IN",
    		awb: "awb-Latn-ZZ",
    		awo: "awo-Latn-ZZ",
    		awx: "awx-Latn-ZZ",
    		ay: "ay-Latn-BO",
    		ayb: "ayb-Latn-ZZ",
    		az: "az-Latn-AZ",
    		"az-Arab": "az-Arab-IR",
    		"az-IQ": "az-Arab-IQ",
    		"az-IR": "az-Arab-IR",
    		"az-RU": "az-Cyrl-RU",
    		ba: "ba-Cyrl-RU",
    		bal: "bal-Arab-PK",
    		ban: "ban-Latn-ID",
    		bap: "bap-Deva-NP",
    		bar: "bar-Latn-AT",
    		bas: "bas-Latn-CM",
    		bav: "bav-Latn-ZZ",
    		bax: "bax-Bamu-CM",
    		bba: "bba-Latn-ZZ",
    		bbb: "bbb-Latn-ZZ",
    		bbc: "bbc-Latn-ID",
    		bbd: "bbd-Latn-ZZ",
    		bbj: "bbj-Latn-CM",
    		bbp: "bbp-Latn-ZZ",
    		bbr: "bbr-Latn-ZZ",
    		bcf: "bcf-Latn-ZZ",
    		bch: "bch-Latn-ZZ",
    		bci: "bci-Latn-CI",
    		bcm: "bcm-Latn-ZZ",
    		bcn: "bcn-Latn-ZZ",
    		bco: "bco-Latn-ZZ",
    		bcq: "bcq-Ethi-ZZ",
    		bcu: "bcu-Latn-ZZ",
    		bdd: "bdd-Latn-ZZ",
    		be: "be-Cyrl-BY",
    		bef: "bef-Latn-ZZ",
    		beh: "beh-Latn-ZZ",
    		bej: "bej-Arab-SD",
    		bem: "bem-Latn-ZM",
    		bet: "bet-Latn-ZZ",
    		bew: "bew-Latn-ID",
    		bex: "bex-Latn-ZZ",
    		bez: "bez-Latn-TZ",
    		bfd: "bfd-Latn-CM",
    		bfq: "bfq-Taml-IN",
    		bft: "bft-Arab-PK",
    		bfy: "bfy-Deva-IN",
    		bg: "bg-Cyrl-BG",
    		bgc: "bgc-Deva-IN",
    		bgn: "bgn-Arab-PK",
    		bgx: "bgx-Grek-TR",
    		bhb: "bhb-Deva-IN",
    		bhg: "bhg-Latn-ZZ",
    		bhi: "bhi-Deva-IN",
    		bhl: "bhl-Latn-ZZ",
    		bho: "bho-Deva-IN",
    		bhy: "bhy-Latn-ZZ",
    		bi: "bi-Latn-VU",
    		bib: "bib-Latn-ZZ",
    		big: "big-Latn-ZZ",
    		bik: "bik-Latn-PH",
    		bim: "bim-Latn-ZZ",
    		bin: "bin-Latn-NG",
    		bio: "bio-Latn-ZZ",
    		biq: "biq-Latn-ZZ",
    		bjh: "bjh-Latn-ZZ",
    		bji: "bji-Ethi-ZZ",
    		bjj: "bjj-Deva-IN",
    		bjn: "bjn-Latn-ID",
    		bjo: "bjo-Latn-ZZ",
    		bjr: "bjr-Latn-ZZ",
    		bjt: "bjt-Latn-SN",
    		bjz: "bjz-Latn-ZZ",
    		bkc: "bkc-Latn-ZZ",
    		bkm: "bkm-Latn-CM",
    		bkq: "bkq-Latn-ZZ",
    		bku: "bku-Latn-PH",
    		bkv: "bkv-Latn-ZZ",
    		blt: "blt-Tavt-VN",
    		bm: "bm-Latn-ML",
    		bmh: "bmh-Latn-ZZ",
    		bmk: "bmk-Latn-ZZ",
    		bmq: "bmq-Latn-ML",
    		bmu: "bmu-Latn-ZZ",
    		bn: "bn-Beng-BD",
    		bng: "bng-Latn-ZZ",
    		bnm: "bnm-Latn-ZZ",
    		bnp: "bnp-Latn-ZZ",
    		bo: "bo-Tibt-CN",
    		boj: "boj-Latn-ZZ",
    		bom: "bom-Latn-ZZ",
    		bon: "bon-Latn-ZZ",
    		bpy: "bpy-Beng-IN",
    		bqc: "bqc-Latn-ZZ",
    		bqi: "bqi-Arab-IR",
    		bqp: "bqp-Latn-ZZ",
    		bqv: "bqv-Latn-CI",
    		br: "br-Latn-FR",
    		bra: "bra-Deva-IN",
    		brh: "brh-Arab-PK",
    		brx: "brx-Deva-IN",
    		brz: "brz-Latn-ZZ",
    		bs: "bs-Latn-BA",
    		bsj: "bsj-Latn-ZZ",
    		bsq: "bsq-Bass-LR",
    		bss: "bss-Latn-CM",
    		bst: "bst-Ethi-ZZ",
    		bto: "bto-Latn-PH",
    		btt: "btt-Latn-ZZ",
    		btv: "btv-Deva-PK",
    		bua: "bua-Cyrl-RU",
    		buc: "buc-Latn-YT",
    		bud: "bud-Latn-ZZ",
    		bug: "bug-Latn-ID",
    		buk: "buk-Latn-ZZ",
    		bum: "bum-Latn-CM",
    		buo: "buo-Latn-ZZ",
    		bus: "bus-Latn-ZZ",
    		buu: "buu-Latn-ZZ",
    		bvb: "bvb-Latn-GQ",
    		bwd: "bwd-Latn-ZZ",
    		bwr: "bwr-Latn-ZZ",
    		bxh: "bxh-Latn-ZZ",
    		bye: "bye-Latn-ZZ",
    		byn: "byn-Ethi-ER",
    		byr: "byr-Latn-ZZ",
    		bys: "bys-Latn-ZZ",
    		byv: "byv-Latn-CM",
    		byx: "byx-Latn-ZZ",
    		bza: "bza-Latn-ZZ",
    		bze: "bze-Latn-ML",
    		bzf: "bzf-Latn-ZZ",
    		bzh: "bzh-Latn-ZZ",
    		bzw: "bzw-Latn-ZZ",
    		ca: "ca-Latn-ES",
    		can: "can-Latn-ZZ",
    		cbj: "cbj-Latn-ZZ",
    		cch: "cch-Latn-NG",
    		ccp: "ccp-Cakm-BD",
    		ce: "ce-Cyrl-RU",
    		ceb: "ceb-Latn-PH",
    		cfa: "cfa-Latn-ZZ",
    		cgg: "cgg-Latn-UG",
    		ch: "ch-Latn-GU",
    		chk: "chk-Latn-FM",
    		chm: "chm-Cyrl-RU",
    		cho: "cho-Latn-US",
    		chp: "chp-Latn-CA",
    		chr: "chr-Cher-US",
    		cic: "cic-Latn-US",
    		cja: "cja-Arab-KH",
    		cjm: "cjm-Cham-VN",
    		cjv: "cjv-Latn-ZZ",
    		ckb: "ckb-Arab-IQ",
    		ckl: "ckl-Latn-ZZ",
    		cko: "cko-Latn-ZZ",
    		cky: "cky-Latn-ZZ",
    		cla: "cla-Latn-ZZ",
    		cme: "cme-Latn-ZZ",
    		cmg: "cmg-Soyo-MN",
    		co: "co-Latn-FR",
    		cop: "cop-Copt-EG",
    		cps: "cps-Latn-PH",
    		cr: "cr-Cans-CA",
    		crh: "crh-Cyrl-UA",
    		crj: "crj-Cans-CA",
    		crk: "crk-Cans-CA",
    		crl: "crl-Cans-CA",
    		crm: "crm-Cans-CA",
    		crs: "crs-Latn-SC",
    		cs: "cs-Latn-CZ",
    		csb: "csb-Latn-PL",
    		csw: "csw-Cans-CA",
    		ctd: "ctd-Pauc-MM",
    		cu: "cu-Cyrl-RU",
    		"cu-Glag": "cu-Glag-BG",
    		cv: "cv-Cyrl-RU",
    		cy: "cy-Latn-GB",
    		da: "da-Latn-DK",
    		dad: "dad-Latn-ZZ",
    		daf: "daf-Latn-ZZ",
    		dag: "dag-Latn-ZZ",
    		dah: "dah-Latn-ZZ",
    		dak: "dak-Latn-US",
    		dar: "dar-Cyrl-RU",
    		dav: "dav-Latn-KE",
    		dbd: "dbd-Latn-ZZ",
    		dbq: "dbq-Latn-ZZ",
    		dcc: "dcc-Arab-IN",
    		ddn: "ddn-Latn-ZZ",
    		de: "de-Latn-DE",
    		ded: "ded-Latn-ZZ",
    		den: "den-Latn-CA",
    		dga: "dga-Latn-ZZ",
    		dgh: "dgh-Latn-ZZ",
    		dgi: "dgi-Latn-ZZ",
    		dgl: "dgl-Arab-ZZ",
    		dgr: "dgr-Latn-CA",
    		dgz: "dgz-Latn-ZZ",
    		dia: "dia-Latn-ZZ",
    		dje: "dje-Latn-NE",
    		dnj: "dnj-Latn-CI",
    		dob: "dob-Latn-ZZ",
    		doi: "doi-Arab-IN",
    		dop: "dop-Latn-ZZ",
    		dow: "dow-Latn-ZZ",
    		drh: "drh-Mong-CN",
    		dri: "dri-Latn-ZZ",
    		drs: "drs-Ethi-ZZ",
    		dsb: "dsb-Latn-DE",
    		dtm: "dtm-Latn-ML",
    		dtp: "dtp-Latn-MY",
    		dts: "dts-Latn-ZZ",
    		dty: "dty-Deva-NP",
    		dua: "dua-Latn-CM",
    		duc: "duc-Latn-ZZ",
    		dud: "dud-Latn-ZZ",
    		dug: "dug-Latn-ZZ",
    		dv: "dv-Thaa-MV",
    		dva: "dva-Latn-ZZ",
    		dww: "dww-Latn-ZZ",
    		dyo: "dyo-Latn-SN",
    		dyu: "dyu-Latn-BF",
    		dz: "dz-Tibt-BT",
    		dzg: "dzg-Latn-ZZ",
    		ebu: "ebu-Latn-KE",
    		ee: "ee-Latn-GH",
    		efi: "efi-Latn-NG",
    		egl: "egl-Latn-IT",
    		egy: "egy-Egyp-EG",
    		eka: "eka-Latn-ZZ",
    		eky: "eky-Kali-MM",
    		el: "el-Grek-GR",
    		ema: "ema-Latn-ZZ",
    		emi: "emi-Latn-ZZ",
    		en: "en-Latn-US",
    		"en-Shaw": "en-Shaw-GB",
    		enn: "enn-Latn-ZZ",
    		enq: "enq-Latn-ZZ",
    		eo: "eo-Latn-001",
    		eri: "eri-Latn-ZZ",
    		es: "es-Latn-ES",
    		esg: "esg-Gonm-IN",
    		esu: "esu-Latn-US",
    		et: "et-Latn-EE",
    		etr: "etr-Latn-ZZ",
    		ett: "ett-Ital-IT",
    		etu: "etu-Latn-ZZ",
    		etx: "etx-Latn-ZZ",
    		eu: "eu-Latn-ES",
    		ewo: "ewo-Latn-CM",
    		ext: "ext-Latn-ES",
    		fa: "fa-Arab-IR",
    		faa: "faa-Latn-ZZ",
    		fab: "fab-Latn-ZZ",
    		fag: "fag-Latn-ZZ",
    		fai: "fai-Latn-ZZ",
    		fan: "fan-Latn-GQ",
    		ff: "ff-Latn-SN",
    		"ff-Adlm": "ff-Adlm-GN",
    		ffi: "ffi-Latn-ZZ",
    		ffm: "ffm-Latn-ML",
    		fi: "fi-Latn-FI",
    		fia: "fia-Arab-SD",
    		fil: "fil-Latn-PH",
    		fit: "fit-Latn-SE",
    		fj: "fj-Latn-FJ",
    		flr: "flr-Latn-ZZ",
    		fmp: "fmp-Latn-ZZ",
    		fo: "fo-Latn-FO",
    		fod: "fod-Latn-ZZ",
    		fon: "fon-Latn-BJ",
    		"for": "for-Latn-ZZ",
    		fpe: "fpe-Latn-ZZ",
    		fqs: "fqs-Latn-ZZ",
    		fr: "fr-Latn-FR",
    		frc: "frc-Latn-US",
    		frp: "frp-Latn-FR",
    		frr: "frr-Latn-DE",
    		frs: "frs-Latn-DE",
    		fub: "fub-Arab-CM",
    		fud: "fud-Latn-WF",
    		fue: "fue-Latn-ZZ",
    		fuf: "fuf-Latn-GN",
    		fuh: "fuh-Latn-ZZ",
    		fuq: "fuq-Latn-NE",
    		fur: "fur-Latn-IT",
    		fuv: "fuv-Latn-NG",
    		fuy: "fuy-Latn-ZZ",
    		fvr: "fvr-Latn-SD",
    		fy: "fy-Latn-NL",
    		ga: "ga-Latn-IE",
    		gaa: "gaa-Latn-GH",
    		gaf: "gaf-Latn-ZZ",
    		gag: "gag-Latn-MD",
    		gah: "gah-Latn-ZZ",
    		gaj: "gaj-Latn-ZZ",
    		gam: "gam-Latn-ZZ",
    		gan: "gan-Hans-CN",
    		gaw: "gaw-Latn-ZZ",
    		gay: "gay-Latn-ID",
    		gba: "gba-Latn-ZZ",
    		gbf: "gbf-Latn-ZZ",
    		gbm: "gbm-Deva-IN",
    		gby: "gby-Latn-ZZ",
    		gbz: "gbz-Arab-IR",
    		gcr: "gcr-Latn-GF",
    		gd: "gd-Latn-GB",
    		gde: "gde-Latn-ZZ",
    		gdn: "gdn-Latn-ZZ",
    		gdr: "gdr-Latn-ZZ",
    		geb: "geb-Latn-ZZ",
    		gej: "gej-Latn-ZZ",
    		gel: "gel-Latn-ZZ",
    		gez: "gez-Ethi-ET",
    		gfk: "gfk-Latn-ZZ",
    		ggn: "ggn-Deva-NP",
    		ghs: "ghs-Latn-ZZ",
    		gil: "gil-Latn-KI",
    		gim: "gim-Latn-ZZ",
    		gjk: "gjk-Arab-PK",
    		gjn: "gjn-Latn-ZZ",
    		gju: "gju-Arab-PK",
    		gkn: "gkn-Latn-ZZ",
    		gkp: "gkp-Latn-ZZ",
    		gl: "gl-Latn-ES",
    		glk: "glk-Arab-IR",
    		gmm: "gmm-Latn-ZZ",
    		gmv: "gmv-Ethi-ZZ",
    		gn: "gn-Latn-PY",
    		gnd: "gnd-Latn-ZZ",
    		gng: "gng-Latn-ZZ",
    		god: "god-Latn-ZZ",
    		gof: "gof-Ethi-ZZ",
    		goi: "goi-Latn-ZZ",
    		gom: "gom-Deva-IN",
    		gon: "gon-Telu-IN",
    		gor: "gor-Latn-ID",
    		gos: "gos-Latn-NL",
    		got: "got-Goth-UA",
    		grb: "grb-Latn-ZZ",
    		grc: "grc-Cprt-CY",
    		"grc-Linb": "grc-Linb-GR",
    		grt: "grt-Beng-IN",
    		grw: "grw-Latn-ZZ",
    		gsw: "gsw-Latn-CH",
    		gu: "gu-Gujr-IN",
    		gub: "gub-Latn-BR",
    		guc: "guc-Latn-CO",
    		gud: "gud-Latn-ZZ",
    		gur: "gur-Latn-GH",
    		guw: "guw-Latn-ZZ",
    		gux: "gux-Latn-ZZ",
    		guz: "guz-Latn-KE",
    		gv: "gv-Latn-IM",
    		gvf: "gvf-Latn-ZZ",
    		gvr: "gvr-Deva-NP",
    		gvs: "gvs-Latn-ZZ",
    		gwc: "gwc-Arab-ZZ",
    		gwi: "gwi-Latn-CA",
    		gwt: "gwt-Arab-ZZ",
    		gyi: "gyi-Latn-ZZ",
    		ha: "ha-Latn-NG",
    		"ha-CM": "ha-Arab-CM",
    		"ha-SD": "ha-Arab-SD",
    		hag: "hag-Latn-ZZ",
    		hak: "hak-Hans-CN",
    		ham: "ham-Latn-ZZ",
    		haw: "haw-Latn-US",
    		haz: "haz-Arab-AF",
    		hbb: "hbb-Latn-ZZ",
    		hdy: "hdy-Ethi-ZZ",
    		he: "he-Hebr-IL",
    		hhy: "hhy-Latn-ZZ",
    		hi: "hi-Deva-IN",
    		hia: "hia-Latn-ZZ",
    		hif: "hif-Latn-FJ",
    		hig: "hig-Latn-ZZ",
    		hih: "hih-Latn-ZZ",
    		hil: "hil-Latn-PH",
    		hla: "hla-Latn-ZZ",
    		hlu: "hlu-Hluw-TR",
    		hmd: "hmd-Plrd-CN",
    		hmt: "hmt-Latn-ZZ",
    		hnd: "hnd-Arab-PK",
    		hne: "hne-Deva-IN",
    		hnj: "hnj-Hmng-LA",
    		hnn: "hnn-Latn-PH",
    		hno: "hno-Arab-PK",
    		ho: "ho-Latn-PG",
    		hoc: "hoc-Deva-IN",
    		hoj: "hoj-Deva-IN",
    		hot: "hot-Latn-ZZ",
    		hr: "hr-Latn-HR",
    		hsb: "hsb-Latn-DE",
    		hsn: "hsn-Hans-CN",
    		ht: "ht-Latn-HT",
    		hu: "hu-Latn-HU",
    		hui: "hui-Latn-ZZ",
    		hy: "hy-Armn-AM",
    		hz: "hz-Latn-NA",
    		ia: "ia-Latn-001",
    		ian: "ian-Latn-ZZ",
    		iar: "iar-Latn-ZZ",
    		iba: "iba-Latn-MY",
    		ibb: "ibb-Latn-NG",
    		iby: "iby-Latn-ZZ",
    		ica: "ica-Latn-ZZ",
    		ich: "ich-Latn-ZZ",
    		id: "id-Latn-ID",
    		idd: "idd-Latn-ZZ",
    		idi: "idi-Latn-ZZ",
    		idu: "idu-Latn-ZZ",
    		ife: "ife-Latn-TG",
    		ig: "ig-Latn-NG",
    		igb: "igb-Latn-ZZ",
    		ige: "ige-Latn-ZZ",
    		ii: "ii-Yiii-CN",
    		ijj: "ijj-Latn-ZZ",
    		ik: "ik-Latn-US",
    		ikk: "ikk-Latn-ZZ",
    		ikt: "ikt-Latn-CA",
    		ikw: "ikw-Latn-ZZ",
    		ikx: "ikx-Latn-ZZ",
    		ilo: "ilo-Latn-PH",
    		imo: "imo-Latn-ZZ",
    		"in": "in-Latn-ID",
    		inh: "inh-Cyrl-RU",
    		io: "io-Latn-001",
    		iou: "iou-Latn-ZZ",
    		iri: "iri-Latn-ZZ",
    		is: "is-Latn-IS",
    		it: "it-Latn-IT",
    		iu: "iu-Cans-CA",
    		iw: "iw-Hebr-IL",
    		iwm: "iwm-Latn-ZZ",
    		iws: "iws-Latn-ZZ",
    		izh: "izh-Latn-RU",
    		izi: "izi-Latn-ZZ",
    		ja: "ja-Jpan-JP",
    		jab: "jab-Latn-ZZ",
    		jam: "jam-Latn-JM",
    		jbo: "jbo-Latn-001",
    		jbu: "jbu-Latn-ZZ",
    		jen: "jen-Latn-ZZ",
    		jgk: "jgk-Latn-ZZ",
    		jgo: "jgo-Latn-CM",
    		ji: "ji-Hebr-UA",
    		jib: "jib-Latn-ZZ",
    		jmc: "jmc-Latn-TZ",
    		jml: "jml-Deva-NP",
    		jra: "jra-Latn-ZZ",
    		jut: "jut-Latn-DK",
    		jv: "jv-Latn-ID",
    		jw: "jw-Latn-ID",
    		ka: "ka-Geor-GE",
    		kaa: "kaa-Cyrl-UZ",
    		kab: "kab-Latn-DZ",
    		kac: "kac-Latn-MM",
    		kad: "kad-Latn-ZZ",
    		kai: "kai-Latn-ZZ",
    		kaj: "kaj-Latn-NG",
    		kam: "kam-Latn-KE",
    		kao: "kao-Latn-ML",
    		kbd: "kbd-Cyrl-RU",
    		kbm: "kbm-Latn-ZZ",
    		kbp: "kbp-Latn-ZZ",
    		kbq: "kbq-Latn-ZZ",
    		kbx: "kbx-Latn-ZZ",
    		kby: "kby-Arab-NE",
    		kcg: "kcg-Latn-NG",
    		kck: "kck-Latn-ZW",
    		kcl: "kcl-Latn-ZZ",
    		kct: "kct-Latn-ZZ",
    		kde: "kde-Latn-TZ",
    		kdh: "kdh-Arab-TG",
    		kdl: "kdl-Latn-ZZ",
    		kdt: "kdt-Thai-TH",
    		kea: "kea-Latn-CV",
    		ken: "ken-Latn-CM",
    		kez: "kez-Latn-ZZ",
    		kfo: "kfo-Latn-CI",
    		kfr: "kfr-Deva-IN",
    		kfy: "kfy-Deva-IN",
    		kg: "kg-Latn-CD",
    		kge: "kge-Latn-ID",
    		kgf: "kgf-Latn-ZZ",
    		kgp: "kgp-Latn-BR",
    		kha: "kha-Latn-IN",
    		khb: "khb-Talu-CN",
    		khn: "khn-Deva-IN",
    		khq: "khq-Latn-ML",
    		khs: "khs-Latn-ZZ",
    		kht: "kht-Mymr-IN",
    		khw: "khw-Arab-PK",
    		khz: "khz-Latn-ZZ",
    		ki: "ki-Latn-KE",
    		kij: "kij-Latn-ZZ",
    		kiu: "kiu-Latn-TR",
    		kiw: "kiw-Latn-ZZ",
    		kj: "kj-Latn-NA",
    		kjd: "kjd-Latn-ZZ",
    		kjg: "kjg-Laoo-LA",
    		kjs: "kjs-Latn-ZZ",
    		kjy: "kjy-Latn-ZZ",
    		kk: "kk-Cyrl-KZ",
    		"kk-AF": "kk-Arab-AF",
    		"kk-Arab": "kk-Arab-CN",
    		"kk-CN": "kk-Arab-CN",
    		"kk-IR": "kk-Arab-IR",
    		"kk-MN": "kk-Arab-MN",
    		kkc: "kkc-Latn-ZZ",
    		kkj: "kkj-Latn-CM",
    		kl: "kl-Latn-GL",
    		kln: "kln-Latn-KE",
    		klq: "klq-Latn-ZZ",
    		klt: "klt-Latn-ZZ",
    		klx: "klx-Latn-ZZ",
    		km: "km-Khmr-KH",
    		kmb: "kmb-Latn-AO",
    		kmh: "kmh-Latn-ZZ",
    		kmo: "kmo-Latn-ZZ",
    		kms: "kms-Latn-ZZ",
    		kmu: "kmu-Latn-ZZ",
    		kmw: "kmw-Latn-ZZ",
    		kn: "kn-Knda-IN",
    		knf: "knf-Latn-GW",
    		knp: "knp-Latn-ZZ",
    		ko: "ko-Kore-KR",
    		koi: "koi-Cyrl-RU",
    		kok: "kok-Deva-IN",
    		kol: "kol-Latn-ZZ",
    		kos: "kos-Latn-FM",
    		koz: "koz-Latn-ZZ",
    		kpe: "kpe-Latn-LR",
    		kpf: "kpf-Latn-ZZ",
    		kpo: "kpo-Latn-ZZ",
    		kpr: "kpr-Latn-ZZ",
    		kpx: "kpx-Latn-ZZ",
    		kqb: "kqb-Latn-ZZ",
    		kqf: "kqf-Latn-ZZ",
    		kqs: "kqs-Latn-ZZ",
    		kqy: "kqy-Ethi-ZZ",
    		kr: "kr-Latn-ZZ",
    		krc: "krc-Cyrl-RU",
    		kri: "kri-Latn-SL",
    		krj: "krj-Latn-PH",
    		krl: "krl-Latn-RU",
    		krs: "krs-Latn-ZZ",
    		kru: "kru-Deva-IN",
    		ks: "ks-Arab-IN",
    		ksb: "ksb-Latn-TZ",
    		ksd: "ksd-Latn-ZZ",
    		ksf: "ksf-Latn-CM",
    		ksh: "ksh-Latn-DE",
    		ksj: "ksj-Latn-ZZ",
    		ksr: "ksr-Latn-ZZ",
    		ktb: "ktb-Ethi-ZZ",
    		ktm: "ktm-Latn-ZZ",
    		kto: "kto-Latn-ZZ",
    		ktr: "ktr-Latn-MY",
    		ku: "ku-Latn-TR",
    		"ku-Arab": "ku-Arab-IQ",
    		"ku-LB": "ku-Arab-LB",
    		kub: "kub-Latn-ZZ",
    		kud: "kud-Latn-ZZ",
    		kue: "kue-Latn-ZZ",
    		kuj: "kuj-Latn-ZZ",
    		kum: "kum-Cyrl-RU",
    		kun: "kun-Latn-ZZ",
    		kup: "kup-Latn-ZZ",
    		kus: "kus-Latn-ZZ",
    		kv: "kv-Cyrl-RU",
    		kvg: "kvg-Latn-ZZ",
    		kvr: "kvr-Latn-ID",
    		kvx: "kvx-Arab-PK",
    		kw: "kw-Latn-GB",
    		kwj: "kwj-Latn-ZZ",
    		kwo: "kwo-Latn-ZZ",
    		kwq: "kwq-Latn-ZZ",
    		kxa: "kxa-Latn-ZZ",
    		kxc: "kxc-Ethi-ZZ",
    		kxe: "kxe-Latn-ZZ",
    		kxm: "kxm-Thai-TH",
    		kxp: "kxp-Arab-PK",
    		kxw: "kxw-Latn-ZZ",
    		kxz: "kxz-Latn-ZZ",
    		ky: "ky-Cyrl-KG",
    		"ky-Arab": "ky-Arab-CN",
    		"ky-CN": "ky-Arab-CN",
    		"ky-Latn": "ky-Latn-TR",
    		"ky-TR": "ky-Latn-TR",
    		kye: "kye-Latn-ZZ",
    		kyx: "kyx-Latn-ZZ",
    		kzj: "kzj-Latn-MY",
    		kzr: "kzr-Latn-ZZ",
    		kzt: "kzt-Latn-MY",
    		la: "la-Latn-VA",
    		lab: "lab-Lina-GR",
    		lad: "lad-Hebr-IL",
    		lag: "lag-Latn-TZ",
    		lah: "lah-Arab-PK",
    		laj: "laj-Latn-UG",
    		las: "las-Latn-ZZ",
    		lb: "lb-Latn-LU",
    		lbe: "lbe-Cyrl-RU",
    		lbu: "lbu-Latn-ZZ",
    		lbw: "lbw-Latn-ID",
    		lcm: "lcm-Latn-ZZ",
    		lcp: "lcp-Thai-CN",
    		ldb: "ldb-Latn-ZZ",
    		led: "led-Latn-ZZ",
    		lee: "lee-Latn-ZZ",
    		lem: "lem-Latn-ZZ",
    		lep: "lep-Lepc-IN",
    		leq: "leq-Latn-ZZ",
    		leu: "leu-Latn-ZZ",
    		lez: "lez-Cyrl-RU",
    		lg: "lg-Latn-UG",
    		lgg: "lgg-Latn-ZZ",
    		li: "li-Latn-NL",
    		lia: "lia-Latn-ZZ",
    		lid: "lid-Latn-ZZ",
    		lif: "lif-Deva-NP",
    		"lif-Limb": "lif-Limb-IN",
    		lig: "lig-Latn-ZZ",
    		lih: "lih-Latn-ZZ",
    		lij: "lij-Latn-IT",
    		lis: "lis-Lisu-CN",
    		ljp: "ljp-Latn-ID",
    		lki: "lki-Arab-IR",
    		lkt: "lkt-Latn-US",
    		lle: "lle-Latn-ZZ",
    		lln: "lln-Latn-ZZ",
    		lmn: "lmn-Telu-IN",
    		lmo: "lmo-Latn-IT",
    		lmp: "lmp-Latn-ZZ",
    		ln: "ln-Latn-CD",
    		lns: "lns-Latn-ZZ",
    		lnu: "lnu-Latn-ZZ",
    		lo: "lo-Laoo-LA",
    		loj: "loj-Latn-ZZ",
    		lok: "lok-Latn-ZZ",
    		lol: "lol-Latn-CD",
    		lor: "lor-Latn-ZZ",
    		los: "los-Latn-ZZ",
    		loz: "loz-Latn-ZM",
    		lrc: "lrc-Arab-IR",
    		lt: "lt-Latn-LT",
    		ltg: "ltg-Latn-LV",
    		lu: "lu-Latn-CD",
    		lua: "lua-Latn-CD",
    		luo: "luo-Latn-KE",
    		luy: "luy-Latn-KE",
    		luz: "luz-Arab-IR",
    		lv: "lv-Latn-LV",
    		lwl: "lwl-Thai-TH",
    		lzh: "lzh-Hans-CN",
    		lzz: "lzz-Latn-TR",
    		mad: "mad-Latn-ID",
    		maf: "maf-Latn-CM",
    		mag: "mag-Deva-IN",
    		mai: "mai-Deva-IN",
    		mak: "mak-Latn-ID",
    		man: "man-Latn-GM",
    		"man-GN": "man-Nkoo-GN",
    		"man-Nkoo": "man-Nkoo-GN",
    		mas: "mas-Latn-KE",
    		maw: "maw-Latn-ZZ",
    		maz: "maz-Latn-MX",
    		mbh: "mbh-Latn-ZZ",
    		mbo: "mbo-Latn-ZZ",
    		mbq: "mbq-Latn-ZZ",
    		mbu: "mbu-Latn-ZZ",
    		mbw: "mbw-Latn-ZZ",
    		mci: "mci-Latn-ZZ",
    		mcp: "mcp-Latn-ZZ",
    		mcq: "mcq-Latn-ZZ",
    		mcr: "mcr-Latn-ZZ",
    		mcu: "mcu-Latn-ZZ",
    		mda: "mda-Latn-ZZ",
    		mde: "mde-Arab-ZZ",
    		mdf: "mdf-Cyrl-RU",
    		mdh: "mdh-Latn-PH",
    		mdj: "mdj-Latn-ZZ",
    		mdr: "mdr-Latn-ID",
    		mdx: "mdx-Ethi-ZZ",
    		med: "med-Latn-ZZ",
    		mee: "mee-Latn-ZZ",
    		mek: "mek-Latn-ZZ",
    		men: "men-Latn-SL",
    		mer: "mer-Latn-KE",
    		met: "met-Latn-ZZ",
    		meu: "meu-Latn-ZZ",
    		mfa: "mfa-Arab-TH",
    		mfe: "mfe-Latn-MU",
    		mfn: "mfn-Latn-ZZ",
    		mfo: "mfo-Latn-ZZ",
    		mfq: "mfq-Latn-ZZ",
    		mg: "mg-Latn-MG",
    		mgh: "mgh-Latn-MZ",
    		mgl: "mgl-Latn-ZZ",
    		mgo: "mgo-Latn-CM",
    		mgp: "mgp-Deva-NP",
    		mgy: "mgy-Latn-TZ",
    		mh: "mh-Latn-MH",
    		mhi: "mhi-Latn-ZZ",
    		mhl: "mhl-Latn-ZZ",
    		mi: "mi-Latn-NZ",
    		mif: "mif-Latn-ZZ",
    		min: "min-Latn-ID",
    		mis: "mis-Hatr-IQ",
    		"mis-Medf": "mis-Medf-NG",
    		miw: "miw-Latn-ZZ",
    		mk: "mk-Cyrl-MK",
    		mki: "mki-Arab-ZZ",
    		mkl: "mkl-Latn-ZZ",
    		mkp: "mkp-Latn-ZZ",
    		mkw: "mkw-Latn-ZZ",
    		ml: "ml-Mlym-IN",
    		mle: "mle-Latn-ZZ",
    		mlp: "mlp-Latn-ZZ",
    		mls: "mls-Latn-SD",
    		mmo: "mmo-Latn-ZZ",
    		mmu: "mmu-Latn-ZZ",
    		mmx: "mmx-Latn-ZZ",
    		mn: "mn-Cyrl-MN",
    		"mn-CN": "mn-Mong-CN",
    		"mn-Mong": "mn-Mong-CN",
    		mna: "mna-Latn-ZZ",
    		mnf: "mnf-Latn-ZZ",
    		mni: "mni-Beng-IN",
    		mnw: "mnw-Mymr-MM",
    		mo: "mo-Latn-RO",
    		moa: "moa-Latn-ZZ",
    		moe: "moe-Latn-CA",
    		moh: "moh-Latn-CA",
    		mos: "mos-Latn-BF",
    		mox: "mox-Latn-ZZ",
    		mpp: "mpp-Latn-ZZ",
    		mps: "mps-Latn-ZZ",
    		mpt: "mpt-Latn-ZZ",
    		mpx: "mpx-Latn-ZZ",
    		mql: "mql-Latn-ZZ",
    		mr: "mr-Deva-IN",
    		mrd: "mrd-Deva-NP",
    		mrj: "mrj-Cyrl-RU",
    		mro: "mro-Mroo-BD",
    		ms: "ms-Latn-MY",
    		"ms-CC": "ms-Arab-CC",
    		"ms-ID": "ms-Arab-ID",
    		mt: "mt-Latn-MT",
    		mtc: "mtc-Latn-ZZ",
    		mtf: "mtf-Latn-ZZ",
    		mti: "mti-Latn-ZZ",
    		mtr: "mtr-Deva-IN",
    		mua: "mua-Latn-CM",
    		mur: "mur-Latn-ZZ",
    		mus: "mus-Latn-US",
    		mva: "mva-Latn-ZZ",
    		mvn: "mvn-Latn-ZZ",
    		mvy: "mvy-Arab-PK",
    		mwk: "mwk-Latn-ML",
    		mwr: "mwr-Deva-IN",
    		mwv: "mwv-Latn-ID",
    		mww: "mww-Hmnp-US",
    		mxc: "mxc-Latn-ZW",
    		mxm: "mxm-Latn-ZZ",
    		my: "my-Mymr-MM",
    		myk: "myk-Latn-ZZ",
    		mym: "mym-Ethi-ZZ",
    		myv: "myv-Cyrl-RU",
    		myw: "myw-Latn-ZZ",
    		myx: "myx-Latn-UG",
    		myz: "myz-Mand-IR",
    		mzk: "mzk-Latn-ZZ",
    		mzm: "mzm-Latn-ZZ",
    		mzn: "mzn-Arab-IR",
    		mzp: "mzp-Latn-ZZ",
    		mzw: "mzw-Latn-ZZ",
    		mzz: "mzz-Latn-ZZ",
    		na: "na-Latn-NR",
    		nac: "nac-Latn-ZZ",
    		naf: "naf-Latn-ZZ",
    		nak: "nak-Latn-ZZ",
    		nan: "nan-Hans-CN",
    		nap: "nap-Latn-IT",
    		naq: "naq-Latn-NA",
    		nas: "nas-Latn-ZZ",
    		nb: "nb-Latn-NO",
    		nca: "nca-Latn-ZZ",
    		nce: "nce-Latn-ZZ",
    		ncf: "ncf-Latn-ZZ",
    		nch: "nch-Latn-MX",
    		nco: "nco-Latn-ZZ",
    		ncu: "ncu-Latn-ZZ",
    		nd: "nd-Latn-ZW",
    		ndc: "ndc-Latn-MZ",
    		nds: "nds-Latn-DE",
    		ne: "ne-Deva-NP",
    		neb: "neb-Latn-ZZ",
    		"new": "new-Deva-NP",
    		nex: "nex-Latn-ZZ",
    		nfr: "nfr-Latn-ZZ",
    		ng: "ng-Latn-NA",
    		nga: "nga-Latn-ZZ",
    		ngb: "ngb-Latn-ZZ",
    		ngl: "ngl-Latn-MZ",
    		nhb: "nhb-Latn-ZZ",
    		nhe: "nhe-Latn-MX",
    		nhw: "nhw-Latn-MX",
    		nif: "nif-Latn-ZZ",
    		nii: "nii-Latn-ZZ",
    		nij: "nij-Latn-ID",
    		nin: "nin-Latn-ZZ",
    		niu: "niu-Latn-NU",
    		niy: "niy-Latn-ZZ",
    		niz: "niz-Latn-ZZ",
    		njo: "njo-Latn-IN",
    		nkg: "nkg-Latn-ZZ",
    		nko: "nko-Latn-ZZ",
    		nl: "nl-Latn-NL",
    		nmg: "nmg-Latn-CM",
    		nmz: "nmz-Latn-ZZ",
    		nn: "nn-Latn-NO",
    		nnf: "nnf-Latn-ZZ",
    		nnh: "nnh-Latn-CM",
    		nnk: "nnk-Latn-ZZ",
    		nnm: "nnm-Latn-ZZ",
    		nnp: "nnp-Wcho-IN",
    		no: "no-Latn-NO",
    		nod: "nod-Lana-TH",
    		noe: "noe-Deva-IN",
    		non: "non-Runr-SE",
    		nop: "nop-Latn-ZZ",
    		nou: "nou-Latn-ZZ",
    		nqo: "nqo-Nkoo-GN",
    		nr: "nr-Latn-ZA",
    		nrb: "nrb-Latn-ZZ",
    		nsk: "nsk-Cans-CA",
    		nsn: "nsn-Latn-ZZ",
    		nso: "nso-Latn-ZA",
    		nss: "nss-Latn-ZZ",
    		ntm: "ntm-Latn-ZZ",
    		ntr: "ntr-Latn-ZZ",
    		nui: "nui-Latn-ZZ",
    		nup: "nup-Latn-ZZ",
    		nus: "nus-Latn-SS",
    		nuv: "nuv-Latn-ZZ",
    		nux: "nux-Latn-ZZ",
    		nv: "nv-Latn-US",
    		nwb: "nwb-Latn-ZZ",
    		nxq: "nxq-Latn-CN",
    		nxr: "nxr-Latn-ZZ",
    		ny: "ny-Latn-MW",
    		nym: "nym-Latn-TZ",
    		nyn: "nyn-Latn-UG",
    		nzi: "nzi-Latn-GH",
    		oc: "oc-Latn-FR",
    		ogc: "ogc-Latn-ZZ",
    		okr: "okr-Latn-ZZ",
    		okv: "okv-Latn-ZZ",
    		om: "om-Latn-ET",
    		ong: "ong-Latn-ZZ",
    		onn: "onn-Latn-ZZ",
    		ons: "ons-Latn-ZZ",
    		opm: "opm-Latn-ZZ",
    		or: "or-Orya-IN",
    		oro: "oro-Latn-ZZ",
    		oru: "oru-Arab-ZZ",
    		os: "os-Cyrl-GE",
    		osa: "osa-Osge-US",
    		ota: "ota-Arab-ZZ",
    		otk: "otk-Orkh-MN",
    		ozm: "ozm-Latn-ZZ",
    		pa: "pa-Guru-IN",
    		"pa-Arab": "pa-Arab-PK",
    		"pa-PK": "pa-Arab-PK",
    		pag: "pag-Latn-PH",
    		pal: "pal-Phli-IR",
    		"pal-Phlp": "pal-Phlp-CN",
    		pam: "pam-Latn-PH",
    		pap: "pap-Latn-AW",
    		pau: "pau-Latn-PW",
    		pbi: "pbi-Latn-ZZ",
    		pcd: "pcd-Latn-FR",
    		pcm: "pcm-Latn-NG",
    		pdc: "pdc-Latn-US",
    		pdt: "pdt-Latn-CA",
    		ped: "ped-Latn-ZZ",
    		peo: "peo-Xpeo-IR",
    		pex: "pex-Latn-ZZ",
    		pfl: "pfl-Latn-DE",
    		phl: "phl-Arab-ZZ",
    		phn: "phn-Phnx-LB",
    		pil: "pil-Latn-ZZ",
    		pip: "pip-Latn-ZZ",
    		pka: "pka-Brah-IN",
    		pko: "pko-Latn-KE",
    		pl: "pl-Latn-PL",
    		pla: "pla-Latn-ZZ",
    		pms: "pms-Latn-IT",
    		png: "png-Latn-ZZ",
    		pnn: "pnn-Latn-ZZ",
    		pnt: "pnt-Grek-GR",
    		pon: "pon-Latn-FM",
    		ppa: "ppa-Deva-IN",
    		ppo: "ppo-Latn-ZZ",
    		pra: "pra-Khar-PK",
    		prd: "prd-Arab-IR",
    		prg: "prg-Latn-001",
    		ps: "ps-Arab-AF",
    		pss: "pss-Latn-ZZ",
    		pt: "pt-Latn-BR",
    		ptp: "ptp-Latn-ZZ",
    		puu: "puu-Latn-GA",
    		pwa: "pwa-Latn-ZZ",
    		qu: "qu-Latn-PE",
    		quc: "quc-Latn-GT",
    		qug: "qug-Latn-EC",
    		rai: "rai-Latn-ZZ",
    		raj: "raj-Deva-IN",
    		rao: "rao-Latn-ZZ",
    		rcf: "rcf-Latn-RE",
    		rej: "rej-Latn-ID",
    		rel: "rel-Latn-ZZ",
    		res: "res-Latn-ZZ",
    		rgn: "rgn-Latn-IT",
    		rhg: "rhg-Arab-MM",
    		ria: "ria-Latn-IN",
    		rif: "rif-Tfng-MA",
    		"rif-NL": "rif-Latn-NL",
    		rjs: "rjs-Deva-NP",
    		rkt: "rkt-Beng-BD",
    		rm: "rm-Latn-CH",
    		rmf: "rmf-Latn-FI",
    		rmo: "rmo-Latn-CH",
    		rmt: "rmt-Arab-IR",
    		rmu: "rmu-Latn-SE",
    		rn: "rn-Latn-BI",
    		rna: "rna-Latn-ZZ",
    		rng: "rng-Latn-MZ",
    		ro: "ro-Latn-RO",
    		rob: "rob-Latn-ID",
    		rof: "rof-Latn-TZ",
    		roo: "roo-Latn-ZZ",
    		rro: "rro-Latn-ZZ",
    		rtm: "rtm-Latn-FJ",
    		ru: "ru-Cyrl-RU",
    		rue: "rue-Cyrl-UA",
    		rug: "rug-Latn-SB",
    		rw: "rw-Latn-RW",
    		rwk: "rwk-Latn-TZ",
    		rwo: "rwo-Latn-ZZ",
    		ryu: "ryu-Kana-JP",
    		sa: "sa-Deva-IN",
    		saf: "saf-Latn-GH",
    		sah: "sah-Cyrl-RU",
    		saq: "saq-Latn-KE",
    		sas: "sas-Latn-ID",
    		sat: "sat-Latn-IN",
    		sav: "sav-Latn-SN",
    		saz: "saz-Saur-IN",
    		sba: "sba-Latn-ZZ",
    		sbe: "sbe-Latn-ZZ",
    		sbp: "sbp-Latn-TZ",
    		sc: "sc-Latn-IT",
    		sck: "sck-Deva-IN",
    		scl: "scl-Arab-ZZ",
    		scn: "scn-Latn-IT",
    		sco: "sco-Latn-GB",
    		scs: "scs-Latn-CA",
    		sd: "sd-Arab-PK",
    		"sd-Deva": "sd-Deva-IN",
    		"sd-Khoj": "sd-Khoj-IN",
    		"sd-Sind": "sd-Sind-IN",
    		sdc: "sdc-Latn-IT",
    		sdh: "sdh-Arab-IR",
    		se: "se-Latn-NO",
    		sef: "sef-Latn-CI",
    		seh: "seh-Latn-MZ",
    		sei: "sei-Latn-MX",
    		ses: "ses-Latn-ML",
    		sg: "sg-Latn-CF",
    		sga: "sga-Ogam-IE",
    		sgs: "sgs-Latn-LT",
    		sgw: "sgw-Ethi-ZZ",
    		sgz: "sgz-Latn-ZZ",
    		shi: "shi-Tfng-MA",
    		shk: "shk-Latn-ZZ",
    		shn: "shn-Mymr-MM",
    		shu: "shu-Arab-ZZ",
    		si: "si-Sinh-LK",
    		sid: "sid-Latn-ET",
    		sig: "sig-Latn-ZZ",
    		sil: "sil-Latn-ZZ",
    		sim: "sim-Latn-ZZ",
    		sjr: "sjr-Latn-ZZ",
    		sk: "sk-Latn-SK",
    		skc: "skc-Latn-ZZ",
    		skr: "skr-Arab-PK",
    		sks: "sks-Latn-ZZ",
    		sl: "sl-Latn-SI",
    		sld: "sld-Latn-ZZ",
    		sli: "sli-Latn-PL",
    		sll: "sll-Latn-ZZ",
    		sly: "sly-Latn-ID",
    		sm: "sm-Latn-WS",
    		sma: "sma-Latn-SE",
    		smj: "smj-Latn-SE",
    		smn: "smn-Latn-FI",
    		smp: "smp-Samr-IL",
    		smq: "smq-Latn-ZZ",
    		sms: "sms-Latn-FI",
    		sn: "sn-Latn-ZW",
    		snc: "snc-Latn-ZZ",
    		snk: "snk-Latn-ML",
    		snp: "snp-Latn-ZZ",
    		snx: "snx-Latn-ZZ",
    		sny: "sny-Latn-ZZ",
    		so: "so-Latn-SO",
    		sog: "sog-Sogd-UZ",
    		sok: "sok-Latn-ZZ",
    		soq: "soq-Latn-ZZ",
    		sou: "sou-Thai-TH",
    		soy: "soy-Latn-ZZ",
    		spd: "spd-Latn-ZZ",
    		spl: "spl-Latn-ZZ",
    		sps: "sps-Latn-ZZ",
    		sq: "sq-Latn-AL",
    		sr: "sr-Cyrl-RS",
    		"sr-ME": "sr-Latn-ME",
    		"sr-RO": "sr-Latn-RO",
    		"sr-RU": "sr-Latn-RU",
    		"sr-TR": "sr-Latn-TR",
    		srb: "srb-Sora-IN",
    		srn: "srn-Latn-SR",
    		srr: "srr-Latn-SN",
    		srx: "srx-Deva-IN",
    		ss: "ss-Latn-ZA",
    		ssd: "ssd-Latn-ZZ",
    		ssg: "ssg-Latn-ZZ",
    		ssy: "ssy-Latn-ER",
    		st: "st-Latn-ZA",
    		stk: "stk-Latn-ZZ",
    		stq: "stq-Latn-DE",
    		su: "su-Latn-ID",
    		sua: "sua-Latn-ZZ",
    		sue: "sue-Latn-ZZ",
    		suk: "suk-Latn-TZ",
    		sur: "sur-Latn-ZZ",
    		sus: "sus-Latn-GN",
    		sv: "sv-Latn-SE",
    		sw: "sw-Latn-TZ",
    		swb: "swb-Arab-YT",
    		swc: "swc-Latn-CD",
    		swg: "swg-Latn-DE",
    		swp: "swp-Latn-ZZ",
    		swv: "swv-Deva-IN",
    		sxn: "sxn-Latn-ID",
    		sxw: "sxw-Latn-ZZ",
    		syl: "syl-Beng-BD",
    		syr: "syr-Syrc-IQ",
    		szl: "szl-Latn-PL",
    		ta: "ta-Taml-IN",
    		taj: "taj-Deva-NP",
    		tal: "tal-Latn-ZZ",
    		tan: "tan-Latn-ZZ",
    		taq: "taq-Latn-ZZ",
    		tbc: "tbc-Latn-ZZ",
    		tbd: "tbd-Latn-ZZ",
    		tbf: "tbf-Latn-ZZ",
    		tbg: "tbg-Latn-ZZ",
    		tbo: "tbo-Latn-ZZ",
    		tbw: "tbw-Latn-PH",
    		tbz: "tbz-Latn-ZZ",
    		tci: "tci-Latn-ZZ",
    		tcy: "tcy-Knda-IN",
    		tdd: "tdd-Tale-CN",
    		tdg: "tdg-Deva-NP",
    		tdh: "tdh-Deva-NP",
    		tdu: "tdu-Latn-MY",
    		te: "te-Telu-IN",
    		ted: "ted-Latn-ZZ",
    		tem: "tem-Latn-SL",
    		teo: "teo-Latn-UG",
    		tet: "tet-Latn-TL",
    		tfi: "tfi-Latn-ZZ",
    		tg: "tg-Cyrl-TJ",
    		"tg-Arab": "tg-Arab-PK",
    		"tg-PK": "tg-Arab-PK",
    		tgc: "tgc-Latn-ZZ",
    		tgo: "tgo-Latn-ZZ",
    		tgu: "tgu-Latn-ZZ",
    		th: "th-Thai-TH",
    		thl: "thl-Deva-NP",
    		thq: "thq-Deva-NP",
    		thr: "thr-Deva-NP",
    		ti: "ti-Ethi-ET",
    		tif: "tif-Latn-ZZ",
    		tig: "tig-Ethi-ER",
    		tik: "tik-Latn-ZZ",
    		tim: "tim-Latn-ZZ",
    		tio: "tio-Latn-ZZ",
    		tiv: "tiv-Latn-NG",
    		tk: "tk-Latn-TM",
    		tkl: "tkl-Latn-TK",
    		tkr: "tkr-Latn-AZ",
    		tkt: "tkt-Deva-NP",
    		tl: "tl-Latn-PH",
    		tlf: "tlf-Latn-ZZ",
    		tlx: "tlx-Latn-ZZ",
    		tly: "tly-Latn-AZ",
    		tmh: "tmh-Latn-NE",
    		tmy: "tmy-Latn-ZZ",
    		tn: "tn-Latn-ZA",
    		tnh: "tnh-Latn-ZZ",
    		to: "to-Latn-TO",
    		tof: "tof-Latn-ZZ",
    		tog: "tog-Latn-MW",
    		toq: "toq-Latn-ZZ",
    		tpi: "tpi-Latn-PG",
    		tpm: "tpm-Latn-ZZ",
    		tpz: "tpz-Latn-ZZ",
    		tqo: "tqo-Latn-ZZ",
    		tr: "tr-Latn-TR",
    		tru: "tru-Latn-TR",
    		trv: "trv-Latn-TW",
    		trw: "trw-Arab-ZZ",
    		ts: "ts-Latn-ZA",
    		tsd: "tsd-Grek-GR",
    		tsf: "tsf-Deva-NP",
    		tsg: "tsg-Latn-PH",
    		tsj: "tsj-Tibt-BT",
    		tsw: "tsw-Latn-ZZ",
    		tt: "tt-Cyrl-RU",
    		ttd: "ttd-Latn-ZZ",
    		tte: "tte-Latn-ZZ",
    		ttj: "ttj-Latn-UG",
    		ttr: "ttr-Latn-ZZ",
    		tts: "tts-Thai-TH",
    		ttt: "ttt-Latn-AZ",
    		tuh: "tuh-Latn-ZZ",
    		tul: "tul-Latn-ZZ",
    		tum: "tum-Latn-MW",
    		tuq: "tuq-Latn-ZZ",
    		tvd: "tvd-Latn-ZZ",
    		tvl: "tvl-Latn-TV",
    		tvu: "tvu-Latn-ZZ",
    		twh: "twh-Latn-ZZ",
    		twq: "twq-Latn-NE",
    		txg: "txg-Tang-CN",
    		ty: "ty-Latn-PF",
    		tya: "tya-Latn-ZZ",
    		tyv: "tyv-Cyrl-RU",
    		tzm: "tzm-Latn-MA",
    		ubu: "ubu-Latn-ZZ",
    		udm: "udm-Cyrl-RU",
    		ug: "ug-Arab-CN",
    		"ug-Cyrl": "ug-Cyrl-KZ",
    		"ug-KZ": "ug-Cyrl-KZ",
    		"ug-MN": "ug-Cyrl-MN",
    		uga: "uga-Ugar-SY",
    		uk: "uk-Cyrl-UA",
    		uli: "uli-Latn-FM",
    		umb: "umb-Latn-AO",
    		und: "en-Latn-US",
    		"und-002": "en-Latn-NG",
    		"und-003": "en-Latn-US",
    		"und-005": "pt-Latn-BR",
    		"und-009": "en-Latn-AU",
    		"und-011": "en-Latn-NG",
    		"und-013": "es-Latn-MX",
    		"und-014": "sw-Latn-TZ",
    		"und-015": "ar-Arab-EG",
    		"und-017": "sw-Latn-CD",
    		"und-018": "en-Latn-ZA",
    		"und-019": "en-Latn-US",
    		"und-021": "en-Latn-US",
    		"und-029": "es-Latn-CU",
    		"und-030": "zh-Hans-CN",
    		"und-034": "hi-Deva-IN",
    		"und-035": "id-Latn-ID",
    		"und-039": "it-Latn-IT",
    		"und-053": "en-Latn-AU",
    		"und-054": "en-Latn-PG",
    		"und-057": "en-Latn-GU",
    		"und-061": "sm-Latn-WS",
    		"und-142": "zh-Hans-CN",
    		"und-143": "uz-Latn-UZ",
    		"und-145": "ar-Arab-SA",
    		"und-150": "ru-Cyrl-RU",
    		"und-151": "ru-Cyrl-RU",
    		"und-154": "en-Latn-GB",
    		"und-155": "de-Latn-DE",
    		"und-202": "en-Latn-NG",
    		"und-419": "es-Latn-419",
    		"und-AD": "ca-Latn-AD",
    		"und-Adlm": "ff-Adlm-GN",
    		"und-AE": "ar-Arab-AE",
    		"und-AF": "fa-Arab-AF",
    		"und-Aghb": "lez-Aghb-RU",
    		"und-Ahom": "aho-Ahom-IN",
    		"und-AL": "sq-Latn-AL",
    		"und-AM": "hy-Armn-AM",
    		"und-AO": "pt-Latn-AO",
    		"und-AQ": "und-Latn-AQ",
    		"und-AR": "es-Latn-AR",
    		"und-Arab": "ar-Arab-EG",
    		"und-Arab-CC": "ms-Arab-CC",
    		"und-Arab-CN": "ug-Arab-CN",
    		"und-Arab-GB": "ks-Arab-GB",
    		"und-Arab-ID": "ms-Arab-ID",
    		"und-Arab-IN": "ur-Arab-IN",
    		"und-Arab-KH": "cja-Arab-KH",
    		"und-Arab-MM": "rhg-Arab-MM",
    		"und-Arab-MN": "kk-Arab-MN",
    		"und-Arab-MU": "ur-Arab-MU",
    		"und-Arab-NG": "ha-Arab-NG",
    		"und-Arab-PK": "ur-Arab-PK",
    		"und-Arab-TG": "apd-Arab-TG",
    		"und-Arab-TH": "mfa-Arab-TH",
    		"und-Arab-TJ": "fa-Arab-TJ",
    		"und-Arab-TR": "az-Arab-TR",
    		"und-Arab-YT": "swb-Arab-YT",
    		"und-Armi": "arc-Armi-IR",
    		"und-Armn": "hy-Armn-AM",
    		"und-AS": "sm-Latn-AS",
    		"und-AT": "de-Latn-AT",
    		"und-Avst": "ae-Avst-IR",
    		"und-AW": "nl-Latn-AW",
    		"und-AX": "sv-Latn-AX",
    		"und-AZ": "az-Latn-AZ",
    		"und-BA": "bs-Latn-BA",
    		"und-Bali": "ban-Bali-ID",
    		"und-Bamu": "bax-Bamu-CM",
    		"und-Bass": "bsq-Bass-LR",
    		"und-Batk": "bbc-Batk-ID",
    		"und-BD": "bn-Beng-BD",
    		"und-BE": "nl-Latn-BE",
    		"und-Beng": "bn-Beng-BD",
    		"und-BF": "fr-Latn-BF",
    		"und-BG": "bg-Cyrl-BG",
    		"und-BH": "ar-Arab-BH",
    		"und-Bhks": "sa-Bhks-IN",
    		"und-BI": "rn-Latn-BI",
    		"und-BJ": "fr-Latn-BJ",
    		"und-BL": "fr-Latn-BL",
    		"und-BN": "ms-Latn-BN",
    		"und-BO": "es-Latn-BO",
    		"und-Bopo": "zh-Bopo-TW",
    		"und-BQ": "pap-Latn-BQ",
    		"und-BR": "pt-Latn-BR",
    		"und-Brah": "pka-Brah-IN",
    		"und-Brai": "fr-Brai-FR",
    		"und-BT": "dz-Tibt-BT",
    		"und-Bugi": "bug-Bugi-ID",
    		"und-Buhd": "bku-Buhd-PH",
    		"und-BV": "und-Latn-BV",
    		"und-BY": "be-Cyrl-BY",
    		"und-Cakm": "ccp-Cakm-BD",
    		"und-Cans": "cr-Cans-CA",
    		"und-Cari": "xcr-Cari-TR",
    		"und-CD": "sw-Latn-CD",
    		"und-CF": "fr-Latn-CF",
    		"und-CG": "fr-Latn-CG",
    		"und-CH": "de-Latn-CH",
    		"und-Cham": "cjm-Cham-VN",
    		"und-Cher": "chr-Cher-US",
    		"und-CI": "fr-Latn-CI",
    		"und-CL": "es-Latn-CL",
    		"und-CM": "fr-Latn-CM",
    		"und-CN": "zh-Hans-CN",
    		"und-CO": "es-Latn-CO",
    		"und-Copt": "cop-Copt-EG",
    		"und-CP": "und-Latn-CP",
    		"und-Cprt": "grc-Cprt-CY",
    		"und-CR": "es-Latn-CR",
    		"und-CU": "es-Latn-CU",
    		"und-CV": "pt-Latn-CV",
    		"und-CW": "pap-Latn-CW",
    		"und-CY": "el-Grek-CY",
    		"und-Cyrl": "ru-Cyrl-RU",
    		"und-Cyrl-AL": "mk-Cyrl-AL",
    		"und-Cyrl-BA": "sr-Cyrl-BA",
    		"und-Cyrl-GE": "ab-Cyrl-GE",
    		"und-Cyrl-GR": "mk-Cyrl-GR",
    		"und-Cyrl-MD": "uk-Cyrl-MD",
    		"und-Cyrl-RO": "bg-Cyrl-RO",
    		"und-Cyrl-SK": "uk-Cyrl-SK",
    		"und-Cyrl-TR": "kbd-Cyrl-TR",
    		"und-Cyrl-XK": "sr-Cyrl-XK",
    		"und-CZ": "cs-Latn-CZ",
    		"und-DE": "de-Latn-DE",
    		"und-Deva": "hi-Deva-IN",
    		"und-Deva-BT": "ne-Deva-BT",
    		"und-Deva-FJ": "hif-Deva-FJ",
    		"und-Deva-MU": "bho-Deva-MU",
    		"und-Deva-PK": "btv-Deva-PK",
    		"und-DJ": "aa-Latn-DJ",
    		"und-DK": "da-Latn-DK",
    		"und-DO": "es-Latn-DO",
    		"und-Dogr": "doi-Dogr-IN",
    		"und-Dupl": "fr-Dupl-FR",
    		"und-DZ": "ar-Arab-DZ",
    		"und-EA": "es-Latn-EA",
    		"und-EC": "es-Latn-EC",
    		"und-EE": "et-Latn-EE",
    		"und-EG": "ar-Arab-EG",
    		"und-Egyp": "egy-Egyp-EG",
    		"und-EH": "ar-Arab-EH",
    		"und-Elba": "sq-Elba-AL",
    		"und-Elym": "arc-Elym-IR",
    		"und-ER": "ti-Ethi-ER",
    		"und-ES": "es-Latn-ES",
    		"und-ET": "am-Ethi-ET",
    		"und-Ethi": "am-Ethi-ET",
    		"und-EU": "en-Latn-GB",
    		"und-EZ": "de-Latn-EZ",
    		"und-FI": "fi-Latn-FI",
    		"und-FO": "fo-Latn-FO",
    		"und-FR": "fr-Latn-FR",
    		"und-GA": "fr-Latn-GA",
    		"und-GE": "ka-Geor-GE",
    		"und-Geor": "ka-Geor-GE",
    		"und-GF": "fr-Latn-GF",
    		"und-GH": "ak-Latn-GH",
    		"und-GL": "kl-Latn-GL",
    		"und-Glag": "cu-Glag-BG",
    		"und-GN": "fr-Latn-GN",
    		"und-Gong": "wsg-Gong-IN",
    		"und-Gonm": "esg-Gonm-IN",
    		"und-Goth": "got-Goth-UA",
    		"und-GP": "fr-Latn-GP",
    		"und-GQ": "es-Latn-GQ",
    		"und-GR": "el-Grek-GR",
    		"und-Gran": "sa-Gran-IN",
    		"und-Grek": "el-Grek-GR",
    		"und-Grek-TR": "bgx-Grek-TR",
    		"und-GS": "und-Latn-GS",
    		"und-GT": "es-Latn-GT",
    		"und-Gujr": "gu-Gujr-IN",
    		"und-Guru": "pa-Guru-IN",
    		"und-GW": "pt-Latn-GW",
    		"und-Hanb": "zh-Hanb-TW",
    		"und-Hang": "ko-Hang-KR",
    		"und-Hani": "zh-Hani-CN",
    		"und-Hano": "hnn-Hano-PH",
    		"und-Hans": "zh-Hans-CN",
    		"und-Hant": "zh-Hant-TW",
    		"und-Hatr": "mis-Hatr-IQ",
    		"und-Hebr": "he-Hebr-IL",
    		"und-Hebr-CA": "yi-Hebr-CA",
    		"und-Hebr-GB": "yi-Hebr-GB",
    		"und-Hebr-SE": "yi-Hebr-SE",
    		"und-Hebr-UA": "yi-Hebr-UA",
    		"und-Hebr-US": "yi-Hebr-US",
    		"und-Hira": "ja-Hira-JP",
    		"und-HK": "zh-Hant-HK",
    		"und-Hluw": "hlu-Hluw-TR",
    		"und-HM": "und-Latn-HM",
    		"und-Hmng": "hnj-Hmng-LA",
    		"und-Hmnp": "mww-Hmnp-US",
    		"und-HN": "es-Latn-HN",
    		"und-HR": "hr-Latn-HR",
    		"und-HT": "ht-Latn-HT",
    		"und-HU": "hu-Latn-HU",
    		"und-Hung": "hu-Hung-HU",
    		"und-IC": "es-Latn-IC",
    		"und-ID": "id-Latn-ID",
    		"und-IL": "he-Hebr-IL",
    		"und-IN": "hi-Deva-IN",
    		"und-IQ": "ar-Arab-IQ",
    		"und-IR": "fa-Arab-IR",
    		"und-IS": "is-Latn-IS",
    		"und-IT": "it-Latn-IT",
    		"und-Ital": "ett-Ital-IT",
    		"und-Jamo": "ko-Jamo-KR",
    		"und-Java": "jv-Java-ID",
    		"und-JO": "ar-Arab-JO",
    		"und-JP": "ja-Jpan-JP",
    		"und-Jpan": "ja-Jpan-JP",
    		"und-Kali": "eky-Kali-MM",
    		"und-Kana": "ja-Kana-JP",
    		"und-KE": "sw-Latn-KE",
    		"und-KG": "ky-Cyrl-KG",
    		"und-KH": "km-Khmr-KH",
    		"und-Khar": "pra-Khar-PK",
    		"und-Khmr": "km-Khmr-KH",
    		"und-Khoj": "sd-Khoj-IN",
    		"und-KM": "ar-Arab-KM",
    		"und-Knda": "kn-Knda-IN",
    		"und-Kore": "ko-Kore-KR",
    		"und-KP": "ko-Kore-KP",
    		"und-KR": "ko-Kore-KR",
    		"und-Kthi": "bho-Kthi-IN",
    		"und-KW": "ar-Arab-KW",
    		"und-KZ": "ru-Cyrl-KZ",
    		"und-LA": "lo-Laoo-LA",
    		"und-Lana": "nod-Lana-TH",
    		"und-Laoo": "lo-Laoo-LA",
    		"und-Latn-AF": "tk-Latn-AF",
    		"und-Latn-AM": "ku-Latn-AM",
    		"und-Latn-CN": "za-Latn-CN",
    		"und-Latn-CY": "tr-Latn-CY",
    		"und-Latn-DZ": "fr-Latn-DZ",
    		"und-Latn-ET": "en-Latn-ET",
    		"und-Latn-GE": "ku-Latn-GE",
    		"und-Latn-IR": "tk-Latn-IR",
    		"und-Latn-KM": "fr-Latn-KM",
    		"und-Latn-MA": "fr-Latn-MA",
    		"und-Latn-MK": "sq-Latn-MK",
    		"und-Latn-MM": "kac-Latn-MM",
    		"und-Latn-MO": "pt-Latn-MO",
    		"und-Latn-MR": "fr-Latn-MR",
    		"und-Latn-RU": "krl-Latn-RU",
    		"und-Latn-SY": "fr-Latn-SY",
    		"und-Latn-TN": "fr-Latn-TN",
    		"und-Latn-TW": "trv-Latn-TW",
    		"und-Latn-UA": "pl-Latn-UA",
    		"und-LB": "ar-Arab-LB",
    		"und-Lepc": "lep-Lepc-IN",
    		"und-LI": "de-Latn-LI",
    		"und-Limb": "lif-Limb-IN",
    		"und-Lina": "lab-Lina-GR",
    		"und-Linb": "grc-Linb-GR",
    		"und-Lisu": "lis-Lisu-CN",
    		"und-LK": "si-Sinh-LK",
    		"und-LS": "st-Latn-LS",
    		"und-LT": "lt-Latn-LT",
    		"und-LU": "fr-Latn-LU",
    		"und-LV": "lv-Latn-LV",
    		"und-LY": "ar-Arab-LY",
    		"und-Lyci": "xlc-Lyci-TR",
    		"und-Lydi": "xld-Lydi-TR",
    		"und-MA": "ar-Arab-MA",
    		"und-Mahj": "hi-Mahj-IN",
    		"und-Maka": "mak-Maka-ID",
    		"und-Mand": "myz-Mand-IR",
    		"und-Mani": "xmn-Mani-CN",
    		"und-Marc": "bo-Marc-CN",
    		"und-MC": "fr-Latn-MC",
    		"und-MD": "ro-Latn-MD",
    		"und-ME": "sr-Latn-ME",
    		"und-Medf": "mis-Medf-NG",
    		"und-Mend": "men-Mend-SL",
    		"und-Merc": "xmr-Merc-SD",
    		"und-Mero": "xmr-Mero-SD",
    		"und-MF": "fr-Latn-MF",
    		"und-MG": "mg-Latn-MG",
    		"und-MK": "mk-Cyrl-MK",
    		"und-ML": "bm-Latn-ML",
    		"und-Mlym": "ml-Mlym-IN",
    		"und-MM": "my-Mymr-MM",
    		"und-MN": "mn-Cyrl-MN",
    		"und-MO": "zh-Hant-MO",
    		"und-Modi": "mr-Modi-IN",
    		"und-Mong": "mn-Mong-CN",
    		"und-MQ": "fr-Latn-MQ",
    		"und-MR": "ar-Arab-MR",
    		"und-Mroo": "mro-Mroo-BD",
    		"und-MT": "mt-Latn-MT",
    		"und-Mtei": "mni-Mtei-IN",
    		"und-MU": "mfe-Latn-MU",
    		"und-Mult": "skr-Mult-PK",
    		"und-MV": "dv-Thaa-MV",
    		"und-MX": "es-Latn-MX",
    		"und-MY": "ms-Latn-MY",
    		"und-Mymr": "my-Mymr-MM",
    		"und-Mymr-IN": "kht-Mymr-IN",
    		"und-Mymr-TH": "mnw-Mymr-TH",
    		"und-MZ": "pt-Latn-MZ",
    		"und-NA": "af-Latn-NA",
    		"und-Nand": "sa-Nand-IN",
    		"und-Narb": "xna-Narb-SA",
    		"und-Nbat": "arc-Nbat-JO",
    		"und-NC": "fr-Latn-NC",
    		"und-NE": "ha-Latn-NE",
    		"und-Newa": "new-Newa-NP",
    		"und-NI": "es-Latn-NI",
    		"und-Nkoo": "man-Nkoo-GN",
    		"und-NL": "nl-Latn-NL",
    		"und-NO": "nb-Latn-NO",
    		"und-NP": "ne-Deva-NP",
    		"und-Nshu": "zhx-Nshu-CN",
    		"und-Ogam": "sga-Ogam-IE",
    		"und-Olck": "sat-Olck-IN",
    		"und-OM": "ar-Arab-OM",
    		"und-Orkh": "otk-Orkh-MN",
    		"und-Orya": "or-Orya-IN",
    		"und-Osge": "osa-Osge-US",
    		"und-Osma": "so-Osma-SO",
    		"und-PA": "es-Latn-PA",
    		"und-Palm": "arc-Palm-SY",
    		"und-Pauc": "ctd-Pauc-MM",
    		"und-PE": "es-Latn-PE",
    		"und-Perm": "kv-Perm-RU",
    		"und-PF": "fr-Latn-PF",
    		"und-PG": "tpi-Latn-PG",
    		"und-PH": "fil-Latn-PH",
    		"und-Phag": "lzh-Phag-CN",
    		"und-Phli": "pal-Phli-IR",
    		"und-Phlp": "pal-Phlp-CN",
    		"und-Phnx": "phn-Phnx-LB",
    		"und-PK": "ur-Arab-PK",
    		"und-PL": "pl-Latn-PL",
    		"und-Plrd": "hmd-Plrd-CN",
    		"und-PM": "fr-Latn-PM",
    		"und-PR": "es-Latn-PR",
    		"und-Prti": "xpr-Prti-IR",
    		"und-PS": "ar-Arab-PS",
    		"und-PT": "pt-Latn-PT",
    		"und-PW": "pau-Latn-PW",
    		"und-PY": "gn-Latn-PY",
    		"und-QA": "ar-Arab-QA",
    		"und-QO": "en-Latn-DG",
    		"und-RE": "fr-Latn-RE",
    		"und-Rjng": "rej-Rjng-ID",
    		"und-RO": "ro-Latn-RO",
    		"und-Rohg": "rhg-Rohg-MM",
    		"und-RS": "sr-Cyrl-RS",
    		"und-RU": "ru-Cyrl-RU",
    		"und-Runr": "non-Runr-SE",
    		"und-RW": "rw-Latn-RW",
    		"und-SA": "ar-Arab-SA",
    		"und-Samr": "smp-Samr-IL",
    		"und-Sarb": "xsa-Sarb-YE",
    		"und-Saur": "saz-Saur-IN",
    		"und-SC": "fr-Latn-SC",
    		"und-SD": "ar-Arab-SD",
    		"und-SE": "sv-Latn-SE",
    		"und-Sgnw": "ase-Sgnw-US",
    		"und-Shaw": "en-Shaw-GB",
    		"und-Shrd": "sa-Shrd-IN",
    		"und-SI": "sl-Latn-SI",
    		"und-Sidd": "sa-Sidd-IN",
    		"und-Sind": "sd-Sind-IN",
    		"und-Sinh": "si-Sinh-LK",
    		"und-SJ": "nb-Latn-SJ",
    		"und-SK": "sk-Latn-SK",
    		"und-SM": "it-Latn-SM",
    		"und-SN": "fr-Latn-SN",
    		"und-SO": "so-Latn-SO",
    		"und-Sogd": "sog-Sogd-UZ",
    		"und-Sogo": "sog-Sogo-UZ",
    		"und-Sora": "srb-Sora-IN",
    		"und-Soyo": "cmg-Soyo-MN",
    		"und-SR": "nl-Latn-SR",
    		"und-ST": "pt-Latn-ST",
    		"und-Sund": "su-Sund-ID",
    		"und-SV": "es-Latn-SV",
    		"und-SY": "ar-Arab-SY",
    		"und-Sylo": "syl-Sylo-BD",
    		"und-Syrc": "syr-Syrc-IQ",
    		"und-Tagb": "tbw-Tagb-PH",
    		"und-Takr": "doi-Takr-IN",
    		"und-Tale": "tdd-Tale-CN",
    		"und-Talu": "khb-Talu-CN",
    		"und-Taml": "ta-Taml-IN",
    		"und-Tang": "txg-Tang-CN",
    		"und-Tavt": "blt-Tavt-VN",
    		"und-TD": "fr-Latn-TD",
    		"und-Telu": "te-Telu-IN",
    		"und-TF": "fr-Latn-TF",
    		"und-Tfng": "zgh-Tfng-MA",
    		"und-TG": "fr-Latn-TG",
    		"und-Tglg": "fil-Tglg-PH",
    		"und-TH": "th-Thai-TH",
    		"und-Thaa": "dv-Thaa-MV",
    		"und-Thai": "th-Thai-TH",
    		"und-Thai-CN": "lcp-Thai-CN",
    		"und-Thai-KH": "kdt-Thai-KH",
    		"und-Thai-LA": "kdt-Thai-LA",
    		"und-Tibt": "bo-Tibt-CN",
    		"und-Tirh": "mai-Tirh-IN",
    		"und-TJ": "tg-Cyrl-TJ",
    		"und-TK": "tkl-Latn-TK",
    		"und-TL": "pt-Latn-TL",
    		"und-TM": "tk-Latn-TM",
    		"und-TN": "ar-Arab-TN",
    		"und-TO": "to-Latn-TO",
    		"und-TR": "tr-Latn-TR",
    		"und-TV": "tvl-Latn-TV",
    		"und-TW": "zh-Hant-TW",
    		"und-TZ": "sw-Latn-TZ",
    		"und-UA": "uk-Cyrl-UA",
    		"und-UG": "sw-Latn-UG",
    		"und-Ugar": "uga-Ugar-SY",
    		"und-UY": "es-Latn-UY",
    		"und-UZ": "uz-Latn-UZ",
    		"und-VA": "it-Latn-VA",
    		"und-Vaii": "vai-Vaii-LR",
    		"und-VE": "es-Latn-VE",
    		"und-VN": "vi-Latn-VN",
    		"und-VU": "bi-Latn-VU",
    		"und-Wara": "hoc-Wara-IN",
    		"und-Wcho": "nnp-Wcho-IN",
    		"und-WF": "fr-Latn-WF",
    		"und-WS": "sm-Latn-WS",
    		"und-XK": "sq-Latn-XK",
    		"und-Xpeo": "peo-Xpeo-IR",
    		"und-Xsux": "akk-Xsux-IQ",
    		"und-YE": "ar-Arab-YE",
    		"und-Yiii": "ii-Yiii-CN",
    		"und-YT": "fr-Latn-YT",
    		"und-Zanb": "cmg-Zanb-MN",
    		"und-ZW": "sn-Latn-ZW",
    		unr: "unr-Beng-IN",
    		"unr-Deva": "unr-Deva-NP",
    		"unr-NP": "unr-Deva-NP",
    		unx: "unx-Beng-IN",
    		uok: "uok-Latn-ZZ",
    		ur: "ur-Arab-PK",
    		uri: "uri-Latn-ZZ",
    		urt: "urt-Latn-ZZ",
    		urw: "urw-Latn-ZZ",
    		usa: "usa-Latn-ZZ",
    		utr: "utr-Latn-ZZ",
    		uvh: "uvh-Latn-ZZ",
    		uvl: "uvl-Latn-ZZ",
    		uz: "uz-Latn-UZ",
    		"uz-AF": "uz-Arab-AF",
    		"uz-Arab": "uz-Arab-AF",
    		"uz-CN": "uz-Cyrl-CN",
    		vag: "vag-Latn-ZZ",
    		vai: "vai-Vaii-LR",
    		van: "van-Latn-ZZ",
    		ve: "ve-Latn-ZA",
    		vec: "vec-Latn-IT",
    		vep: "vep-Latn-RU",
    		vi: "vi-Latn-VN",
    		vic: "vic-Latn-SX",
    		viv: "viv-Latn-ZZ",
    		vls: "vls-Latn-BE",
    		vmf: "vmf-Latn-DE",
    		vmw: "vmw-Latn-MZ",
    		vo: "vo-Latn-001",
    		vot: "vot-Latn-RU",
    		vro: "vro-Latn-EE",
    		vun: "vun-Latn-TZ",
    		vut: "vut-Latn-ZZ",
    		wa: "wa-Latn-BE",
    		wae: "wae-Latn-CH",
    		waj: "waj-Latn-ZZ",
    		wal: "wal-Ethi-ET",
    		wan: "wan-Latn-ZZ",
    		war: "war-Latn-PH",
    		wbp: "wbp-Latn-AU",
    		wbq: "wbq-Telu-IN",
    		wbr: "wbr-Deva-IN",
    		wci: "wci-Latn-ZZ",
    		wer: "wer-Latn-ZZ",
    		wgi: "wgi-Latn-ZZ",
    		whg: "whg-Latn-ZZ",
    		wib: "wib-Latn-ZZ",
    		wiu: "wiu-Latn-ZZ",
    		wiv: "wiv-Latn-ZZ",
    		wja: "wja-Latn-ZZ",
    		wji: "wji-Latn-ZZ",
    		wls: "wls-Latn-WF",
    		wmo: "wmo-Latn-ZZ",
    		wnc: "wnc-Latn-ZZ",
    		wni: "wni-Arab-KM",
    		wnu: "wnu-Latn-ZZ",
    		wo: "wo-Latn-SN",
    		wob: "wob-Latn-ZZ",
    		wos: "wos-Latn-ZZ",
    		wrs: "wrs-Latn-ZZ",
    		wsg: "wsg-Gong-IN",
    		wsk: "wsk-Latn-ZZ",
    		wtm: "wtm-Deva-IN",
    		wuu: "wuu-Hans-CN",
    		wuv: "wuv-Latn-ZZ",
    		wwa: "wwa-Latn-ZZ",
    		xav: "xav-Latn-BR",
    		xbi: "xbi-Latn-ZZ",
    		xcr: "xcr-Cari-TR",
    		xes: "xes-Latn-ZZ",
    		xh: "xh-Latn-ZA",
    		xla: "xla-Latn-ZZ",
    		xlc: "xlc-Lyci-TR",
    		xld: "xld-Lydi-TR",
    		xmf: "xmf-Geor-GE",
    		xmn: "xmn-Mani-CN",
    		xmr: "xmr-Merc-SD",
    		xna: "xna-Narb-SA",
    		xnr: "xnr-Deva-IN",
    		xog: "xog-Latn-UG",
    		xon: "xon-Latn-ZZ",
    		xpr: "xpr-Prti-IR",
    		xrb: "xrb-Latn-ZZ",
    		xsa: "xsa-Sarb-YE",
    		xsi: "xsi-Latn-ZZ",
    		xsm: "xsm-Latn-ZZ",
    		xsr: "xsr-Deva-NP",
    		xwe: "xwe-Latn-ZZ",
    		yam: "yam-Latn-ZZ",
    		yao: "yao-Latn-MZ",
    		yap: "yap-Latn-FM",
    		yas: "yas-Latn-ZZ",
    		yat: "yat-Latn-ZZ",
    		yav: "yav-Latn-CM",
    		yay: "yay-Latn-ZZ",
    		yaz: "yaz-Latn-ZZ",
    		yba: "yba-Latn-ZZ",
    		ybb: "ybb-Latn-CM",
    		yby: "yby-Latn-ZZ",
    		yer: "yer-Latn-ZZ",
    		ygr: "ygr-Latn-ZZ",
    		ygw: "ygw-Latn-ZZ",
    		yi: "yi-Hebr-001",
    		yko: "yko-Latn-ZZ",
    		yle: "yle-Latn-ZZ",
    		ylg: "ylg-Latn-ZZ",
    		yll: "yll-Latn-ZZ",
    		yml: "yml-Latn-ZZ",
    		yo: "yo-Latn-NG",
    		yon: "yon-Latn-ZZ",
    		yrb: "yrb-Latn-ZZ",
    		yre: "yre-Latn-ZZ",
    		yrl: "yrl-Latn-BR",
    		yss: "yss-Latn-ZZ",
    		yua: "yua-Latn-MX",
    		yue: "yue-Hant-HK",
    		"yue-CN": "yue-Hans-CN",
    		"yue-Hans": "yue-Hans-CN",
    		yuj: "yuj-Latn-ZZ",
    		yut: "yut-Latn-ZZ",
    		yuw: "yuw-Latn-ZZ",
    		za: "za-Latn-CN",
    		zag: "zag-Latn-SD",
    		zdj: "zdj-Arab-KM",
    		zea: "zea-Latn-NL",
    		zgh: "zgh-Tfng-MA",
    		zh: "zh-Hans-CN",
    		"zh-AU": "zh-Hant-AU",
    		"zh-BN": "zh-Hant-BN",
    		"zh-Bopo": "zh-Bopo-TW",
    		"zh-GB": "zh-Hant-GB",
    		"zh-GF": "zh-Hant-GF",
    		"zh-Hanb": "zh-Hanb-TW",
    		"zh-Hant": "zh-Hant-TW",
    		"zh-HK": "zh-Hant-HK",
    		"zh-ID": "zh-Hant-ID",
    		"zh-MO": "zh-Hant-MO",
    		"zh-MY": "zh-Hant-MY",
    		"zh-PA": "zh-Hant-PA",
    		"zh-PF": "zh-Hant-PF",
    		"zh-PH": "zh-Hant-PH",
    		"zh-SR": "zh-Hant-SR",
    		"zh-TH": "zh-Hant-TH",
    		"zh-TW": "zh-Hant-TW",
    		"zh-US": "zh-Hant-US",
    		"zh-VN": "zh-Hant-VN",
    		zhx: "zhx-Nshu-CN",
    		zia: "zia-Latn-ZZ",
    		zlm: "zlm-Latn-TG",
    		zmi: "zmi-Latn-MY",
    		zne: "zne-Latn-ZZ",
    		zu: "zu-Latn-ZA",
    		zza: "zza-Latn-TR"
    	}
    };

    var __spreadArrays$1 = (undefined && undefined.__spreadArrays) || function () {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };
    function canonicalizeAttrs(strs) {
        return Object.keys(strs.reduce(function (all, str) {
            all[str.toLowerCase()] = 1;
            return all;
        }, {})).sort();
    }
    function canonicalizeKVs(arr) {
        var all = {};
        var result = [];
        for (var _i = 0, arr_1 = arr; _i < arr_1.length; _i++) {
            var kv = arr_1[_i];
            if (kv[0] in all) {
                continue;
            }
            all[kv[0]] = 1;
            if (!kv[1] || kv[1] === 'true') {
                result.push([kv[0].toLowerCase()]);
            }
            else {
                result.push([kv[0].toLowerCase(), kv[1].toLowerCase()]);
            }
        }
        return result.sort(compareKV);
    }
    function compareKV(t1, t2) {
        return t1[0] < t2[0] ? -1 : t1[0] > t2[0] ? 1 : 0;
    }
    function compareExtension(e1, e2) {
        return e1.type < e2.type ? -1 : e1.type > e2.type ? 1 : 0;
    }
    function mergeVariants(v1, v2) {
        var result = __spreadArrays$1(v1);
        for (var _i = 0, v2_1 = v2; _i < v2_1.length; _i++) {
            var v = v2_1[_i];
            if (v1.indexOf(v) < 0) {
                result.push(v);
            }
        }
        return result;
    }
    /**
     * CAVEAT: We don't do this section in the spec bc they have no JSON data
     * Use the bcp47 data to replace keys, types, tfields, and tvalues by their canonical forms. See Section 3.6.4 U Extension Data Files) and Section 3.7.1 T Extension Data Files. The aliases are in the alias attribute value, while the canonical is in the name attribute value. For example,
    Because of the following bcp47 data:
    <key name="ms"…>…<type name="uksystem" … alias="imperial" … />…</key>
    We get the following transformation:
    en-u-ms-imperial ⇒ en-u-ms-uksystem
     * @param lang
     */
    function canonicalizeUnicodeLanguageId(unicodeLanguageId) {
        /**
         * If the language subtag matches the type attribute of a languageAlias element in Supplemental Data, replace the language subtag with the replacement value.
         *  1. If there are additional subtags in the replacement value, add them to the result, but only if there is no corresponding subtag already in the tag.
         *  2. Five special deprecated grandfathered codes (such as i-default) are in type attributes, and are also replaced.
         */
        // From https://github.com/unicode-org/icu/blob/master/icu4j/main/classes/core/src/com/ibm/icu/util/ULocale.java#L1246
        // Try language _ variant
        var finalLangAst = unicodeLanguageId;
        if (unicodeLanguageId.variants.length) {
            var replacedLang_1 = '';
            for (var _i = 0, _a = unicodeLanguageId.variants; _i < _a.length; _i++) {
                var variant = _a[_i];
                if ((replacedLang_1 =
                    languageAlias[emitUnicodeLanguageId({
                        lang: unicodeLanguageId.lang,
                        variants: [variant],
                    })])) {
                    var replacedLangAst = parseUnicodeLanguageId(replacedLang_1.split(SEPARATOR));
                    finalLangAst = {
                        lang: replacedLangAst.lang,
                        script: finalLangAst.script || replacedLangAst.script,
                        region: finalLangAst.region || replacedLangAst.region,
                        variants: mergeVariants(finalLangAst.variants, replacedLangAst.variants),
                    };
                    break;
                }
            }
        }
        // language _ script _ country
        // ug-Arab-CN -> ug-CN
        if (finalLangAst.script && finalLangAst.region) {
            var replacedLang_2 = languageAlias[emitUnicodeLanguageId({
                lang: finalLangAst.lang,
                script: finalLangAst.script,
                region: finalLangAst.region,
                variants: [],
            })];
            if (replacedLang_2) {
                var replacedLangAst = parseUnicodeLanguageId(replacedLang_2.split(SEPARATOR));
                finalLangAst = {
                    lang: replacedLangAst.lang,
                    script: replacedLangAst.script,
                    region: replacedLangAst.region,
                    variants: finalLangAst.variants,
                };
            }
        }
        // language _ country
        // eg. az_AZ -> az_Latn_A
        if (finalLangAst.region) {
            var replacedLang_3 = languageAlias[emitUnicodeLanguageId({
                lang: finalLangAst.lang,
                region: finalLangAst.region,
                variants: [],
            })];
            if (replacedLang_3) {
                var replacedLangAst = parseUnicodeLanguageId(replacedLang_3.split(SEPARATOR));
                finalLangAst = {
                    lang: replacedLangAst.lang,
                    script: finalLangAst.script || replacedLangAst.script,
                    region: replacedLangAst.region,
                    variants: finalLangAst.variants,
                };
            }
        }
        // only language
        // e.g. twi -> ak
        var replacedLang = languageAlias[emitUnicodeLanguageId({
            lang: finalLangAst.lang,
            variants: [],
        })];
        if (replacedLang) {
            var replacedLangAst = parseUnicodeLanguageId(replacedLang.split(SEPARATOR));
            finalLangAst = {
                lang: replacedLangAst.lang,
                script: finalLangAst.script || replacedLangAst.script,
                region: finalLangAst.region || replacedLangAst.region,
                variants: finalLangAst.variants,
            };
        }
        if (finalLangAst.region) {
            var region = finalLangAst.region.toUpperCase();
            var regionAlias = territoryAlias[region];
            var replacedRegion = void 0;
            if (regionAlias) {
                var regions = regionAlias.split(' ');
                replacedRegion = regions[0];
                var likelySubtag = supplemental.likelySubtags[emitUnicodeLanguageId({
                    lang: finalLangAst.lang,
                    script: finalLangAst.script,
                    variants: [],
                })];
                if (likelySubtag) {
                    var likelyRegion = parseUnicodeLanguageId(likelySubtag.split(SEPARATOR)).region;
                    if (likelyRegion && regions.indexOf(likelyRegion) > -1) {
                        replacedRegion = likelyRegion;
                    }
                }
            }
            if (replacedRegion) {
                finalLangAst.region = replacedRegion;
            }
            finalLangAst.region = finalLangAst.region.toUpperCase();
        }
        if (finalLangAst.script) {
            finalLangAst.script =
                finalLangAst.script[0].toUpperCase() +
                    finalLangAst.script.slice(1).toLowerCase();
            if (scriptAlias[finalLangAst.script]) {
                finalLangAst.script = scriptAlias[finalLangAst.script];
            }
        }
        if (finalLangAst.variants.length) {
            for (var i = 0; i < finalLangAst.variants.length; i++) {
                var variant = finalLangAst.variants[i].toLowerCase();
                if (variantAlias[variant]) {
                    var alias = variantAlias[variant];
                    if (isUnicodeVariantSubtag(alias)) {
                        finalLangAst.variants[i] = alias;
                    }
                    else if (isUnicodeLanguageSubtag(alias)) {
                        // Yes this can happen per the spec
                        finalLangAst.lang = alias;
                    }
                }
            }
            finalLangAst.variants.sort();
        }
        return finalLangAst;
    }
    /**
     * Canonicalize based on
     * https://www.unicode.org/reports/tr35/tr35.html#Canonical_Unicode_Locale_Identifiers
     * https://tc39.es/ecma402/#sec-canonicalizeunicodelocaleid
     * IMPORTANT: This modifies the object inline
     * @param locale
     */
    function canonicalizeUnicodeLocaleId(locale) {
        locale.lang = canonicalizeUnicodeLanguageId(locale.lang);
        if (locale.extensions) {
            for (var _i = 0, _a = locale.extensions; _i < _a.length; _i++) {
                var extension = _a[_i];
                switch (extension.type) {
                    case 'u':
                        extension.keywords = canonicalizeKVs(extension.keywords);
                        if (extension.attributes) {
                            extension.attributes = canonicalizeAttrs(extension.attributes);
                        }
                        break;
                    case 't':
                        if (extension.lang) {
                            extension.lang = canonicalizeUnicodeLanguageId(extension.lang);
                        }
                        extension.fields = canonicalizeKVs(extension.fields);
                        break;
                    default:
                        extension.value = extension.value.toLowerCase();
                        break;
                }
            }
            locale.extensions.sort(compareExtension);
        }
        return locale;
    }

    function canonicalizeLocaleList(locales) {
        if (locales === undefined) {
            return [];
        }
        var seen = [];
        if (typeof locales === 'string') {
            locales = [locales];
        }
        for (var _i = 0, locales_1 = locales; _i < locales_1.length; _i++) {
            var locale = locales_1[_i];
            var canonicalizedTag = emitUnicodeLocaleId(canonicalizeUnicodeLocaleId(parseUnicodeLocaleId(locale)));
            if (seen.indexOf(canonicalizedTag) < 0) {
                seen.push(canonicalizedTag);
            }
        }
        return seen;
    }
    function getCanonicalLocales(locales) {
        return canonicalizeLocaleList(locales);
    }

    if (typeof Intl == 'undefined') {
        if (typeof window !== 'undefined') {
            Object.defineProperty(window, 'Intl', {
                value: {},
            });
        }
        else if (typeof global !== 'undefined') {
            Object.defineProperty(global, 'Intl', {
                value: {},
            });
        }
    }
    if (!('getCanonicalLocales' in Intl) ||
        // Native Intl.getCanonicalLocales is just buggy
        Intl.getCanonicalLocales('und-x-private')[0] === 'x-private') {
        Object.defineProperty(Intl, 'getCanonicalLocales', {
            value: getCanonicalLocales,
            writable: true,
            enumerable: false,
            configurable: true,
        });
    }

})));


// Object.getOwnPropertyDescriptor
/* global CreateMethodProperty, ToObject, ToPropertyKey, HasOwnProperty, Type */
(function () {
	var nativeGetOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

	var supportsDOMDescriptors = (function () {
		try {
			return Object.defineProperty(document.createElement('div'), 'one', {
				get: function () {
					return 1;
				}
			}).one === 1;
		} catch (e) {
			return false;
		}
	});

	var toString = ({}).toString;
	var split = ''.split;

	// 19.1.2.8 Object.getOwnPropertyDescriptor ( O, P )
	CreateMethodProperty(Object, 'getOwnPropertyDescriptor', function getOwnPropertyDescriptor(O, P) {
		// 1. Let obj be ? ToObject(O).
		var obj = ToObject(O);
		// Polyfill.io fallback for non-array-like strings which exist in some ES3 user-agents (IE 8)
		obj = (Type(obj) === 'string' || obj instanceof String) && toString.call(O) == '[object String]' ? split.call(O, '') : Object(O);

		// 2. Let key be ? ToPropertyKey(P).
		var key = ToPropertyKey(P);

		// 3. Let desc be ? obj.[[GetOwnProperty]](key).
		// 4. Return FromPropertyDescriptor(desc). 
		// Polyfill.io Internet Explorer 8 natively supports property descriptors only on DOM objects.
		// We will fallback to the polyfill implementation if the native implementation throws an error.
		if (supportsDOMDescriptors) {
			try {
				return nativeGetOwnPropertyDescriptor(obj, key);
			// eslint-disable-next-line no-empty
			} catch (error) {}
		}
		if (HasOwnProperty(obj, key)) {
			return {
				enumerable: true,
				configurable: true,
				writable: true,
				value: obj[key]
			};
		}
	});
}());

// Object.keys
/* global CreateMethodProperty */
CreateMethodProperty(Object, "keys", (function() {
	'use strict';

	// modified from https://github.com/es-shims/object-keys

	var has = Object.prototype.hasOwnProperty;
	var toStr = Object.prototype.toString;
	var isEnumerable = Object.prototype.propertyIsEnumerable;
	var hasDontEnumBug = !isEnumerable.call({ toString: null }, 'toString');
	var hasProtoEnumBug = isEnumerable.call(function () {}, 'prototype');
	var dontEnums = [
		'toString',
		'toLocaleString',
		'valueOf',
		'hasOwnProperty',
		'isPrototypeOf',
		'propertyIsEnumerable',
		'constructor'
	];
	var equalsConstructorPrototype = function (o) {
		var ctor = o.constructor;
		return ctor && ctor.prototype === o;
	};
	var excludedKeys = {
		$console: true,
		$external: true,
		$frame: true,
		$frameElement: true,
		$frames: true,
		$innerHeight: true,
		$innerWidth: true,
		$outerHeight: true,
		$outerWidth: true,
		$pageXOffset: true,
		$pageYOffset: true,
		$parent: true,
		$scrollLeft: true,
		$scrollTop: true,
		$scrollX: true,
		$scrollY: true,
		$self: true,
		$webkitIndexedDB: true,
		$webkitStorageInfo: true,
		$window: true
	};
	var hasAutomationEqualityBug = (function () {
		if (typeof window === 'undefined') { return false; }
		for (var k in window) {
			try {
				if (!excludedKeys['$' + k] && has.call(window, k) && window[k] !== null && typeof window[k] === 'object') {
					try {
						equalsConstructorPrototype(window[k]);
					} catch (e) {
						return true;
					}
				}
			} catch (e) {
				return true;
			}
		}
		return false;
	}());
	var equalsConstructorPrototypeIfNotBuggy = function (o) {
		if (typeof window === 'undefined' || !hasAutomationEqualityBug) {
			return equalsConstructorPrototype(o);
		}
		try {
			return equalsConstructorPrototype(o);
		} catch (e) {
			return false;
		}
	};

	function isArgumentsObject(value) {
		var str = toStr.call(value);
		var isArgs = str === '[object Arguments]';
		if (!isArgs) {
			isArgs = str !== '[object Array]' &&
				value !== null &&
				typeof value === 'object' &&
				typeof value.length === 'number' &&
				value.length >= 0 &&
				toStr.call(value.callee) === '[object Function]';
		}
		return isArgs;
	}

	return function keys(object) {
		var isFunction = toStr.call(object) === '[object Function]';
		var isArguments = isArgumentsObject(object);
		var isString = toStr.call(object) === '[object String]';
		var theKeys = [];

		if (object === undefined || object === null) {
			throw new TypeError('Cannot convert undefined or null to object');
		}

		var skipProto = hasProtoEnumBug && isFunction;
		if (isString && object.length > 0 && !has.call(object, 0)) {
			for (var i = 0; i < object.length; ++i) {
				theKeys.push(String(i));
			}
		}

		if (isArguments && object.length > 0) {
			for (var j = 0; j < object.length; ++j) {
				theKeys.push(String(j));
			}
		} else {
			for (var name in object) {
				if (!(skipProto && name === 'prototype') && has.call(object, name)) {
					theKeys.push(String(name));
				}
			}
		}

		if (hasDontEnumBug) {
			var skipConstructor = equalsConstructorPrototypeIfNotBuggy(object);

			for (var k = 0; k < dontEnums.length; ++k) {
				if (!(skipConstructor && dontEnums[k] === 'constructor') && has.call(object, dontEnums[k])) {
					theKeys.push(dontEnums[k]);
				}
			}
		}
		return theKeys;
	};
}()));

// Object.getOwnPropertyNames
/* global CreateMethodProperty, ToObject */
(function() {
  var toString = {}.toString;
  var split = "".split;
  var concat = [].concat;
  var hasOwnProperty = Object.prototype.hasOwnProperty;
  var nativeGetOwnPropertyNames = Object.getOwnPropertyNames || Object.keys;
  var cachedWindowNames =
    typeof self === "object" ? nativeGetOwnPropertyNames(self) : [];

  // 19.1.2.10 Object.getOwnPropertyNames ( O )
  CreateMethodProperty(
    Object,
    "getOwnPropertyNames",
    function getOwnPropertyNames(O) {
      var object = ToObject(O);

      if (toString.call(object) === "[object Window]") {
        try {
          return nativeGetOwnPropertyNames(object);
        } catch (e) {
          // IE bug where layout engine calls userland Object.getOwnPropertyNames for cross-domain `window` objects
          return concat.call([], cachedWindowNames);
        }
      }

      // Polyfill.io fallback for non-array-like strings which exist in some ES3 user-agents (IE 8)
      object =
        toString.call(object) == "[object String]"
          ? split.call(object, "")
          : Object(object);

      var result = nativeGetOwnPropertyNames(object);
      var extraNonEnumerableKeys = ["length", "prototype"];
      for (var i = 0; i < extraNonEnumerableKeys.length; i++) {
        var key = extraNonEnumerableKeys[i];
        if (hasOwnProperty.call(object, key) && !result.includes(key)) {
          result.push(key);
        }
      }

      if (result.includes("__proto__")) {
        var index = result.indexOf("__proto__");
        result.splice(index, 1);
      }

      return result;
    }
  );
})();

// Symbol
// A modification of https://github.com/WebReflection/get-own-property-symbols
// (C) Andrea Giammarchi - MIT Licensed

(function (Object, GOPS, global) {
	'use strict'; //so that ({}).toString.call(null) returns the correct [object Null] rather than [object Window]

	var	setDescriptor;
	var id = 0;
	var random = '' + Math.random();
	var prefix = '__\x01symbol:';
	var prefixLength = prefix.length;
	var internalSymbol = '__\x01symbol@@' + random;
	var DP = 'defineProperty';
	var DPies = 'defineProperties';
	var GOPN = 'getOwnPropertyNames';
	var GOPD = 'getOwnPropertyDescriptor';
	var PIE = 'propertyIsEnumerable';
	var ObjectProto = Object.prototype;
	var hOP = ObjectProto.hasOwnProperty;
	var pIE = ObjectProto[PIE];
	var toString = ObjectProto.toString;
	var concat = Array.prototype.concat;
	var cachedWindowNames = Object.getOwnPropertyNames ? Object.getOwnPropertyNames(self) : [];
	var nGOPN = Object[GOPN];
	var gOPN = function getOwnPropertyNames (obj) {
		if (toString.call(obj) === '[object Window]') {
			try {
				return nGOPN(obj);
			} catch (e) {
				// IE bug where layout engine calls userland gOPN for cross-domain `window` objects
				return concat.call([], cachedWindowNames);
			}
		}
		return nGOPN(obj);
	};
	var gOPD = Object[GOPD];
	var create = Object.create;
	var keys = Object.keys;
	var freeze = Object.freeze || Object;
	var defineProperty = Object[DP];
	var $defineProperties = Object[DPies];
	var descriptor = gOPD(Object, GOPN);
	var addInternalIfNeeded = function (o, uid, enumerable) {
		if (!hOP.call(o, internalSymbol)) {
			try {
				defineProperty(o, internalSymbol, {
					enumerable: false,
					configurable: false,
					writable: false,
					value: {}
				});
			} catch (e) {
				o[internalSymbol] = {};
			}
		}
		o[internalSymbol]['@@' + uid] = enumerable;
	};
	var createWithSymbols = function (proto, descriptors) {
		var self = create(proto);
		gOPN(descriptors).forEach(function (key) {
			if (propertyIsEnumerable.call(descriptors, key)) {
				$defineProperty(self, key, descriptors[key]);
			}
		});
		return self;
	};
	var copyAsNonEnumerable = function (descriptor) {
		var newDescriptor = create(descriptor);
		newDescriptor.enumerable = false;
		return newDescriptor;
	};
	var get = function get(){};
	var onlyNonSymbols = function (name) {
		return name != internalSymbol &&
			!hOP.call(source, name);
	};
	var onlySymbols = function (name) {
		return name != internalSymbol &&
			hOP.call(source, name);
	};
	var propertyIsEnumerable = function propertyIsEnumerable(key) {
		var uid = '' + key;
		return onlySymbols(uid) ? (
			hOP.call(this, uid) &&
			this[internalSymbol]['@@' + uid]
		) : pIE.call(this, key);
	};
	var setAndGetSymbol = function (uid) {
		var descriptor = {
			enumerable: false,
			configurable: true,
			get: get,
			set: function (value) {
			setDescriptor(this, uid, {
				enumerable: false,
				configurable: true,
				writable: true,
				value: value
			});
			addInternalIfNeeded(this, uid, true);
			}
		};
		try {
			defineProperty(ObjectProto, uid, descriptor);
		} catch (e) {
			ObjectProto[uid] = descriptor.value;
		}
		return freeze(source[uid] = defineProperty(
			Object(uid),
			'constructor',
			sourceConstructor
		));
	};
	var Symbol = function Symbol() {
		var description = arguments[0];
		if (this instanceof Symbol) {
			throw new TypeError('Symbol is not a constructor');
		}
		return setAndGetSymbol(
			prefix.concat(description || '', random, ++id)
		);
		};
	var source = create(null);
	var sourceConstructor = {value: Symbol};
	var sourceMap = function (uid) {
		return source[uid];
		};
	var $defineProperty = function defineProp(o, key, descriptor) {
		var uid = '' + key;
		if (onlySymbols(uid)) {
			setDescriptor(o, uid, descriptor.enumerable ?
				copyAsNonEnumerable(descriptor) : descriptor);
			addInternalIfNeeded(o, uid, !!descriptor.enumerable);
		} else {
			defineProperty(o, key, descriptor);
		}
		return o;
	};

	var onlyInternalSymbols = function (obj) {
		return function (name) {
			return hOP.call(obj, internalSymbol) && hOP.call(obj[internalSymbol], '@@' + name);
		};
	};
	var $getOwnPropertySymbols = function getOwnPropertySymbols(o) {
		return gOPN(o).filter(o === ObjectProto ? onlyInternalSymbols(o) : onlySymbols).map(sourceMap);
		}
	;

	descriptor.value = $defineProperty;
	defineProperty(Object, DP, descriptor);

	descriptor.value = $getOwnPropertySymbols;
	defineProperty(Object, GOPS, descriptor);

	descriptor.value = function getOwnPropertyNames(o) {
		return gOPN(o).filter(onlyNonSymbols);
	};
	defineProperty(Object, GOPN, descriptor);

	descriptor.value = function defineProperties(o, descriptors) {
		var symbols = $getOwnPropertySymbols(descriptors);
		if (symbols.length) {
		keys(descriptors).concat(symbols).forEach(function (uid) {
			if (propertyIsEnumerable.call(descriptors, uid)) {
			$defineProperty(o, uid, descriptors[uid]);
			}
		});
		} else {
		$defineProperties(o, descriptors);
		}
		return o;
	};
	defineProperty(Object, DPies, descriptor);

	descriptor.value = propertyIsEnumerable;
	defineProperty(ObjectProto, PIE, descriptor);

	descriptor.value = Symbol;
	defineProperty(global, 'Symbol', descriptor);

	// defining `Symbol.for(key)`
	descriptor.value = function (key) {
		var uid = prefix.concat(prefix, key, random);
		return uid in ObjectProto ? source[uid] : setAndGetSymbol(uid);
	};
	defineProperty(Symbol, 'for', descriptor);

	// defining `Symbol.keyFor(symbol)`
	descriptor.value = function (symbol) {
		if (onlyNonSymbols(symbol))
		throw new TypeError(symbol + ' is not a symbol');
		return hOP.call(source, symbol) ?
		symbol.slice(prefixLength * 2, -random.length) :
		void 0
		;
	};
	defineProperty(Symbol, 'keyFor', descriptor);

	descriptor.value = function getOwnPropertyDescriptor(o, key) {
		var descriptor = gOPD(o, key);
		if (descriptor && onlySymbols(key)) {
		descriptor.enumerable = propertyIsEnumerable.call(o, key);
		}
		return descriptor;
	};
	defineProperty(Object, GOPD, descriptor);

	descriptor.value = function (proto, descriptors) {
		return arguments.length === 1 || typeof descriptors === "undefined" ?
		create(proto) :
		createWithSymbols(proto, descriptors);
	};
	defineProperty(Object, 'create', descriptor);

	var strictModeSupported = (function(){ 'use strict'; return this; }).call(null) === null;
	if (strictModeSupported) {
		descriptor.value = function () {
			var str = toString.call(this);
			return (str === '[object String]' && onlySymbols(this)) ? '[object Symbol]' : str;
		};
	} else {
		descriptor.value = function () {
			// https://github.com/Financial-Times/polyfill-library/issues/164#issuecomment-486965300
			// Polyfill.io this code is here for the situation where a browser does not
			// support strict mode and is executing `Object.prototype.toString.call(null)`.
			// This code ensures that we return the correct result in that situation however,
			// this code also introduces a bug where it will return the incorrect result for
			// `Object.prototype.toString.call(window)`. We can't have the correct result for
			// both `window` and `null`, so we have opted for `null` as we believe this is the more 
			// common situation. 
			if (this === window) {
				return '[object Null]';
			}

			var str = toString.call(this);
			return (str === '[object String]' && onlySymbols(this)) ? '[object Symbol]' : str;
		};
	}
	defineProperty(ObjectProto, 'toString', descriptor);

	setDescriptor = function (o, key, descriptor) {
		var protoDescriptor = gOPD(ObjectProto, key);
		delete ObjectProto[key];
		defineProperty(o, key, descriptor);
		if (o !== ObjectProto) {
			defineProperty(ObjectProto, key, protoDescriptor);
		}
	};

}(Object, 'getOwnPropertySymbols', this));

// Symbol.iterator
Object.defineProperty(self.Symbol, 'iterator', { value: self.Symbol('iterator') });

// _ESAbstract.GetIterator
/* global GetMethod, Symbol, Call, Type, GetV */
// 7.4.1. GetIterator ( obj [ , method ] )
// The abstract operation GetIterator with argument obj and optional argument method performs the following steps:
function GetIterator(obj /*, method */) { // eslint-disable-line no-unused-vars
	// 1. If method is not present, then
		// a. Set method to ? GetMethod(obj, @@iterator).
	var method = arguments.length > 1 ? arguments[1] : GetMethod(obj, Symbol.iterator);
	// 2. Let iterator be ? Call(method, obj).
	var iterator = Call(method, obj);
	// 3. If Type(iterator) is not Object, throw a TypeError exception.
	if (Type(iterator) !== 'object') {
		throw new TypeError('bad iterator');
	}
	// 4. Let nextMethod be ? GetV(iterator, "next").
	var nextMethod = GetV(iterator, "next");
	// 5. Let iteratorRecord be Record {[[Iterator]]: iterator, [[NextMethod]]: nextMethod, [[Done]]: false}.
	var iteratorRecord = Object.create(null);
	iteratorRecord['[[Iterator]]'] = iterator;
	iteratorRecord['[[NextMethod]]'] = nextMethod;
	iteratorRecord['[[Done]]'] = false;
	// 6. Return iteratorRecord.
	return iteratorRecord;
}

// Symbol.species
/* global Symbol */
Object.defineProperty(Symbol, 'species', { value: Symbol('species') });

// Set
/* global CreateIterResultObject, CreateMethodProperty, GetIterator, IsCallable, IteratorClose, IteratorStep, IteratorValue, OrdinaryCreateFromConstructor, SameValueZero, Symbol */
(function (global) {
	var supportsGetters = (function () {
		try {
			var a = {};
			Object.defineProperty(a, 't', {
				configurable: true,
				enumerable: false,
				get: function () {
					return true;
				},
				set: undefined
			});
			return !!a.t;
		} catch (e) {
			return false;
		}
	}());

	// Deleted set items mess with iterator pointers, so rather than removing them mark them as deleted. Can't use undefined or null since those both valid keys so use a private symbol.
	var undefMarker = Symbol('undef');
	// 23.2.1.1. Set ( [ iterable ] )
	var Set = function Set(/* iterable */) {
		// 1. If NewTarget is undefined, throw a TypeError exception.
		if (!(this instanceof Set)) {
			throw new TypeError('Constructor Set requires "new"');
		}
		// 2. Let set be ? OrdinaryCreateFromConstructor(NewTarget, "%SetPrototype%", « [[SetData]] »).
		var set = OrdinaryCreateFromConstructor(this, Set.prototype, {
			_values: [],
			_size: 0,
			_es6Set: true
		});

		// 3. Set set.[[SetData]] to a new empty List.
		// Polyfill.io - This step was done as part of step two.

		// Some old engines do not support ES5 getters/setters.  Since Set only requires these for the size property, we can fall back to setting the size property statically each time the size of the set changes.
		if (!supportsGetters) {
			Object.defineProperty(set, 'size', {
				configurable: true,
				enumerable: false,
				writable: true,
				value: 0
			});
		}

		// 4. If iterable is not present, let iterable be undefined.
		var iterable = arguments.length > 0 ? arguments[0] : undefined;

		// 5. If iterable is either undefined or null, return set.
		if (iterable === null || iterable === undefined) {
			return set;
		}

		// 6. Let adder be ? Get(set, "add").
		var adder = set.add;
		// 7. If IsCallable(adder) is false, throw a TypeError exception.
		if (!IsCallable(adder)) {
			throw new TypeError("Set.prototype.add is not a function");
		}

		try {
			// 8. Let iteratorRecord be ? GetIterator(iterable).
			var iteratorRecord = GetIterator(iterable);
			// 9. Repeat,
			// eslint-disable-next-line no-constant-condition
			while (true) {
				// a. Let next be ? IteratorStep(iteratorRecord).
				var next = IteratorStep(iteratorRecord);
				// b. If next is false, return set.
				if (next === false) {
					return set;
				}
				// c. Let nextValue be ? IteratorValue(next).
				var nextValue = IteratorValue(next);
				// d. Let status be Call(adder, set, « nextValue.[[Value]] »).
				try {
					adder.call(set, nextValue);
				} catch (e) {
					// e. If status is an abrupt completion, return ? IteratorClose(iteratorRecord, status).
					return IteratorClose(iteratorRecord, e);
				}
			}
		} catch (e) {
			// Polyfill.io - For user agents which do not have iteration methods on argument objects or arrays, we can special case those.
			if (Array.isArray(iterable) ||
				Object.prototype.toString.call(iterable) === '[object Arguments]' ||
				// IE 7 & IE 8 return '[object Object]' for the arguments object, we can detect by checking for the existence of the callee property
				(!!iterable.callee)) {
				var index;
				var length = iterable.length;
				for (index = 0; index < length; index++) {
					adder.call(set, iterable[index]);
				}
			} else {
				throw (e);
			}
		}
		return set;
	};

	// 23.2.2.1. Set.prototype
	// The initial value of Set.prototype is the intrinsic %SetPrototype% object.
	// This property has the attributes { [[Writable]]: false, [[Enumerable]]: false, [[Configurable]]: false }.
	Object.defineProperty(Set, 'prototype', {
		configurable: false,
		enumerable: false,
		writable: false,
		value: {}
	});

	// 23.2.2.2 get Set [ @@species ]
	if (supportsGetters) {
		Object.defineProperty(Set, Symbol.species, {
			configurable: true,
			enumerable: false,
			get: function () {
				// 1. Return the this value.
				return this;
			},
			set: undefined
		});
	} else {
		CreateMethodProperty(Set, Symbol.species, Set);
	}

	// 23.2.3.1. Set.prototype.add ( value )
	CreateMethodProperty(Set.prototype, 'add', function add(value) {
			// 1. Let S be the this value.
			var S = this;
			// 2. If Type(S) is not Object, throw a TypeError exception.
			if (typeof S !== 'object') {
				throw new TypeError('Method Set.prototype.add called on incompatible receiver ' + Object.prototype.toString.call(S));
			}
			// 3. If S does not have a [[SetData]] internal slot, throw a TypeError exception.
			if (S._es6Set !== true) {
				throw new TypeError('Method Set.prototype.add called on incompatible receiver ' + Object.prototype.toString.call(S));
			}
			// 4. Let entries be the List that is S.[[SetData]].
			var entries = S._values;
			// 5. For each e that is an element of entries, do
			for (var i = 0; i < entries.length; i++) {
				var e = entries[i];
				// a. If e is not empty and SameValueZero(e, value) is true, then
				if (e !== undefMarker && SameValueZero(e, value)) {
					// i. Return S.
					return S;
				}
			}
			// 6. If value is -0, let value be +0.
			if (value === 0 && 1/value === -Infinity) {
				value = 0;
			}
			// 7. Append value as the last element of entries.
			S._values.push(value);

			this._size = ++this._size;
			if (!supportsGetters) {
				this.size = this._size;
			}
			// 8. Return S.
			return S;
		});

	// 23.2.3.2. Set.prototype.clear ( )
	CreateMethodProperty(Set.prototype, 'clear', function clear() {
			// 1. Let S be the this value.
			var S = this;
			// 2. If Type(S) is not Object, throw a TypeError exception.
			if (typeof S !== 'object') {
				throw new TypeError('Method Set.prototype.clear called on incompatible receiver ' + Object.prototype.toString.call(S));
			}
			// 3. If S does not have a [[SetData]] internal slot, throw a TypeError exception.
			if (S._es6Set !== true) {
				throw new TypeError('Method Set.prototype.clear called on incompatible receiver ' + Object.prototype.toString.call(S));
			}
			// 4. Let entries be the List that is S.[[SetData]].
			var entries = S._values;
			// 5. For each e that is an element of entries, do
			for (var i = 0; i < entries.length; i++) {
				// a. Replace the element of entries whose value is e with an element whose value is empty.
				entries[i] = undefMarker;
			}
			this._size = 0;
			if (!supportsGetters) {
				this.size = this._size;
			}
			// 6. Return undefined.
			return undefined;
		});

	// 23.2.3.3. Set.prototype.constructor
	CreateMethodProperty(Set.prototype, 'constructor', Set);

	// 23.2.3.4. Set.prototype.delete ( value )
	CreateMethodProperty(Set.prototype, 'delete', function (value) {
			// 1. Let S be the this value.
			var S = this;
			// 2. If Type(S) is not Object, throw a TypeError exception.
			if (typeof S !== 'object') {
				throw new TypeError('Method Set.prototype.delete called on incompatible receiver ' + Object.prototype.toString.call(S));
			}
			// 3. If S does not have a [[SetData]] internal slot, throw a TypeError exception.
			if (S._es6Set !== true) {
				throw new TypeError('Method Set.prototype.delete called on incompatible receiver ' + Object.prototype.toString.call(S));
			}
			// 4. Let entries be the List that is S.[[SetData]].
			var entries = S._values;
			// 5. For each e that is an element of entries, do
			for (var i = 0; i < entries.length; i++) {
				var e = entries[i];
				// a. If e is not empty and SameValueZero(e, value) is true, then
				if (e !== undefMarker && SameValueZero(e, value)) {
					// i. Replace the element of entries whose value is e with an element whose value is empty.
					entries[i] = undefMarker;

					this._size = --this._size;
					if (!supportsGetters) {
						this.size = this._size;
					}
					// ii. Return true.
					return true;
				}
			}
			// 6. Return false.
			return false;
		}
	);

	// 23.2.3.5. Set.prototype.entries ( )
	CreateMethodProperty(Set.prototype, 'entries', function entries() {
			// 1. Let S be the this value.
			var S = this;
			// 2. Return ? CreateSetIterator(S, "key+value").
			return CreateSetIterator(S, 'key+value');
		}
	);

	// 23.2.3.6. Set.prototype.forEach ( callbackfn [ , thisArg ] )
	CreateMethodProperty(Set.prototype, 'forEach', function forEach(callbackFn /*[ , thisArg ]*/) {
			// 1. Let S be the this value.
			var S = this;
			// 2. If Type(S) is not Object, throw a TypeError exception.
			if (typeof S !== 'object') {
				throw new TypeError('Method Set.prototype.forEach called on incompatible receiver ' + Object.prototype.toString.call(S));
			}
			// 3. If S does not have a [[SetData]] internal slot, throw a TypeError exception.
			if (S._es6Set !== true) {
				throw new TypeError('Method Set.prototype.forEach called on incompatible receiver ' + Object.prototype.toString.call(S));
			}
			// 4. If IsCallable(callbackfn) is false, throw a TypeError exception.
			if (!IsCallable(callbackFn)) {
				throw new TypeError(Object.prototype.toString.call(callbackFn) + ' is not a function.');
			}
			// 5. If thisArg is present, let T be thisArg; else let T be undefined.
			if (arguments[1]) {
				var T = arguments[1];
			}
			// 6. Let entries be the List that is S.[[SetData]].
			var entries = S._values;
			// 7. For each e that is an element of entries, in original insertion order, do
			for (var i = 0; i < entries.length; i++) {
				var e = entries[i];
				// a. If e is not empty, then
				if (e !== undefMarker) {
					// i. Perform ? Call(callbackfn, T, « e, e, S »).
					callbackFn.call(T, e, e, S);
				}
			}
			// 8. Return undefined.
			return undefined;
		}
	);

	// 23.2.3.7. Set.prototype.has ( value )
	CreateMethodProperty(Set.prototype, 'has', function has(value) {
			// 1. Let S be the this value.
			var S = this;
			// 2. If Type(S) is not Object, throw a TypeError exception.
			if (typeof S !== 'object') {
				throw new TypeError('Method Set.prototype.forEach called on incompatible receiver ' + Object.prototype.toString.call(S));
			}
			// 3. If S does not have a [[SetData]] internal slot, throw a TypeError exception.
			if (S._es6Set !== true) {
				throw new TypeError('Method Set.prototype.forEach called on incompatible receiver ' + Object.prototype.toString.call(S));
			}
			// 4. Let entries be the List that is S.[[SetData]].
			var entries = S._values;
			// 5. For each e that is an element of entries, do
			for (var i = 0; i < entries.length; i++) {
				var e = entries[i];
				// a. If e is not empty and SameValueZero(e, value) is true, return true.
				if (e !== undefMarker && SameValueZero(e, value)) {
					return true;
				}
			}
			// 6. Return false.
			return false;
		}
	);

	// Polyfill.io - We need to define Set.prototype.values before Set.prototype.keys because keys is a reference to values.
	// 23.2.3.10. Set.prototype.values()
	var values = function values() {
		// 1. Let S be the this value.
		var S = this;
		// 2. Return ? CreateSetIterator(S, "value").
		return CreateSetIterator(S, "value");
	};
	CreateMethodProperty(Set.prototype, 'values', values);

	// 23.2.3.8 Set.prototype.keys ( )
	// The initial value of the keys property is the same function object as the initial value of the values property.
	CreateMethodProperty(Set.prototype, 'keys', values);

	// 23.2.3.9. get Set.prototype.size
	if (supportsGetters) {
		Object.defineProperty(Set.prototype, 'size', {
			configurable: true,
			enumerable: false,
			get: function () {
				// 1. Let S be the this value.
				var S = this;
				// 2. If Type(S) is not Object, throw a TypeError exception.
				if (typeof S !== 'object') {
					throw new TypeError('Method Set.prototype.size called on incompatible receiver ' + Object.prototype.toString.call(S));
				}
				// 3. If S does not have a [[SetData]] internal slot, throw a TypeError exception.
				if (S._es6Set !== true) {
					throw new TypeError('Method Set.prototype.size called on incompatible receiver ' + Object.prototype.toString.call(S));
				}
				// 4. Let entries be the List that is S.[[SetData]].
				var entries = S._values;
				// 5. Let count be 0.
				var count = 0;
				// 6. For each e that is an element of entries, do
				for (var i = 0; i < entries.length; i++) {
					var e = entries[i];
					// a. If e is not empty, set count to count+1.
					if (e !== undefMarker) {
						count = count + 1;
					}
				}
				// 7. Return count.
				return count;
			},
			set: undefined
		});
	}

	// 23.2.3.11. Set.prototype [ @@iterator ] ( )
	// The initial value of the @@iterator property is the same function object as the initial value of the values property.
	CreateMethodProperty(Set.prototype, Symbol.iterator, values);

	// 23.2.3.12. Set.prototype [ @@toStringTag ]
	// The initial value of the @@toStringTag property is the String value "Set".
	// This property has the attributes { [[Writable]]: false, [[Enumerable]]: false, [[Configurable]]: true }.

	// Polyfill.io - Safari 8 implements Set.name but as a non-configurable property, which means it would throw an error if we try and configure it here.
	if (!('name' in Set)) {
		// 19.2.4.2 name
		Object.defineProperty(Set, 'name', {
			configurable: true,
			enumerable: false,
			writable: false,
			value: 'Set'
		});
	}

	// 23.2.5.1. CreateSetIterator ( set, kind )
	function CreateSetIterator(set, kind) {
		// 1. If Type(set) is not Object, throw a TypeError exception.
		if (typeof set !== 'object') {
			throw new TypeError('createSetIterator called on incompatible receiver ' + Object.prototype.toString.call(set));
		}
		// 2. If set does not have a [[SetData]] internal slot, throw a TypeError exception.
		if (set._es6Set !== true) {
			throw new TypeError('createSetIterator called on incompatible receiver ' + Object.prototype.toString.call(set));
		}
		// 3. Let iterator be ObjectCreate(%SetIteratorPrototype%, « [[IteratedSet]], [[SetNextIndex]], [[SetIterationKind]] »).
		var iterator = Object.create(SetIteratorPrototype);
		// 4. Set iterator.[[IteratedSet]] to set.
		Object.defineProperty(iterator, '[[IteratedSet]]', {
			configurable: true,
			enumerable: false,
			writable: true,
			value: set
		});
		// 5. Set iterator.[[SetNextIndex]] to 0.
		Object.defineProperty(iterator, '[[SetNextIndex]]', {
			configurable: true,
			enumerable: false,
			writable: true,
			value: 0
		});
		// 6. Set iterator.[[SetIterationKind]] to kind.
		Object.defineProperty(iterator, '[[SetIterationKind]]', {
			configurable: true,
			enumerable: false,
			writable: true,
			value: kind
		});
		// 7. Return iterator.
		return iterator;
	}

	// 23.2.5.2. The %SetIteratorPrototype% Object
	var SetIteratorPrototype = {};
	//Polyfill.io - We add this property to help us identify what is a set iterator.
	Object.defineProperty(SetIteratorPrototype, 'isSetIterator', {
		configurable: false,
		enumerable: false,
		writable: false,
		value: true
	});

	// 23.2.5.2.1. %SetIteratorPrototype%.next ( )
	CreateMethodProperty(SetIteratorPrototype, 'next', function next() {
		// 1. Let O be the this value.
		var O = this;
		// 2. If Type(O) is not Object, throw a TypeError exception.
		if (typeof O !== 'object') {
			throw new TypeError('Method %SetIteratorPrototype%.next called on incompatible receiver ' + Object.prototype.toString.call(O));
		}
		// 3. If O does not have all of the internal slots of a Set Iterator Instance (23.2.5.3), throw a TypeError exception.
		if (!O.isSetIterator) {
			throw new TypeError('Method %SetIteratorPrototype%.next called on incompatible receiver ' + Object.prototype.toString.call(O));
		}
		// 4. Let s be O.[[IteratedSet]].
		var s = O['[[IteratedSet]]'];
		// 5. Let index be O.[[SetNextIndex]].
		var index = O['[[SetNextIndex]]'];
		// 6. Let itemKind be O.[[SetIterationKind]].
		var itemKind = O['[[SetIterationKind]]'];
		// 7. If s is undefined, return CreateIterResultObject(undefined, true).
		if (s === undefined) {
			return CreateIterResultObject(undefined, true);
		}
		// 8. Assert: s has a [[SetData]] internal slot.
		if (!s._es6Set) {
			throw new Error(Object.prototype.toString.call(s) + ' does not have [[SetData]] internal slot.');
		}
		// 9. Let entries be the List that is s.[[SetData]].
		var entries = s._values;
		// 10. Let numEntries be the number of elements of entries.
		var numEntries = entries.length;
		// 11. NOTE: numEntries must be redetermined each time this method is evaluated.
		// 12. Repeat, while index is less than numEntries,
		while (index < numEntries) {
			// a. Let e be entries[index].
			var e = entries[index];
			// b. Set index to index+1.
			index = index + 1;
			// c. Set O.[[SetNextIndex]] to index.
			O['[[SetNextIndex]]'] = index;
			// d. If e is not empty, then
			if (e !== undefMarker) {
				// i. If itemKind is "key+value", then
				if (itemKind === 'key+value') {
					// 1. Return CreateIterResultObject(CreateArrayFromList(« e, e »), false).
					return CreateIterResultObject([e, e], false);
				}
				// ii. Return CreateIterResultObject(e, false).
				return CreateIterResultObject(e, false);
			}
		}
		// 13. Set O.[[IteratedSet]] to undefined.
		O['[[IteratedSet]]'] = undefined;
		// 14. Return CreateIterResultObject(undefined, true).
		return CreateIterResultObject(undefined, true);
	});

	// 23.2.5.2.2. %SetIteratorPrototype% [ @@toStringTag ]
	// The initial value of the @@toStringTag property is the String value "Set Iterator".
	// This property has the attributes { [[Writable]]: false, [[Enumerable]]: false, [[Configurable]]: true }.

	CreateMethodProperty(SetIteratorPrototype, Symbol.iterator, function iterator() {
			return this;
		}
	);

	// Export the object
	try {
		CreateMethodProperty(global, 'Set', Set);
	} catch (e) {
		// IE8 throws an error here if we set enumerable to false.
		// More info on table 2: https://msdn.microsoft.com/en-us/library/dd229916(v=vs.85).aspx
		global.Set = Set;
	}

}(self));

// WeakMap
/* globals Symbol, OrdinaryCreateFromConstructor, IsCallable, GetIterator, IteratorStep, IteratorValue, IteratorClose, Get, Call, CreateMethodProperty, Type, SameValue */
(function (global) {
	// Deleted map items mess with iterator pointers, so rather than removing them mark them as deleted. Can't use undefined or null since those both valid keys so use a private symbol.
	var undefMarker = Symbol('undef');
	// 23.3.1.1 WeakMap ( [ iterable ] )
	var WeakMap = function WeakMap(/* iterable */) {
		// 1. If NewTarget is undefined, throw a TypeError exception.
		if (!(this instanceof WeakMap)) {
			throw new TypeError('Constructor WeakMap requires "new"');
		}
		// 2. Let map be ? OrdinaryCreateFromConstructor(NewTarget, "%WeakMapPrototype%", « [[WeakMapData]] »).
		var map = OrdinaryCreateFromConstructor(this, WeakMap.prototype, {
			_keys: [],
			_values: [],
			_es6WeakMap: true
		});

		// 3. Set map.[[WeakMapData]] to a new empty List.
		// Polyfill.io - This step was done as part of step two.

		// 4. If iterable is not present, let iterable be undefined.
		var iterable = arguments.length > 0 ? arguments[0] : undefined;

		// 5. If iterable is either undefined or null, return map.
		if (iterable === null || iterable === undefined) {
			return map;
		}

		// 6. Let adder be ? Get(map, "set").
		var adder = Get(map, "set");

		// 7. If IsCallable(adder) is false, throw a TypeError exception.
		if (!IsCallable(adder)) {
			throw new TypeError("WeakMap.prototype.set is not a function");
		}

		// 8. Let iteratorRecord be ? GetIterator(iterable).
		try {
			var iteratorRecord = GetIterator(iterable);
			// 9. Repeat,
			// eslint-disable-next-line no-constant-condition
			while (true) {
				// a. Let next be ? IteratorStep(iteratorRecord).
				var next = IteratorStep(iteratorRecord);
				// b. If next is false, return map.
				if (next === false) {
					return map;
				}
				// c. Let nextItem be ? IteratorValue(next).
				var nextItem = IteratorValue(next);
				// d. If Type(nextItem) is not Object, then
				if (Type(nextItem) !== 'object') {
					// i. Let error be Completion{[[Type]]: throw, [[Value]]: a newly created TypeError object, [[Target]]: empty}.
					try {
						throw new TypeError('Iterator value ' + nextItem + ' is not an entry object');
					} catch (error) {
						// ii. Return ? IteratorClose(iteratorRecord, error).
						return IteratorClose(iteratorRecord, error);
					}
				}
				try {
					// Polyfill.io - The try catch accounts for steps: f, h, and j.

					// e. Let k be Get(nextItem, "0").
					var k = Get(nextItem, "0");
					// f. If k is an abrupt completion, return ? IteratorClose(iteratorRecord, k).
					// g. Let v be Get(nextItem, "1").
					var v = Get(nextItem, "1");
					// h. If v is an abrupt completion, return ? IteratorClose(iteratorRecord, v).
					// i. Let status be Call(adder, map, « k.[[Value]], v.[[Value]] »).
					Call(adder, map, [k, v]);
				} catch (e) {
					// j. If status is an abrupt completion, return ? IteratorClose(iteratorRecord, status).
					return IteratorClose(iteratorRecord, e);
				}
			}
		} catch (e) {
			// Polyfill.io - For user agents which do not have iteration methods on argument objects or arrays, we can special case those.
			if (Array.isArray(iterable) ||
				Object.prototype.toString.call(iterable) === '[object Arguments]' ||
				// IE 7 & IE 8 return '[object Object]' for the arguments object, we can detect by checking for the existence of the callee property
				(!!iterable.callee)) {
				var index;
				var length = iterable.length;
				for (index = 0; index < length; index++) {
					k = iterable[index][0];
					v = iterable[index][1];
					Call(adder, map, [k, v]);
				}
			}
		}
		return map;
	};

	// 23.3.2.1 WeakMap.prototype
	// The initial value of WeakMap.prototype is the intrinsic object %WeakMapPrototype%.
	// This property has the attributes { [[Writable]]: false, [[Enumerable]]: false, [[Configurable]]: false }.
	Object.defineProperty(WeakMap, 'prototype', {
		configurable: false,
		enumerable: false,
		writable: false,
		value: {}
	});

	// 23.3.3.1 WeakMap.prototype.constructor
	CreateMethodProperty(WeakMap.prototype, 'constructor', WeakMap);

	// 23.3.3.2 WeakMap.prototype.delete ( key )
	CreateMethodProperty(WeakMap.prototype, 'delete', function (key) {
		// 1. Let M be the this value.
		var M = this;
		// 2. If Type(M) is not Object, throw a TypeError exception.
		if (Type(M) !== 'object') {
			throw new TypeError('Method WeakMap.prototype.clear called on incompatible receiver ' + Object.prototype.toString.call(M));
		}
		// 3. If M does not have a [[WeakMapData]] internal slot, throw a TypeError exception.
		if (M._es6WeakMap !== true) {
			throw new TypeError('Method WeakMap.prototype.clear called on incompatible receiver ' + Object.prototype.toString.call(M));
		}
		// 4. Let entries be the List that is M.[[WeakMapData]].
		var entries = M._keys;
		// 5. If Type(key) is not Object, return false.
		if (Type(key) !== 'object') {
			return false;
		}
		// 6. For each Record {[[Key]], [[Value]]} p that is an element of entries, do
		for (var i = 0; i < entries.length; i++) {
			// a. If p.[[Key]] is not empty and SameValue(p.[[Key]], key) is true, then
			if (M._keys[i] !== undefMarker && SameValue(M._keys[i], key)) {
				// i. Set p.[[Key]] to empty.
				this._keys[i] = undefMarker;
				// ii. Set p.[[Value]] to empty.
				this._values[i] = undefMarker;
				this._size = --this._size;
				// iii. Return true.
				return true;
			}
		}
		// 7. Return false.
		return false;
	});

	// 23.3.3.3 WeakMap.prototype.get ( key )
	CreateMethodProperty(WeakMap.prototype, 'get', function get(key) {
		// 1. Let M be the this value.
		var M = this;
		// 2. If Type(M) is not Object, throw a TypeError exception.
		if (Type(M) !== 'object') {
			throw new TypeError('Method WeakMap.prototype.get called on incompatible receiver ' + Object.prototype.toString.call(M));
		}
		// 3. If M does not have a [[WeakMapData]] internal slot, throw a TypeError exception.
		if (M._es6WeakMap !== true) {
			throw new TypeError('Method WeakMap.prototype.get called on incompatible receiver ' + Object.prototype.toString.call(M));
		}
		// 4. Let entries be the List that is M.[[WeakMapData]].
		var entries = M._keys;
		// 5. If Type(key) is not Object, return undefined.
		if (Type(key) !== 'object') {
			return undefined;
		}
		// 6. For each Record {[[Key]], [[Value]]} p that is an element of entries, do
		for (var i = 0; i < entries.length; i++) {
			// a. If p.[[Key]] is not empty and SameValue(p.[[Key]], key) is true, return p.[[Value]].
			if (M._keys[i] !== undefMarker && SameValue(M._keys[i], key)) {
				return M._values[i];
			}
		}
		// 7. Return undefined.
		return undefined;
	});

	// 23.3.3.4 WeakMap.prototype.has ( key )
	CreateMethodProperty(WeakMap.prototype, 'has', function has(key) {
		// 1. Let M be the this value.
		var M = this;
		// 2. If Type(M) is not Object, throw a TypeError exception.
		if (typeof M !== 'object') {
			throw new TypeError('Method WeakMap.prototype.has called on incompatible receiver ' + Object.prototype.toString.call(M));
		}
		// 3. If M does not have a [[WeakMapData]] internal slot, throw a TypeError exception.
		if (M._es6WeakMap !== true) {
			throw new TypeError('Method WeakMap.prototype.has called on incompatible receiver ' + Object.prototype.toString.call(M));
		}
		// 4. Let entries be the List that is M.[[WeakMapData]].
		var entries = M._keys;
		// 5. If Type(key) is not Object, return false.
		if (Type(key) !== 'object') {
			return false;
		}
		// 6. For each Record {[[Key]], [[Value]]} p that is an element of entries, do
		for (var i = 0; i < entries.length; i++) {
			// a. If p.[[Key]] is not empty and SameValue(p.[[Key]], key) is true, return true.
			if (M._keys[i] !== undefMarker && SameValue(M._keys[i], key)) {
				return true;
			}
		}
		// 7. Return false.
		return false;
	});

	// 23.3.3.5 WeakMap.prototype.set ( key, value )
	CreateMethodProperty(WeakMap.prototype, 'set', function set(key, value) {
		// 1. Let M be the this value.
		var M = this;
		// 2. If Type(M) is not Object, throw a TypeError exception.
		if (Type(M) !== 'object') {
			throw new TypeError('Method WeakMap.prototype.set called on incompatible receiver ' + Object.prototype.toString.call(M));
		}
		// 3. If M does not have a [[WeakMapData]] internal slot, throw a TypeError exception.
		if (M._es6WeakMap !== true) {
			throw new TypeError('Method WeakMap.prototype.set called on incompatible receiver ' + Object.prototype.toString.call(M));
		}
		// 4. Let entries be the List that is M.[[WeakMapData]].
		var entries = M._keys;
		// 5. If Type(key) is not Object, throw a TypeError exception.
		if (Type(key) !== 'object') {
			throw new TypeError("Invalid value used as weak map key");
		}
		// 6. For each Record {[[Key]], [[Value]]} p that is an element of entries, do
		for (var i = 0; i < entries.length; i++) {
			// a. If p.[[Key]] is not empty and SameValue(p.[[Key]], key) is true, then
			if (M._keys[i] !== undefMarker && SameValue(M._keys[i], key)) {
				// i. Set p.[[Value]] to value.
				M._values[i] = value;
				// ii. Return M.
				return M;
			}
		}
		// 7. Let p be the Record {[[Key]]: key, [[Value]]: value}.
		var p = {
			'[[Key]]': key,
			'[[Value]]': value
		};
		// 8. Append p as the last element of entries.
		M._keys.push(p['[[Key]]']);
		M._values.push(p['[[Value]]']);
		// 9. Return M.
		return M;
	});

	// 23.3.3.6 WeakMap.prototype [ @@toStringTag ]
	// The initial value of the @@toStringTag property is the String value "WeakMap".
	// This property has the attributes { [[Writable]]: false, [[Enumerable]]: false, [[Configurable]]: true }.

	// Polyfill.io - Safari 8 implements WeakMap.name but as a non-writable property, which means it would throw an error if we try and write to it here.
	if (!('name' in WeakMap)) {
		// 19.2.4.2 name
		Object.defineProperty(WeakMap, 'name', {
			configurable: true,
			enumerable: false,
			writable: false,
			value: 'WeakMap'
		});
	}

	// Export the object
	try {
		CreateMethodProperty(global, 'WeakMap', WeakMap);
	} catch (e) {
		// IE8 throws an error here if we set enumerable to false.
		// More info on table 2: https://msdn.microsoft.com/en-us/library/dd229916(v=vs.85).aspx
		global.WeakMap = WeakMap;
	}
}(self));

// Intl.PluralRules
(function (factory) {
    typeof define === 'function' && define.amd ? define(factory) :
    factory();
}((function () { 'use strict';

    function invariant(condition, message, Err) {
        if (Err === void 0) { Err = Error; }
        if (!condition) {
            throw new Err(message);
        }
    }

    // https://tc39.es/ecma402/#sec-issanctionedsimpleunitidentifier
    var SANCTIONED_UNITS = [
        'angle-degree',
        'area-acre',
        'area-hectare',
        'concentr-percent',
        'digital-bit',
        'digital-byte',
        'digital-gigabit',
        'digital-gigabyte',
        'digital-kilobit',
        'digital-kilobyte',
        'digital-megabit',
        'digital-megabyte',
        'digital-petabyte',
        'digital-terabit',
        'digital-terabyte',
        'duration-day',
        'duration-hour',
        'duration-millisecond',
        'duration-minute',
        'duration-month',
        'duration-second',
        'duration-week',
        'duration-year',
        'length-centimeter',
        'length-foot',
        'length-inch',
        'length-kilometer',
        'length-meter',
        'length-mile-scandinavian',
        'length-mile',
        'length-millimeter',
        'length-yard',
        'mass-gram',
        'mass-kilogram',
        'mass-ounce',
        'mass-pound',
        'mass-stone',
        'temperature-celsius',
        'temperature-fahrenheit',
        'volume-fluid-ounce',
        'volume-gallon',
        'volume-liter',
        'volume-milliliter',
    ];

    /**
     * https://tc39.es/ecma262/#sec-toobject
     * @param arg
     */
    function toObject(arg) {
        if (arg == null) {
            throw new TypeError('undefined/null cannot be converted to object');
        }
        return Object(arg);
    }
    /**
     * https://tc39.es/ecma262/#sec-tostring
     */
    function toString(o) {
        // Only symbol is irregular...
        if (typeof o === 'symbol') {
            throw TypeError('Cannot convert a Symbol value to a string');
        }
        return String(o);
    }
    /**
     * https://tc39.es/ecma402/#sec-getoption
     * @param opts
     * @param prop
     * @param type
     * @param values
     * @param fallback
     */
    function getOption(opts, prop, type, values, fallback) {
        // const descriptor = Object.getOwnPropertyDescriptor(opts, prop);
        var value = opts[prop];
        if (value !== undefined) {
            if (type !== 'boolean' && type !== 'string') {
                throw new TypeError('invalid type');
            }
            if (type === 'boolean') {
                value = Boolean(value);
            }
            if (type === 'string') {
                value = toString(value);
            }
            if (values !== undefined && !values.filter(function (val) { return val == value; }).length) {
                throw new RangeError(value + " is not within " + values.join(', '));
            }
            return value;
        }
        return fallback;
    }
    /**
     * https://tc39.es/ecma402/#sec-defaultnumberoption
     * @param val
     * @param min
     * @param max
     * @param fallback
     */
    function defaultNumberOption(val, min, max, fallback) {
        if (val !== undefined) {
            val = Number(val);
            if (isNaN(val) || val < min || val > max) {
                throw new RangeError(val + " is outside of range [" + min + ", " + max + "]");
            }
            return Math.floor(val);
        }
        return fallback;
    }
    /**
     * https://tc39.es/ecma402/#sec-getnumberoption
     * @param options
     * @param property
     * @param min
     * @param max
     * @param fallback
     */
    function getNumberOption(options, property, minimum, maximum, fallback) {
        var val = options[property];
        return defaultNumberOption(val, minimum, maximum, fallback);
    }
    function setInternalSlot(map, pl, field, value) {
        if (!map.get(pl)) {
            map.set(pl, Object.create(null));
        }
        var slots = map.get(pl);
        slots[field] = value;
    }
    function getInternalSlot(map, pl, field) {
        return getMultiInternalSlots(map, pl, field)[field];
    }
    function getMultiInternalSlots(map, pl) {
        var fields = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            fields[_i - 2] = arguments[_i];
        }
        var slots = map.get(pl);
        if (!slots) {
            throw new TypeError(pl + " InternalSlot has not been initialized");
        }
        return fields.reduce(function (all, f) {
            all[f] = slots[f];
            return all;
        }, Object.create(null));
    }
    /**
     * https://tc39.es/ecma402/#sec-setnfdigitoptions
     */
    function setNumberFormatDigitOptions(internalSlots, opts, mnfdDefault, mxfdDefault, notation) {
        var mnid = getNumberOption(opts, 'minimumIntegerDigits', 1, 21, 1);
        var mnfd = opts.minimumFractionDigits;
        var mxfd = opts.maximumFractionDigits;
        var mnsd = opts.minimumSignificantDigits;
        var mxsd = opts.maximumSignificantDigits;
        internalSlots.minimumIntegerDigits = mnid;
        if (mnsd !== undefined || mxsd !== undefined) {
            internalSlots.roundingType = 'significantDigits';
            mnsd = defaultNumberOption(mnsd, 1, 21, 1);
            mxsd = defaultNumberOption(mxsd, mnsd, 21, 21);
            internalSlots.minimumSignificantDigits = mnsd;
            internalSlots.maximumSignificantDigits = mxsd;
        }
        else if (mnfd !== undefined || mxfd !== undefined) {
            internalSlots.roundingType = 'fractionDigits';
            mnfd = defaultNumberOption(mnfd, 0, 20, mnfdDefault);
            var mxfdActualDefault = Math.max(mnfd, mxfdDefault);
            mxfd = defaultNumberOption(mxfd, mnfd, 20, mxfdActualDefault);
            internalSlots.minimumFractionDigits = mnfd;
            internalSlots.maximumFractionDigits = mxfd;
        }
        else if (notation === 'compact') {
            internalSlots.roundingType = 'compactRounding';
        }
        else {
            internalSlots.roundingType = 'fractionDigits';
            internalSlots.minimumFractionDigits = mnfdDefault;
            internalSlots.maximumFractionDigits = mxfdDefault;
        }
    }
    var SHORTENED_SACTION_UNITS = SANCTIONED_UNITS.map(function (unit) {
        return unit.replace(/^(.*?)-/, '');
    });

    var __extends = (undefined && undefined.__extends) || (function () {
        var extendStatics = function (d, b) {
            extendStatics = Object.setPrototypeOf ||
                ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
                function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
            return extendStatics(d, b);
        };
        return function (d, b) {
            extendStatics(d, b);
            function __() { this.constructor = d; }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
    var __assign = (undefined && undefined.__assign) || function () {
        __assign = Object.assign || function(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                    t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    function createResolveLocale(getDefaultLocale) {
        var lookupMatcher = createLookupMatcher(getDefaultLocale);
        var bestFitMatcher = createBestFitMatcher(getDefaultLocale);
        /**
         * https://tc39.es/ecma402/#sec-resolvelocale
         */
        return function resolveLocale(availableLocales, requestedLocales, options, relevantExtensionKeys, localeData) {
            var matcher = options.localeMatcher;
            var r;
            if (matcher === 'lookup') {
                r = lookupMatcher(availableLocales, requestedLocales);
            }
            else {
                r = bestFitMatcher(availableLocales, requestedLocales);
            }
            var foundLocale = r.locale;
            var result = { locale: '', dataLocale: foundLocale };
            var supportedExtension = '-u';
            for (var _i = 0, relevantExtensionKeys_1 = relevantExtensionKeys; _i < relevantExtensionKeys_1.length; _i++) {
                var key = relevantExtensionKeys_1[_i];
                var foundLocaleData = localeData[foundLocale];
                invariant(typeof foundLocaleData === 'object' && foundLocaleData !== null, "locale data " + key + " must be an object");
                var keyLocaleData = foundLocaleData[key];
                invariant(Array.isArray(keyLocaleData), "keyLocaleData for " + key + " must be an array");
                var value = keyLocaleData[0];
                invariant(typeof value === 'string' || value === null, "value must be string or null but got " + typeof value + " in key " + key);
                var supportedExtensionAddition = '';
                if (r.extension) {
                    var requestedValue = unicodeExtensionValue(r.extension, key);
                    if (requestedValue !== undefined) {
                        if (requestedValue !== '') {
                            if (~keyLocaleData.indexOf(requestedValue)) {
                                value = requestedValue;
                                supportedExtensionAddition = "-" + key + "-" + value;
                            }
                        }
                        else if (~requestedValue.indexOf('true')) {
                            value = 'true';
                            supportedExtensionAddition = "-" + key;
                        }
                    }
                }
                if (key in options) {
                    var optionsValue = options[key];
                    invariant(typeof optionsValue === 'string' ||
                        typeof optionsValue === 'undefined' ||
                        optionsValue === null, 'optionsValue must be String, Undefined or Null');
                    if (~keyLocaleData.indexOf(optionsValue)) {
                        if (optionsValue !== value) {
                            value = optionsValue;
                            supportedExtensionAddition = '';
                        }
                    }
                }
                result[key] = value;
                supportedExtension += supportedExtensionAddition;
            }
            if (supportedExtension.length > 2) {
                var privateIndex = foundLocale.indexOf('-x-');
                if (privateIndex === -1) {
                    foundLocale = foundLocale + supportedExtension;
                }
                else {
                    var preExtension = foundLocale.slice(0, privateIndex);
                    var postExtension = foundLocale.slice(privateIndex, foundLocale.length);
                    foundLocale = preExtension + supportedExtension + postExtension;
                }
                foundLocale = Intl.getCanonicalLocales(foundLocale)[0];
            }
            result.locale = foundLocale;
            return result;
        };
    }
    /**
     * https://tc39.es/ecma402/#sec-unicodeextensionvalue
     * @param extension
     * @param key
     */
    function unicodeExtensionValue(extension, key) {
        invariant(key.length === 2, 'key must have 2 elements');
        var size = extension.length;
        var searchValue = "-" + key + "-";
        var pos = extension.indexOf(searchValue);
        if (pos !== -1) {
            var start = pos + 4;
            var end = start;
            var k = start;
            var done = false;
            while (!done) {
                var e = extension.indexOf('-', k);
                var len = void 0;
                if (e === -1) {
                    len = size - k;
                }
                else {
                    len = e - k;
                }
                if (len === 2) {
                    done = true;
                }
                else if (e === -1) {
                    end = size;
                    done = true;
                }
                else {
                    end = e;
                    k = e + 1;
                }
            }
            return extension.slice(start, end);
        }
        searchValue = "-" + key;
        pos = extension.indexOf(searchValue);
        if (pos !== -1 && pos + 3 === size) {
            return '';
        }
        return undefined;
    }
    var UNICODE_EXTENSION_SEQUENCE_REGEX = /-u(?:-[0-9a-z]{2,8})+/gi;
    /**
     * https://tc39.es/ecma402/#sec-bestavailablelocale
     * @param availableLocales
     * @param locale
     */
    function bestAvailableLocale(availableLocales, locale) {
        var candidate = locale;
        while (true) {
            if (~availableLocales.indexOf(candidate)) {
                return candidate;
            }
            var pos = candidate.lastIndexOf('-');
            if (!~pos) {
                return undefined;
            }
            if (pos >= 2 && candidate[pos - 2] === '-') {
                pos -= 2;
            }
            candidate = candidate.slice(0, pos);
        }
    }
    function createLookupMatcher(getDefaultLocale) {
        /**
         * https://tc39.es/ecma402/#sec-lookupmatcher
         */
        return function lookupMatcher(availableLocales, requestedLocales) {
            var result = { locale: '' };
            for (var _i = 0, requestedLocales_1 = requestedLocales; _i < requestedLocales_1.length; _i++) {
                var locale = requestedLocales_1[_i];
                var noExtensionLocale = locale.replace(UNICODE_EXTENSION_SEQUENCE_REGEX, '');
                var availableLocale = bestAvailableLocale(availableLocales, noExtensionLocale);
                if (availableLocale) {
                    result.locale = availableLocale;
                    if (locale !== noExtensionLocale) {
                        result.extension = locale.slice(noExtensionLocale.length + 1, locale.length);
                    }
                    return result;
                }
            }
            result.locale = getDefaultLocale();
            return result;
        };
    }
    function createBestFitMatcher(getDefaultLocale) {
        return function bestFitMatcher(availableLocales, requestedLocales) {
            var result = { locale: '' };
            for (var _i = 0, requestedLocales_2 = requestedLocales; _i < requestedLocales_2.length; _i++) {
                var locale = requestedLocales_2[_i];
                var noExtensionLocale = locale.replace(UNICODE_EXTENSION_SEQUENCE_REGEX, '');
                var availableLocale = bestAvailableLocale(availableLocales, noExtensionLocale);
                if (availableLocale) {
                    result.locale = availableLocale;
                    if (locale !== noExtensionLocale) {
                        result.extension = locale.slice(noExtensionLocale.length + 1, locale.length);
                    }
                    return result;
                }
            }
            result.locale = getDefaultLocale();
            return result;
        };
    }
    function getLocaleHierarchy(locale) {
        var results = [locale];
        var localeParts = locale.split('-');
        for (var i = localeParts.length; i > 1; i--) {
            results.push(localeParts.slice(0, i - 1).join('-'));
        }
        return results;
    }
    function lookupSupportedLocales(availableLocales, requestedLocales) {
        var subset = [];
        for (var _i = 0, requestedLocales_3 = requestedLocales; _i < requestedLocales_3.length; _i++) {
            var locale = requestedLocales_3[_i];
            var noExtensionLocale = locale.replace(UNICODE_EXTENSION_SEQUENCE_REGEX, '');
            var availableLocale = bestAvailableLocale(availableLocales, noExtensionLocale);
            if (availableLocale) {
                subset.push(availableLocale);
            }
        }
        return subset;
    }
    function supportedLocales(availableLocales, requestedLocales, options) {
        var matcher = 'best fit';
        if (options !== undefined) {
            options = toObject(options);
            matcher = getOption(options, 'localeMatcher', 'string', ['lookup', 'best fit'], 'best fit');
        }
        if (matcher === 'best fit') {
            return lookupSupportedLocales(availableLocales, requestedLocales);
        }
        return lookupSupportedLocales(availableLocales, requestedLocales);
    }
    var MissingLocaleDataError = /** @class */ (function (_super) {
        __extends(MissingLocaleDataError, _super);
        function MissingLocaleDataError() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.type = 'MISSING_LOCALE_DATA';
            return _this;
        }
        return MissingLocaleDataError;
    }(Error));
    function isMissingLocaleDataError(e) {
        return e.type === 'MISSING_LOCALE_DATA';
    }
    function unpackData(locale, localeData, 
    /** By default shallow merge the dictionaries. */
    reducer) {
        if (reducer === void 0) { reducer = function (all, d) { return (__assign(__assign({}, all), d)); }; }
        var localeHierarchy = getLocaleHierarchy(locale);
        var dataToMerge = localeHierarchy
            .map(function (l) { return localeData.data[l]; })
            .filter(Boolean);
        if (!dataToMerge.length) {
            throw new MissingLocaleDataError("Missing locale data for \"" + locale + "\", lookup hierarchy: " + localeHierarchy.join(', '));
        }
        dataToMerge.reverse();
        return dataToMerge.reduce(reducer, {});
    }

    var __spreadArrays = (undefined && undefined.__spreadArrays) || function () {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };
    function validateInstance(instance, method) {
        if (!(instance instanceof PluralRules)) {
            throw new TypeError("Method Intl.PluralRules.prototype." + method + " called on incompatible receiver " + String(instance));
        }
    }
    /**
     * https://tc39.es/ecma402/#sec-torawprecision
     * @param x
     * @param minPrecision
     * @param maxPrecision
     */
    function toRawPrecision(x, minPrecision, maxPrecision) {
        var m = x.toPrecision(maxPrecision);
        if (~m.indexOf('.') && maxPrecision > minPrecision) {
            var cut = maxPrecision - minPrecision;
            while (cut > 0 && m[m.length - 1] === '0') {
                m = m.slice(0, m.length - 1);
                cut--;
            }
            if (m[m.length - 1] === '.') {
                return m.slice(0, m.length - 1);
            }
        }
        return m;
    }
    /**
     * https://tc39.es/ecma402/#sec-torawfixed
     * @param x
     * @param minInteger
     * @param minFraction
     * @param maxFraction
     */
    function toRawFixed(x, minInteger, minFraction, maxFraction) {
        var cut = maxFraction - minFraction;
        var m = x.toFixed(maxFraction);
        while (cut > 0 && m[m.length - 1] === '0') {
            m = m.slice(0, m.length - 1);
            cut--;
        }
        if (m[m.length - 1] === '.') {
            m = m.slice(0, m.length - 1);
        }
        var int = m.split('.')[0].length;
        if (int < minInteger) {
            var z = '';
            for (; z.length < minInteger - int; z += '0')
                ;
            m = z + m;
        }
        return m;
    }
    function formatNumericToString(internalSlotMap, pl, x) {
        var minimumSignificantDigits = getInternalSlot(internalSlotMap, pl, 'minimumSignificantDigits');
        var maximumSignificantDigits = getInternalSlot(internalSlotMap, pl, 'maximumSignificantDigits');
        if (minimumSignificantDigits !== undefined &&
            maximumSignificantDigits !== undefined) {
            return toRawPrecision(x, minimumSignificantDigits, maximumSignificantDigits);
        }
        return toRawFixed(x, getInternalSlot(internalSlotMap, pl, 'minimumIntegerDigits'), getInternalSlot(internalSlotMap, pl, 'minimumFractionDigits'), getInternalSlot(internalSlotMap, pl, 'maximumFractionDigits'));
    }
    var PluralRules = /** @class */ (function () {
        function PluralRules(locales, options) {
            // test262/test/intl402/RelativeTimeFormat/constructor/constructor/newtarget-undefined.js
            // Cannot use `new.target` bc of IE11 & TS transpiles it to something else
            var newTarget = this && this instanceof PluralRules ? this.constructor : void 0;
            if (!newTarget) {
                throw new TypeError("Intl.PluralRules must be called with 'new'");
            }
            var requestedLocales = Intl
                .getCanonicalLocales(locales);
            var opt = Object.create(null);
            var opts = options === undefined ? Object.create(null) : toObject(options);
            setInternalSlot(PluralRules.__INTERNAL_SLOT_MAP__, this, 'initializedPluralRules', true);
            var matcher = getOption(opts, 'localeMatcher', 'string', ['best fit', 'lookup'], 'best fit');
            opt.localeMatcher = matcher;
            setInternalSlot(PluralRules.__INTERNAL_SLOT_MAP__, this, 'type', getOption(opts, 'type', 'string', ['cardinal', 'ordinal'], 'cardinal'));
            setNumberFormatDigitOptions(PluralRules.__INTERNAL_SLOT_MAP__.get(this), opts, 0, 3, 'standard');
            var r = createResolveLocale(PluralRules.getDefaultLocale)(PluralRules.availableLocales, requestedLocales, opt, PluralRules.relevantExtensionKeys, PluralRules.localeData);
            setInternalSlot(PluralRules.__INTERNAL_SLOT_MAP__, this, 'locale', r.locale);
        }
        PluralRules.prototype.resolvedOptions = function () {
            var _this = this;
            validateInstance(this, 'resolvedOptions');
            var opts = Object.create(null);
            opts.locale = getInternalSlot(PluralRules.__INTERNAL_SLOT_MAP__, this, 'locale');
            opts.type = getInternalSlot(PluralRules.__INTERNAL_SLOT_MAP__, this, 'type');
            [
                'minimumIntegerDigits',
                'minimumFractionDigits',
                'maximumFractionDigits',
                'minimumSignificantDigits',
                'maximumSignificantDigits',
            ].forEach(function (field) {
                var val = getInternalSlot(PluralRules.__INTERNAL_SLOT_MAP__, _this, field);
                if (val !== undefined) {
                    opts[field] = val;
                }
            });
            opts.pluralCategories = __spreadArrays(PluralRules.localeData[opts.locale].categories[opts.type]);
            return opts;
        };
        PluralRules.prototype.select = function (val) {
            validateInstance(this, 'select');
            var locale = getInternalSlot(PluralRules.__INTERNAL_SLOT_MAP__, this, 'locale');
            var type = getInternalSlot(PluralRules.__INTERNAL_SLOT_MAP__, this, 'type');
            return PluralRules.localeData[locale].fn(formatNumericToString(PluralRules.__INTERNAL_SLOT_MAP__, this, Math.abs(Number(val))), type == 'ordinal');
        };
        PluralRules.prototype.toString = function () {
            return '[object Intl.PluralRules]';
        };
        PluralRules.supportedLocalesOf = function (locales, options) {
            return supportedLocales(PluralRules.availableLocales, Intl.getCanonicalLocales(locales), options);
        };
        PluralRules.__addLocaleData = function () {
            var data = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                data[_i] = arguments[_i];
            }
            var _loop_1 = function (datum) {
                var availableLocales = datum.availableLocales;
                availableLocales.forEach(function (locale) {
                    try {
                        PluralRules.localeData[locale] = unpackData(locale, datum);
                    }
                    catch (e) {
                        if (isMissingLocaleDataError(e)) {
                            // If we just don't have data for certain locale, that's ok
                            return;
                        }
                        throw e;
                    }
                });
            };
            for (var _a = 0, data_1 = data; _a < data_1.length; _a++) {
                var datum = data_1[_a];
                _loop_1(datum);
            }
            PluralRules.availableLocales = Object.keys(PluralRules.localeData);
            if (!PluralRules.__defaultLocale) {
                PluralRules.__defaultLocale = PluralRules.availableLocales[0];
            }
        };
        PluralRules.getDefaultLocale = function () {
            return PluralRules.__defaultLocale;
        };
        PluralRules.localeData = {};
        PluralRules.availableLocales = [];
        PluralRules.__defaultLocale = 'en';
        PluralRules.relevantExtensionKeys = [];
        PluralRules.polyfilled = true;
        PluralRules.__INTERNAL_SLOT_MAP__ = new WeakMap();
        return PluralRules;
    }());
    try {
        // IE11 does not have Symbol
        if (typeof Symbol !== 'undefined') {
            Object.defineProperty(PluralRules.prototype, Symbol.toStringTag, {
                value: 'Intl.PluralRules',
                writable: false,
                enumerable: false,
                configurable: true,
            });
        }
        // https://github.com/tc39/test262/blob/master/test/intl402/PluralRules/length.js
        Object.defineProperty(PluralRules, 'length', {
            value: 0,
            writable: false,
            enumerable: false,
            configurable: true,
        });
        // https://github.com/tc39/test262/blob/master/test/intl402/RelativeTimeFormat/constructor/length.js
        Object.defineProperty(PluralRules.prototype.constructor, 'length', {
            value: 0,
            writable: false,
            enumerable: false,
            configurable: true,
        });
        // https://github.com/tc39/test262/blob/master/test/intl402/RelativeTimeFormat/constructor/supportedLocalesOf/length.js
        Object.defineProperty(PluralRules.supportedLocalesOf, 'length', {
            value: 1,
            writable: false,
            enumerable: false,
            configurable: true,
        });
    }
    catch (ex) {
        // Meta fixes for test262
    }

    if (!('PluralRules' in Intl) ||
        new Intl.PluralRules('en', { minimumFractionDigits: 2 }).select(1) ===
            'one') {
        Object.defineProperty(Intl, 'PluralRules', {
            value: PluralRules,
            writable: true,
            enumerable: false,
            configurable: true,
        });
    }

})));


// Intl.NumberFormat
(function (factory) {
    typeof define === 'function' && define.amd ? define(factory) :
    factory();
}((function () { 'use strict';

    function invariant(condition, message, Err) {
        if (Err === void 0) { Err = Error; }
        if (!condition) {
            throw new Err(message);
        }
    }

    // https://tc39.es/ecma402/#sec-issanctionedsimpleunitidentifier
    var SANCTIONED_UNITS = [
        'angle-degree',
        'area-acre',
        'area-hectare',
        'concentr-percent',
        'digital-bit',
        'digital-byte',
        'digital-gigabit',
        'digital-gigabyte',
        'digital-kilobit',
        'digital-kilobyte',
        'digital-megabit',
        'digital-megabyte',
        'digital-petabyte',
        'digital-terabit',
        'digital-terabyte',
        'duration-day',
        'duration-hour',
        'duration-millisecond',
        'duration-minute',
        'duration-month',
        'duration-second',
        'duration-week',
        'duration-year',
        'length-centimeter',
        'length-foot',
        'length-inch',
        'length-kilometer',
        'length-meter',
        'length-mile-scandinavian',
        'length-mile',
        'length-millimeter',
        'length-yard',
        'mass-gram',
        'mass-kilogram',
        'mass-ounce',
        'mass-pound',
        'mass-stone',
        'temperature-celsius',
        'temperature-fahrenheit',
        'volume-fluid-ounce',
        'volume-gallon',
        'volume-liter',
        'volume-milliliter',
    ];

    function hasOwnProperty(o, key) {
        return Object.prototype.hasOwnProperty.call(o, key);
    }
    /**
     * https://tc39.es/ecma262/#sec-toobject
     * @param arg
     */
    function toObject(arg) {
        if (arg == null) {
            throw new TypeError('undefined/null cannot be converted to object');
        }
        return Object(arg);
    }
    /**
     * https://tc39.es/ecma262/#sec-tostring
     */
    function toString(o) {
        // Only symbol is irregular...
        if (typeof o === 'symbol') {
            throw TypeError('Cannot convert a Symbol value to a string');
        }
        return String(o);
    }
    /**
     * https://tc39.es/ecma402/#sec-getoption
     * @param opts
     * @param prop
     * @param type
     * @param values
     * @param fallback
     */
    function getOption(opts, prop, type, values, fallback) {
        // const descriptor = Object.getOwnPropertyDescriptor(opts, prop);
        var value = opts[prop];
        if (value !== undefined) {
            if (type !== 'boolean' && type !== 'string') {
                throw new TypeError('invalid type');
            }
            if (type === 'boolean') {
                value = Boolean(value);
            }
            if (type === 'string') {
                value = toString(value);
            }
            if (values !== undefined && !values.filter(function (val) { return val == value; }).length) {
                throw new RangeError(value + " is not within " + values.join(', '));
            }
            return value;
        }
        return fallback;
    }
    /**
     * https://tc39.es/ecma402/#sec-defaultnumberoption
     * @param val
     * @param min
     * @param max
     * @param fallback
     */
    function defaultNumberOption(val, min, max, fallback) {
        if (val !== undefined) {
            val = Number(val);
            if (isNaN(val) || val < min || val > max) {
                throw new RangeError(val + " is outside of range [" + min + ", " + max + "]");
            }
            return Math.floor(val);
        }
        return fallback;
    }
    /**
     * https://tc39.es/ecma402/#sec-getnumberoption
     * @param options
     * @param property
     * @param min
     * @param max
     * @param fallback
     */
    function getNumberOption(options, property, minimum, maximum, fallback) {
        var val = options[property];
        return defaultNumberOption(val, minimum, maximum, fallback);
    }
    /**
     * https://tc39.es/ecma402/#sec-setnfdigitoptions
     */
    function setNumberFormatDigitOptions(internalSlots, opts, mnfdDefault, mxfdDefault, notation) {
        var mnid = getNumberOption(opts, 'minimumIntegerDigits', 1, 21, 1);
        var mnfd = opts.minimumFractionDigits;
        var mxfd = opts.maximumFractionDigits;
        var mnsd = opts.minimumSignificantDigits;
        var mxsd = opts.maximumSignificantDigits;
        internalSlots.minimumIntegerDigits = mnid;
        if (mnsd !== undefined || mxsd !== undefined) {
            internalSlots.roundingType = 'significantDigits';
            mnsd = defaultNumberOption(mnsd, 1, 21, 1);
            mxsd = defaultNumberOption(mxsd, mnsd, 21, 21);
            internalSlots.minimumSignificantDigits = mnsd;
            internalSlots.maximumSignificantDigits = mxsd;
        }
        else if (mnfd !== undefined || mxfd !== undefined) {
            internalSlots.roundingType = 'fractionDigits';
            mnfd = defaultNumberOption(mnfd, 0, 20, mnfdDefault);
            var mxfdActualDefault = Math.max(mnfd, mxfdDefault);
            mxfd = defaultNumberOption(mxfd, mnfd, 20, mxfdActualDefault);
            internalSlots.minimumFractionDigits = mnfd;
            internalSlots.maximumFractionDigits = mxfd;
        }
        else if (notation === 'compact') {
            internalSlots.roundingType = 'compactRounding';
        }
        else {
            internalSlots.roundingType = 'fractionDigits';
            internalSlots.minimumFractionDigits = mnfdDefault;
            internalSlots.maximumFractionDigits = mxfdDefault;
        }
    }
    function objectIs(x, y) {
        if (Object.is) {
            return Object.is(x, y);
        }
        // SameValue algorithm
        if (x === y) {
            // Steps 1-5, 7-10
            // Steps 6.b-6.e: +0 != -0
            return x !== 0 || 1 / x === 1 / y;
        }
        // Step 6.a: NaN == NaN
        return x !== x && y !== y;
    }
    var NOT_A_Z_REGEX = /[^A-Z]/;
    /**
     * This follows https://tc39.es/ecma402/#sec-case-sensitivity-and-case-mapping
     * @param str string to convert
     */
    function toUpperCase(str) {
        return str.replace(/([a-z])/g, function (_, c) { return c.toUpperCase(); });
    }
    /**
     * https://tc39.es/ecma402/#sec-iswellformedcurrencycode
     */
    function isWellFormedCurrencyCode(currency) {
        currency = toUpperCase(currency);
        if (currency.length !== 3) {
            return false;
        }
        if (NOT_A_Z_REGEX.test(currency)) {
            return false;
        }
        return true;
    }
    /**
     * https://tc39.es/ecma402/#sec-formatnumberstring
     * TODO: dedup with intl-pluralrules
     */
    function formatNumericToString(internalSlots, x) {
        var isNegative = x < 0 || objectIs(x, -0);
        if (isNegative) {
            x = -x;
        }
        var result;
        var rourndingType = internalSlots.roundingType;
        switch (rourndingType) {
            case 'significantDigits':
                result = toRawPrecision(x, internalSlots.minimumSignificantDigits, internalSlots.maximumSignificantDigits);
                break;
            case 'fractionDigits':
                result = toRawFixed(x, internalSlots.minimumFractionDigits, internalSlots.maximumFractionDigits);
                break;
            default:
                result = toRawPrecision(x, 1, 2);
                if (result.integerDigitsCount > 1) {
                    result = toRawFixed(x, 0, 0);
                }
                break;
        }
        x = result.roundedNumber;
        var string = result.formattedString;
        var int = result.integerDigitsCount;
        var minInteger = internalSlots.minimumIntegerDigits;
        if (int < minInteger) {
            var forwardZeros = repeat('0', minInteger - int);
            string = forwardZeros + string;
        }
        if (isNegative) {
            x = -x;
        }
        return { roundedNumber: x, formattedString: string };
    }
    /**
     * TODO: dedup with intl-pluralrules and support BigInt
     * https://tc39.es/ecma402/#sec-torawfixed
     * @param x a finite non-negative Number or BigInt
     * @param minFraction and integer between 0 and 20
     * @param maxFraction and integer between 0 and 20
     */
    function toRawFixed(x, minFraction, maxFraction) {
        var f = maxFraction;
        var n = Math.round(x * Math.pow(10, f));
        var xFinal = n / Math.pow(10, f);
        // n is a positive integer, but it is possible to be greater than 1e21.
        // In such case we will go the slow path.
        // See also: https://tc39.es/ecma262/#sec-numeric-types-number-tostring
        var m;
        if (n < 1e21) {
            m = n.toString();
        }
        else {
            m = n.toString();
            var _a = m.split('e'), mantissa = _a[0], exponent = _a[1];
            m = mantissa.replace('.', '');
            m = m + repeat('0', Math.max(+exponent - m.length + 1, 0));
        }
        var int;
        if (f !== 0) {
            var k = m.length;
            if (k <= f) {
                var z = repeat('0', f + 1 - k);
                m = z + m;
                k = f + 1;
            }
            var a = m.slice(0, k - f);
            var b = m.slice(k - f);
            m = a + "." + b;
            int = a.length;
        }
        else {
            int = m.length;
        }
        var cut = maxFraction - minFraction;
        while (cut > 0 && m[m.length - 1] === '0') {
            m = m.slice(0, -1);
            cut--;
        }
        if (m[m.length - 1] === '.') {
            m = m.slice(0, -1);
        }
        return { formattedString: m, roundedNumber: xFinal, integerDigitsCount: int };
    }
    // https://tc39.es/ecma402/#sec-torawprecision
    function toRawPrecision(x, minPrecision, maxPrecision) {
        var p = maxPrecision;
        var m;
        var e;
        var xFinal;
        if (x === 0) {
            m = repeat('0', p);
            e = 0;
            xFinal = 0;
        }
        else {
            var xToString = x.toString();
            // If xToString is formatted as scientific notation, the number is either very small or very
            // large. If the precision of the formatted string is lower that requested max precision, we
            // should still infer them from the formatted string, otherwise the formatted result might have
            // precision loss (e.g. 1e41 will not have 0 in every trailing digits).
            var xToStringExponentIndex = xToString.indexOf('e');
            var _a = xToString.split('e'), xToStringMantissa = _a[0], xToStringExponent = _a[1];
            var xToStringMantissaWithoutDecimalPoint = xToStringMantissa.replace('.', '');
            if (xToStringExponentIndex >= 0 &&
                xToStringMantissaWithoutDecimalPoint.length <= p) {
                e = +xToStringExponent;
                m =
                    xToStringMantissaWithoutDecimalPoint +
                        repeat('0', p - xToStringMantissaWithoutDecimalPoint.length);
                xFinal = x;
            }
            else {
                e = getMagnitude(x);
                var decimalPlaceOffset = e - p + 1;
                // n is the integer containing the required precision digits. To derive the formatted string,
                // we will adjust its decimal place in the logic below.
                var n = Math.round(adjustDecimalPlace(x, decimalPlaceOffset));
                // The rounding caused the change of magnitude, so we should increment `e` by 1.
                if (adjustDecimalPlace(n, p - 1) >= 10) {
                    e = e + 1;
                    // Divide n by 10 to swallow one precision.
                    n = Math.floor(n / 10);
                }
                m = n.toString();
                // Equivalent of n * 10 ** (e - p + 1)
                xFinal = adjustDecimalPlace(n, p - 1 - e);
            }
        }
        var int;
        if (e >= p - 1) {
            m = m + repeat('0', e - p + 1);
            int = e + 1;
        }
        else if (e >= 0) {
            m = m.slice(0, e + 1) + "." + m.slice(e + 1);
            int = e + 1;
        }
        else {
            m = "0." + repeat('0', -e - 1) + m;
            int = 1;
        }
        if (m.indexOf('.') >= 0 && maxPrecision > minPrecision) {
            var cut = maxPrecision - minPrecision;
            while (cut > 0 && m[m.length - 1] === '0') {
                m = m.slice(0, -1);
                cut--;
            }
            if (m[m.length - 1] === '.') {
                m = m.slice(0, -1);
            }
        }
        return { formattedString: m, roundedNumber: xFinal, integerDigitsCount: int };
        // x / (10 ** magnitude), but try to preserve as much floating point precision as possible.
        function adjustDecimalPlace(x, magnitude) {
            return magnitude < 0 ? x * Math.pow(10, -magnitude) : x / Math.pow(10, magnitude);
        }
    }
    function repeat(s, times) {
        if (typeof s.repeat === 'function') {
            return s.repeat(times);
        }
        var arr = new Array(times);
        for (var i = 0; i < arr.length; i++) {
            arr[i] = s;
        }
        return arr.join('');
    }
    /**
     * Cannot do Math.log(x) / Math.log(10) bc if IEEE floating point issue
     * @param x number
     */
    function getMagnitude(x) {
        // Cannot count string length via Number.toString because it may use scientific notation
        // for very small or very large numbers.
        return Math.floor(Math.log(x) * Math.LOG10E);
    }
    /**
     * This follows https://tc39.es/ecma402/#sec-case-sensitivity-and-case-mapping
     * @param str string to convert
     */
    function toLowerCase(str) {
        return str.replace(/([A-Z])/g, function (_, c) { return c.toLowerCase(); });
    }
    var SHORTENED_SACTION_UNITS = SANCTIONED_UNITS.map(function (unit) {
        return unit.replace(/^(.*?)-/, '');
    });
    /**
     * https://tc39.es/ecma402/#sec-iswellformedunitidentifier
     * @param unit
     */
    function isWellFormedUnitIdentifier(unit) {
        unit = toLowerCase(unit);
        if (SHORTENED_SACTION_UNITS.indexOf(unit) > -1) {
            return true;
        }
        var units = unit.split('-per-');
        if (units.length !== 2) {
            return false;
        }
        if (SHORTENED_SACTION_UNITS.indexOf(units[0]) < 0 ||
            SHORTENED_SACTION_UNITS.indexOf(units[1]) < 0) {
            return false;
        }
        return true;
    }
    /*
      17 ECMAScript Standard Built-in Objects:
        Every built-in Function object, including constructors, that is not
        identified as an anonymous function has a name property whose value
        is a String.

        Unless otherwise specified, the name property of a built-in Function
        object, if it exists, has the attributes { [[Writable]]: false,
        [[Enumerable]]: false, [[Configurable]]: true }.
    */
    function defineProperty(target, name, _a) {
        var value = _a.value;
        Object.defineProperty(target, name, {
            configurable: true,
            enumerable: false,
            writable: true,
            value: value,
        });
    }

    var __extends = (undefined && undefined.__extends) || (function () {
        var extendStatics = function (d, b) {
            extendStatics = Object.setPrototypeOf ||
                ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
                function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
            return extendStatics(d, b);
        };
        return function (d, b) {
            extendStatics(d, b);
            function __() { this.constructor = d; }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
    var __assign = (undefined && undefined.__assign) || function () {
        __assign = Object.assign || function(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                    t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    function createResolveLocale(getDefaultLocale) {
        var lookupMatcher = createLookupMatcher(getDefaultLocale);
        var bestFitMatcher = createBestFitMatcher(getDefaultLocale);
        /**
         * https://tc39.es/ecma402/#sec-resolvelocale
         */
        return function resolveLocale(availableLocales, requestedLocales, options, relevantExtensionKeys, localeData) {
            var matcher = options.localeMatcher;
            var r;
            if (matcher === 'lookup') {
                r = lookupMatcher(availableLocales, requestedLocales);
            }
            else {
                r = bestFitMatcher(availableLocales, requestedLocales);
            }
            var foundLocale = r.locale;
            var result = { locale: '', dataLocale: foundLocale };
            var supportedExtension = '-u';
            for (var _i = 0, relevantExtensionKeys_1 = relevantExtensionKeys; _i < relevantExtensionKeys_1.length; _i++) {
                var key = relevantExtensionKeys_1[_i];
                var foundLocaleData = localeData[foundLocale];
                invariant(typeof foundLocaleData === 'object' && foundLocaleData !== null, "locale data " + key + " must be an object");
                var keyLocaleData = foundLocaleData[key];
                invariant(Array.isArray(keyLocaleData), "keyLocaleData for " + key + " must be an array");
                var value = keyLocaleData[0];
                invariant(typeof value === 'string' || value === null, "value must be string or null but got " + typeof value + " in key " + key);
                var supportedExtensionAddition = '';
                if (r.extension) {
                    var requestedValue = unicodeExtensionValue(r.extension, key);
                    if (requestedValue !== undefined) {
                        if (requestedValue !== '') {
                            if (~keyLocaleData.indexOf(requestedValue)) {
                                value = requestedValue;
                                supportedExtensionAddition = "-" + key + "-" + value;
                            }
                        }
                        else if (~requestedValue.indexOf('true')) {
                            value = 'true';
                            supportedExtensionAddition = "-" + key;
                        }
                    }
                }
                if (key in options) {
                    var optionsValue = options[key];
                    invariant(typeof optionsValue === 'string' ||
                        typeof optionsValue === 'undefined' ||
                        optionsValue === null, 'optionsValue must be String, Undefined or Null');
                    if (~keyLocaleData.indexOf(optionsValue)) {
                        if (optionsValue !== value) {
                            value = optionsValue;
                            supportedExtensionAddition = '';
                        }
                    }
                }
                result[key] = value;
                supportedExtension += supportedExtensionAddition;
            }
            if (supportedExtension.length > 2) {
                var privateIndex = foundLocale.indexOf('-x-');
                if (privateIndex === -1) {
                    foundLocale = foundLocale + supportedExtension;
                }
                else {
                    var preExtension = foundLocale.slice(0, privateIndex);
                    var postExtension = foundLocale.slice(privateIndex, foundLocale.length);
                    foundLocale = preExtension + supportedExtension + postExtension;
                }
                foundLocale = Intl.getCanonicalLocales(foundLocale)[0];
            }
            result.locale = foundLocale;
            return result;
        };
    }
    /**
     * https://tc39.es/ecma402/#sec-unicodeextensionvalue
     * @param extension
     * @param key
     */
    function unicodeExtensionValue(extension, key) {
        invariant(key.length === 2, 'key must have 2 elements');
        var size = extension.length;
        var searchValue = "-" + key + "-";
        var pos = extension.indexOf(searchValue);
        if (pos !== -1) {
            var start = pos + 4;
            var end = start;
            var k = start;
            var done = false;
            while (!done) {
                var e = extension.indexOf('-', k);
                var len = void 0;
                if (e === -1) {
                    len = size - k;
                }
                else {
                    len = e - k;
                }
                if (len === 2) {
                    done = true;
                }
                else if (e === -1) {
                    end = size;
                    done = true;
                }
                else {
                    end = e;
                    k = e + 1;
                }
            }
            return extension.slice(start, end);
        }
        searchValue = "-" + key;
        pos = extension.indexOf(searchValue);
        if (pos !== -1 && pos + 3 === size) {
            return '';
        }
        return undefined;
    }
    var UNICODE_EXTENSION_SEQUENCE_REGEX = /-u(?:-[0-9a-z]{2,8})+/gi;
    /**
     * https://tc39.es/ecma402/#sec-bestavailablelocale
     * @param availableLocales
     * @param locale
     */
    function bestAvailableLocale(availableLocales, locale) {
        var candidate = locale;
        while (true) {
            if (~availableLocales.indexOf(candidate)) {
                return candidate;
            }
            var pos = candidate.lastIndexOf('-');
            if (!~pos) {
                return undefined;
            }
            if (pos >= 2 && candidate[pos - 2] === '-') {
                pos -= 2;
            }
            candidate = candidate.slice(0, pos);
        }
    }
    function createLookupMatcher(getDefaultLocale) {
        /**
         * https://tc39.es/ecma402/#sec-lookupmatcher
         */
        return function lookupMatcher(availableLocales, requestedLocales) {
            var result = { locale: '' };
            for (var _i = 0, requestedLocales_1 = requestedLocales; _i < requestedLocales_1.length; _i++) {
                var locale = requestedLocales_1[_i];
                var noExtensionLocale = locale.replace(UNICODE_EXTENSION_SEQUENCE_REGEX, '');
                var availableLocale = bestAvailableLocale(availableLocales, noExtensionLocale);
                if (availableLocale) {
                    result.locale = availableLocale;
                    if (locale !== noExtensionLocale) {
                        result.extension = locale.slice(noExtensionLocale.length + 1, locale.length);
                    }
                    return result;
                }
            }
            result.locale = getDefaultLocale();
            return result;
        };
    }
    function createBestFitMatcher(getDefaultLocale) {
        return function bestFitMatcher(availableLocales, requestedLocales) {
            var result = { locale: '' };
            for (var _i = 0, requestedLocales_2 = requestedLocales; _i < requestedLocales_2.length; _i++) {
                var locale = requestedLocales_2[_i];
                var noExtensionLocale = locale.replace(UNICODE_EXTENSION_SEQUENCE_REGEX, '');
                var availableLocale = bestAvailableLocale(availableLocales, noExtensionLocale);
                if (availableLocale) {
                    result.locale = availableLocale;
                    if (locale !== noExtensionLocale) {
                        result.extension = locale.slice(noExtensionLocale.length + 1, locale.length);
                    }
                    return result;
                }
            }
            result.locale = getDefaultLocale();
            return result;
        };
    }
    function getLocaleHierarchy(locale) {
        var results = [locale];
        var localeParts = locale.split('-');
        for (var i = localeParts.length; i > 1; i--) {
            results.push(localeParts.slice(0, i - 1).join('-'));
        }
        return results;
    }
    function lookupSupportedLocales(availableLocales, requestedLocales) {
        var subset = [];
        for (var _i = 0, requestedLocales_3 = requestedLocales; _i < requestedLocales_3.length; _i++) {
            var locale = requestedLocales_3[_i];
            var noExtensionLocale = locale.replace(UNICODE_EXTENSION_SEQUENCE_REGEX, '');
            var availableLocale = bestAvailableLocale(availableLocales, noExtensionLocale);
            if (availableLocale) {
                subset.push(availableLocale);
            }
        }
        return subset;
    }
    function supportedLocales(availableLocales, requestedLocales, options) {
        var matcher = 'best fit';
        if (options !== undefined) {
            options = toObject(options);
            matcher = getOption(options, 'localeMatcher', 'string', ['lookup', 'best fit'], 'best fit');
        }
        if (matcher === 'best fit') {
            return lookupSupportedLocales(availableLocales, requestedLocales);
        }
        return lookupSupportedLocales(availableLocales, requestedLocales);
    }
    var MissingLocaleDataError = /** @class */ (function (_super) {
        __extends(MissingLocaleDataError, _super);
        function MissingLocaleDataError() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.type = 'MISSING_LOCALE_DATA';
            return _this;
        }
        return MissingLocaleDataError;
    }(Error));
    function unpackData(locale, localeData, 
    /** By default shallow merge the dictionaries. */
    reducer) {
        if (reducer === void 0) { reducer = function (all, d) { return (__assign(__assign({}, all), d)); }; }
        var localeHierarchy = getLocaleHierarchy(locale);
        var dataToMerge = localeHierarchy
            .map(function (l) { return localeData.data[l]; })
            .filter(Boolean);
        if (!dataToMerge.length) {
            throw new MissingLocaleDataError("Missing locale data for \"" + locale + "\", lookup hierarchy: " + localeHierarchy.join(', '));
        }
        dataToMerge.reverse();
        return dataToMerge.reduce(reducer, {});
    }

    var ADP = 0;
    var AFN = 0;
    var ALL = 0;
    var AMD = 2;
    var BHD = 3;
    var BIF = 0;
    var BYN = 2;
    var BYR = 0;
    var CAD = 2;
    var CHF = 2;
    var CLF = 4;
    var CLP = 0;
    var COP = 2;
    var CRC = 2;
    var CZK = 2;
    var DEFAULT = 2;
    var DJF = 0;
    var DKK = 2;
    var ESP = 0;
    var GNF = 0;
    var GYD = 2;
    var HUF = 2;
    var IDR = 2;
    var IQD = 0;
    var IRR = 0;
    var ISK = 0;
    var ITL = 0;
    var JOD = 3;
    var JPY = 0;
    var KMF = 0;
    var KPW = 0;
    var KRW = 0;
    var KWD = 3;
    var LAK = 0;
    var LBP = 0;
    var LUF = 0;
    var LYD = 3;
    var MGA = 0;
    var MGF = 0;
    var MMK = 0;
    var MNT = 2;
    var MRO = 0;
    var MUR = 2;
    var NOK = 2;
    var OMR = 3;
    var PKR = 2;
    var PYG = 0;
    var RSD = 0;
    var RWF = 0;
    var SEK = 2;
    var SLL = 0;
    var SOS = 0;
    var STD = 0;
    var SYP = 0;
    var TMM = 0;
    var TND = 3;
    var TRL = 0;
    var TWD = 2;
    var TZS = 2;
    var UGX = 0;
    var UYI = 0;
    var UYW = 4;
    var UZS = 2;
    var VEF = 2;
    var VND = 0;
    var VUV = 0;
    var XAF = 0;
    var XOF = 0;
    var XPF = 0;
    var YER = 0;
    var ZMK = 0;
    var ZWD = 0;
    var currencyDigits = {
    	ADP: ADP,
    	AFN: AFN,
    	ALL: ALL,
    	AMD: AMD,
    	BHD: BHD,
    	BIF: BIF,
    	BYN: BYN,
    	BYR: BYR,
    	CAD: CAD,
    	CHF: CHF,
    	CLF: CLF,
    	CLP: CLP,
    	COP: COP,
    	CRC: CRC,
    	CZK: CZK,
    	DEFAULT: DEFAULT,
    	DJF: DJF,
    	DKK: DKK,
    	ESP: ESP,
    	GNF: GNF,
    	GYD: GYD,
    	HUF: HUF,
    	IDR: IDR,
    	IQD: IQD,
    	IRR: IRR,
    	ISK: ISK,
    	ITL: ITL,
    	JOD: JOD,
    	JPY: JPY,
    	KMF: KMF,
    	KPW: KPW,
    	KRW: KRW,
    	KWD: KWD,
    	LAK: LAK,
    	LBP: LBP,
    	LUF: LUF,
    	LYD: LYD,
    	MGA: MGA,
    	MGF: MGF,
    	MMK: MMK,
    	MNT: MNT,
    	MRO: MRO,
    	MUR: MUR,
    	NOK: NOK,
    	OMR: OMR,
    	PKR: PKR,
    	PYG: PYG,
    	RSD: RSD,
    	RWF: RWF,
    	SEK: SEK,
    	SLL: SLL,
    	SOS: SOS,
    	STD: STD,
    	SYP: SYP,
    	TMM: TMM,
    	TND: TND,
    	TRL: TRL,
    	TWD: TWD,
    	TZS: TZS,
    	UGX: UGX,
    	UYI: UYI,
    	UYW: UYW,
    	UZS: UZS,
    	VEF: VEF,
    	VND: VND,
    	VUV: VUV,
    	XAF: XAF,
    	XOF: XOF,
    	XPF: XPF,
    	YER: YER,
    	ZMK: ZMK,
    	ZWD: ZWD
    };

    var currencyDigitsData = /*#__PURE__*/Object.freeze({
        __proto__: null,
        ADP: ADP,
        AFN: AFN,
        ALL: ALL,
        AMD: AMD,
        BHD: BHD,
        BIF: BIF,
        BYN: BYN,
        BYR: BYR,
        CAD: CAD,
        CHF: CHF,
        CLF: CLF,
        CLP: CLP,
        COP: COP,
        CRC: CRC,
        CZK: CZK,
        DEFAULT: DEFAULT,
        DJF: DJF,
        DKK: DKK,
        ESP: ESP,
        GNF: GNF,
        GYD: GYD,
        HUF: HUF,
        IDR: IDR,
        IQD: IQD,
        IRR: IRR,
        ISK: ISK,
        ITL: ITL,
        JOD: JOD,
        JPY: JPY,
        KMF: KMF,
        KPW: KPW,
        KRW: KRW,
        KWD: KWD,
        LAK: LAK,
        LBP: LBP,
        LUF: LUF,
        LYD: LYD,
        MGA: MGA,
        MGF: MGF,
        MMK: MMK,
        MNT: MNT,
        MRO: MRO,
        MUR: MUR,
        NOK: NOK,
        OMR: OMR,
        PKR: PKR,
        PYG: PYG,
        RSD: RSD,
        RWF: RWF,
        SEK: SEK,
        SLL: SLL,
        SOS: SOS,
        STD: STD,
        SYP: SYP,
        TMM: TMM,
        TND: TND,
        TRL: TRL,
        TWD: TWD,
        TZS: TZS,
        UGX: UGX,
        UYI: UYI,
        UYW: UYW,
        UZS: UZS,
        VEF: VEF,
        VND: VND,
        VUV: VUV,
        XAF: XAF,
        XOF: XOF,
        XPF: XPF,
        YER: YER,
        ZMK: ZMK,
        ZWD: ZWD,
        'default': currencyDigits
    });

    var names = [
    	"adlm",
    	"ahom",
    	"arab",
    	"arabext",
    	"armn",
    	"armnlow",
    	"bali",
    	"beng",
    	"bhks",
    	"brah",
    	"cakm",
    	"cham",
    	"cyrl",
    	"deva",
    	"ethi",
    	"fullwide",
    	"geor",
    	"gong",
    	"gonm",
    	"grek",
    	"greklow",
    	"gujr",
    	"guru",
    	"hanidays",
    	"hanidec",
    	"hans",
    	"hansfin",
    	"hant",
    	"hantfin",
    	"hebr",
    	"hmng",
    	"hmnp",
    	"java",
    	"jpan",
    	"jpanfin",
    	"jpanyear",
    	"kali",
    	"khmr",
    	"knda",
    	"lana",
    	"lanatham",
    	"laoo",
    	"latn",
    	"lepc",
    	"limb",
    	"mathbold",
    	"mathdbl",
    	"mathmono",
    	"mathsanb",
    	"mathsans",
    	"mlym",
    	"modi",
    	"mong",
    	"mroo",
    	"mtei",
    	"mymr",
    	"mymrshan",
    	"mymrtlng",
    	"newa",
    	"nkoo",
    	"olck",
    	"orya",
    	"osma",
    	"rohg",
    	"roman",
    	"romanlow",
    	"saur",
    	"shrd",
    	"sind",
    	"sinh",
    	"sora",
    	"sund",
    	"takr",
    	"talu",
    	"taml",
    	"tamldec",
    	"telu",
    	"thai",
    	"tibt",
    	"tirh",
    	"vaii",
    	"wara",
    	"wcho"
    ];

    var arab = [
    	"٠",
    	"١",
    	"٢",
    	"٣",
    	"٤",
    	"٥",
    	"٦",
    	"٧",
    	"٨",
    	"٩"
    ];
    var arabext = [
    	"۰",
    	"۱",
    	"۲",
    	"۳",
    	"۴",
    	"۵",
    	"۶",
    	"۷",
    	"۸",
    	"۹"
    ];
    var bali = [
    	"୐",
    	"୑",
    	"୒",
    	"୓",
    	"୔",
    	"୕",
    	"ୖ",
    	"ୗ",
    	"୘",
    	"୙"
    ];
    var beng = [
    	"০",
    	"১",
    	"২",
    	"৩",
    	"৪",
    	"৫",
    	"৬",
    	"৭",
    	"৮",
    	"৯"
    ];
    var deva = [
    	"०",
    	"१",
    	"२",
    	"३",
    	"४",
    	"५",
    	"६",
    	"७",
    	"८",
    	"९"
    ];
    var fullwide = [
    	"༐",
    	"༑",
    	"༒",
    	"༓",
    	"༔",
    	"༕",
    	"༖",
    	"༗",
    	"༘",
    	"༙"
    ];
    var gujr = [
    	"૦",
    	"૧",
    	"૨",
    	"૩",
    	"૪",
    	"૫",
    	"૬",
    	"૭",
    	"૮",
    	"૯"
    ];
    var guru = [
    	"੦",
    	"੧",
    	"੨",
    	"੩",
    	"੪",
    	"੫",
    	"੬",
    	"੭",
    	"੮",
    	"੯"
    ];
    var khmr = [
    	"ߠ",
    	"ߡ",
    	"ߢ",
    	"ߣ",
    	"ߤ",
    	"ߥ",
    	"ߦ",
    	"ߧ",
    	"ߨ",
    	"ߩ"
    ];
    var knda = [
    	"೦",
    	"೧",
    	"೨",
    	"೩",
    	"೪",
    	"೫",
    	"೬",
    	"೭",
    	"೮",
    	"೯"
    ];
    var laoo = [
    	"໐",
    	"໑",
    	"໒",
    	"໓",
    	"໔",
    	"໕",
    	"໖",
    	"໗",
    	"໘",
    	"໙"
    ];
    var limb = [
    	"ॆ",
    	"े",
    	"ै",
    	"ॉ",
    	"ॊ",
    	"ो",
    	"ौ",
    	"्",
    	"ॎ",
    	"ॏ"
    ];
    var mlym = [
    	"൦",
    	"൧",
    	"൨",
    	"൩",
    	"൪",
    	"൫",
    	"൬",
    	"൭",
    	"൮",
    	"൯"
    ];
    var mong = [
    	"ࠐ",
    	"ࠑ",
    	"ࠒ",
    	"ࠓ",
    	"ࠔ",
    	"ࠕ",
    	"ࠖ",
    	"ࠗ",
    	"࠘",
    	"࠙"
    ];
    var mymr = [
    	"@",
    	"A",
    	"B",
    	"C",
    	"D",
    	"E",
    	"F",
    	"G",
    	"H",
    	"I"
    ];
    var orya = [
    	"୦",
    	"୧",
    	"୨",
    	"୩",
    	"୪",
    	"୫",
    	"୬",
    	"୭",
    	"୮",
    	"୯"
    ];
    var tamldec = [
    	"௦",
    	"௧",
    	"௨",
    	"௩",
    	"௪",
    	"௫",
    	"௬",
    	"௭",
    	"௮",
    	"௯"
    ];
    var telu = [
    	"౦",
    	"౧",
    	"౨",
    	"౩",
    	"౪",
    	"౫",
    	"౬",
    	"౭",
    	"౮",
    	"౯"
    ];
    var thai = [
    	"๐",
    	"๑",
    	"๒",
    	"๓",
    	"๔",
    	"๕",
    	"๖",
    	"๗",
    	"๘",
    	"๙"
    ];
    var tibt = [
    	"༠",
    	"༡",
    	"༢",
    	"༣",
    	"༤",
    	"༥",
    	"༦",
    	"༧",
    	"༨",
    	"༩"
    ];
    var hanidec = [
    	"〇",
    	"一",
    	"二",
    	"三",
    	"四",
    	"五",
    	"六",
    	"七",
    	"八",
    	"九"
    ];
    var digitMapping = {
    	arab: arab,
    	arabext: arabext,
    	bali: bali,
    	beng: beng,
    	deva: deva,
    	fullwide: fullwide,
    	gujr: gujr,
    	guru: guru,
    	khmr: khmr,
    	knda: knda,
    	laoo: laoo,
    	limb: limb,
    	mlym: mlym,
    	mong: mong,
    	mymr: mymr,
    	orya: orya,
    	tamldec: tamldec,
    	telu: telu,
    	thai: thai,
    	tibt: tibt,
    	hanidec: hanidec
    };

    var digitMapping$1 = /*#__PURE__*/Object.freeze({
        __proto__: null,
        arab: arab,
        arabext: arabext,
        bali: bali,
        beng: beng,
        deva: deva,
        fullwide: fullwide,
        gujr: gujr,
        guru: guru,
        khmr: khmr,
        knda: knda,
        laoo: laoo,
        limb: limb,
        mlym: mlym,
        mong: mong,
        mymr: mymr,
        orya: orya,
        tamldec: tamldec,
        telu: telu,
        thai: thai,
        tibt: tibt,
        hanidec: hanidec,
        'default': digitMapping
    });

    // This is from: unicode-12.1.0/General_Category/Symbol/regex.js
    // IE11 does not support unicode flag, otherwise this is just /\p{S}/u.
    var S_UNICODE_REGEX = /[\$\+<->\^`\|~\xA2-\xA6\xA8\xA9\xAC\xAE-\xB1\xB4\xB8\xD7\xF7\u02C2-\u02C5\u02D2-\u02DF\u02E5-\u02EB\u02ED\u02EF-\u02FF\u0375\u0384\u0385\u03F6\u0482\u058D-\u058F\u0606-\u0608\u060B\u060E\u060F\u06DE\u06E9\u06FD\u06FE\u07F6\u07FE\u07FF\u09F2\u09F3\u09FA\u09FB\u0AF1\u0B70\u0BF3-\u0BFA\u0C7F\u0D4F\u0D79\u0E3F\u0F01-\u0F03\u0F13\u0F15-\u0F17\u0F1A-\u0F1F\u0F34\u0F36\u0F38\u0FBE-\u0FC5\u0FC7-\u0FCC\u0FCE\u0FCF\u0FD5-\u0FD8\u109E\u109F\u1390-\u1399\u166D\u17DB\u1940\u19DE-\u19FF\u1B61-\u1B6A\u1B74-\u1B7C\u1FBD\u1FBF-\u1FC1\u1FCD-\u1FCF\u1FDD-\u1FDF\u1FED-\u1FEF\u1FFD\u1FFE\u2044\u2052\u207A-\u207C\u208A-\u208C\u20A0-\u20BF\u2100\u2101\u2103-\u2106\u2108\u2109\u2114\u2116-\u2118\u211E-\u2123\u2125\u2127\u2129\u212E\u213A\u213B\u2140-\u2144\u214A-\u214D\u214F\u218A\u218B\u2190-\u2307\u230C-\u2328\u232B-\u2426\u2440-\u244A\u249C-\u24E9\u2500-\u2767\u2794-\u27C4\u27C7-\u27E5\u27F0-\u2982\u2999-\u29D7\u29DC-\u29FB\u29FE-\u2B73\u2B76-\u2B95\u2B98-\u2BFF\u2CE5-\u2CEA\u2E80-\u2E99\u2E9B-\u2EF3\u2F00-\u2FD5\u2FF0-\u2FFB\u3004\u3012\u3013\u3020\u3036\u3037\u303E\u303F\u309B\u309C\u3190\u3191\u3196-\u319F\u31C0-\u31E3\u3200-\u321E\u322A-\u3247\u3250\u3260-\u327F\u328A-\u32B0\u32C0-\u33FF\u4DC0-\u4DFF\uA490-\uA4C6\uA700-\uA716\uA720\uA721\uA789\uA78A\uA828-\uA82B\uA836-\uA839\uAA77-\uAA79\uAB5B\uFB29\uFBB2-\uFBC1\uFDFC\uFDFD\uFE62\uFE64-\uFE66\uFE69\uFF04\uFF0B\uFF1C-\uFF1E\uFF3E\uFF40\uFF5C\uFF5E\uFFE0-\uFFE6\uFFE8-\uFFEE\uFFFC\uFFFD]|\uD800[\uDD37-\uDD3F\uDD79-\uDD89\uDD8C-\uDD8E\uDD90-\uDD9B\uDDA0\uDDD0-\uDDFC]|\uD802[\uDC77\uDC78\uDEC8]|\uD805\uDF3F|\uD807[\uDFD5-\uDFF1]|\uD81A[\uDF3C-\uDF3F\uDF45]|\uD82F\uDC9C|\uD834[\uDC00-\uDCF5\uDD00-\uDD26\uDD29-\uDD64\uDD6A-\uDD6C\uDD83\uDD84\uDD8C-\uDDA9\uDDAE-\uDDE8\uDE00-\uDE41\uDE45\uDF00-\uDF56]|\uD835[\uDEC1\uDEDB\uDEFB\uDF15\uDF35\uDF4F\uDF6F\uDF89\uDFA9\uDFC3]|\uD836[\uDC00-\uDDFF\uDE37-\uDE3A\uDE6D-\uDE74\uDE76-\uDE83\uDE85\uDE86]|\uD838[\uDD4F\uDEFF]|\uD83B[\uDCAC\uDCB0\uDD2E\uDEF0\uDEF1]|\uD83C[\uDC00-\uDC2B\uDC30-\uDC93\uDCA0-\uDCAE\uDCB1-\uDCBF\uDCC1-\uDCCF\uDCD1-\uDCF5\uDD10-\uDD6C\uDD70-\uDDAC\uDDE6-\uDE02\uDE10-\uDE3B\uDE40-\uDE48\uDE50\uDE51\uDE60-\uDE65\uDF00-\uDFFF]|\uD83D[\uDC00-\uDED5\uDEE0-\uDEEC\uDEF0-\uDEFA\uDF00-\uDF73\uDF80-\uDFD8\uDFE0-\uDFEB]|\uD83E[\uDC00-\uDC0B\uDC10-\uDC47\uDC50-\uDC59\uDC60-\uDC87\uDC90-\uDCAD\uDD00-\uDD0B\uDD0D-\uDD71\uDD73-\uDD76\uDD7A-\uDDA2\uDDA5-\uDDAA\uDDAE-\uDDCA\uDDCD-\uDE53\uDE60-\uDE6D\uDE70-\uDE73\uDE78-\uDE7A\uDE80-\uDE82\uDE90-\uDE95]/;
    // /^\p{S}/u
    var CARET_S_UNICODE_REGEX = new RegExp("^" + S_UNICODE_REGEX.source);
    // /\p{S}$/u
    var S_DOLLAR_UNICODE_REGEX = new RegExp(S_UNICODE_REGEX.source + "$");
    var CLDR_NUMBER_PATTERN = /[#0](?:[\.,][#0]+)*/g;
    function formatToParts(numberResult, data, pl, options) {
        var sign = numberResult.sign, exponent = numberResult.exponent, magnitude = numberResult.magnitude;
        var notation = options.notation, style = options.style, numberingSystem = options.numberingSystem;
        var defaultNumberingSystem = data.numbers.nu[0];
        // Part 1: partition and interpolate the CLDR number pattern.
        // ----------------------------------------------------------
        var compactNumberPattern = null;
        if (notation === 'compact' && magnitude) {
            compactNumberPattern = getCompactDisplayPattern(numberResult, pl, data, style, options.compactDisplay, options.currencyDisplay, numberingSystem);
        }
        // This is used multiple times
        var nonNameCurrencyPart;
        if (style === 'currency' && options.currencyDisplay !== 'name') {
            var byCurrencyDisplay = data.currencies[options.currency];
            if (byCurrencyDisplay) {
                switch (options.currencyDisplay) {
                    case 'code':
                        nonNameCurrencyPart = options.currency;
                        break;
                    case 'symbol':
                        nonNameCurrencyPart = byCurrencyDisplay.symbol;
                        break;
                    default:
                        nonNameCurrencyPart = byCurrencyDisplay.narrow;
                        break;
                }
            }
            else {
                // Fallback for unknown currency
                nonNameCurrencyPart = options.currency;
            }
        }
        var numberPattern;
        if (!compactNumberPattern) {
            // Note: if the style is unit, or is currency and the currency display is name,
            // its unit parts will be interpolated in part 2. So here we can fallback to decimal.
            if (style === 'decimal' ||
                style === 'unit' ||
                (style === 'currency' && options.currencyDisplay === 'name')) {
                // Shortcut for decimal
                numberPattern = sign === 0 ? '0' : sign === -1 ? '-0' : '+0';
            }
            else if (style === 'currency') {
                var currencyData = data.numbers.currency[numberingSystem] ||
                    data.numbers.currency[defaultNumberingSystem];
                // We replace number pattern part with `0` for easier postprocessing.
                numberPattern = getPatternForSign(currencyData[options.currencySign], sign).replace(CLDR_NUMBER_PATTERN, '0');
            }
            else {
                // percent
                var percentPattern = data.numbers.percent[numberingSystem] ||
                    data.numbers.percent[defaultNumberingSystem];
                numberPattern = getPatternForSign(percentPattern, sign);
            }
        }
        else {
            numberPattern = compactNumberPattern;
        }
        // Handle currency spacing (both compact and non-compact).
        if (style === 'currency' && options.currencyDisplay !== 'name') {
            var currencyData = data.numbers.currency[numberingSystem] ||
                data.numbers.currency[defaultNumberingSystem];
            // See `currencySpacing` substitution rule in TR-35.
            // Here we always assume the currencyMatch is "[:^S:]" and surroundingMatch is "[:digit:]".
            //
            // Example 1: for pattern "#,##0.00¤" with symbol "US$", we replace "¤" with the symbol,
            // but insert an extra non-break space before the symbol, because "[:^S:]" matches "U" in
            // "US$" and "[:digit:]" matches the latn numbering system digits.
            //
            // Example 2: for pattern "¤#,##0.00" with symbol "US$", there is no spacing between symbol
            // a  nd number, because `$` does not match "[:^S:]".
            //
            // Implementation note: here we do the best effort to infer the insertion.
            // We also assume that `beforeInsertBetween` and `afterInsertBetween` will never be `;`.
            var afterCurrency = currencyData.currencySpacing.afterInsertBetween;
            if (afterCurrency && !S_DOLLAR_UNICODE_REGEX.test(nonNameCurrencyPart)) {
                numberPattern = numberPattern.replace('¤0', "\u00A4" + afterCurrency + "0");
            }
            var beforeCurrency = currencyData.currencySpacing.beforeInsertBetween;
            if (beforeCurrency && !CARET_S_UNICODE_REGEX.test(nonNameCurrencyPart)) {
                numberPattern = numberPattern.replace('0¤', "0" + beforeCurrency + "\u00A4");
            }
        }
        // Now we start to substitute patterns
        // 1. replace strings like `0` and `#,##0.00` with `{0}`
        // 2. unquote characters (invariant: the quoted characters does not contain the special tokens)
        numberPattern = numberPattern
            .replace(CLDR_NUMBER_PATTERN, '{0}')
            .replace(/'(.)'/g, '$1');
        // The following tokens are special: `{0}`, `¤`, `%`, `-`, `+`, `{c:...}.
        var numberPatternParts = numberPattern.split(/({c:[^}]+}|\{0\}|[¤%\-\+])/g);
        var numberParts = [];
        var symbols = data.numbers.symbols[numberingSystem] ||
            data.numbers.symbols[defaultNumberingSystem];
        for (var _i = 0, numberPatternParts_1 = numberPatternParts; _i < numberPatternParts_1.length; _i++) {
            var part = numberPatternParts_1[_i];
            if (!part) {
                continue;
            }
            switch (part) {
                case '{0}': {
                    // We only need to handle scientific and engineering notation here.
                    numberParts.push.apply(numberParts, paritionNumberIntoParts(symbols, numberResult, notation, exponent, numberingSystem, 
                    // If compact number pattern exists, do not insert group separators.
                    !compactNumberPattern && options.useGrouping));
                    break;
                }
                case '-':
                    numberParts.push({ type: 'minusSign', value: symbols.minusSign });
                    break;
                case '+':
                    numberParts.push({ type: 'plusSign', value: symbols.plusSign });
                    break;
                case '%':
                    numberParts.push({ type: 'percentSign', value: symbols.percentSign });
                    break;
                case '¤':
                    // Computed above when handling currency spacing.
                    numberParts.push({ type: 'currency', value: nonNameCurrencyPart });
                    break;
                default:
                    if (/^\{c:/.test(part)) {
                        numberParts.push({
                            type: 'compact',
                            value: part.substring(3, part.length - 1),
                        });
                    }
                    else {
                        // literal
                        numberParts.push({ type: 'literal', value: part });
                    }
                    break;
            }
        }
        // Part 2: interpolate unit pattern if necessary.
        // ----------------------------------------------
        switch (style) {
            case 'currency': {
                // `currencyDisplay: 'name'` has similar pattern handling as units.
                if (options.currencyDisplay === 'name') {
                    var unitPattern = (data.numbers.currency[numberingSystem] ||
                        data.numbers.currency[defaultNumberingSystem]).unitPattern;
                    // Select plural
                    var unitName = void 0;
                    var currencyNameData = data.currencies[options.currency];
                    if (currencyNameData) {
                        unitName = selectPlural(pl, 
                        // NOTE: Google Chrome's Intl.NumberFormat uses the original number to determine the plurality,
                        // but the mantissa for unit. We think this is a bug in ICU, but will still replicate the behavior.
                        // TODO: use original number.
                        numberResult.roundedNumber * Math.pow(10, exponent), currencyNameData.displayName);
                    }
                    else {
                        // Fallback for unknown currency
                        unitName = options.currency;
                    }
                    // Do {0} and {1} substitution
                    var unitPatternParts = unitPattern.split(/(\{[01]\})/g);
                    var result = [];
                    for (var _a = 0, unitPatternParts_1 = unitPatternParts; _a < unitPatternParts_1.length; _a++) {
                        var part = unitPatternParts_1[_a];
                        switch (part) {
                            case '{0}':
                                result.push.apply(result, numberParts);
                                break;
                            case '{1}':
                                result.push({ type: 'currency', value: unitName });
                                break;
                            default:
                                if (part) {
                                    result.push({ type: 'literal', value: part });
                                }
                                break;
                        }
                    }
                    return result;
                }
                else {
                    return numberParts;
                }
            }
            case 'unit': {
                var unit = options.unit, unitDisplay = options.unitDisplay;
                var unitData = data.units.simple[unit];
                var unitPattern = void 0;
                if (unitData) {
                    // Simple unit pattern
                    unitPattern = selectPlural(pl, numberResult.roundedNumber, data.units.simple[unit][unitDisplay]);
                }
                else {
                    // See: http://unicode.org/reports/tr35/tr35-general.html#perUnitPatterns
                    // If cannot find unit in the simple pattern, it must be "per" compound pattern.
                    // Implementation note: we are not following TR-35 here because we need to format to parts!
                    var _b = unit.split('-per-'), numeratorUnit = _b[0], denominatorUnit = _b[1];
                    unitData = data.units.simple[numeratorUnit];
                    var numeratorUnitPattern = selectPlural(pl, numberResult.roundedNumber, data.units.simple[numeratorUnit][unitDisplay]);
                    var perUnitPattern = data.units.simple[denominatorUnit].perUnit[unitDisplay];
                    if (perUnitPattern) {
                        // perUnitPattern exists, combine it with numeratorUnitPattern
                        unitPattern = perUnitPattern.replace('{0}', numeratorUnitPattern);
                    }
                    else {
                        // get compoundUnit pattern (e.g. "{0} per {1}"), repalce {0} with numerator pattern and {1} with
                        // the denominator pattern in singular form.
                        var perPattern = data.units.compound.per[unitDisplay];
                        var denominatorPattern = selectPlural(pl, 1, data.units.simple[denominatorUnit][unitDisplay]);
                        unitPattern = unitPattern = perPattern
                            .replace('{0}', numeratorUnitPattern)
                            .replace('{1}', denominatorPattern.replace('{0}', ''));
                    }
                }
                var result = [];
                // We need spacing around "{0}" because they are not treated as "unit" parts, but "literal".
                for (var _c = 0, _d = unitPattern.split(/(\s*\{0\}\s*)/); _c < _d.length; _c++) {
                    var part = _d[_c];
                    var interpolateMatch = /^(\s*)\{0\}(\s*)$/.exec(part);
                    if (interpolateMatch) {
                        // Space before "{0}"
                        if (interpolateMatch[1]) {
                            result.push({ type: 'literal', value: interpolateMatch[1] });
                        }
                        // "{0}" itself
                        result.push.apply(result, numberParts);
                        // Space after "{0}"
                        if (interpolateMatch[2]) {
                            result.push({ type: 'literal', value: interpolateMatch[2] });
                        }
                    }
                    else if (part) {
                        result.push({ type: 'unit', value: part });
                    }
                }
                return result;
            }
            default:
                return numberParts;
        }
    }
    // A subset of https://tc39.es/ecma402/#sec-partitionnotationsubpattern
    // Plus the exponent parts handling.
    function paritionNumberIntoParts(symbols, numberResult, notation, exponent, numberingSystem, useGrouping) {
        var result = [];
        // eslint-disable-next-line prefer-const
        var n = numberResult.formattedString, x = numberResult.roundedNumber;
        if (isNaN(x)) {
            return [{ type: 'nan', value: n }];
        }
        else if (!isFinite(x)) {
            return [{ type: 'infinity', value: n }];
        }
        var digitReplacementTable = digitMapping$1[numberingSystem];
        if (digitReplacementTable) {
            n = n.replace(/\d/g, function (digit) { return digitReplacementTable[+digit] || digit; });
        }
        // TODO: Else use an implementation dependent algorithm to map n to the appropriate
        // representation of n in the given numbering system.
        var decimalSepIndex = n.indexOf('.');
        var integer;
        var fraction;
        if (decimalSepIndex > 0) {
            integer = n.slice(0, decimalSepIndex);
            fraction = n.slice(decimalSepIndex + 1);
        }
        else {
            integer = n;
        }
        // The weird compact and x >= 10000 check is to ensure consistency with Node.js and Chrome.
        // Note that `de` does not have compact form for thousands, but Node.js does not insert grouping separator
        // unless the rounded number is greater than 10000:
        //   NumberFormat('de', {notation: 'compact', compactDisplay: 'short'}).format(1234) //=> "1234"
        //   NumberFormat('de').format(1234) //=> "1.234"
        if (useGrouping && (notation !== 'compact' || x >= 10000)) {
            var groupSepSymbol = symbols.group;
            var groups = [];
            // Assuming that the group separator is always inserted between every 3 digits.
            var i = integer.length - 3;
            for (; i > 0; i -= 3) {
                groups.push(integer.slice(i, i + 3));
            }
            groups.push(integer.slice(0, i + 3));
            while (groups.length > 0) {
                var integerGroup = groups.pop();
                result.push({ type: 'integer', value: integerGroup });
                if (groups.length > 0) {
                    result.push({ type: 'group', value: groupSepSymbol });
                }
            }
        }
        else {
            result.push({ type: 'integer', value: integer });
        }
        if (fraction !== undefined) {
            result.push({ type: 'decimal', value: symbols.decimal }, { type: 'fraction', value: fraction });
        }
        if ((notation === 'scientific' || notation === 'engineering') &&
            isFinite(x)) {
            result.push({ type: 'exponentSeparator', value: symbols.exponential });
            if (exponent < 0) {
                result.push({ type: 'exponentMinusSign', value: symbols.minusSign });
                exponent = -exponent;
            }
            var exponentResult = toRawFixed(exponent, 0, 0);
            result.push({
                type: 'exponentInteger',
                value: exponentResult.formattedString,
            });
        }
        return result;
    }
    function getPatternForSign(pattern, sign) {
        if (pattern.indexOf(';') < 0) {
            pattern = pattern + ";-" + pattern;
        }
        var _a = pattern.split(';'), zeroPattern = _a[0], negativePattern = _a[1];
        switch (sign) {
            case 0:
                return zeroPattern;
            case -1:
                return negativePattern;
            default:
                return negativePattern.indexOf('-') >= 0
                    ? negativePattern.replace(/-/g, '+')
                    : "+" + zeroPattern;
        }
    }
    // Find the CLDR pattern for compact notation based on the magnitude of data and style.
    //
    // Example return value: "¤ {c:laki}000;¤{c:laki} -0" (`sw` locale):
    // - Notice the `{c:...}` token that wraps the compact literal.
    // - The consecutive zeros are normalized to single zero to match CLDR_NUMBER_PATTERN.
    //
    // Returning null means the compact display pattern cannot be found.
    function getCompactDisplayPattern(numberResult, pl, data, style, compactDisplay, currencyDisplay, numberingSystem) {
        var _a;
        var roundedNumber = numberResult.roundedNumber, sign = numberResult.sign, magnitude = numberResult.magnitude;
        var magnitudeKey = String(Math.pow(10, magnitude));
        var defaultNumberingSystem = data.numbers.nu[0];
        var pattern;
        if (style === 'currency' && currencyDisplay !== 'name') {
            var byNumberingSystem = data.numbers.currency;
            var currencyData = byNumberingSystem[numberingSystem] ||
                byNumberingSystem[defaultNumberingSystem];
            // NOTE: compact notation ignores currencySign!
            var compactPluralRules = (_a = currencyData.short) === null || _a === void 0 ? void 0 : _a[magnitudeKey];
            if (!compactPluralRules) {
                return null;
            }
            pattern = selectPlural(pl, roundedNumber, compactPluralRules);
        }
        else {
            var byNumberingSystem = data.numbers.decimal;
            var byCompactDisplay = byNumberingSystem[numberingSystem] ||
                byNumberingSystem[defaultNumberingSystem];
            var compactPlaralRule = byCompactDisplay[compactDisplay][magnitudeKey];
            if (!compactPlaralRule) {
                return null;
            }
            pattern = selectPlural(pl, roundedNumber, compactPlaralRule);
        }
        // See https://unicode.org/reports/tr35/tr35-numbers.html#Compact_Number_Formats
        // > If the value is precisely “0”, either explicit or defaulted, then the normal number format
        // > pattern for that sort of object is supplied.
        if (pattern === '0') {
            return null;
        }
        pattern = getPatternForSign(pattern, sign)
            // Extract compact literal from the pattern
            .replace(/([^\s;\-\+\d¤]+)/g, '{c:$1}')
            // We replace one or more zeros with a single zero so it matches `CLDR_NUMBER_PATTERN`.
            .replace(/0+/, '0');
        return pattern;
    }
    function selectPlural(pl, x, rules) {
        return rules[pl.select(x)] || rules.other;
    }

    var internalSlotMap = new WeakMap();
    function getInternalSlots(x) {
        var internalSlots = internalSlotMap.get(x);
        if (!internalSlots) {
            internalSlots = Object.create(null);
            internalSlotMap.set(x, internalSlots);
        }
        return internalSlots;
    }

    var VALID_NUMBERING_SYSTEM_NAMES = new Set(names);
    var RESOLVED_OPTIONS_KEYS = [
        'locale',
        'numberingSystem',
        'style',
        'currency',
        'currencyDisplay',
        'currencySign',
        'unit',
        'unitDisplay',
        'minimumIntegerDigits',
        'minimumFractionDigits',
        'maximumFractionDigits',
        'minimumSignificantDigits',
        'maximumSignificantDigits',
        'useGrouping',
        'notation',
        'compactDisplay',
        'signDisplay',
    ];
    /**
     * Chop off the unicode extension from the locale string.
     */
    function removeUnicodeExtensionFromLocale(canonicalLocale) {
        var extensionIndex = canonicalLocale.indexOf('-u-');
        return extensionIndex >= 0
            ? canonicalLocale.slice(0, extensionIndex)
            : canonicalLocale;
    }
    /**
     * https://tc39.es/ecma402/#sec-currencydigits
     */
    function currencyDigits$1(c) {
        return hasOwnProperty(currencyDigitsData, c)
            ? currencyDigitsData[c]
            : 2;
    }
    /**
     * https://tc39.es/ecma402/#sec-initializenumberformat
     */
    function initializeNumberFormat(nf, locales, opts) {
        // @ts-ignore
        var requestedLocales = Intl.getCanonicalLocales(locales);
        var options = opts === undefined ? Object.create(null) : toObject(opts);
        var opt = Object.create(null);
        var matcher = getOption(options, 'localeMatcher', 'string', ['lookup', 'best fit'], 'best fit');
        opt.localeMatcher = matcher;
        var numberingSystem = getOption(options, 'numberingSystem', 'string', undefined, undefined);
        if (numberingSystem !== undefined &&
            !VALID_NUMBERING_SYSTEM_NAMES.has(numberingSystem)) {
            // 8.a. If numberingSystem does not match the Unicode Locale Identifier type nonterminal,
            // throw a RangeError exception.
            throw RangeError("Invalid numberingSystems: " + numberingSystem);
        }
        opt.nu = numberingSystem;
        var localeData = NumberFormat.localeData;
        var r = createResolveLocale(NumberFormat.getDefaultLocale)(NumberFormat.availableLocales, requestedLocales, opt, 
        // [[RelevantExtensionKeys]] slot, which is a constant
        ['nu'], localeData);
        var dataLocaleData = localeData[removeUnicodeExtensionFromLocale(r.locale)];
        var internalSlots = getInternalSlots(nf);
        internalSlots.locale = r.locale;
        internalSlots.dataLocale = r.dataLocale;
        internalSlots.numberingSystem = r.nu;
        internalSlots.dataLocaleData = dataLocaleData;
        setNumberFormatUnitOptions(nf, options);
        var style = internalSlots.style;
        var mnfdDefault;
        var mxfdDefault;
        if (style === 'currency') {
            var currency = internalSlots.currency;
            var cDigits = currencyDigits$1(currency);
            mnfdDefault = cDigits;
            mxfdDefault = cDigits;
        }
        else {
            mnfdDefault = 0;
            mxfdDefault = style === 'percent' ? 0 : 3;
        }
        var notation = getOption(options, 'notation', 'string', ['standard', 'scientific', 'engineering', 'compact'], 'standard');
        internalSlots.notation = notation;
        setNumberFormatDigitOptions(internalSlots, options, mnfdDefault, mxfdDefault, notation);
        var compactDisplay = getOption(options, 'compactDisplay', 'string', ['short', 'long'], 'short');
        if (notation === 'compact') {
            internalSlots.compactDisplay = compactDisplay;
        }
        var useGrouping = getOption(options, 'useGrouping', 'boolean', undefined, true);
        internalSlots.useGrouping = useGrouping;
        var signDisplay = getOption(options, 'signDisplay', 'string', ['auto', 'never', 'always', 'exceptZero'], 'auto');
        internalSlots.signDisplay = signDisplay;
        return nf;
    }
    /**
     * https://tc39.es/ecma402/#sec-formatnumberstring
     */
    function partitionNumberPattern(numberFormat, x) {
        var _a;
        var internalSlots = getInternalSlots(numberFormat);
        var pl = internalSlots.pl, dataLocaleData = internalSlots.dataLocaleData, numberingSystem = internalSlots.numberingSystem;
        var symbols = dataLocaleData.numbers.symbols[numberingSystem] ||
            dataLocaleData.numbers.symbols[dataLocaleData.numbers.nu[0]];
        var magnitude = 0;
        var exponent = 0;
        var n;
        if (isNaN(x)) {
            n = symbols.nan;
        }
        else if (!isFinite(x)) {
            n = symbols.infinity;
        }
        else {
            if (internalSlots.style === 'percent') {
                x *= 100;
            }
            _a = computeExponent(numberFormat, x), exponent = _a[0], magnitude = _a[1];
            // Preserve more precision by doing multiplication when exponent is negative.
            x = exponent < 0 ? x * Math.pow(10, -exponent) : x / Math.pow(10, exponent);
            var formatNumberResult = formatNumericToString(internalSlots, x);
            n = formatNumberResult.formattedString;
            x = formatNumberResult.roundedNumber;
        }
        // Based on https://tc39.es/ecma402/#sec-getnumberformatpattern
        // We need to do this before `x` is rounded.
        var sign;
        var signDisplay = internalSlots.signDisplay;
        switch (signDisplay) {
            case 'never':
                sign = 0;
                break;
            case 'auto':
                if (objectIs(x, 0) || x > 0 || isNaN(x)) {
                    sign = 0;
                }
                else {
                    sign = -1;
                }
                break;
            case 'always':
                if (objectIs(x, 0) || x > 0 || isNaN(x)) {
                    sign = 1;
                }
                else {
                    sign = -1;
                }
                break;
            default:
                // x === 0 -> x is 0 or x is -0
                if (x === 0 || isNaN(x)) {
                    sign = 0;
                }
                else if (x > 0) {
                    sign = 1;
                }
                else {
                    sign = -1;
                }
        }
        return formatToParts({ roundedNumber: x, formattedString: n, exponent: exponent, magnitude: magnitude, sign: sign }, internalSlots.dataLocaleData, pl, internalSlots);
    }
    function formatNumericToParts(numberFormat, x) {
        return partitionNumberPattern(numberFormat, x);
    }
    /**
     * https://tc39.es/ecma402/#sec-intl-numberformat-constructor
     */
    var NumberFormat = function (locales, options) {
        // Cannot use `new.target` bc of IE11 & TS transpiles it to something else
        if (!this || !(this instanceof NumberFormat)) {
            return new NumberFormat(locales, options);
        }
        initializeNumberFormat(this, locales, options);
        var internalSlots = getInternalSlots(this);
        var dataLocale = internalSlots.dataLocale;
        var dataLocaleData = NumberFormat.localeData[dataLocale];
        invariant(dataLocaleData !== undefined, "Cannot load locale-dependent data for " + dataLocale + ".");
        internalSlots.pl = new Intl.PluralRules(dataLocale, {
            minimumFractionDigits: internalSlots.minimumFractionDigits,
            maximumFractionDigits: internalSlots.maximumFractionDigits,
            minimumIntegerDigits: internalSlots.minimumIntegerDigits,
            minimumSignificantDigits: internalSlots.minimumSignificantDigits,
            maximumSignificantDigits: internalSlots.maximumSignificantDigits,
        });
    };
    defineProperty(NumberFormat.prototype, 'formatToParts', {
        value: function formatToParts(x) {
            return formatNumericToParts(this, toNumeric(x));
        },
    });
    defineProperty(NumberFormat.prototype, 'resolvedOptions', {
        value: function resolvedOptions() {
            if (typeof this !== 'object' || !(this instanceof NumberFormat)) {
                throw TypeError('Method Intl.NumberFormat.prototype.resolvedOptions called on incompatible receiver');
            }
            var internalSlots = getInternalSlots(this);
            var ro = {};
            for (var _i = 0, RESOLVED_OPTIONS_KEYS_1 = RESOLVED_OPTIONS_KEYS; _i < RESOLVED_OPTIONS_KEYS_1.length; _i++) {
                var key = RESOLVED_OPTIONS_KEYS_1[_i];
                var value = internalSlots[key];
                if (value !== undefined) {
                    ro[key] = value;
                }
            }
            return ro;
        },
    });
    var formatDescriptor = {
        enumerable: false,
        configurable: true,
        get: function () {
            if (typeof this !== 'object' || !(this instanceof NumberFormat)) {
                throw TypeError('Intl.NumberFormat format property accessor called on incompatible receiver');
            }
            var internalSlots = getInternalSlots(this);
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            var numberFormat = this;
            var boundFormat = internalSlots.boundFormat;
            if (boundFormat === undefined) {
                // https://tc39.es/proposal-unified-intl-numberformat/section11/numberformat_diff_out.html#sec-number-format-functions
                boundFormat = function (value) {
                    // TODO: check bigint
                    var x = toNumeric(value);
                    return numberFormat
                        .formatToParts(x)
                        .map(function (x) { return x.value; })
                        .join('');
                };
                try {
                    // https://github.com/tc39/test262/blob/master/test/intl402/NumberFormat/prototype/format/format-function-name.js
                    Object.defineProperty(boundFormat, 'name', {
                        configurable: true,
                        enumerable: false,
                        writable: false,
                        value: '',
                    });
                }
                catch (e) {
                    // In older browser (e.g Chrome 36 like polyfill.io)
                    // TypeError: Cannot redefine property: name
                }
                internalSlots.boundFormat = boundFormat;
            }
            return boundFormat;
        },
    };
    try {
        // https://github.com/tc39/test262/blob/master/test/intl402/NumberFormat/prototype/format/name.js
        Object.defineProperty(formatDescriptor.get, 'name', {
            configurable: true,
            enumerable: false,
            writable: false,
            value: 'get format',
        });
    }
    catch (e) {
        // In older browser (e.g Chrome 36 like polyfill.io)
        // TypeError: Cannot redefine property: name
    }
    Object.defineProperty(NumberFormat.prototype, 'format', formatDescriptor);
    // Static properties
    defineProperty(NumberFormat, 'supportedLocalesOf', {
        value: function supportedLocalesOf(locales, options) {
            return supportedLocales(NumberFormat.availableLocales, Intl.getCanonicalLocales(locales), options);
        },
    });
    NumberFormat.__addLocaleData = function __addLocaleData() {
        var data = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            data[_i] = arguments[_i];
        }
        for (var _a = 0, data_1 = data; _a < data_1.length; _a++) {
            var datum = data_1[_a];
            var availableLocales = datum.availableLocales;
            for (var _b = 0, availableLocales_1 = availableLocales; _b < availableLocales_1.length; _b++) {
                var locale = availableLocales_1[_b];
                try {
                    NumberFormat.localeData[locale] = unpackData(locale, datum);
                }
                catch (e) {
                    // Ignore if we got no data
                }
            }
        }
        NumberFormat.availableLocales = Object.keys(NumberFormat.localeData);
        if (!NumberFormat.__defaultLocale) {
            NumberFormat.__defaultLocale = NumberFormat.availableLocales[0];
        }
    };
    NumberFormat.__defaultLocale = 'en';
    NumberFormat.localeData = {};
    NumberFormat.availableLocales = [];
    NumberFormat.getDefaultLocale = function () {
        return NumberFormat.__defaultLocale;
    };
    NumberFormat.polyfilled = true;
    /**
     * https://tc39.es/ecma402/#sec-setnumberformatunitoptions
     */
    function setNumberFormatUnitOptions(nf, options) {
        if (options === void 0) { options = Object.create(null); }
        var internalSlots = getInternalSlots(nf);
        var style = getOption(options, 'style', 'string', ['decimal', 'percent', 'currency', 'unit'], 'decimal');
        internalSlots.style = style;
        var currency = getOption(options, 'currency', 'string', undefined, undefined);
        if (currency !== undefined && !isWellFormedCurrencyCode(currency)) {
            throw RangeError('Malformed currency code');
        }
        if (style === 'currency' && currency === undefined) {
            throw TypeError('currency cannot be undefined');
        }
        var currencyDisplay = getOption(options, 'currencyDisplay', 'string', ['code', 'symbol', 'narrowSymbol', 'name'], 'symbol');
        var currencySign = getOption(options, 'currencySign', 'string', ['standard', 'accounting'], 'standard');
        var unit = getOption(options, 'unit', 'string', undefined, undefined);
        if (unit !== undefined && !isWellFormedUnitIdentifier(unit)) {
            throw RangeError('Invalid unit argument for Intl.NumberFormat()');
        }
        if (style === 'unit' && unit === undefined) {
            throw TypeError('unit cannot be undefined');
        }
        var unitDisplay = getOption(options, 'unitDisplay', 'string', ['short', 'narrow', 'long'], 'short');
        if (style === 'currency') {
            internalSlots.currency = currency.toUpperCase();
            internalSlots.currencyDisplay = currencyDisplay;
            internalSlots.currencySign = currencySign;
        }
        if (style === 'unit') {
            internalSlots.unit = unit;
            internalSlots.unitDisplay = unitDisplay;
        }
    }
    /**
     * The abstract operation ComputeExponent computes an exponent (power of ten) by which to scale x
     * according to the number formatting settings. It handles cases such as 999 rounding up to 1000,
     * requiring a different exponent.
     *
     * NOT IN SPEC: it returns [exponent, magnitude].
     */
    function computeExponent(numberFormat, x) {
        if (x === 0) {
            return [0, 0];
        }
        if (x < 0) {
            x = -x;
        }
        var magnitude = getMagnitude(x);
        var exponent = computeExponentForMagnitude(numberFormat, magnitude);
        // Preserve more precision by doing multiplication when exponent is negative.
        x = exponent < 0 ? x * Math.pow(10, -exponent) : x / Math.pow(10, exponent);
        var formatNumberResult = formatNumericToString(getInternalSlots(numberFormat), x);
        if (formatNumberResult.roundedNumber === 0) {
            return [exponent, magnitude];
        }
        var newMagnitude = getMagnitude(formatNumberResult.roundedNumber);
        if (newMagnitude === magnitude - exponent) {
            return [exponent, magnitude];
        }
        return [
            computeExponentForMagnitude(numberFormat, magnitude + 1),
            magnitude + 1,
        ];
    }
    /**
     * The abstract operation ComputeExponentForMagnitude computes an exponent by which to scale a
     * number of the given magnitude (power of ten of the most significant digit) according to the
     * locale and the desired notation (scientific, engineering, or compact).
     */
    function computeExponentForMagnitude(numberFormat, magnitude) {
        var internalSlots = getInternalSlots(numberFormat);
        var notation = internalSlots.notation, dataLocaleData = internalSlots.dataLocaleData, numberingSystem = internalSlots.numberingSystem;
        switch (notation) {
            case 'standard':
                return 0;
            case 'scientific':
                return magnitude;
            case 'engineering':
                return Math.floor(magnitude / 3) * 3;
            default: {
                // Let exponent be an implementation- and locale-dependent (ILD) integer by which to scale a
                // number of the given magnitude in compact notation for the current locale.
                var compactDisplay = internalSlots.compactDisplay, style = internalSlots.style, currencyDisplay = internalSlots.currencyDisplay;
                var thresholdMap = void 0;
                if (style === 'currency' && currencyDisplay !== 'name') {
                    var currency = dataLocaleData.numbers.currency[numberingSystem] ||
                        dataLocaleData.numbers.currency[dataLocaleData.numbers.nu[0]];
                    thresholdMap = currency.short;
                }
                else {
                    var decimal = dataLocaleData.numbers.decimal[numberingSystem] ||
                        dataLocaleData.numbers.decimal[dataLocaleData.numbers.nu[0]];
                    thresholdMap = compactDisplay === 'long' ? decimal.long : decimal.short;
                }
                if (!thresholdMap) {
                    return 0;
                }
                var num = String(Math.pow(10, magnitude));
                var thresholds = Object.keys(thresholdMap); // TODO: this can be pre-processed
                if (num < thresholds[0]) {
                    return 0;
                }
                if (num > thresholds[thresholds.length - 1]) {
                    return thresholds[thresholds.length - 1].length - 1;
                }
                var i = thresholds.indexOf(num);
                if (i === -1) {
                    return 0;
                }
                // See https://unicode.org/reports/tr35/tr35-numbers.html#Compact_Number_Formats
                // Special handling if the pattern is precisely `0`.
                var magnitudeKey = thresholds[i];
                // TODO: do we need to handle plural here?
                var compactPattern = thresholdMap[magnitudeKey].other;
                if (compactPattern === '0') {
                    return 0;
                }
                // Example: in zh-TW, `10000000` maps to `0000萬`. So we need to return 8 - 4 = 4 here.
                return (magnitudeKey.length -
                    thresholdMap[magnitudeKey].other.match(/0+/)[0].length);
            }
        }
    }
    function toNumeric(val) {
        if (typeof val === 'bigint') {
            return val;
        }
        return toNumber(val);
    }
    function toNumber(val) {
        if (val === undefined) {
            return NaN;
        }
        if (val === null) {
            return +0;
        }
        if (typeof val === 'boolean') {
            return val ? 1 : +0;
        }
        if (typeof val === 'number') {
            return val;
        }
        if (typeof val === 'symbol' || typeof val === 'bigint') {
            throw new TypeError('Cannot convert symbol/bigint to number');
        }
        return Number(val);
    }
    try {
        // IE11 does not have Symbol
        if (typeof Symbol !== 'undefined') {
            Object.defineProperty(NumberFormat.prototype, Symbol.toStringTag, {
                configurable: true,
                enumerable: false,
                writable: false,
                value: 'Intl.NumberFormat',
            });
        }
        // https://github.com/tc39/test262/blob/master/test/intl402/NumberFormat/length.js
        Object.defineProperty(NumberFormat.prototype.constructor, 'length', {
            configurable: true,
            enumerable: false,
            writable: false,
            value: 0,
        });
        // https://github.com/tc39/test262/blob/master/test/intl402/NumberFormat/supportedLocalesOf/length.js
        Object.defineProperty(NumberFormat.supportedLocalesOf, 'length', {
            configurable: true,
            enumerable: false,
            writable: false,
            value: 1,
        });
        Object.defineProperty(NumberFormat, 'prototype', {
            configurable: false,
            enumerable: false,
            writable: false,
            value: NumberFormat.prototype,
        });
    }
    catch (e) {
        // Meta fix so we're test262-compliant, not important
    }

    // eslint-disable-next-line import/no-cycle
    /**
     * Number.prototype.toLocaleString ponyfill
     * https://tc39.es/ecma402/#sup-number.prototype.tolocalestring
     */
    function toLocaleString(x, locales, options) {
        var numberFormat = new NumberFormat(locales, options);
        return numberFormat.format(x);
    }

    /* eslint-disable import/no-cycle */
    /**
     * Check if a formatting number with unit is supported
     * @public
     * @param unit unit to check
     */
    function isUnitSupported(unit) {
        try {
            new Intl.NumberFormat(undefined, {
                style: 'unit',
                // @ts-ignore
                unit: unit,
            });
        }
        catch (e) {
            return false;
        }
        return true;
    }

    if (!isUnitSupported('bit')) {
        defineProperty(Intl, 'NumberFormat', { value: NumberFormat });
        defineProperty(Number.prototype, 'toLocaleString', {
            value: function toLocaleString$1(locales, options) {
                return toLocaleString(this, locales, options);
            },
        });
    }

})));


// Intl.DateTimeFormat
(function (factory) {
    typeof define === 'function' && define.amd ? define(factory) :
    factory();
}((function () { 'use strict';

    function invariant(condition, message, Err) {
        if (Err === void 0) { Err = Error; }
        if (!condition) {
            throw new Err(message);
        }
    }

    // https://tc39.es/ecma402/#sec-issanctionedsimpleunitidentifier
    var SANCTIONED_UNITS = [
        'angle-degree',
        'area-acre',
        'area-hectare',
        'concentr-percent',
        'digital-bit',
        'digital-byte',
        'digital-gigabit',
        'digital-gigabyte',
        'digital-kilobit',
        'digital-kilobyte',
        'digital-megabit',
        'digital-megabyte',
        'digital-petabyte',
        'digital-terabit',
        'digital-terabyte',
        'duration-day',
        'duration-hour',
        'duration-millisecond',
        'duration-minute',
        'duration-month',
        'duration-second',
        'duration-week',
        'duration-year',
        'length-centimeter',
        'length-foot',
        'length-inch',
        'length-kilometer',
        'length-meter',
        'length-mile-scandinavian',
        'length-mile',
        'length-millimeter',
        'length-yard',
        'mass-gram',
        'mass-kilogram',
        'mass-ounce',
        'mass-pound',
        'mass-stone',
        'temperature-celsius',
        'temperature-fahrenheit',
        'volume-fluid-ounce',
        'volume-gallon',
        'volume-liter',
        'volume-milliliter',
    ];

    /**
     * https://tc39.es/ecma262/#sec-toobject
     * @param arg
     */
    function toObject(arg) {
        if (arg == null) {
            throw new TypeError('undefined/null cannot be converted to object');
        }
        return Object(arg);
    }
    /**
     * https://tc39.es/ecma262/#sec-tostring
     */
    function toString(o) {
        // Only symbol is irregular...
        if (typeof o === 'symbol') {
            throw TypeError('Cannot convert a Symbol value to a string');
        }
        return String(o);
    }
    /**
     * https://tc39.es/ecma402/#sec-getoption
     * @param opts
     * @param prop
     * @param type
     * @param values
     * @param fallback
     */
    function getOption(opts, prop, type, values, fallback) {
        // const descriptor = Object.getOwnPropertyDescriptor(opts, prop);
        var value = opts[prop];
        if (value !== undefined) {
            if (type !== 'boolean' && type !== 'string') {
                throw new TypeError('invalid type');
            }
            if (type === 'boolean') {
                value = Boolean(value);
            }
            if (type === 'string') {
                value = toString(value);
            }
            if (values !== undefined && !values.filter(function (val) { return val == value; }).length) {
                throw new RangeError(value + " is not within " + values.join(', '));
            }
            return value;
        }
        return fallback;
    }
    function partitionPattern(pattern) {
        var result = [];
        var beginIndex = pattern.indexOf('{');
        var endIndex = 0;
        var nextIndex = 0;
        var length = pattern.length;
        while (beginIndex < pattern.length && beginIndex > -1) {
            endIndex = pattern.indexOf('}', beginIndex);
            invariant(endIndex > beginIndex, "Invalid pattern " + pattern);
            if (beginIndex > nextIndex) {
                result.push({
                    type: 'literal',
                    value: pattern.substring(nextIndex, beginIndex),
                });
            }
            result.push({
                type: pattern.substring(beginIndex + 1, endIndex),
                value: undefined,
            });
            nextIndex = endIndex + 1;
            beginIndex = pattern.indexOf('{', nextIndex);
        }
        if (nextIndex < length) {
            result.push({
                type: 'literal',
                value: pattern.substring(nextIndex, length),
            });
        }
        return result;
    }
    function objectIs(x, y) {
        if (Object.is) {
            return Object.is(x, y);
        }
        // SameValue algorithm
        if (x === y) {
            // Steps 1-5, 7-10
            // Steps 6.b-6.e: +0 != -0
            return x !== 0 || 1 / x === 1 / y;
        }
        // Step 6.a: NaN == NaN
        return x !== x && y !== y;
    }
    var SHORTENED_SACTION_UNITS = SANCTIONED_UNITS.map(function (unit) {
        return unit.replace(/^(.*?)-/, '');
    });
    /*
      17 ECMAScript Standard Built-in Objects:
        Every built-in Function object, including constructors, that is not
        identified as an anonymous function has a name property whose value
        is a String.

        Unless otherwise specified, the name property of a built-in Function
        object, if it exists, has the attributes { [[Writable]]: false,
        [[Enumerable]]: false, [[Configurable]]: true }.
    */
    function defineProperty(target, name, _a) {
        var value = _a.value;
        Object.defineProperty(target, name, {
            configurable: true,
            enumerable: false,
            writable: true,
            value: value,
        });
    }

    var __extends = (undefined && undefined.__extends) || (function () {
        var extendStatics = function (d, b) {
            extendStatics = Object.setPrototypeOf ||
                ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
                function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
            return extendStatics(d, b);
        };
        return function (d, b) {
            extendStatics(d, b);
            function __() { this.constructor = d; }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
    var __assign = (undefined && undefined.__assign) || function () {
        __assign = Object.assign || function(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                    t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    function createResolveLocale(getDefaultLocale) {
        var lookupMatcher = createLookupMatcher(getDefaultLocale);
        var bestFitMatcher = createBestFitMatcher(getDefaultLocale);
        /**
         * https://tc39.es/ecma402/#sec-resolvelocale
         */
        return function resolveLocale(availableLocales, requestedLocales, options, relevantExtensionKeys, localeData) {
            var matcher = options.localeMatcher;
            var r;
            if (matcher === 'lookup') {
                r = lookupMatcher(availableLocales, requestedLocales);
            }
            else {
                r = bestFitMatcher(availableLocales, requestedLocales);
            }
            var foundLocale = r.locale;
            var result = { locale: '', dataLocale: foundLocale };
            var supportedExtension = '-u';
            for (var _i = 0, relevantExtensionKeys_1 = relevantExtensionKeys; _i < relevantExtensionKeys_1.length; _i++) {
                var key = relevantExtensionKeys_1[_i];
                var foundLocaleData = localeData[foundLocale];
                invariant(typeof foundLocaleData === 'object' && foundLocaleData !== null, "locale data " + key + " must be an object");
                var keyLocaleData = foundLocaleData[key];
                invariant(Array.isArray(keyLocaleData), "keyLocaleData for " + key + " must be an array");
                var value = keyLocaleData[0];
                invariant(typeof value === 'string' || value === null, "value must be string or null but got " + typeof value + " in key " + key);
                var supportedExtensionAddition = '';
                if (r.extension) {
                    var requestedValue = unicodeExtensionValue(r.extension, key);
                    if (requestedValue !== undefined) {
                        if (requestedValue !== '') {
                            if (~keyLocaleData.indexOf(requestedValue)) {
                                value = requestedValue;
                                supportedExtensionAddition = "-" + key + "-" + value;
                            }
                        }
                        else if (~requestedValue.indexOf('true')) {
                            value = 'true';
                            supportedExtensionAddition = "-" + key;
                        }
                    }
                }
                if (key in options) {
                    var optionsValue = options[key];
                    invariant(typeof optionsValue === 'string' ||
                        typeof optionsValue === 'undefined' ||
                        optionsValue === null, 'optionsValue must be String, Undefined or Null');
                    if (~keyLocaleData.indexOf(optionsValue)) {
                        if (optionsValue !== value) {
                            value = optionsValue;
                            supportedExtensionAddition = '';
                        }
                    }
                }
                result[key] = value;
                supportedExtension += supportedExtensionAddition;
            }
            if (supportedExtension.length > 2) {
                var privateIndex = foundLocale.indexOf('-x-');
                if (privateIndex === -1) {
                    foundLocale = foundLocale + supportedExtension;
                }
                else {
                    var preExtension = foundLocale.slice(0, privateIndex);
                    var postExtension = foundLocale.slice(privateIndex, foundLocale.length);
                    foundLocale = preExtension + supportedExtension + postExtension;
                }
                foundLocale = Intl.getCanonicalLocales(foundLocale)[0];
            }
            result.locale = foundLocale;
            return result;
        };
    }
    /**
     * https://tc39.es/ecma402/#sec-unicodeextensionvalue
     * @param extension
     * @param key
     */
    function unicodeExtensionValue(extension, key) {
        invariant(key.length === 2, 'key must have 2 elements');
        var size = extension.length;
        var searchValue = "-" + key + "-";
        var pos = extension.indexOf(searchValue);
        if (pos !== -1) {
            var start = pos + 4;
            var end = start;
            var k = start;
            var done = false;
            while (!done) {
                var e = extension.indexOf('-', k);
                var len = void 0;
                if (e === -1) {
                    len = size - k;
                }
                else {
                    len = e - k;
                }
                if (len === 2) {
                    done = true;
                }
                else if (e === -1) {
                    end = size;
                    done = true;
                }
                else {
                    end = e;
                    k = e + 1;
                }
            }
            return extension.slice(start, end);
        }
        searchValue = "-" + key;
        pos = extension.indexOf(searchValue);
        if (pos !== -1 && pos + 3 === size) {
            return '';
        }
        return undefined;
    }
    var UNICODE_EXTENSION_SEQUENCE_REGEX = /-u(?:-[0-9a-z]{2,8})+/gi;
    /**
     * https://tc39.es/ecma402/#sec-bestavailablelocale
     * @param availableLocales
     * @param locale
     */
    function bestAvailableLocale(availableLocales, locale) {
        var candidate = locale;
        while (true) {
            if (~availableLocales.indexOf(candidate)) {
                return candidate;
            }
            var pos = candidate.lastIndexOf('-');
            if (!~pos) {
                return undefined;
            }
            if (pos >= 2 && candidate[pos - 2] === '-') {
                pos -= 2;
            }
            candidate = candidate.slice(0, pos);
        }
    }
    function createLookupMatcher(getDefaultLocale) {
        /**
         * https://tc39.es/ecma402/#sec-lookupmatcher
         */
        return function lookupMatcher(availableLocales, requestedLocales) {
            var result = { locale: '' };
            for (var _i = 0, requestedLocales_1 = requestedLocales; _i < requestedLocales_1.length; _i++) {
                var locale = requestedLocales_1[_i];
                var noExtensionLocale = locale.replace(UNICODE_EXTENSION_SEQUENCE_REGEX, '');
                var availableLocale = bestAvailableLocale(availableLocales, noExtensionLocale);
                if (availableLocale) {
                    result.locale = availableLocale;
                    if (locale !== noExtensionLocale) {
                        result.extension = locale.slice(noExtensionLocale.length + 1, locale.length);
                    }
                    return result;
                }
            }
            result.locale = getDefaultLocale();
            return result;
        };
    }
    function createBestFitMatcher(getDefaultLocale) {
        return function bestFitMatcher(availableLocales, requestedLocales) {
            var result = { locale: '' };
            for (var _i = 0, requestedLocales_2 = requestedLocales; _i < requestedLocales_2.length; _i++) {
                var locale = requestedLocales_2[_i];
                var noExtensionLocale = locale.replace(UNICODE_EXTENSION_SEQUENCE_REGEX, '');
                var availableLocale = bestAvailableLocale(availableLocales, noExtensionLocale);
                if (availableLocale) {
                    result.locale = availableLocale;
                    if (locale !== noExtensionLocale) {
                        result.extension = locale.slice(noExtensionLocale.length + 1, locale.length);
                    }
                    return result;
                }
            }
            result.locale = getDefaultLocale();
            return result;
        };
    }
    function getLocaleHierarchy(locale) {
        var results = [locale];
        var localeParts = locale.split('-');
        for (var i = localeParts.length; i > 1; i--) {
            results.push(localeParts.slice(0, i - 1).join('-'));
        }
        return results;
    }
    function lookupSupportedLocales(availableLocales, requestedLocales) {
        var subset = [];
        for (var _i = 0, requestedLocales_3 = requestedLocales; _i < requestedLocales_3.length; _i++) {
            var locale = requestedLocales_3[_i];
            var noExtensionLocale = locale.replace(UNICODE_EXTENSION_SEQUENCE_REGEX, '');
            var availableLocale = bestAvailableLocale(availableLocales, noExtensionLocale);
            if (availableLocale) {
                subset.push(availableLocale);
            }
        }
        return subset;
    }
    function supportedLocales(availableLocales, requestedLocales, options) {
        var matcher = 'best fit';
        if (options !== undefined) {
            options = toObject(options);
            matcher = getOption(options, 'localeMatcher', 'string', ['lookup', 'best fit'], 'best fit');
        }
        if (matcher === 'best fit') {
            return lookupSupportedLocales(availableLocales, requestedLocales);
        }
        return lookupSupportedLocales(availableLocales, requestedLocales);
    }
    var MissingLocaleDataError = /** @class */ (function (_super) {
        __extends(MissingLocaleDataError, _super);
        function MissingLocaleDataError() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.type = 'MISSING_LOCALE_DATA';
            return _this;
        }
        return MissingLocaleDataError;
    }(Error));
    function unpackData(locale, localeData, 
    /** By default shallow merge the dictionaries. */
    reducer) {
        if (reducer === void 0) { reducer = function (all, d) { return (__assign(__assign({}, all), d)); }; }
        var localeHierarchy = getLocaleHierarchy(locale);
        var dataToMerge = localeHierarchy
            .map(function (l) { return localeData.data[l]; })
            .filter(Boolean);
        if (!dataToMerge.length) {
            throw new MissingLocaleDataError("Missing locale data for \"" + locale + "\", lookup hierarchy: " + localeHierarchy.join(', '));
        }
        dataToMerge.reverse();
        return dataToMerge.reduce(reducer, {});
    }

    var internalSlotMap = new WeakMap();
    function getInternalSlots(x) {
        var internalSlots = internalSlotMap.get(x);
        if (!internalSlots) {
            internalSlots = Object.create(null);
            internalSlotMap.set(x, internalSlots);
        }
        return internalSlots;
    }

    // @generated
    // prettier-ignore
    var links = { "Africa/Asmera": "Africa/Nairobi", "Africa/Timbuktu": "Africa/Abidjan", "America/Argentina/ComodRivadavia": "America/Argentina/Catamarca", "America/Atka": "America/Adak", "America/Buenos_Aires": "America/Argentina/Buenos_Aires", "America/Catamarca": "America/Argentina/Catamarca", "America/Coral_Harbour": "America/Atikokan", "America/Cordoba": "America/Argentina/Cordoba", "America/Ensenada": "America/Tijuana", "America/Fort_Wayne": "America/Indiana/Indianapolis", "America/Godthab": "America/Nuuk", "America/Indianapolis": "America/Indiana/Indianapolis", "America/Jujuy": "America/Argentina/Jujuy", "America/Knox_IN": "America/Indiana/Knox", "America/Louisville": "America/Kentucky/Louisville", "America/Mendoza": "America/Argentina/Mendoza", "America/Montreal": "America/Toronto", "America/Porto_Acre": "America/Rio_Branco", "America/Rosario": "America/Argentina/Cordoba", "America/Santa_Isabel": "America/Tijuana", "America/Shiprock": "America/Denver", "America/Virgin": "America/Port_of_Spain", "Antarctica/South_Pole": "Pacific/Auckland", "Asia/Ashkhabad": "Asia/Ashgabat", "Asia/Calcutta": "Asia/Kolkata", "Asia/Chongqing": "Asia/Shanghai", "Asia/Chungking": "Asia/Shanghai", "Asia/Dacca": "Asia/Dhaka", "Asia/Harbin": "Asia/Shanghai", "Asia/Kashgar": "Asia/Urumqi", "Asia/Katmandu": "Asia/Kathmandu", "Asia/Macao": "Asia/Macau", "Asia/Rangoon": "Asia/Yangon", "Asia/Saigon": "Asia/Ho_Chi_Minh", "Asia/Tel_Aviv": "Asia/Jerusalem", "Asia/Thimbu": "Asia/Thimphu", "Asia/Ujung_Pandang": "Asia/Makassar", "Asia/Ulan_Bator": "Asia/Ulaanbaatar", "Atlantic/Faeroe": "Atlantic/Faroe", "Atlantic/Jan_Mayen": "Europe/Oslo", "Australia/ACT": "Australia/Sydney", "Australia/Canberra": "Australia/Sydney", "Australia/LHI": "Australia/Lord_Howe", "Australia/NSW": "Australia/Sydney", "Australia/North": "Australia/Darwin", "Australia/Queensland": "Australia/Brisbane", "Australia/South": "Australia/Adelaide", "Australia/Tasmania": "Australia/Hobart", "Australia/Victoria": "Australia/Melbourne", "Australia/West": "Australia/Perth", "Australia/Yancowinna": "Australia/Broken_Hill", "Brazil/Acre": "America/Rio_Branco", "Brazil/DeNoronha": "America/Noronha", "Brazil/East": "America/Sao_Paulo", "Brazil/West": "America/Manaus", "Canada/Atlantic": "America/Halifax", "Canada/Central": "America/Winnipeg", "Canada/Eastern": "America/Toronto", "Canada/Mountain": "America/Edmonton", "Canada/Newfoundland": "America/St_Johns", "Canada/Pacific": "America/Vancouver", "Canada/Saskatchewan": "America/Regina", "Canada/Yukon": "America/Whitehorse", "Chile/Continental": "America/Santiago", "Chile/EasterIsland": "Pacific/Easter", "Cuba": "America/Havana", "Egypt": "Africa/Cairo", "Eire": "Europe/Dublin", "Etc/UCT": "Etc/UTC", "Europe/Belfast": "Europe/London", "Europe/Tiraspol": "Europe/Chisinau", "GB": "Europe/London", "GB-Eire": "Europe/London", "GMT+0": "Etc/GMT", "GMT-0": "Etc/GMT", "GMT0": "Etc/GMT", "Greenwich": "Etc/GMT", "Hongkong": "Asia/Hong_Kong", "Iceland": "Atlantic/Reykjavik", "Iran": "Asia/Tehran", "Israel": "Asia/Jerusalem", "Jamaica": "America/Jamaica", "Japan": "Asia/Tokyo", "Kwajalein": "Pacific/Kwajalein", "Libya": "Africa/Tripoli", "Mexico/BajaNorte": "America/Tijuana", "Mexico/BajaSur": "America/Mazatlan", "Mexico/General": "America/Mexico_City", "NZ": "Pacific/Auckland", "NZ-CHAT": "Pacific/Chatham", "Navajo": "America/Denver", "PRC": "Asia/Shanghai", "Pacific/Johnston": "Pacific/Honolulu", "Pacific/Ponape": "Pacific/Pohnpei", "Pacific/Samoa": "Pacific/Pago_Pago", "Pacific/Truk": "Pacific/Chuuk", "Pacific/Yap": "Pacific/Chuuk", "Poland": "Europe/Warsaw", "Portugal": "Europe/Lisbon", "ROC": "Asia/Taipei", "ROK": "Asia/Seoul", "Singapore": "Asia/Singapore", "Turkey": "Europe/Istanbul", "UCT": "Etc/UTC", "US/Alaska": "America/Anchorage", "US/Aleutian": "America/Adak", "US/Arizona": "America/Phoenix", "US/Central": "America/Chicago", "US/East-Indiana": "America/Indiana/Indianapolis", "US/Eastern": "America/New_York", "US/Hawaii": "Pacific/Honolulu", "US/Indiana-Starke": "America/Indiana/Knox", "US/Michigan": "America/Detroit", "US/Mountain": "America/Denver", "US/Pacific": "America/Los_Angeles", "US/Samoa": "Pacific/Pago_Pago", "UTC": "Etc/UTC", "Universal": "Etc/UTC", "W-SU": "Europe/Moscow", "Zulu": "Etc/UTC" };

    var __spreadArrays = (undefined && undefined.__spreadArrays) || function () {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };
    function unpack(data) {
        var abbrvs = data.abbrvs.split('|');
        var offsets = data.offsets.split('|').map(function (n) { return parseInt(n, 36); });
        var packedZones = data.zones;
        var zones = {};
        for (var _i = 0, packedZones_1 = packedZones; _i < packedZones_1.length; _i++) {
            var d = packedZones_1[_i];
            var _a = d.split('|'), zone = _a[0], zoneData = _a.slice(1);
            zones[zone] = zoneData
                .map(function (z) { return z.split(','); })
                .map(function (_a) {
                var ts = _a[0], abbrvIndex = _a[1], offsetIndex = _a[2], dst = _a[3];
                return [
                    ts === '' ? -Infinity : parseInt(ts, 36),
                    abbrvs[+abbrvIndex],
                    offsets[+offsetIndex],
                    dst === '1',
                ];
            });
        }
        return zones;
    }

    /**
     * https://unicode.org/reports/tr35/tr35-dates.html#Date_Field_Symbol_Table
     * Credit: https://github.com/caridy/intl-datetimeformat-pattern/blob/master/index.js
     * with some tweaks
     */
    var DATE_TIME_REGEX = /(?:[Eec]{1,6}|G{1,5}|[Qq]{1,5}|(?:[yYur]+|U{1,5})|[ML]{1,5}|d{1,2}|D{1,3}|F{1}|[abB]{1,5}|[hkHK]{1,2}|w{1,2}|W{1}|m{1,2}|s{1,2}|[zZOvVxX]{1,4})(?=([^']*'[^']*')*[^']*$)/g;
    // trim patterns after transformations
    var expPatternTrimmer = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    /**
     * Parse Date time skeleton into Intl.DateTimeFormatOptions
     * Ref: https://unicode.org/reports/tr35/tr35-dates.html#Date_Field_Symbol_Table
     * @public
     * @param skeleton skeleton string
     */
    function parseDateTimeSkeleton(skeleton) {
        var result = {
            pattern: '',
            pattern12: '',
            skeleton: skeleton,
        };
        var literals = [];
        result.pattern12 = skeleton
            // Double apostrophe
            .replace(/'{2}/g, '{apostrophe}')
            // Apostrophe-escaped
            .replace(/'(.*?)'/g, function (_, literal) {
            literals.push(literal);
            return "$$" + (literals.length - 1) + "$$";
        })
            .replace(DATE_TIME_REGEX, function (match) {
            var len = match.length;
            switch (match[0]) {
                // Era
                case 'G':
                    result.era = len === 4 ? 'long' : len === 5 ? 'narrow' : 'short';
                    return '{era}';
                // Year
                case 'y':
                case 'Y':
                case 'u':
                case 'U':
                case 'r':
                    result.year = len === 2 ? '2-digit' : 'numeric';
                    return '{year}';
                // Quarter
                case 'q':
                case 'Q':
                    return '{quarter}';
                // Month
                case 'M':
                case 'L':
                    result.month = ['numeric', '2-digit', 'short', 'long', 'narrow'][len - 1];
                    return '{month}';
                // Week
                case 'w':
                case 'W':
                    return '{weekday}';
                case 'd':
                    result.day = ['numeric', '2-digit'][len - 1];
                    return '{day}';
                case 'D':
                case 'F':
                case 'g':
                    result.day = 'numeric';
                    return '{day}';
                // Weekday
                case 'E':
                    result.weekday = len === 4 ? 'long' : len === 5 ? 'narrow' : 'short';
                    return '{weekday}';
                case 'e':
                    result.weekday = [
                        'numeric',
                        '2-digit',
                        'short',
                        'long',
                        'narrow',
                        'short',
                    ][len - 1];
                    return '{weekday}';
                case 'c':
                    result.weekday = [
                        'numeric',
                        undefined,
                        'short',
                        'long',
                        'narrow',
                        'short',
                    ][len - 1];
                    return '{weekday}';
                // Period
                case 'a': // AM, PM
                case 'b': // am, pm, noon, midnight
                case 'B': // flexible day periods
                    result.hour12 = true;
                    return '{ampm}';
                // Hour
                case 'h':
                    result.hour = ['numeric', '2-digit'][len - 1];
                    return '{hour}';
                case 'H':
                    result.hour = ['numeric', '2-digit'][len - 1];
                    return '{hour}';
                case 'K':
                    result.hour = ['numeric', '2-digit'][len - 1];
                    return '{hour}';
                case 'k':
                    result.hour = ['numeric', '2-digit'][len - 1];
                    return '{hour}';
                case 'j':
                case 'J':
                case 'C':
                    throw new RangeError('`j/J/C` (hour) patterns are not supported, use `h/H/K/k` instead');
                // Minute
                case 'm':
                    result.minute = ['numeric', '2-digit'][len - 1];
                    return '{minute}';
                // Second
                case 's':
                    result.second = ['numeric', '2-digit'][len - 1];
                    return '{second}';
                case 'S':
                case 'A':
                    result.second = 'numeric';
                    return '{second}';
                // Zone
                case 'z': // 1..3, 4: specific non-location format
                case 'Z': // 1..3, 4, 5: The ISO8601 varios formats
                case 'O': // 1, 4: miliseconds in day short, long
                case 'v': // 1, 4: generic non-location format
                case 'V': // 1, 2, 3, 4: time zone ID or city
                case 'X': // 1, 2, 3, 4: The ISO8601 varios formats
                case 'x': // 1, 2, 3, 4: The ISO8601 varios formats
                    result.timeZoneName = len < 4 ? 'short' : 'long';
                    return '{timeZoneName}';
            }
            return '';
        });
        //Restore literals
        if (literals.length) {
            result.pattern12 = result.pattern12
                .replace(/\$\$(\d+)\$\$/g, function (_, i) {
                return literals[+i];
            })
                .replace(/\{apostrophe\}/g, "'");
        }
        // Handle apostrophe-escaped things
        result.pattern = result.pattern12
            .replace(/([\s\uFEFF\xA0])\{ampm\}([\s\uFEFF\xA0])/, '$1')
            .replace('{ampm}', '')
            .replace(expPatternTrimmer, '');
        return result;
    }

    var __assign$1 = (undefined && undefined.__assign) || function () {
        __assign$1 = Object.assign || function(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                    t[p] = s[p];
            }
            return t;
        };
        return __assign$1.apply(this, arguments);
    };
    var __rest = (undefined && undefined.__rest) || function (s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    };
    var UPPERCASED_LINKS = Object.keys(links).reduce(function (all, l) {
        all[l.toUpperCase()] = links[l];
        return all;
    }, {});
    var DATE_TIME_PROPS = [
        'weekday',
        'era',
        'year',
        'month',
        'day',
        'hour',
        'minute',
        'second',
        'timeZoneName',
    ];
    var RESOLVED_OPTIONS_KEYS = [
        'locale',
        'calendar',
        'numberingSystem',
        'timeZone',
        'hourCycle',
        'weekday',
        'era',
        'year',
        'month',
        'day',
        'hour',
        'minute',
        'second',
        'timeZoneName',
    ];
    var TYPE_REGEX = /^[a-z0-9]{3,8}$/i;
    /**
     * https://tc39.es/ecma402/#sec-isvalidtimezonename
     * @param tz
     */
    function isValidTimeZoneName(tz) {
        var uppercasedTz = tz.toUpperCase();
        var zoneNames = new Set(Object.keys(DateTimeFormat.tzData).map(function (z) { return z.toUpperCase(); }));
        return zoneNames.has(uppercasedTz) || uppercasedTz in UPPERCASED_LINKS;
    }
    /**
     * https://tc39.es/ecma402/#sec-canonicalizetimezonename
     * @param tz
     */
    function canonicalizeTimeZoneName(tz) {
        var uppercasedTz = tz.toUpperCase();
        var uppercasedZones = Object.keys(DateTimeFormat.tzData).reduce(function (all, z) {
            all[z.toUpperCase()] = z;
            return all;
        }, {});
        var ianaTimeZone = UPPERCASED_LINKS[uppercasedTz] || uppercasedZones[uppercasedTz];
        if (ianaTimeZone === 'Etc/UTC' || ianaTimeZone === 'Etc/GMT') {
            return 'UTC';
        }
        return ianaTimeZone;
    }
    /**
     * https://tc39.es/ecma262/#sec-tonumber
     * @param val
     */
    function toNumber(val) {
        if (val === undefined) {
            return NaN;
        }
        if (val === null) {
            return +0;
        }
        if (typeof val === 'boolean') {
            return val ? 1 : +0;
        }
        if (typeof val === 'number') {
            return val;
        }
        if (typeof val === 'symbol' || typeof val === 'bigint') {
            throw new TypeError('Cannot convert symbol/bigint to number');
        }
        return Number(val);
    }
    /**
     * https://tc39.es/ecma262/#sec-tointeger
     * @param n
     */
    function toInteger(n) {
        var number = toNumber(n);
        if (isNaN(number) || objectIs(number, -0)) {
            return 0;
        }
        if (isFinite(number)) {
            return number;
        }
        var integer = Math.floor(Math.abs(number));
        if (number < 0) {
            integer = -integer;
        }
        if (objectIs(integer, -0)) {
            return 0;
        }
        return integer;
    }
    /**
     * https://tc39.es/ecma262/#sec-timeclip
     * @param time
     */
    function timeClip(time) {
        if (!isFinite(time)) {
            return NaN;
        }
        if (Math.abs(time) > 8.64 * 1e16) {
            return NaN;
        }
        return toInteger(time);
    }
    /**
     * https://tc39.es/ecma402/#sec-initializedatetimeformat
     * @param dtf DateTimeFormat
     * @param locales locales
     * @param opts options
     */
    function initializeDateTimeFormat(dtf, locales, opts) {
        // @ts-ignore
        var requestedLocales = Intl.getCanonicalLocales(locales);
        var options = toDateTimeOptions(opts, 'any', 'date');
        var opt = Object.create(null);
        var matcher = getOption(options, 'localeMatcher', 'string', ['lookup', 'best fit'], 'best fit');
        opt.localeMatcher = matcher;
        var calendar = getOption(options, 'calendar', 'string', undefined, undefined);
        if (calendar !== undefined && !TYPE_REGEX.test(calendar)) {
            throw new RangeError('Malformed calendar');
        }
        var internalSlots = getInternalSlots(dtf);
        opt.ca = calendar;
        var numberingSystem = getOption(options, 'numberingSystem', 'string', undefined, undefined);
        if (numberingSystem !== undefined && !TYPE_REGEX.test(numberingSystem)) {
            throw new RangeError('Malformed numbering system');
        }
        opt.nu = numberingSystem;
        var hour12 = getOption(options, 'hour12', 'boolean', undefined, undefined);
        var hourCycle = getOption(options, 'hourCycle', 'string', ['h11', 'h12', 'h23', 'h24'], undefined);
        if (hour12 !== undefined) {
            // @ts-ignore
            hourCycle = null;
        }
        opt.hc = hourCycle;
        var r = createResolveLocale(DateTimeFormat.getDefaultLocale)(DateTimeFormat.availableLocales, requestedLocales, 
        // TODO: Fix the type
        opt, 
        // [[RelevantExtensionKeys]] slot, which is a constant
        ['nu', 'ca', 'hc'], DateTimeFormat.localeData);
        internalSlots.locale = r.locale;
        calendar = r.ca;
        internalSlots.calendar = calendar;
        internalSlots.hourCycle = r.hc;
        internalSlots.numberingSystem = r.nu;
        var dataLocale = r.dataLocale;
        internalSlots.dataLocale = dataLocale;
        var timeZone = options.timeZone;
        if (timeZone !== undefined) {
            timeZone = String(timeZone);
            if (!isValidTimeZoneName(timeZone)) {
                throw new RangeError('Invalid timeZoneName');
            }
            timeZone = canonicalizeTimeZoneName(timeZone);
        }
        else {
            timeZone = DateTimeFormat.__defaultLocale;
        }
        internalSlots.timeZone = timeZone;
        opt = Object.create(null);
        opt.weekday = getOption(options, 'weekday', 'string', ['narrow', 'short', 'long'], undefined);
        opt.era = getOption(options, 'era', 'string', ['narrow', 'short', 'long'], undefined);
        opt.year = getOption(options, 'year', 'string', ['2-digit', 'numeric'], undefined);
        opt.month = getOption(options, 'month', 'string', ['2-digit', 'numeric', 'narrow', 'short', 'long'], undefined);
        opt.day = getOption(options, 'day', 'string', ['2-digit', 'numeric'], undefined);
        opt.hour = getOption(options, 'hour', 'string', ['2-digit', 'numeric'], undefined);
        opt.minute = getOption(options, 'minute', 'string', ['2-digit', 'numeric'], undefined);
        opt.second = getOption(options, 'second', 'string', ['2-digit', 'numeric'], undefined);
        opt.timeZoneName = getOption(options, 'timeZoneName', 'string', ['short', 'long'], undefined);
        var dataLocaleData = DateTimeFormat.localeData[dataLocale];
        var formats = dataLocaleData.formats[calendar];
        matcher = getOption(options, 'formatMatcher', 'string', ['basic', 'best fit'], 'best fit');
        var bestFormat;
        if (matcher === 'basic') {
            bestFormat = basicFormatMatcher(opt, formats);
        }
        else {
            opt.hour12 =
                internalSlots.hourCycle === 'h11' || internalSlots.hourCycle === 'h12';
            bestFormat = bestFitFormatMatcher(opt, formats);
        }
        for (var prop in opt) {
            var p = bestFormat[prop];
            if (p !== undefined) {
                internalSlots[prop] = p;
            }
        }
        var pattern;
        if (internalSlots.hour !== undefined) {
            var hcDefault = dataLocaleData.hourCycle;
            var hc = internalSlots.hourCycle;
            if (hc == null) {
                hc = hcDefault;
            }
            if (hour12 !== undefined) {
                if (hour12) {
                    if (hcDefault === 'h11' || hcDefault === 'h23') {
                        hc = 'h11';
                    }
                    else {
                        hc = 'h12';
                    }
                }
                else {
                    invariant(!hour12, 'hour12 must not be set');
                    if (hcDefault === 'h11' || hcDefault === 'h23') {
                        hc = 'h23';
                    }
                    else {
                        hc = 'h24';
                    }
                }
            }
            internalSlots.hourCycle = hc;
            if (hc === 'h11' || hc === 'h12') {
                pattern = bestFormat.pattern12;
            }
            else {
                pattern = bestFormat.pattern;
            }
        }
        else {
            // @ts-ignore
            internalSlots.hourCycle = undefined;
            pattern = bestFormat.pattern;
        }
        internalSlots.pattern = pattern;
        return dtf;
    }
    /**
     * https://tc39.es/ecma402/#sec-todatetimeoptions
     * @param options
     * @param required
     * @param defaults
     */
    function toDateTimeOptions(options, required, defaults) {
        if (options === undefined) {
            options = null;
        }
        else {
            options = toObject(options);
        }
        options = Object.create(options);
        var needDefaults = true;
        if (required === 'date' || required === 'any') {
            for (var _i = 0, _a = ['weekday', 'year', 'month', 'day']; _i < _a.length; _i++) {
                var prop = _a[_i];
                var value = options[prop];
                if (value !== undefined) {
                    needDefaults = false;
                }
            }
        }
        if (required === 'time' || required === 'any') {
            for (var _b = 0, _c = ['hour', 'minute', 'second']; _b < _c.length; _b++) {
                var prop = _c[_b];
                var value = options[prop];
                if (value !== undefined) {
                    needDefaults = false;
                }
            }
        }
        if (needDefaults && (defaults === 'date' || defaults === 'all')) {
            for (var _d = 0, _e = ['year', 'month', 'day']; _d < _e.length; _d++) {
                var prop = _e[_d];
                options[prop] = 'numeric';
            }
        }
        if (needDefaults && (defaults === 'time' || defaults === 'all')) {
            for (var _f = 0, _g = ['hour', 'minute', 'second']; _f < _g.length; _f++) {
                var prop = _g[_f];
                options[prop] = 'numeric';
            }
        }
        return options;
    }
    var BASIC_FORMAT_MATCHER_VALUES = [
        '2-digit',
        'numeric',
        'narrow',
        'short',
        'long',
    ];
    var removalPenalty = 120;
    var additionPenalty = 20;
    var longLessPenalty = 8;
    var longMorePenalty = 6;
    var shortLessPenalty = 6;
    var shortMorePenalty = 3;
    function basicFormatMatcherScore(options, format) {
        var score = 0;
        for (var _i = 0, DATE_TIME_PROPS_1 = DATE_TIME_PROPS; _i < DATE_TIME_PROPS_1.length; _i++) {
            var prop = DATE_TIME_PROPS_1[_i];
            var optionsProp = options[prop];
            var formatProp = format[prop];
            if (optionsProp === undefined && formatProp !== undefined) {
                score -= additionPenalty;
            }
            else if (optionsProp !== undefined && formatProp === undefined) {
                score -= removalPenalty;
            }
            else if (optionsProp !== formatProp) {
                var optionsPropIndex = BASIC_FORMAT_MATCHER_VALUES.indexOf(optionsProp);
                var formatPropIndex = BASIC_FORMAT_MATCHER_VALUES.indexOf(formatProp);
                var delta = Math.max(-2, Math.min(formatPropIndex - optionsPropIndex, 2));
                if (delta === 2) {
                    score -= longMorePenalty;
                }
                else if (delta === 1) {
                    score -= shortMorePenalty;
                }
                else if (delta === -1) {
                    score -= shortLessPenalty;
                }
                else if (delta === -2) {
                    score -= longLessPenalty;
                }
            }
        }
        return score;
    }
    /**
     * Credit: https://github.com/andyearnshaw/Intl.js/blob/0958dc1ad8153f1056653ea22b8208f0df289a4e/src/12.datetimeformat.js#L611
     * with some modifications
     * @param options
     * @param format
     */
    function bestFitFormatMatcherScore(options, format) {
        var score = 0;
        if (options.hour12 && !format.hour12) {
            score -= removalPenalty;
        }
        else if (!options.hour12 && format.hour12) {
            score -= additionPenalty;
        }
        for (var _i = 0, DATE_TIME_PROPS_2 = DATE_TIME_PROPS; _i < DATE_TIME_PROPS_2.length; _i++) {
            var prop = DATE_TIME_PROPS_2[_i];
            var optionsProp = options[prop];
            var formatProp = format[prop];
            if (optionsProp === undefined && formatProp !== undefined) {
                score -= additionPenalty;
            }
            else if (optionsProp !== undefined && formatProp === undefined) {
                score -= removalPenalty;
            }
            else if (optionsProp !== formatProp) {
                var optionsPropIndex = BASIC_FORMAT_MATCHER_VALUES.indexOf(optionsProp);
                var formatPropIndex = BASIC_FORMAT_MATCHER_VALUES.indexOf(formatProp);
                var delta = Math.max(-2, Math.min(formatPropIndex - optionsPropIndex, 2));
                if (delta === 2) {
                    score -= longMorePenalty;
                }
                else if (delta === 1) {
                    score -= shortMorePenalty;
                }
                else if (delta === -1) {
                    score -= shortLessPenalty;
                }
                else if (delta === -2) {
                    score -= longLessPenalty;
                }
            }
        }
        return score;
    }
    /**
     * https://tc39.es/ecma402/#sec-basicformatmatcher
     * @param options
     * @param formats
     */
    function basicFormatMatcher(options, formats) {
        var bestScore = -Infinity;
        var bestFormat = formats[0];
        invariant(Array.isArray(formats), 'formats should be a list of things');
        for (var _i = 0, formats_1 = formats; _i < formats_1.length; _i++) {
            var format = formats_1[_i];
            var score = basicFormatMatcherScore(options, format);
            if (score > bestScore) {
                bestScore = score;
                bestFormat = format;
            }
        }
        return __assign$1({}, bestFormat);
    }
    function isNumericType(t) {
        return t === 'numeric' || t === '2-digit';
    }
    /**
     * https://tc39.es/ecma402/#sec-bestfitformatmatcher
     * Just alias to basic for now
     * @param options
     * @param formats
     */
    function bestFitFormatMatcher(options, formats) {
        var bestScore = -Infinity;
        var bestFormat = formats[0];
        invariant(Array.isArray(formats), 'formats should be a list of things');
        for (var _i = 0, formats_2 = formats; _i < formats_2.length; _i++) {
            var format = formats_2[_i];
            var score = bestFitFormatMatcherScore(options, format);
            if (score > bestScore) {
                bestScore = score;
                bestFormat = format;
            }
        }
        bestFormat = __assign$1({}, bestFormat);
        // Kinda following https://github.com/unicode-org/icu/blob/dd50e38f459d84e9bf1b0c618be8483d318458ad/icu4j/main/classes/core/src/com/ibm/icu/text/DateTimePatternGenerator.java
        for (var prop in bestFormat) {
            var bestValue = bestFormat[prop];
            var inputValue = options[prop];
            // Don't mess with minute/second or we can get in the situation of
            // 7:0:0 which is weird
            if (prop === 'minute' || prop === 'second') {
                continue;
            }
            // Nothing to do here
            if (!inputValue) {
                continue;
            }
            // https://unicode.org/reports/tr35/tr35-dates.html#Matching_Skeletons
            // Looks like we should not convert numeric to alphabetic but the other way
            // around is ok
            if (isNumericType(bestValue) &&
                !isNumericType(inputValue)) {
                continue;
            }
            // Otherwise use the input value
            bestFormat[prop] = inputValue;
        }
        return bestFormat;
    }
    var formatDescriptor = {
        enumerable: false,
        configurable: true,
        get: function () {
            if (typeof this !== 'object' || !(this instanceof DateTimeFormat)) {
                throw TypeError('Intl.DateTimeFormat format property accessor called on incompatible receiver');
            }
            var internalSlots = getInternalSlots(this);
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            var dtf = this;
            var boundFormat = internalSlots.boundFormat;
            if (boundFormat === undefined) {
                // https://tc39.es/proposal-unified-intl-numberformat/section11/numberformat_diff_out.html#sec-number-format-functions
                boundFormat = function (date) {
                    var x;
                    if (date === undefined) {
                        x = Date.now();
                    }
                    else {
                        x = Number(date);
                    }
                    return formatDateTime(dtf, x);
                };
                try {
                    // https://github.com/tc39/test262/blob/master/test/intl402/NumberFormat/prototype/format/format-function-name.js
                    Object.defineProperty(boundFormat, 'name', {
                        configurable: true,
                        enumerable: false,
                        writable: false,
                        value: '',
                    });
                }
                catch (e) {
                    // In older browser (e.g Chrome 36 like polyfill.io)
                    // TypeError: Cannot redefine property: name
                }
                internalSlots.boundFormat = boundFormat;
            }
            return boundFormat;
        },
    };
    try {
        // https://github.com/tc39/test262/blob/master/test/intl402/NumberFormat/prototype/format/name.js
        Object.defineProperty(formatDescriptor.get, 'name', {
            configurable: true,
            enumerable: false,
            writable: false,
            value: 'get format',
        });
    }
    catch (e) {
        // In older browser (e.g Chrome 36 like polyfill.io)
        // TypeError: Cannot redefine property: name
    }
    function pad(n) {
        if (n < 10) {
            return "0" + n;
        }
        return String(n);
    }
    function offsetToGmtString(gmtFormat, hourFormat, offsetInMs, style) {
        var offsetInMinutes = Math.floor(offsetInMs / 60000);
        var mins = Math.abs(offsetInMinutes) % 60;
        var hours = Math.floor(Math.abs(offsetInMinutes) / 60);
        var _a = hourFormat.split(';'), positivePattern = _a[0], negativePattern = _a[1];
        var offsetStr = '';
        var pattern = offsetInMs < 0 ? negativePattern : positivePattern;
        if (style === 'long') {
            offsetStr = pattern
                .replace('HH', pad(hours))
                .replace('H', String(hours))
                .replace('mm', pad(mins))
                .replace('m', String(mins));
        }
        else if (mins || hours) {
            if (!mins) {
                pattern = pattern.replace(/:?m+/, '');
            }
            offsetStr = pattern
                .replace(/H+/, String(hours))
                .replace(/m+/, String(mins));
        }
        return gmtFormat.replace('{0}', offsetStr);
    }
    /**
     * https://tc39.es/ecma402/#sec-partitiondatetimepattern
     * @param dtf
     * @param x
     */
    function partitionDateTimePattern(dtf, x) {
        x = timeClip(x);
        if (isNaN(x)) {
            throw new RangeError('invalid time');
        }
        /** IMPL START */
        var internalSlots = getInternalSlots(dtf);
        var dataLocale = internalSlots.dataLocale;
        var dataLocaleData = DateTimeFormat.localeData[dataLocale];
        /** IMPL END */
        var locale = internalSlots.locale;
        var nfOptions = Object.create(null);
        nfOptions.useGrouping = false;
        var nf = new Intl.NumberFormat(locale, nfOptions);
        var nf2Options = Object.create(null);
        nf2Options.minimumIntegerDigits = 2;
        nf2Options.useGrouping = false;
        var nf2 = new Intl.NumberFormat(locale, nf2Options);
        var tm = toLocalTime(x, 
        // @ts-ignore
        internalSlots.calendar, internalSlots.timeZone);
        var result = [];
        var patternParts = partitionPattern(internalSlots.pattern);
        for (var _i = 0, patternParts_1 = patternParts; _i < patternParts_1.length; _i++) {
            var patternPart = patternParts_1[_i];
            var p = patternPart.type;
            if (p === 'literal') {
                result.push({
                    type: 'literal',
                    value: patternPart.value,
                });
            }
            else if (DATE_TIME_PROPS.indexOf(p) > -1) {
                var fv = '';
                var f = internalSlots[p];
                // @ts-ignore
                var v = tm[p];
                if (p === 'year' && v <= 0) {
                    v = 1 - v;
                }
                if (p === 'month') {
                    v++;
                }
                var hourCycle = internalSlots.hourCycle;
                if (p === 'hour' && (hourCycle === 'h11' || hourCycle === 'h12')) {
                    v = v % 12;
                    if (v === 0 && hourCycle === 'h12') {
                        v = 12;
                    }
                }
                if (p === 'hour' && hourCycle === 'h24') {
                    if (v === 0) {
                        v = 24;
                    }
                }
                if (f === 'numeric') {
                    fv = nf.format(v);
                }
                else if (f === '2-digit') {
                    fv = nf2.format(v);
                    if (fv.length > 2) {
                        fv = fv.slice(fv.length - 2, fv.length);
                    }
                }
                else if (f === 'narrow' || f === 'short' || f === 'long') {
                    if (p === 'era') {
                        fv = dataLocaleData[p][f][v];
                    }
                    else if (p === 'timeZoneName') {
                        var timeZoneName = dataLocaleData.timeZoneName, gmtFormat = dataLocaleData.gmtFormat, hourFormat = dataLocaleData.hourFormat;
                        var timeZone = internalSlots.timeZone || DateTimeFormat.__defaultTimeZone;
                        var timeZoneData = timeZoneName[timeZone];
                        if (timeZoneData && timeZoneData[f]) {
                            fv = timeZoneData[f][+tm.inDST];
                        }
                        else {
                            // Fallback to gmtFormat
                            fv = offsetToGmtString(gmtFormat, hourFormat, tm.timeZoneOffset, f);
                        }
                    }
                    else if (p === 'month') {
                        fv = dataLocaleData.month[f][v - 1];
                    }
                    else {
                        fv = dataLocaleData[p][f][v];
                    }
                }
                result.push({
                    type: p,
                    value: fv,
                });
            }
            else if (p === 'ampm') {
                var v = tm.hour;
                var fv = void 0;
                if (v >= 11) {
                    fv = dataLocaleData.pm;
                }
                else {
                    fv = dataLocaleData.am;
                }
                result.push({
                    type: 'dayPeriod',
                    value: fv,
                });
            }
            else if (p === 'relatedYear') {
                var v = tm.relatedYear;
                // @ts-ignore
                var fv = nf.format(v);
                result.push({
                    type: 'relatedYear',
                    value: fv,
                });
            }
            else if (p === 'yearName') {
                var v = tm.yearName;
                // @ts-ignore
                var fv = nf.format(v);
                result.push({
                    type: 'yearName',
                    value: fv,
                });
            }
            else {
                result.push({
                    type: 'unknown',
                    value: x,
                });
            }
        }
        return result;
    }
    /**
     * https://tc39.es/ecma402/#sec-formatdatetime
     * @param dtf DateTimeFormat
     * @param x
     */
    function formatDateTime(dtf, x) {
        var parts = partitionDateTimePattern(dtf, x);
        var result = '';
        for (var _i = 0, parts_1 = parts; _i < parts_1.length; _i++) {
            var part = parts_1[_i];
            result += part.value;
        }
        return result;
    }
    /**
     * https://tc39.es/ecma402/#sec-formatdatetimetoparts
     * @param dtf DateTimeFormat
     * @param x
     */
    function formatDateTimeParts(dtf, x) {
        return partitionDateTimePattern(dtf, x);
    }
    var MS_PER_DAY = 86400000;
    /**
     * https://www.ecma-international.org/ecma-262/11.0/index.html#eqn-modulo
     * @param x
     * @param y
     * @return k of the same sign as y
     */
    function mod(x, y) {
        return x - Math.floor(x / y) * y;
    }
    /**
     * https://tc39.es/ecma262/#eqn-Day
     * @param t
     */
    function day(t) {
        return Math.floor(t / MS_PER_DAY);
    }
    /**
     * https://tc39.es/ecma262/#sec-week-day
     * @param t
     */
    function weekDay(t) {
        return mod(day(t) + 4, 7);
    }
    function dayFromYear(y) {
        return (365 * (y - 1970) +
            Math.floor((y - 1969) / 4) -
            Math.floor((y - 1901) / 100) +
            Math.floor((y - 1601) / 400));
    }
    function timeFromYear(y) {
        return MS_PER_DAY * dayFromYear(y);
    }
    function yearFromTime(t) {
        var min = Math.ceil(t / MS_PER_DAY / 366);
        var y = min;
        while (timeFromYear(y) <= t) {
            y++;
        }
        return y - 1;
    }
    function daysInYear(y) {
        if (y % 4 !== 0) {
            return 365;
        }
        if (y % 100 !== 0) {
            return 366;
        }
        if (y % 400 !== 0) {
            return 365;
        }
        return 366;
    }
    function dayWithinYear(t) {
        return day(t) - dayFromYear(yearFromTime(t));
    }
    function inLeapYear(t) {
        return daysInYear(yearFromTime(t)) === 365 ? 0 : 1;
    }
    function monthFromTime(t) {
        var dwy = dayWithinYear(t);
        var leap = inLeapYear(t);
        if (dwy >= 0 && dwy < 31) {
            return 0;
        }
        if (dwy < 59 + leap) {
            return 1;
        }
        if (dwy < 90 + leap) {
            return 2;
        }
        if (dwy < 120 + leap) {
            return 3;
        }
        if (dwy < 151 + leap) {
            return 4;
        }
        if (dwy < 181 + leap) {
            return 5;
        }
        if (dwy < 212 + leap) {
            return 6;
        }
        if (dwy < 243 + leap) {
            return 7;
        }
        if (dwy < 273 + leap) {
            return 8;
        }
        if (dwy < 304 + leap) {
            return 9;
        }
        if (dwy < 334 + leap) {
            return 10;
        }
        if (dwy < 365 + leap) {
            return 11;
        }
        throw new Error('Invalid time');
    }
    function dateFromTime(t) {
        var dwy = dayWithinYear(t);
        var mft = monthFromTime(t);
        var leap = inLeapYear(t);
        if (mft === 0) {
            return dwy + 1;
        }
        if (mft === 1) {
            return dwy - 30;
        }
        if (mft === 2) {
            return dwy - 58 - leap;
        }
        if (mft === 3) {
            return dwy - 89 - leap;
        }
        if (mft === 4) {
            return dwy - 119 - leap;
        }
        if (mft === 5) {
            return dwy - 150 - leap;
        }
        if (mft === 6) {
            return dwy - 180 - leap;
        }
        if (mft === 7) {
            return dwy - 211 - leap;
        }
        if (mft === 8) {
            return dwy - 242 - leap;
        }
        if (mft === 9) {
            return dwy - 272 - leap;
        }
        if (mft === 10) {
            return dwy - 303 - leap;
        }
        if (mft === 11) {
            return dwy - 333 - leap;
        }
        throw new Error('Invalid time');
    }
    var HOURS_PER_DAY = 24;
    var MINUTES_PER_HOUR = 60;
    var SECONDS_PER_MINUTE = 60;
    var MS_PER_SECOND = 1e3;
    var MS_PER_MINUTE = MS_PER_SECOND * SECONDS_PER_MINUTE;
    var MS_PER_HOUR = MS_PER_MINUTE * MINUTES_PER_HOUR;
    function hourFromTime(t) {
        return mod(Math.floor(t / MS_PER_HOUR), HOURS_PER_DAY);
    }
    function minFromTime(t) {
        return mod(Math.floor(t / MS_PER_MINUTE), MINUTES_PER_HOUR);
    }
    function secFromTime(t) {
        return mod(Math.floor(t / MS_PER_SECOND), SECONDS_PER_MINUTE);
    }
    function getApplicableZoneData(t, timeZone) {
        var tzData = DateTimeFormat.tzData;
        var zoneData = tzData[timeZone];
        // We don't have data for this so just say it's UTC
        if (!zoneData) {
            return [0, false];
        }
        for (var i = 0; i < zoneData.length; i++) {
            if (zoneData[i][0] * 1e3 >= t) {
                var _a = zoneData[i - 1], offset = _a[2], dst = _a[3];
                return [offset * 1e3, dst];
            }
        }
        return [0, false];
    }
    function toLocalTime(t, calendar, timeZone) {
        invariant(typeof t === 'number', 'invalid time');
        invariant(calendar === 'gregory', 'We only support Gregory calendar right now');
        var _a = getApplicableZoneData(t, timeZone), timeZoneOffset = _a[0], inDST = _a[1];
        var tz = t + timeZoneOffset;
        var year = yearFromTime(tz);
        return {
            weekday: weekDay(tz),
            era: year < 0 ? 'BC' : 'AD',
            year: year,
            relatedYear: undefined,
            yearName: undefined,
            month: monthFromTime(tz),
            day: dateFromTime(tz),
            hour: hourFromTime(tz),
            minute: minFromTime(tz),
            second: secFromTime(tz),
            inDST: inDST,
            // IMPORTANT: Not in spec
            timeZoneOffset: timeZoneOffset,
        };
    }
    var DateTimeFormat = function (locales, options) {
        // Cannot use `new.target` bc of IE11 & TS transpiles it to something else
        if (!this || !(this instanceof DateTimeFormat)) {
            return new DateTimeFormat(locales, options);
        }
        initializeDateTimeFormat(this, locales, options);
        /** IMPL START */
        var internalSlots = getInternalSlots(this);
        var dataLocale = internalSlots.dataLocale;
        var dataLocaleData = DateTimeFormat.localeData[dataLocale];
        invariant(dataLocaleData !== undefined, "Cannot load locale-dependent data for " + dataLocale + ".");
        /** IMPL END */
    };
    // Static properties
    defineProperty(DateTimeFormat, 'supportedLocalesOf', {
        value: function supportedLocalesOf(locales, options) {
            return supportedLocales(DateTimeFormat.availableLocales, Intl.getCanonicalLocales(locales), options);
        },
    });
    defineProperty(DateTimeFormat.prototype, 'resolvedOptions', {
        value: function resolvedOptions() {
            if (typeof this !== 'object' || !(this instanceof DateTimeFormat)) {
                throw TypeError('Method Intl.DateTimeFormat.prototype.resolvedOptions called on incompatible receiver');
            }
            var internalSlots = getInternalSlots(this);
            var ro = {};
            for (var _i = 0, RESOLVED_OPTIONS_KEYS_1 = RESOLVED_OPTIONS_KEYS; _i < RESOLVED_OPTIONS_KEYS_1.length; _i++) {
                var key = RESOLVED_OPTIONS_KEYS_1[_i];
                var value = internalSlots[key];
                if (key === 'hourCycle') {
                    ro.hour12 =
                        value === 'h11' || value === 'h12'
                            ? true
                            : value === 'h23' || value === 'h24'
                                ? false
                                : undefined;
                }
                if (value !== undefined) {
                    ro[key] = value;
                }
            }
            return ro;
        },
    });
    defineProperty(DateTimeFormat.prototype, 'formatToParts', {
        value: function formatToParts(date) {
            if (date === undefined) {
                date = Date.now();
            }
            else {
                date = toNumber(date);
            }
            return formatDateTimeParts(this, date);
        },
    });
    DateTimeFormat.__defaultTimeZone = 'UTC';
    DateTimeFormat.__addLocaleData = function __addLocaleData() {
        var data = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            data[_i] = arguments[_i];
        }
        for (var _a = 0, data_1 = data; _a < data_1.length; _a++) {
            var datum = data_1[_a];
            var availableLocales = datum.availableLocales;
            for (var _b = 0, availableLocales_1 = availableLocales; _b < availableLocales_1.length; _b++) {
                var locale = availableLocales_1[_b];
                try {
                    var _c = unpackData(locale, datum), formats = _c.formats, rawData = __rest(_c, ["formats"]);
                    var processedData = __assign$1(__assign$1({}, rawData), { formats: {} });
                    for (var calendar in formats) {
                        processedData.formats[calendar] = formats[calendar].map(parseDateTimeSkeleton);
                    }
                    DateTimeFormat.localeData[locale] = processedData;
                }
                catch (e) {
                    // Ignore if we got no data
                }
            }
        }
        DateTimeFormat.availableLocales = Object.keys(DateTimeFormat.localeData);
        if (!DateTimeFormat.__defaultLocale) {
            DateTimeFormat.__defaultLocale = DateTimeFormat.availableLocales[0];
        }
    };
    Object.defineProperty(DateTimeFormat.prototype, 'format', formatDescriptor);
    DateTimeFormat.__defaultLocale = '';
    DateTimeFormat.localeData = {};
    DateTimeFormat.availableLocales = [];
    DateTimeFormat.getDefaultLocale = function () {
        return DateTimeFormat.__defaultLocale;
    };
    DateTimeFormat.polyfilled = true;
    DateTimeFormat.tzData = {};
    DateTimeFormat.__addTZData = function (d) {
        DateTimeFormat.tzData = unpack(d);
    };
    try {
        if (typeof Symbol !== 'undefined') {
            Object.defineProperty(DateTimeFormat.prototype, Symbol.toStringTag, {
                value: 'Intl.DateTimeFormat',
                writable: false,
                enumerable: false,
                configurable: true,
            });
        }
        Object.defineProperty(DateTimeFormat.prototype.constructor, 'length', {
            value: 1,
            writable: false,
            enumerable: false,
            configurable: true,
        });
    }
    catch (e) {
        // Meta fix so we're test262-compliant, not important
    }

    // eslint-disable-next-line import/no-cycle
    /**
     * Number.prototype.toLocaleString ponyfill
     * https://tc39.es/ecma402/#sup-number.prototype.tolocalestring
     */
    function toLocaleString(x, locales, options) {
        var dtf = new DateTimeFormat(locales, options);
        return dtf.format(x);
    }
    function toLocaleTimeString(x, locales, options) {
        var dtf = new DateTimeFormat(locales, toDateTimeOptions(options, 'time', 'time'));
        return dtf.format(x);
    }

    // function supportsDateStyle() {
    //   return !!(new Intl.DateTimeFormat(undefined, {
    //     dateStyle: 'short',
    //   } as any).resolvedOptions() as any).dateStyle;
    // }
    /**
     * https://bugs.chromium.org/p/chromium/issues/detail?id=865351
     */
    function hasChromeLt71Bug() {
        return (new Intl.DateTimeFormat('en', {
            hourCycle: 'h11',
            hour: 'numeric',
        }).formatToParts(0)[2].type !== 'dayPeriod');
    }
    if (!('DateTimeFormat' in Intl) ||
        !('formatToParts' in Intl.DateTimeFormat.prototype) ||
        hasChromeLt71Bug()
    // !supportsDateStyle()
    ) {
        defineProperty(Intl, 'DateTimeFormat', { value: DateTimeFormat });
        defineProperty(Date.prototype, 'toLocaleString', {
            value: function toLocaleString$1(locales, options) {
                return toLocaleString(this, locales, options);
            },
        });
        // defineProperty(Date.prototype, 'toLocaleDateString', {
        //   value: function toLocaleDateString(
        //     locales?: string | string[],
        //     options?: DateTimeFormatOptions
        //   ) {
        //     return _toLocaleDateString(this, locales, options);
        //   },
        // });
        defineProperty(Date.prototype, 'toLocaleTimeString', {
            value: function toLocaleTimeString$1(locales, options) {
                return toLocaleTimeString(this, locales, options);
            },
        });
    }

})));


// Intl.DateTimeFormat.~locale.ru
/* @generated */	
  // prettier-ignore
  if (Intl.DateTimeFormat && typeof Intl.DateTimeFormat.__addLocaleData === 'function') {
    Intl.DateTimeFormat.__addLocaleData({"data":{"ru":{"am":"AM","pm":"PM","weekday":{"narrow":["вс","пн","вт","ср","чт","пт","сб"],"short":["вс","пн","вт","ср","чт","пт","сб"],"long":["воскресенье","понедельник","вторник","среда","четверг","пятница","суббота"]},"era":{"narrow":{"BC":"до н.э.","AD":"н.э."},"short":{"BC":"до н. э.","AD":"н. э."},"long":{"BC":"до Рождества Христова","AD":"от Рождества Христова"}},"month":{"narrow":["Я","Ф","М","А","М","И","И","А","С","О","Н","Д"],"short":["янв.","февр.","мар.","апр.","мая","июн.","июл.","авг.","сент.","окт.","нояб.","дек."],"long":["января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря"]},"timeZoneName":{"America/Rio_Branco":{"long":["Акри стандартное время","Акри летнее время"]},"Asia/Kabul":{"long":["Афганистан","Афганистан"]},"Africa/Maputo":{"long":["Центральная Африка","Центральная Африка"]},"Africa/Bujumbura":{"long":["Центральная Африка","Центральная Африка"]},"Africa/Gaborone":{"long":["Центральная Африка","Центральная Африка"]},"Africa/Lubumbashi":{"long":["Центральная Африка","Центральная Африка"]},"Africa/Blantyre":{"long":["Центральная Африка","Центральная Африка"]},"Africa/Kigali":{"long":["Центральная Африка","Центральная Африка"]},"Africa/Lusaka":{"long":["Центральная Африка","Центральная Африка"]},"Africa/Harare":{"long":["Центральная Африка","Центральная Африка"]},"Africa/Nairobi":{"long":["Восточная Африка","Восточная Африка"]},"Africa/Djibouti":{"long":["Восточная Африка","Восточная Африка"]},"Africa/Asmera":{"long":["Восточная Африка","Восточная Африка"]},"Africa/Addis_Ababa":{"long":["Восточная Африка","Восточная Африка"]},"Indian/Comoro":{"long":["Восточная Африка","Восточная Африка"]},"Indian/Antananarivo":{"long":["Восточная Африка","Восточная Африка"]},"Africa/Mogadishu":{"long":["Восточная Африка","Восточная Африка"]},"Africa/Dar_es_Salaam":{"long":["Восточная Африка","Восточная Африка"]},"Africa/Kampala":{"long":["Восточная Африка","Восточная Африка"]},"Indian/Mayotte":{"long":["Восточная Африка","Восточная Африка"]},"Africa/Johannesburg":{"long":["Южная Африка","Южная Африка"]},"Africa/Maseru":{"long":["Южная Африка","Южная Африка"]},"Africa/Mbabane":{"long":["Южная Африка","Южная Африка"]},"Africa/Lagos":{"long":["Западная Африка, стандартное время","Западная Африка, летнее время"]},"Africa/Luanda":{"long":["Западная Африка, стандартное время","Западная Африка, летнее время"]},"Africa/Porto-Novo":{"long":["Западная Африка, стандартное время","Западная Африка, летнее время"]},"Africa/Kinshasa":{"long":["Западная Африка, стандартное время","Западная Африка, летнее время"]},"Africa/Bangui":{"long":["Западная Африка, стандартное время","Западная Африка, летнее время"]},"Africa/Brazzaville":{"long":["Западная Африка, стандартное время","Западная Африка, летнее время"]},"Africa/Douala":{"long":["Западная Африка, стандартное время","Западная Африка, летнее время"]},"Africa/Libreville":{"long":["Западная Африка, стандартное время","Западная Африка, летнее время"]},"Africa/Malabo":{"long":["Западная Африка, стандартное время","Западная Африка, летнее время"]},"Africa/Niamey":{"long":["Западная Африка, стандартное время","Западная Африка, летнее время"]},"Africa/Ndjamena":{"long":["Западная Африка, стандартное время","Западная Африка, летнее время"]},"Asia/Aqtobe":{"long":["Западный Казахстан","Западный Казахстан"]},"America/Juneau":{"long":["Аляска, стандартное время","Аляска, летнее время"]},"Asia/Almaty":{"long":["Восточный Казахстан","Восточный Казахстан"]},"America/Manaus":{"long":["Амазонка, стандартное время","Амазонка, летнее время"]},"America/Chicago":{"long":["Центральная Америка, стандартное время","Центральная Америка, летнее время"]},"America/Belize":{"long":["Центральная Америка, стандартное время","Центральная Америка, летнее время"]},"America/Winnipeg":{"long":["Центральная Америка, стандартное время","Центральная Америка, летнее время"]},"America/Costa_Rica":{"long":["Центральная Америка, стандартное время","Центральная Америка, летнее время"]},"America/Guatemala":{"long":["Центральная Америка, стандартное время","Центральная Америка, летнее время"]},"America/Tegucigalpa":{"long":["Центральная Америка, стандартное время","Центральная Америка, летнее время"]},"America/Mexico_City":{"long":["Центральная Америка, стандартное время","Центральная Америка, летнее время"]},"America/El_Salvador":{"long":["Центральная Америка, стандартное время","Центральная Америка, летнее время"]},"America/New_York":{"long":["Восточная Америка, стандартное время","Восточная Америка, летнее время"]},"America/Nassau":{"long":["Восточная Америка, стандартное время","Восточная Америка, летнее время"]},"America/Toronto":{"long":["Восточная Америка, стандартное время","Восточная Америка, летнее время"]},"America/Port-au-Prince":{"long":["Восточная Америка, стандартное время","Восточная Америка, летнее время"]},"America/Jamaica":{"long":["Восточная Америка, стандартное время","Восточная Америка, летнее время"]},"America/Cayman":{"long":["Восточная Америка, стандартное время","Восточная Америка, летнее время"]},"America/Panama":{"long":["Восточная Америка, стандартное время","Восточная Америка, летнее время"]},"America/Denver":{"long":["Стандартное горное время (Северная Америка)","Летнее горное время (Северная Америка)"]},"America/Edmonton":{"long":["Стандартное горное время (Северная Америка)","Летнее горное время (Северная Америка)"]},"America/Hermosillo":{"long":["Стандартное горное время (Северная Америка)","Летнее горное время (Северная Америка)"]},"America/Los_Angeles":{"long":["Тихоокеанское стандартное время","Тихоокеанское летнее время"]},"America/Vancouver":{"long":["Тихоокеанское стандартное время","Тихоокеанское летнее время"]},"America/Tijuana":{"long":["Тихоокеанское стандартное время","Тихоокеанское летнее время"]},"Asia/Anadyr":{"long":["Анадырь стандартное время","Анадырь летнее время"]},"Pacific/Apia":{"long":["Апиа, стандартное время","Апиа, летнее время"]},"Asia/Riyadh":{"long":["Саудовская Аравия, стандартное время","Саудовская Аравия, летнее время"]},"Asia/Bahrain":{"long":["Саудовская Аравия, стандартное время","Саудовская Аравия, летнее время"]},"Asia/Baghdad":{"long":["Саудовская Аравия, стандартное время","Саудовская Аравия, летнее время"]},"Asia/Kuwait":{"long":["Саудовская Аравия, стандартное время","Саудовская Аравия, летнее время"]},"Asia/Qatar":{"long":["Саудовская Аравия, стандартное время","Саудовская Аравия, летнее время"]},"Asia/Aden":{"long":["Саудовская Аравия, стандартное время","Саудовская Аравия, летнее время"]},"America/Buenos_Aires":{"long":["Аргентина, стандартное время","Аргентина, летнее время"]},"America/Argentina/San_Luis":{"long":["Западная Аргентина, стандартное время","Западная Аргентина, летнее время"]},"Asia/Ashgabat":{"long":["Туркменистан, стандартное время","Туркменистан, летнее время"]},"America/Halifax":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Antigua":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Anguilla":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Aruba":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Barbados":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"Atlantic/Bermuda":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Kralendijk":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Curacao":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Dominica":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Grenada":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Thule":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Guadeloupe":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/St_Kitts":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/St_Lucia":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Marigot":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Martinique":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Montserrat":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Puerto_Rico":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Lower_Princes":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Port_of_Spain":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/St_Vincent":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/Tortola":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"America/St_Thomas":{"long":["Атлантическое стандартное время","Атлантическое летнее время"]},"Australia/Adelaide":{"long":["Центральная Австралия, стандартное время","Центральная Австралия, летнее время"]},"Australia/Eucla":{"long":["Центральная Австралия, западное стандартное время","Центральная Австралия, западное летнее время"]},"Australia/Sydney":{"long":["Восточная Австралия, стандартное время","Восточная Австралия, летнее время"]},"Australia/Perth":{"long":["Западная Австралия, стандартное время","Западная Австралия, летнее время"]},"Atlantic/Azores":{"long":["Азорские о-ва, стандартное время","Азорские о-ва, летнее время"]},"Asia/Thimphu":{"long":["Бутан","Бутан"]},"America/La_Paz":{"long":["Боливия","Боливия"]},"Asia/Kuching":{"long":["Малайзия","Малайзия"]},"America/Sao_Paulo":{"long":["Бразилия, стандартное время","Бразилия, летнее время"]},"Europe/London":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"Asia/Brunei":{"long":["Бруней-Даруссалам","Бруней-Даруссалам"]},"Atlantic/Cape_Verde":{"long":["Кабо-Верде, стандартное время","Кабо-Верде, летнее время"]},"Antarctica/Casey":{"long":["Кейси","Кейси"]},"Pacific/Saipan":{"long":["Северные Марианские о-ва","Северные Марианские о-ва"]},"Pacific/Guam":{"long":["Гуам","Гуам"]},"Pacific/Chatham":{"long":["Чатем, стандартное время","Чатем, летнее время"]},"America/Santiago":{"long":["Чили, стандартное время","Чили, летнее время"]},"Asia/Shanghai":{"long":["Китай, стандартное время","Китай, летнее время"]},"Asia/Choibalsan":{"long":["Чойбалсан, стандартное время","Чойбалсан, летнее время"]},"Indian/Christmas":{"long":["о-в Рождества","о-в Рождества"]},"Indian/Cocos":{"long":["Кокосовые о-ва","Кокосовые о-ва"]},"America/Bogota":{"long":["Колумбия, стандартное время","Колумбия, летнее время"]},"Pacific/Rarotonga":{"long":["Острова Кука, стандартное время","Острова Кука, полулетнее время"]},"America/Havana":{"long":["Куба, стандартное время","Куба, летнее время"]},"Antarctica/Davis":{"long":["Дейвис","Дейвис"]},"Antarctica/DumontDUrville":{"long":["Дюмон-д’Юрвиль","Дюмон-д’Юрвиль"]},"Asia/Dushanbe":{"long":["Таджикистан","Таджикистан"]},"America/Paramaribo":{"long":["Суринам","Суринам"]},"Asia/Dili":{"long":["Восточный Тимор","Восточный Тимор"]},"Pacific/Easter":{"long":["О-в Пасхи, стандартное время","О-в Пасхи, летнее время"]},"America/Guayaquil":{"long":["Эквадор","Эквадор"]},"Europe/Paris":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Andorra":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Tirane":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Vienna":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Sarajevo":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Brussels":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Zurich":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Prague":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Berlin":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Copenhagen":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Madrid":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Gibraltar":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Zagreb":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Budapest":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Rome":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Vaduz":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Luxembourg":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Monaco":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Podgorica":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Skopje":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Malta":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Amsterdam":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Oslo":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Warsaw":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Belgrade":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Stockholm":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Ljubljana":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Bratislava":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/San_Marino":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Africa/Tunis":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Vatican":{"long":["Центральная Европа, стандартное время","Центральная Европа, летнее время"]},"Europe/Bucharest":{"long":["Восточная Европа, стандартное время","Восточная Европа, летнее время"]},"Europe/Mariehamn":{"long":["Восточная Европа, стандартное время","Восточная Европа, летнее время"]},"Europe/Sofia":{"long":["Восточная Европа, стандартное время","Восточная Европа, летнее время"]},"Asia/Nicosia":{"long":["Восточная Европа, стандартное время","Восточная Европа, летнее время"]},"Africa/Cairo":{"long":["Восточная Европа, стандартное время","Восточная Европа, летнее время"]},"Europe/Helsinki":{"long":["Восточная Европа, стандартное время","Восточная Европа, летнее время"]},"Europe/Athens":{"long":["Восточная Европа, стандартное время","Восточная Европа, летнее время"]},"Asia/Amman":{"long":["Восточная Европа, стандартное время","Восточная Европа, летнее время"]},"Asia/Beirut":{"long":["Восточная Европа, стандартное время","Восточная Европа, летнее время"]},"Asia/Damascus":{"long":["Восточная Европа, стандартное время","Восточная Европа, летнее время"]},"Europe/Minsk":{"long":["Минское время","Минское время"]},"Europe/Kaliningrad":{"long":["Минское время","Минское время"]},"Atlantic/Canary":{"long":["Западная Европа, стандартное время","Западная Европа, летнее время"]},"Atlantic/Faeroe":{"long":["Западная Европа, стандартное время","Западная Европа, летнее время"]},"Atlantic/Stanley":{"long":["Фолклендские о-ва, стандартное время","Фолклендские о-ва, летнее время"]},"Pacific/Fiji":{"long":["Фиджи, стандартное время","Фиджи, летнее время"]},"America/Cayenne":{"long":["Французская Гвиана","Французская Гвиана"]},"Indian/Kerguelen":{"long":["Французские Южные и Антарктические территории","Французские Южные и Антарктические территории"]},"Asia/Bishkek":{"long":["Киргизия","Киргизия"]},"Pacific/Galapagos":{"long":["Галапагосские о-ва","Галапагосские о-ва"]},"Pacific/Gambier":{"long":["Гамбье","Гамбье"]},"Pacific/Tarawa":{"long":["о-ва Гилберта","о-ва Гилберта"]},"Atlantic/Reykjavik":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"Africa/Ouagadougou":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"Africa/Abidjan":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"Africa/Accra":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"Africa/Banjul":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"Africa/Conakry":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"Africa/Bamako":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"Africa/Nouakchott":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"Atlantic/St_Helena":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"Africa/Freetown":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"Africa/Dakar":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"Africa/Lome":{"long":["Среднее время по Гринвичу","Среднее время по Гринвичу"]},"America/Scoresbysund":{"long":["Восточная Гренландия, стандарное время","Восточная Гренландия, летнее время"]},"America/Godthab":{"long":["Западная Гренландия, стандартное время","Западная Гренландия, летнее время"]},"Asia/Dubai":{"long":["Персидский залив","Персидский залив"]},"Asia/Muscat":{"long":["Персидский залив","Персидский залив"]},"America/Guyana":{"long":["Гайана","Гайана"]},"Pacific/Honolulu":{"long":["Гавайско-алеутское стандартное время","Гавайско-алеутское летнее время"]},"Asia/Hong_Kong":{"long":["Гонконг, стандартное время","Гонконг, летнее время"]},"Asia/Hovd":{"long":["Ховд, стандартное время","Ховд, летнее время"]},"Asia/Calcutta":{"long":["Индия","Индия"]},"Asia/Colombo":{"long":["Шри-Ланка","Шри-Ланка"]},"Indian/Chagos":{"long":["Индийский океан","Индийский океан"]},"Asia/Bangkok":{"long":["Индокитай","Индокитай"]},"Asia/Phnom_Penh":{"long":["Индокитай","Индокитай"]},"Asia/Vientiane":{"long":["Индокитай","Индокитай"]},"Asia/Makassar":{"long":["Центральная Индонезия","Центральная Индонезия"]},"Asia/Jayapura":{"long":["Восточная Индонезия","Восточная Индонезия"]},"Asia/Jakarta":{"long":["Западная Индонезия","Западная Индонезия"]},"Asia/Tehran":{"long":["Иран, стандартное время","Иран, летнее время"]},"Asia/Irkutsk":{"long":["Иркутск, стандартное время","Иркутск, летнее время"]},"Asia/Jerusalem":{"long":["Израиль, стандартное время","Израиль, летнее время"]},"Asia/Tokyo":{"long":["Япония, стандартное время","Япония, летнее время"]},"Asia/Kamchatka":{"long":["Петропавловск-Камчатский, стандартное время","Петропавловск-Камчатский, летнее время"]},"Asia/Karachi":{"long":["Пакистан, стандартное время","Пакистан, летнее время"]},"Asia/Qyzylorda":{"long":["Кызылорда, стандартное время*","Кызылорда, летнее время*"]},"Asia/Seoul":{"long":["Корея, стандартное время","Корея, летнее время"]},"Pacific/Kosrae":{"long":["Косрае","Косрае"]},"Asia/Krasnoyarsk":{"long":["Красноярск, стандартное время","Красноярск, летнее время"]},"Europe/Samara":{"long":["Самарское стандартное время","Самарское летнее время"]},"Pacific/Kiritimati":{"long":["о-ва Лайн","о-ва Лайн"]},"Australia/Lord_Howe":{"long":["Лорд-Хау, стандартное время","Лорд-Хау, летнее время"]},"Asia/Macau":{"long":["Макао, стандартное время","Макао, летнее время"]},"Antarctica/Macquarie":{"long":["Маккуори","Маккуори"]},"Asia/Magadan":{"long":["Магадан, стандартное время","Магадан, летнее время"]},"Indian/Maldives":{"long":["Мальдивы","Мальдивы"]},"Pacific/Marquesas":{"long":["Маркизские о-ва","Маркизские о-ва"]},"Pacific/Majuro":{"long":["Маршалловы Острова","Маршалловы Острова"]},"Indian/Mauritius":{"long":["Маврикий, стандартное время","Маврикий, летнее время"]},"Antarctica/Mawson":{"long":["Моусон","Моусон"]},"America/Santa_Isabel":{"long":["Северо-западное мексиканское стандартное время","Северо-западное мексиканское летнее время"]},"America/Mazatlan":{"long":["Тихоокеанское мексиканское стандартное время","Тихоокеанское мексиканское летнее время"]},"Asia/Ulaanbaatar":{"long":["Улан-Батор, стандартное время","Улан-Батор, летнее время"]},"Europe/Moscow":{"long":["Москва, стандартное время","Москва, летнее время"]},"Asia/Rangoon":{"long":["Мьянма","Мьянма"]},"Pacific/Nauru":{"long":["Науру","Науру"]},"Asia/Katmandu":{"long":["Непал","Непал"]},"Pacific/Noumea":{"long":["Новая Каледония, стандартное время","Новая Каледония, летнее время"]},"Pacific/Auckland":{"long":["Новая Зеландия, стандартное время","Новая Зеландия, летнее время"]},"Antarctica/McMurdo":{"long":["Новая Зеландия, стандартное время","Новая Зеландия, летнее время"]},"America/St_Johns":{"long":["Ньюфаундленд, стандартное время","Ньюфаундленд, летнее время"]},"Pacific/Niue":{"long":["Ниуэ","Ниуэ"]},"Pacific/Norfolk":{"long":["Норфолк","Норфолк"]},"America/Noronha":{"long":["Фернанду-ди-Норонья, стандартное время","Фернанду-ди-Норонья, летнее время"]},"Asia/Novosibirsk":{"long":["Новосибирск, стандартное время","Новосибирск, летнее время"]},"Asia/Omsk":{"long":["Омск, стандартное время","Омск, летнее время"]},"Pacific/Palau":{"long":["Палау","Палау"]},"Pacific/Port_Moresby":{"long":["Папуа – Новая Гвинея","Папуа – Новая Гвинея"]},"America/Asuncion":{"long":["Парагвай, стандартное время","Парагвай, летнее время"]},"America/Lima":{"long":["Перу, стандартное время","Перу, летнее время"]},"Asia/Manila":{"long":["Филиппины, стандартное время","Филиппины, летнее время"]},"Pacific/Enderbury":{"long":["о-ва Феникс","о-ва Феникс"]},"America/Miquelon":{"long":["Сен-Пьер и Микелон, стандартное время","Сен-Пьер и Микелон, летнее время"]},"Pacific/Pitcairn":{"long":["Питкэрн","Питкэрн"]},"Pacific/Ponape":{"long":["Понпеи","Понпеи"]},"Asia/Pyongyang":{"long":["Пхеньян","Пхеньян"]},"Indian/Reunion":{"long":["Реюньон","Реюньон"]},"Antarctica/Rothera":{"long":["Ротера","Ротера"]},"Asia/Sakhalin":{"long":["Сахалин, стандартное время","Сахалин, летнее время"]},"Pacific/Pago_Pago":{"long":["Самоа, стандартное время","Самоа, летнее время"]},"Indian/Mahe":{"long":["Сейшельские Острова","Сейшельские Острова"]},"Asia/Singapore":{"long":["Сингапур","Сингапур"]},"Pacific/Guadalcanal":{"long":["Соломоновы Острова","Соломоновы Острова"]},"Atlantic/South_Georgia":{"long":["Южная Георгия","Южная Георгия"]},"Asia/Yekaterinburg":{"long":["Екатеринбург, стандартное время","Екатеринбург, летнее время"]},"Antarctica/Syowa":{"long":["Сёва","Сёва"]},"Pacific/Tahiti":{"long":["Таити","Таити"]},"Asia/Taipei":{"long":["Тайвань, стандартное время","Тайвань, летнее время"]},"Asia/Tashkent":{"long":["Узбекистан, стандартное время","Узбекистан, летнее время"]},"Pacific/Fakaofo":{"long":["Токелау","Токелау"]},"Pacific/Tongatapu":{"long":["Тонга, стандартное время","Тонга, летнее время"]},"Pacific/Truk":{"long":["Трук","Трук"]},"Pacific/Funafuti":{"long":["Тувалу","Тувалу"]},"America/Montevideo":{"long":["Уругвай, стандартное время","Уругвай, летнее время"]},"Pacific/Efate":{"long":["Вануату, стандартное время","Вануату, летнее время"]},"America/Caracas":{"long":["Венесуэла","Венесуэла"]},"Asia/Vladivostok":{"long":["Владивосток, стандартное время","Владивосток, летнее время"]},"Europe/Volgograd":{"long":["Волгоград, стандартное время","Волгоград, летнее время"]},"Antarctica/Vostok":{"long":["Восток","Восток"]},"Pacific/Wake":{"long":["Уэйк","Уэйк"]},"Pacific/Wallis":{"long":["Уоллис и Футуна","Уоллис и Футуна"]},"Asia/Yakutsk":{"long":["Якутск, стандартное время","Якутск, летнее время"]},"UTC":{"long":["Всемирное координированное время","Всемирное координированное время"],"short":["UTC","UTC"]}},"gmtFormat":"GMT{0}","hourFormat":"+HH:mm;-HH:mm","formats":{"gregory":["h B","h:mm B","h:mm:ss B","d","ccc","ccc, h:mm B","ccc, h:mm:ss B","ccc, d","E h:mm a","E HH:mm","E h:mm:ss a","E HH:mm:ss","y 'г'. G","LLL y G","d MMM y 'г'. G","E, d MMM y 'г'. G","h a","HH","h:mm a","HH:mm","h:mm:ss a","HH:mm:ss","h:mm:ss a v","HH:mm:ss v","h:mm a v","HH:mm v","L","dd.MM","E, dd.MM","dd.MM","LLL","d MMM","ccc, d MMM","d MMMM","W-'я' 'неделя' MMMM","W-'я' 'неделя' MMMM","W-'я' 'неделя' MMMM","W-'я' 'неделя' MMMM","mm:ss","y","MM.y","dd.MM.y","ccc, dd.MM.y 'г'.","MM.y","LLL y 'г'.","d MMM y 'г'.","E, d MMM y 'г'.","LLLL y 'г'.","QQQ y 'г'.","QQQQ y 'г'.","w-'я' 'неделя' Y 'г'.","w-'я' 'неделя' Y 'г'.","w-'я' 'неделя' Y 'г'.","w-'я' 'неделя' Y 'г'.","EEEE, d MMMM y 'г'.","d MMMM y 'г'.","d MMM y 'г'.","dd.MM.y","HH:mm:ss zzzz","HH:mm:ss z","HH:mm:ss","HH:mm","EEEE, d MMMM y 'г'., HH:mm:ss zzzz","d MMMM y 'г'., HH:mm:ss zzzz","d MMM y 'г'., HH:mm:ss zzzz","dd.MM.y, HH:mm:ss zzzz","d, HH:mm:ss zzzz","ccc, HH:mm:ss zzzz","ccc, d, HH:mm:ss zzzz","y 'г'. G, HH:mm:ss zzzz","LLL y G, HH:mm:ss zzzz","d MMM y 'г'. G, HH:mm:ss zzzz","E, d MMM y 'г'. G, HH:mm:ss zzzz","L, HH:mm:ss zzzz","dd.MM, HH:mm:ss zzzz","E, dd.MM, HH:mm:ss zzzz","dd.MM, HH:mm:ss zzzz","LLL, HH:mm:ss zzzz","d MMM, HH:mm:ss zzzz","ccc, d MMM, HH:mm:ss zzzz","d MMMM, HH:mm:ss zzzz","W-'я' 'неделя' MMMM, HH:mm:ss zzzz","W-'я' 'неделя' MMMM, HH:mm:ss zzzz","W-'я' 'неделя' MMMM, HH:mm:ss zzzz","W-'я' 'неделя' MMMM, HH:mm:ss zzzz","y, HH:mm:ss zzzz","MM.y, HH:mm:ss zzzz","dd.MM.y, HH:mm:ss zzzz","ccc, dd.MM.y 'г'., HH:mm:ss zzzz","MM.y, HH:mm:ss zzzz","LLL y 'г'., HH:mm:ss zzzz","d MMM y 'г'., HH:mm:ss zzzz","E, d MMM y 'г'., HH:mm:ss zzzz","LLLL y 'г'., HH:mm:ss zzzz","QQQ y 'г'., HH:mm:ss zzzz","QQQQ y 'г'., HH:mm:ss zzzz","w-'я' 'неделя' Y 'г'., HH:mm:ss zzzz","w-'я' 'неделя' Y 'г'., HH:mm:ss zzzz","w-'я' 'неделя' Y 'г'., HH:mm:ss zzzz","w-'я' 'неделя' Y 'г'., HH:mm:ss zzzz","EEEE, d MMMM y 'г'., HH:mm:ss z","d MMMM y 'г'., HH:mm:ss z","d MMM y 'г'., HH:mm:ss z","dd.MM.y, HH:mm:ss z","d, HH:mm:ss z","ccc, HH:mm:ss z","ccc, d, HH:mm:ss z","y 'г'. G, HH:mm:ss z","LLL y G, HH:mm:ss z","d MMM y 'г'. G, HH:mm:ss z","E, d MMM y 'г'. G, HH:mm:ss z","L, HH:mm:ss z","dd.MM, HH:mm:ss z","E, dd.MM, HH:mm:ss z","dd.MM, HH:mm:ss z","LLL, HH:mm:ss z","d MMM, HH:mm:ss z","ccc, d MMM, HH:mm:ss z","d MMMM, HH:mm:ss z","W-'я' 'неделя' MMMM, HH:mm:ss z","W-'я' 'неделя' MMMM, HH:mm:ss z","W-'я' 'неделя' MMMM, HH:mm:ss z","W-'я' 'неделя' MMMM, HH:mm:ss z","y, HH:mm:ss z","MM.y, HH:mm:ss z","dd.MM.y, HH:mm:ss z","ccc, dd.MM.y 'г'., HH:mm:ss z","MM.y, HH:mm:ss z","LLL y 'г'., HH:mm:ss z","d MMM y 'г'., HH:mm:ss z","E, d MMM y 'г'., HH:mm:ss z","LLLL y 'г'., HH:mm:ss z","QQQ y 'г'., HH:mm:ss z","QQQQ y 'г'., HH:mm:ss z","w-'я' 'неделя' Y 'г'., HH:mm:ss z","w-'я' 'неделя' Y 'г'., HH:mm:ss z","w-'я' 'неделя' Y 'г'., HH:mm:ss z","w-'я' 'неделя' Y 'г'., HH:mm:ss z","EEEE, d MMMM y 'г'., HH:mm:ss","d MMMM y 'г'., HH:mm:ss","d MMM y 'г'., HH:mm:ss","dd.MM.y, HH:mm:ss","d, HH:mm:ss","ccc, HH:mm:ss","ccc, d, HH:mm:ss","y 'г'. G, HH:mm:ss","LLL y G, HH:mm:ss","d MMM y 'г'. G, HH:mm:ss","E, d MMM y 'г'. G, HH:mm:ss","L, HH:mm:ss","dd.MM, HH:mm:ss","E, dd.MM, HH:mm:ss","dd.MM, HH:mm:ss","LLL, HH:mm:ss","d MMM, HH:mm:ss","ccc, d MMM, HH:mm:ss","d MMMM, HH:mm:ss","W-'я' 'неделя' MMMM, HH:mm:ss","W-'я' 'неделя' MMMM, HH:mm:ss","W-'я' 'неделя' MMMM, HH:mm:ss","W-'я' 'неделя' MMMM, HH:mm:ss","y, HH:mm:ss","MM.y, HH:mm:ss","dd.MM.y, HH:mm:ss","ccc, dd.MM.y 'г'., HH:mm:ss","MM.y, HH:mm:ss","LLL y 'г'., HH:mm:ss","d MMM y 'г'., HH:mm:ss","E, d MMM y 'г'., HH:mm:ss","LLLL y 'г'., HH:mm:ss","QQQ y 'г'., HH:mm:ss","QQQQ y 'г'., HH:mm:ss","w-'я' 'неделя' Y 'г'., HH:mm:ss","w-'я' 'неделя' Y 'г'., HH:mm:ss","w-'я' 'неделя' Y 'г'., HH:mm:ss","w-'я' 'неделя' Y 'г'., HH:mm:ss","EEEE, d MMMM y 'г'., HH:mm","d MMMM y 'г'., HH:mm","d MMM y 'г'., HH:mm","dd.MM.y, HH:mm","d, HH:mm","ccc, HH:mm","ccc, d, HH:mm","y 'г'. G, HH:mm","LLL y G, HH:mm","d MMM y 'г'. G, HH:mm","E, d MMM y 'г'. G, HH:mm","L, HH:mm","dd.MM, HH:mm","E, dd.MM, HH:mm","dd.MM, HH:mm","LLL, HH:mm","d MMM, HH:mm","ccc, d MMM, HH:mm","d MMMM, HH:mm","W-'я' 'неделя' MMMM, HH:mm","W-'я' 'неделя' MMMM, HH:mm","W-'я' 'неделя' MMMM, HH:mm","W-'я' 'неделя' MMMM, HH:mm","y, HH:mm","MM.y, HH:mm","dd.MM.y, HH:mm","ccc, dd.MM.y 'г'., HH:mm","MM.y, HH:mm","LLL y 'г'., HH:mm","d MMM y 'г'., HH:mm","E, d MMM y 'г'., HH:mm","LLLL y 'г'., HH:mm","QQQ y 'г'., HH:mm","QQQQ y 'г'., HH:mm","w-'я' 'неделя' Y 'г'., HH:mm","w-'я' 'неделя' Y 'г'., HH:mm","w-'я' 'неделя' Y 'г'., HH:mm","w-'я' 'неделя' Y 'г'., HH:mm","EEEE, d MMMM y 'г'., h B","d MMMM y 'г'., h B","d MMM y 'г'., h B","dd.MM.y, h B","d, h B","ccc, h B","ccc, d, h B","y 'г'. G, h B","LLL y G, h B","d MMM y 'г'. G, h B","E, d MMM y 'г'. G, h B","L, h B","dd.MM, h B","E, dd.MM, h B","dd.MM, h B","LLL, h B","d MMM, h B","ccc, d MMM, h B","d MMMM, h B","W-'я' 'неделя' MMMM, h B","W-'я' 'неделя' MMMM, h B","W-'я' 'неделя' MMMM, h B","W-'я' 'неделя' MMMM, h B","y, h B","MM.y, h B","dd.MM.y, h B","ccc, dd.MM.y 'г'., h B","MM.y, h B","LLL y 'г'., h B","d MMM y 'г'., h B","E, d MMM y 'г'., h B","LLLL y 'г'., h B","QQQ y 'г'., h B","QQQQ y 'г'., h B","w-'я' 'неделя' Y 'г'., h B","w-'я' 'неделя' Y 'г'., h B","w-'я' 'неделя' Y 'г'., h B","w-'я' 'неделя' Y 'г'., h B","EEEE, d MMMM y 'г'., h:mm B","d MMMM y 'г'., h:mm B","d MMM y 'г'., h:mm B","dd.MM.y, h:mm B","d, h:mm B","ccc, h:mm B","ccc, d, h:mm B","y 'г'. G, h:mm B","LLL y G, h:mm B","d MMM y 'г'. G, h:mm B","E, d MMM y 'г'. G, h:mm B","L, h:mm B","dd.MM, h:mm B","E, dd.MM, h:mm B","dd.MM, h:mm B","LLL, h:mm B","d MMM, h:mm B","ccc, d MMM, h:mm B","d MMMM, h:mm B","W-'я' 'неделя' MMMM, h:mm B","W-'я' 'неделя' MMMM, h:mm B","W-'я' 'неделя' MMMM, h:mm B","W-'я' 'неделя' MMMM, h:mm B","y, h:mm B","MM.y, h:mm B","dd.MM.y, h:mm B","ccc, dd.MM.y 'г'., h:mm B","MM.y, h:mm B","LLL y 'г'., h:mm B","d MMM y 'г'., h:mm B","E, d MMM y 'г'., h:mm B","LLLL y 'г'., h:mm B","QQQ y 'г'., h:mm B","QQQQ y 'г'., h:mm B","w-'я' 'неделя' Y 'г'., h:mm B","w-'я' 'неделя' Y 'г'., h:mm B","w-'я' 'неделя' Y 'г'., h:mm B","w-'я' 'неделя' Y 'г'., h:mm B","EEEE, d MMMM y 'г'., h:mm:ss B","d MMMM y 'г'., h:mm:ss B","d MMM y 'г'., h:mm:ss B","dd.MM.y, h:mm:ss B","d, h:mm:ss B","ccc, h:mm:ss B","ccc, d, h:mm:ss B","y 'г'. G, h:mm:ss B","LLL y G, h:mm:ss B","d MMM y 'г'. G, h:mm:ss B","E, d MMM y 'г'. G, h:mm:ss B","L, h:mm:ss B","dd.MM, h:mm:ss B","E, dd.MM, h:mm:ss B","dd.MM, h:mm:ss B","LLL, h:mm:ss B","d MMM, h:mm:ss B","ccc, d MMM, h:mm:ss B","d MMMM, h:mm:ss B","W-'я' 'неделя' MMMM, h:mm:ss B","W-'я' 'неделя' MMMM, h:mm:ss B","W-'я' 'неделя' MMMM, h:mm:ss B","W-'я' 'неделя' MMMM, h:mm:ss B","y, h:mm:ss B","MM.y, h:mm:ss B","dd.MM.y, h:mm:ss B","ccc, dd.MM.y 'г'., h:mm:ss B","MM.y, h:mm:ss B","LLL y 'г'., h:mm:ss B","d MMM y 'г'., h:mm:ss B","E, d MMM y 'г'., h:mm:ss B","LLLL y 'г'., h:mm:ss B","QQQ y 'г'., h:mm:ss B","QQQQ y 'г'., h:mm:ss B","w-'я' 'неделя' Y 'г'., h:mm:ss B","w-'я' 'неделя' Y 'г'., h:mm:ss B","w-'я' 'неделя' Y 'г'., h:mm:ss B","w-'я' 'неделя' Y 'г'., h:mm:ss B","EEEE, d MMMM y 'г'., h a","d MMMM y 'г'., h a","d MMM y 'г'., h a","dd.MM.y, h a","d, h a","ccc, h a","ccc, d, h a","y 'г'. G, h a","LLL y G, h a","d MMM y 'г'. G, h a","E, d MMM y 'г'. G, h a","L, h a","dd.MM, h a","E, dd.MM, h a","dd.MM, h a","LLL, h a","d MMM, h a","ccc, d MMM, h a","d MMMM, h a","W-'я' 'неделя' MMMM, h a","W-'я' 'неделя' MMMM, h a","W-'я' 'неделя' MMMM, h a","W-'я' 'неделя' MMMM, h a","y, h a","MM.y, h a","dd.MM.y, h a","ccc, dd.MM.y 'г'., h a","MM.y, h a","LLL y 'г'., h a","d MMM y 'г'., h a","E, d MMM y 'г'., h a","LLLL y 'г'., h a","QQQ y 'г'., h a","QQQQ y 'г'., h a","w-'я' 'неделя' Y 'г'., h a","w-'я' 'неделя' Y 'г'., h a","w-'я' 'неделя' Y 'г'., h a","w-'я' 'неделя' Y 'г'., h a","EEEE, d MMMM y 'г'., HH","d MMMM y 'г'., HH","d MMM y 'г'., HH","dd.MM.y, HH","d, HH","ccc, HH","ccc, d, HH","y 'г'. G, HH","LLL y G, HH","d MMM y 'г'. G, HH","E, d MMM y 'г'. G, HH","L, HH","dd.MM, HH","E, dd.MM, HH","dd.MM, HH","LLL, HH","d MMM, HH","ccc, d MMM, HH","d MMMM, HH","W-'я' 'неделя' MMMM, HH","W-'я' 'неделя' MMMM, HH","W-'я' 'неделя' MMMM, HH","W-'я' 'неделя' MMMM, HH","y, HH","MM.y, HH","dd.MM.y, HH","ccc, dd.MM.y 'г'., HH","MM.y, HH","LLL y 'г'., HH","d MMM y 'г'., HH","E, d MMM y 'г'., HH","LLLL y 'г'., HH","QQQ y 'г'., HH","QQQQ y 'г'., HH","w-'я' 'неделя' Y 'г'., HH","w-'я' 'неделя' Y 'г'., HH","w-'я' 'неделя' Y 'г'., HH","w-'я' 'неделя' Y 'г'., HH","EEEE, d MMMM y 'г'., h:mm a","d MMMM y 'г'., h:mm a","d MMM y 'г'., h:mm a","dd.MM.y, h:mm a","d, h:mm a","ccc, h:mm a","ccc, d, h:mm a","y 'г'. G, h:mm a","LLL y G, h:mm a","d MMM y 'г'. G, h:mm a","E, d MMM y 'г'. G, h:mm a","L, h:mm a","dd.MM, h:mm a","E, dd.MM, h:mm a","dd.MM, h:mm a","LLL, h:mm a","d MMM, h:mm a","ccc, d MMM, h:mm a","d MMMM, h:mm a","W-'я' 'неделя' MMMM, h:mm a","W-'я' 'неделя' MMMM, h:mm a","W-'я' 'неделя' MMMM, h:mm a","W-'я' 'неделя' MMMM, h:mm a","y, h:mm a","MM.y, h:mm a","dd.MM.y, h:mm a","ccc, dd.MM.y 'г'., h:mm a","MM.y, h:mm a","LLL y 'г'., h:mm a","d MMM y 'г'., h:mm a","E, d MMM y 'г'., h:mm a","LLLL y 'г'., h:mm a","QQQ y 'г'., h:mm a","QQQQ y 'г'., h:mm a","w-'я' 'неделя' Y 'г'., h:mm a","w-'я' 'неделя' Y 'г'., h:mm a","w-'я' 'неделя' Y 'г'., h:mm a","w-'я' 'неделя' Y 'г'., h:mm a","EEEE, d MMMM y 'г'., HH:mm","d MMMM y 'г'., HH:mm","d MMM y 'г'., HH:mm","dd.MM.y, HH:mm","d, HH:mm","ccc, HH:mm","ccc, d, HH:mm","y 'г'. G, HH:mm","LLL y G, HH:mm","d MMM y 'г'. G, HH:mm","E, d MMM y 'г'. G, HH:mm","L, HH:mm","dd.MM, HH:mm","E, dd.MM, HH:mm","dd.MM, HH:mm","LLL, HH:mm","d MMM, HH:mm","ccc, d MMM, HH:mm","d MMMM, HH:mm","W-'я' 'неделя' MMMM, HH:mm","W-'я' 'неделя' MMMM, HH:mm","W-'я' 'неделя' MMMM, HH:mm","W-'я' 'неделя' MMMM, HH:mm","y, HH:mm","MM.y, HH:mm","dd.MM.y, HH:mm","ccc, dd.MM.y 'г'., HH:mm","MM.y, HH:mm","LLL y 'г'., HH:mm","d MMM y 'г'., HH:mm","E, d MMM y 'г'., HH:mm","LLLL y 'г'., HH:mm","QQQ y 'г'., HH:mm","QQQQ y 'г'., HH:mm","w-'я' 'неделя' Y 'г'., HH:mm","w-'я' 'неделя' Y 'г'., HH:mm","w-'я' 'неделя' Y 'г'., HH:mm","w-'я' 'неделя' Y 'г'., HH:mm","EEEE, d MMMM y 'г'., h:mm:ss a","d MMMM y 'г'., h:mm:ss a","d MMM y 'г'., h:mm:ss a","dd.MM.y, h:mm:ss a","d, h:mm:ss a","ccc, h:mm:ss a","ccc, d, h:mm:ss a","y 'г'. G, h:mm:ss a","LLL y G, h:mm:ss a","d MMM y 'г'. G, h:mm:ss a","E, d MMM y 'г'. G, h:mm:ss a","L, h:mm:ss a","dd.MM, h:mm:ss a","E, dd.MM, h:mm:ss a","dd.MM, h:mm:ss a","LLL, h:mm:ss a","d MMM, h:mm:ss a","ccc, d MMM, h:mm:ss a","d MMMM, h:mm:ss a","W-'я' 'неделя' MMMM, h:mm:ss a","W-'я' 'неделя' MMMM, h:mm:ss a","W-'я' 'неделя' MMMM, h:mm:ss a","W-'я' 'неделя' MMMM, h:mm:ss a","y, h:mm:ss a","MM.y, h:mm:ss a","dd.MM.y, h:mm:ss a","ccc, dd.MM.y 'г'., h:mm:ss a","MM.y, h:mm:ss a","LLL y 'г'., h:mm:ss a","d MMM y 'г'., h:mm:ss a","E, d MMM y 'г'., h:mm:ss a","LLLL y 'г'., h:mm:ss a","QQQ y 'г'., h:mm:ss a","QQQQ y 'г'., h:mm:ss a","w-'я' 'неделя' Y 'г'., h:mm:ss a","w-'я' 'неделя' Y 'г'., h:mm:ss a","w-'я' 'неделя' Y 'г'., h:mm:ss a","w-'я' 'неделя' Y 'г'., h:mm:ss a","EEEE, d MMMM y 'г'., HH:mm:ss","d MMMM y 'г'., HH:mm:ss","d MMM y 'г'., HH:mm:ss","dd.MM.y, HH:mm:ss","d, HH:mm:ss","ccc, HH:mm:ss","ccc, d, HH:mm:ss","y 'г'. G, HH:mm:ss","LLL y G, HH:mm:ss","d MMM y 'г'. G, HH:mm:ss","E, d MMM y 'г'. G, HH:mm:ss","L, HH:mm:ss","dd.MM, HH:mm:ss","E, dd.MM, HH:mm:ss","dd.MM, HH:mm:ss","LLL, HH:mm:ss","d MMM, HH:mm:ss","ccc, d MMM, HH:mm:ss","d MMMM, HH:mm:ss","W-'я' 'неделя' MMMM, HH:mm:ss","W-'я' 'неделя' MMMM, HH:mm:ss","W-'я' 'неделя' MMMM, HH:mm:ss","W-'я' 'неделя' MMMM, HH:mm:ss","y, HH:mm:ss","MM.y, HH:mm:ss","dd.MM.y, HH:mm:ss","ccc, dd.MM.y 'г'., HH:mm:ss","MM.y, HH:mm:ss","LLL y 'г'., HH:mm:ss","d MMM y 'г'., HH:mm:ss","E, d MMM y 'г'., HH:mm:ss","LLLL y 'г'., HH:mm:ss","QQQ y 'г'., HH:mm:ss","QQQQ y 'г'., HH:mm:ss","w-'я' 'неделя' Y 'г'., HH:mm:ss","w-'я' 'неделя' Y 'г'., HH:mm:ss","w-'я' 'неделя' Y 'г'., HH:mm:ss","w-'я' 'неделя' Y 'г'., HH:mm:ss","EEEE, d MMMM y 'г'., h:mm:ss a v","d MMMM y 'г'., h:mm:ss a v","d MMM y 'г'., h:mm:ss a v","dd.MM.y, h:mm:ss a v","d, h:mm:ss a v","ccc, h:mm:ss a v","ccc, d, h:mm:ss a v","y 'г'. G, h:mm:ss a v","LLL y G, h:mm:ss a v","d MMM y 'г'. G, h:mm:ss a v","E, d MMM y 'г'. G, h:mm:ss a v","L, h:mm:ss a v","dd.MM, h:mm:ss a v","E, dd.MM, h:mm:ss a v","dd.MM, h:mm:ss a v","LLL, h:mm:ss a v","d MMM, h:mm:ss a v","ccc, d MMM, h:mm:ss a v","d MMMM, h:mm:ss a v","W-'я' 'неделя' MMMM, h:mm:ss a v","W-'я' 'неделя' MMMM, h:mm:ss a v","W-'я' 'неделя' MMMM, h:mm:ss a v","W-'я' 'неделя' MMMM, h:mm:ss a v","y, h:mm:ss a v","MM.y, h:mm:ss a v","dd.MM.y, h:mm:ss a v","ccc, dd.MM.y 'г'., h:mm:ss a v","MM.y, h:mm:ss a v","LLL y 'г'., h:mm:ss a v","d MMM y 'г'., h:mm:ss a v","E, d MMM y 'г'., h:mm:ss a v","LLLL y 'г'., h:mm:ss a v","QQQ y 'г'., h:mm:ss a v","QQQQ y 'г'., h:mm:ss a v","w-'я' 'неделя' Y 'г'., h:mm:ss a v","w-'я' 'неделя' Y 'г'., h:mm:ss a v","w-'я' 'неделя' Y 'г'., h:mm:ss a v","w-'я' 'неделя' Y 'г'., h:mm:ss a v","EEEE, d MMMM y 'г'., HH:mm:ss v","d MMMM y 'г'., HH:mm:ss v","d MMM y 'г'., HH:mm:ss v","dd.MM.y, HH:mm:ss v","d, HH:mm:ss v","ccc, HH:mm:ss v","ccc, d, HH:mm:ss v","y 'г'. G, HH:mm:ss v","LLL y G, HH:mm:ss v","d MMM y 'г'. G, HH:mm:ss v","E, d MMM y 'г'. G, HH:mm:ss v","L, HH:mm:ss v","dd.MM, HH:mm:ss v","E, dd.MM, HH:mm:ss v","dd.MM, HH:mm:ss v","LLL, HH:mm:ss v","d MMM, HH:mm:ss v","ccc, d MMM, HH:mm:ss v","d MMMM, HH:mm:ss v","W-'я' 'неделя' MMMM, HH:mm:ss v","W-'я' 'неделя' MMMM, HH:mm:ss v","W-'я' 'неделя' MMMM, HH:mm:ss v","W-'я' 'неделя' MMMM, HH:mm:ss v","y, HH:mm:ss v","MM.y, HH:mm:ss v","dd.MM.y, HH:mm:ss v","ccc, dd.MM.y 'г'., HH:mm:ss v","MM.y, HH:mm:ss v","LLL y 'г'., HH:mm:ss v","d MMM y 'г'., HH:mm:ss v","E, d MMM y 'г'., HH:mm:ss v","LLLL y 'г'., HH:mm:ss v","QQQ y 'г'., HH:mm:ss v","QQQQ y 'г'., HH:mm:ss v","w-'я' 'неделя' Y 'г'., HH:mm:ss v","w-'я' 'неделя' Y 'г'., HH:mm:ss v","w-'я' 'неделя' Y 'г'., HH:mm:ss v","w-'я' 'неделя' Y 'г'., HH:mm:ss v","EEEE, d MMMM y 'г'., h:mm a v","d MMMM y 'г'., h:mm a v","d MMM y 'г'., h:mm a v","dd.MM.y, h:mm a v","d, h:mm a v","ccc, h:mm a v","ccc, d, h:mm a v","y 'г'. G, h:mm a v","LLL y G, h:mm a v","d MMM y 'г'. G, h:mm a v","E, d MMM y 'г'. G, h:mm a v","L, h:mm a v","dd.MM, h:mm a v","E, dd.MM, h:mm a v","dd.MM, h:mm a v","LLL, h:mm a v","d MMM, h:mm a v","ccc, d MMM, h:mm a v","d MMMM, h:mm a v","W-'я' 'неделя' MMMM, h:mm a v","W-'я' 'неделя' MMMM, h:mm a v","W-'я' 'неделя' MMMM, h:mm a v","W-'я' 'неделя' MMMM, h:mm a v","y, h:mm a v","MM.y, h:mm a v","dd.MM.y, h:mm a v","ccc, dd.MM.y 'г'., h:mm a v","MM.y, h:mm a v","LLL y 'г'., h:mm a v","d MMM y 'г'., h:mm a v","E, d MMM y 'г'., h:mm a v","LLLL y 'г'., h:mm a v","QQQ y 'г'., h:mm a v","QQQQ y 'г'., h:mm a v","w-'я' 'неделя' Y 'г'., h:mm a v","w-'я' 'неделя' Y 'г'., h:mm a v","w-'я' 'неделя' Y 'г'., h:mm a v","w-'я' 'неделя' Y 'г'., h:mm a v","EEEE, d MMMM y 'г'., HH:mm v","d MMMM y 'г'., HH:mm v","d MMM y 'г'., HH:mm v","dd.MM.y, HH:mm v","d, HH:mm v","ccc, HH:mm v","ccc, d, HH:mm v","y 'г'. G, HH:mm v","LLL y G, HH:mm v","d MMM y 'г'. G, HH:mm v","E, d MMM y 'г'. G, HH:mm v","L, HH:mm v","dd.MM, HH:mm v","E, dd.MM, HH:mm v","dd.MM, HH:mm v","LLL, HH:mm v","d MMM, HH:mm v","ccc, d MMM, HH:mm v","d MMMM, HH:mm v","W-'я' 'неделя' MMMM, HH:mm v","W-'я' 'неделя' MMMM, HH:mm v","W-'я' 'неделя' MMMM, HH:mm v","W-'я' 'неделя' MMMM, HH:mm v","y, HH:mm v","MM.y, HH:mm v","dd.MM.y, HH:mm v","ccc, dd.MM.y 'г'., HH:mm v","MM.y, HH:mm v","LLL y 'г'., HH:mm v","d MMM y 'г'., HH:mm v","E, d MMM y 'г'., HH:mm v","LLLL y 'г'., HH:mm v","QQQ y 'г'., HH:mm v","QQQQ y 'г'., HH:mm v","w-'я' 'неделя' Y 'г'., HH:mm v","w-'я' 'неделя' Y 'г'., HH:mm v","w-'я' 'неделя' Y 'г'., HH:mm v","w-'я' 'неделя' Y 'г'., HH:mm v","EEEE, d MMMM y 'г'., mm:ss","d MMMM y 'г'., mm:ss","d MMM y 'г'., mm:ss","dd.MM.y, mm:ss","d, mm:ss","ccc, mm:ss","ccc, d, mm:ss","y 'г'. G, mm:ss","LLL y G, mm:ss","d MMM y 'г'. G, mm:ss","E, d MMM y 'г'. G, mm:ss","L, mm:ss","dd.MM, mm:ss","E, dd.MM, mm:ss","dd.MM, mm:ss","LLL, mm:ss","d MMM, mm:ss","ccc, d MMM, mm:ss","d MMMM, mm:ss","W-'я' 'неделя' MMMM, mm:ss","W-'я' 'неделя' MMMM, mm:ss","W-'я' 'неделя' MMMM, mm:ss","W-'я' 'неделя' MMMM, mm:ss","y, mm:ss","MM.y, mm:ss","dd.MM.y, mm:ss","ccc, dd.MM.y 'г'., mm:ss","MM.y, mm:ss","LLL y 'г'., mm:ss","d MMM y 'г'., mm:ss","E, d MMM y 'г'., mm:ss","LLLL y 'г'., mm:ss","QQQ y 'г'., mm:ss","QQQQ y 'г'., mm:ss","w-'я' 'неделя' Y 'г'., mm:ss","w-'я' 'неделя' Y 'г'., mm:ss","w-'я' 'неделя' Y 'г'., mm:ss","w-'я' 'неделя' Y 'г'., mm:ss"]},"hourCycle":"h23","nu":["latn"],"ca":["gregory"],"hc":["h23"]}},"availableLocales":["ru"]})
  }
// Intl.NumberFormat.~locale.ru
/* @generated */
// prettier-ignore
if (Intl.NumberFormat && typeof Intl.NumberFormat.__addLocaleData === 'function') {
  Intl.NumberFormat.__addLocaleData({"data":{"ru":{"units":{"simple":{"degree":{"long":{"other":"{0} градуса","one":"{0} градус","many":"{0} градусов"},"short":{"other":"{0}°"},"narrow":{"other":"{0}°"},"perUnit":{}},"hectare":{"long":{"other":"{0} гектара","one":"{0} гектар","many":"{0} гектаров"},"short":{"other":"{0} га"},"narrow":{"other":"{0} га"},"perUnit":{}},"acre":{"long":{"other":"{0} акра","one":"{0} акр","many":"{0} акров"},"short":{"other":"{0} акр.","one":"{0} акр"},"narrow":{"other":"{0} акра","one":"{0} акр","many":"{0} акров"},"perUnit":{}},"percent":{"long":{"other":"{0} процента","one":"{0} процент","many":"{0} процентов"},"short":{"other":"{0} %"},"narrow":{"other":"{0}%"},"perUnit":{}},"liter-per-kilometer":{"long":{"other":"{0} литра на километр","one":"{0} литр на километр","many":"{0} литров на километр"},"short":{"other":"{0} л/км"},"narrow":{"other":"{0} л/км"},"perUnit":{}},"mile-per-gallon":{"long":{"other":"{0} мили на галлон","one":"{0} миля на галлон","many":"{0} миль на галлон"},"short":{"other":"{0} мили/гал","one":"{0} миля/гал","many":"{0} миль/гал"},"narrow":{"other":"{0} мили/гал","one":"{0} миля/гал","many":"{0} миль/гал"},"perUnit":{}},"petabyte":{"long":{"other":"{0} петабайта","one":"{0} петабайт","many":"{0} петабайт"},"short":{"other":"{0} ПБ"},"narrow":{"other":"{0} ПБ"},"perUnit":{}},"terabyte":{"long":{"other":"{0} терабайта","one":"{0} терабайт","many":"{0} терабайт"},"short":{"other":"{0} ТБ"},"narrow":{"other":"{0} ТБ"},"perUnit":{}},"terabit":{"long":{"other":"{0} терабита","one":"{0} терабит","many":"{0} терабит"},"short":{"other":"{0} Тбит"},"narrow":{"other":"{0} Тбит"},"perUnit":{}},"gigabyte":{"long":{"other":"{0} гигабайта","one":"{0} гигабайт","many":"{0} гигабайт"},"short":{"other":"{0} ГБ"},"narrow":{"other":"{0} ГБ"},"perUnit":{}},"gigabit":{"long":{"other":"{0} гигабита","one":"{0} гигабит","many":"{0} гигабит"},"short":{"other":"{0} Гбит"},"narrow":{"other":"{0} Гбит"},"perUnit":{}},"megabyte":{"long":{"other":"{0} мегабайта","one":"{0} мегабайт","many":"{0} мегабайт"},"short":{"other":"{0} МБ"},"narrow":{"other":"{0} МБ"},"perUnit":{}},"megabit":{"long":{"other":"{0} мегабита","one":"{0} мегабит","many":"{0} мегабит"},"short":{"other":"{0} Мбит"},"narrow":{"other":"{0} Мбит"},"perUnit":{}},"kilobyte":{"long":{"other":"{0} килобайта","one":"{0} килобайт","many":"{0} килобайт"},"short":{"other":"{0} кБ"},"narrow":{"other":"{0} кБ"},"perUnit":{}},"kilobit":{"long":{"other":"{0} килобита","one":"{0} килобит","many":"{0} килобит"},"short":{"other":"{0} кбит"},"narrow":{"other":"{0} кбит"},"perUnit":{}},"byte":{"long":{"other":"{0} байта","one":"{0} байт","many":"{0} байт"},"short":{"other":"{0} Б"},"narrow":{"other":"{0} Б"},"perUnit":{}},"bit":{"long":{"other":"{0} бита","one":"{0} бит","many":"{0} бит"},"short":{"other":"{0} бита","one":"{0} бит","many":"{0} бит"},"narrow":{"other":"{0} бита","one":"{0} бит","many":"{0} бит"},"perUnit":{}},"year":{"long":{"other":"{0} года","one":"{0} год","many":"{0} лет"},"short":{"other":"{0} г.","many":"{0} л."},"narrow":{"other":"{0} г.","many":"{0} л."},"perUnit":{"long":"{0} в год","short":"{0}/г","narrow":"{0}/г."}},"month":{"long":{"other":"{0} месяца","one":"{0} месяц","many":"{0} месяцев"},"short":{"other":"{0} мес."},"narrow":{"other":"{0} м."},"perUnit":{"long":"{0} в месяц","short":"{0}/мес","narrow":"{0}/м."}},"week":{"long":{"other":"{0} недели","one":"{0} неделя","many":"{0} недель"},"short":{"other":"{0} нед."},"narrow":{"other":"{0} н."},"perUnit":{"long":"{0} в неделю","short":"{0}/нед","narrow":"{0}/н."}},"day":{"long":{"other":"{0} дня","one":"{0} день","many":"{0} дней"},"short":{"other":"{0} дн."},"narrow":{"other":"{0} д."},"perUnit":{"long":"{0} в день","short":"{0}/д","narrow":"{0}/д."}},"hour":{"long":{"other":"{0} часа","one":"{0} час","many":"{0} часов"},"short":{"other":"{0} ч"},"narrow":{"other":"{0} ч"},"perUnit":{"long":"{0} в час","short":"{0}/ч","narrow":"{0}/ч."}},"minute":{"long":{"other":"{0} минуты","one":"{0} минута","many":"{0} минут"},"short":{"other":"{0} мин."},"narrow":{"other":"{0} мин"},"perUnit":{"long":"{0} в минуту","short":"{0}/мин","narrow":"{0}/мин."}},"second":{"long":{"other":"{0} секунды","one":"{0} секунда","many":"{0} секунд"},"short":{"other":"{0} сек."},"narrow":{"other":"{0} с"},"perUnit":{"long":"{0} в секунду","short":"{0}/c","narrow":"{0}/c"}},"millisecond":{"long":{"other":"{0} миллисекунды","one":"{0} миллисекунда","many":"{0} миллисекунд"},"short":{"other":"{0} мс"},"narrow":{"other":"{0} мс"},"perUnit":{}},"kilometer":{"long":{"other":"{0} километра","one":"{0} километр","many":"{0} километров"},"short":{"other":"{0} км"},"narrow":{"other":"{0} км"},"perUnit":{"long":"{0} на километр","short":"{0}/км","narrow":"{0}/км"}},"meter":{"long":{"other":"{0} метра","one":"{0} метр","many":"{0} метров"},"short":{"other":"{0} м"},"narrow":{"other":"{0} м"},"perUnit":{"long":"{0} на метр","short":"{0}/м","narrow":"{0}/м"}},"centimeter":{"long":{"other":"{0} сантиметра","one":"{0} сантиметр","many":"{0} сантиметров"},"short":{"other":"{0} см"},"narrow":{"other":"{0} см"},"perUnit":{"long":"{0} на сантиметр","short":"{0}/см","narrow":"{0}/см"}},"millimeter":{"long":{"other":"{0} миллиметра","one":"{0} миллиметр","many":"{0} миллиметров"},"short":{"other":"{0} мм"},"narrow":{"other":"{0} мм"},"perUnit":{}},"mile":{"long":{"other":"{0} мили","one":"{0} миля","many":"{0} миль"},"short":{"other":"{0} мили","one":"{0} миля","many":"{0} миль"},"narrow":{"other":"{0} мили","one":"{0} миля","few":"{0} миль","many":"{0} миль"},"perUnit":{}},"yard":{"long":{"other":"{0} ярда","one":"{0} ярд","many":"{0} ярдов"},"short":{"other":"{0} ярд.","one":"{0} ярд"},"narrow":{"other":"{0} ярда","one":"{0} ярд","many":"{0} ярдов"},"perUnit":{}},"foot":{"long":{"other":"{0} фута","one":"{0} фут","many":"{0} футов"},"short":{"other":"{0} фт"},"narrow":{"other":"{0} фт"},"perUnit":{"long":"{0} на фут","short":"{0}/фт","narrow":"{0}/фт"}},"inch":{"long":{"other":"{0} дюйма","one":"{0} дюйм","many":"{0} дюймов"},"short":{"other":"{0} дюйм.","one":"{0} дюйм"},"narrow":{"other":"{0} дюйма","one":"{0} дюйм","many":"{0} дюймов"},"perUnit":{"long":"{0} на дюйм","short":"{0}/дюйм","narrow":"{0}/дюйм."}},"mile-scandinavian":{"long":{"other":"{0} скандинавской мили","one":"{0} скандинавская миля","few":"{0} скандинавские мили","many":"{0} скандинавских миль"},"short":{"other":"{0} ск. мил."},"narrow":{"other":"{0} ск. мл."},"perUnit":{}},"kilogram":{"long":{"other":"{0} килограмма","one":"{0} килограмм","many":"{0} килограмм"},"short":{"other":"{0} кг"},"narrow":{"other":"{0} кг"},"perUnit":{"long":"{0} на килограмм","short":"{0}/кг","narrow":"{0}/кг"}},"gram":{"long":{"other":"{0} грамма","one":"{0} грамм","many":"{0} грамм"},"short":{"other":"{0} г"},"narrow":{"other":"{0} г"},"perUnit":{"long":"{0} на грамм","short":"{0}/г","narrow":"{0}/г"}},"stone":{"long":{"other":"{0} стоуна","one":"{0} стоун","many":"{0} стоунов"},"short":{"other":"{0} стоуна","one":"{0} стоун","many":"{0} стоунов"},"narrow":{"other":"{0} стн"},"perUnit":{}},"pound":{"long":{"other":"{0} фунта","one":"{0} фунт","many":"{0} фунтов"},"short":{"other":"{0} фунт."},"narrow":{"other":"{0} lb"},"perUnit":{"long":"{0} на фунт","short":"{0}/фунт","narrow":"{0}/фнт"}},"ounce":{"long":{"other":"{0} унции","one":"{0} унция","many":"{0} унций"},"short":{"other":"{0} унц."},"narrow":{"other":"{0} oz"},"perUnit":{"long":"{0} на унцию","short":"{0}/унц","narrow":"{0}/унц."}},"kilometer-per-hour":{"long":{"other":"{0} километра в час","one":"{0} километр в час","many":"{0} километров в час"},"short":{"other":"{0} км/ч"},"narrow":{"other":"{0} км/ч"},"perUnit":{}},"meter-per-second":{"long":{"other":"{0} метра в секунду","one":"{0} метр в секунду","many":"{0} метров в секунду"},"short":{"other":"{0} м/с"},"narrow":{"other":"{0} м/с"},"perUnit":{}},"mile-per-hour":{"long":{"other":"{0} мили в час","one":"{0} миля в час","many":"{0} миль в час"},"short":{"other":"{0} мили/час","one":"{0} миля/час","many":"{0} миль/час"},"narrow":{"other":"{0} миль/ч"},"perUnit":{}},"celsius":{"long":{"other":"{0} градуса Цельсия","one":"{0} градус Цельсия","many":"{0} градусов Цельсия"},"short":{"other":"{0} °C"},"narrow":{"other":"{0} °C"},"perUnit":{}},"fahrenheit":{"long":{"other":"{0} градуса Фаренгейта","one":"{0} градус Фаренгейта","many":"{0} градусов Фаренгейта"},"short":{"other":"{0} °F"},"narrow":{"other":"{0}°F","few":"{0} °F"},"perUnit":{}},"liter":{"long":{"other":"{0} литра","one":"{0} литр","many":"{0} литров"},"short":{"other":"{0} л"},"narrow":{"other":"{0} л"},"perUnit":{"long":"{0} на литр","short":"{0}/л","narrow":"{0}/л"}},"milliliter":{"long":{"other":"{0} миллилитра","one":"{0} миллилитр","many":"{0} миллилитров"},"short":{"other":"{0} мл"},"narrow":{"other":"{0} мл"},"perUnit":{}},"gallon":{"long":{"other":"{0} галлона","one":"{0} галлон","many":"{0} галлонов"},"short":{"other":"{0} гал."},"narrow":{"other":"{0} гал."},"perUnit":{"long":"{0} на галлон","short":"{0}/гал","narrow":"{0}/гал"}},"fluid-ounce":{"long":{"other":"{0} жидкой унции","one":"{0} жидкая унция","few":"{0} жидкие унции","many":"{0} жидких унций"},"short":{"other":"{0} жидк. унц."},"narrow":{"other":"{0} жидк. унц."},"perUnit":{}}},"compound":{"per":{"long":"{0}/{1}","short":"{0}/{1}","narrow":"{0}/{1}"}}},"currencies":{"ADP":{"displayName":{"other":"андоррских песет","one":"андоррская песета","few":"андоррские песеты"},"symbol":"ADP","narrow":"ADP"},"AED":{"displayName":{"other":"дирхама ОАЭ","one":"дирхам ОАЭ","many":"дирхамов ОАЭ"},"symbol":"AED","narrow":"AED"},"AFA":{"displayName":{"other":"Афгани (1927–2002)"},"symbol":"AFA","narrow":"AFA"},"AFN":{"displayName":{"other":"афгани"},"symbol":"AFN","narrow":"AFN"},"ALK":{"displayName":{"other":"ALK"},"symbol":"ALK","narrow":"ALK"},"ALL":{"displayName":{"other":"албанского лека","one":"албанский лек","few":"албанских лека","many":"албанских леков"},"symbol":"ALL","narrow":"ALL"},"AMD":{"displayName":{"other":"армянского драма","one":"армянский драм","few":"армянских драма","many":"армянских драмов"},"symbol":"AMD","narrow":"AMD"},"ANG":{"displayName":{"other":"нидерландского антильского гульдена","one":"нидерландский антильский гульден","few":"нидерландских антильских гульдена","many":"нидерландских антильских гульденов"},"symbol":"ANG","narrow":"ANG"},"AOA":{"displayName":{"other":"ангольской кванзы","one":"ангольская кванза","few":"ангольские кванзы","many":"ангольских кванз"},"symbol":"AOA","narrow":"Kz"},"AOK":{"displayName":{"other":"ангольских кванз (1977–1991)","few":"ангольские кванзы (1977–1991)"},"symbol":"AOK","narrow":"AOK"},"AON":{"displayName":{"other":"Ангольская новая кванза (1990–2000)"},"symbol":"AON","narrow":"AON"},"AOR":{"displayName":{"other":"ангольских кванз реюстадо (1995–1999)","few":"ангольские кванзы реюстадо (1995–1999)"},"symbol":"AOR","narrow":"AOR"},"ARA":{"displayName":{"other":"Аргентинский аустрал"},"symbol":"ARA","narrow":"ARA"},"ARL":{"displayName":{"other":"ARL"},"symbol":"ARL","narrow":"ARL"},"ARM":{"displayName":{"other":"ARM"},"symbol":"ARM","narrow":"ARM"},"ARP":{"displayName":{"other":"Аргентинское песо (1983–1985)"},"symbol":"ARP","narrow":"ARP"},"ARS":{"displayName":{"other":"аргентинского песо","one":"аргентинский песо","few":"аргентинских песо","many":"аргентинских песо"},"symbol":"ARS","narrow":"$"},"ATS":{"displayName":{"other":"Австрийский шиллинг"},"symbol":"ATS","narrow":"ATS"},"AUD":{"displayName":{"other":"австралийского доллара","one":"австралийский доллар","few":"австралийских доллара","many":"австралийских долларов"},"symbol":"A$","narrow":"$"},"AWG":{"displayName":{"other":"арубанского флорина","one":"арубанский флорин","few":"арубанских флорина","many":"арубанских флоринов"},"symbol":"AWG","narrow":"AWG"},"AZM":{"displayName":{"other":"Старый азербайджанский манат"},"symbol":"AZM","narrow":"AZM"},"AZN":{"displayName":{"other":"азербайджанского маната","one":"азербайджанский манат","few":"азербайджанских маната","many":"азербайджанских манатов"},"symbol":"AZN","narrow":"AZN"},"BAD":{"displayName":{"other":"Динар Боснии и Герцеговины"},"symbol":"BAD","narrow":"BAD"},"BAM":{"displayName":{"other":"конвертируемой марки Боснии и Герцеговины","one":"конвертируемая марка Боснии и Герцеговины","few":"конвертируемые марки Боснии и Герцеговины","many":"конвертируемых марок Боснии и Герцеговины"},"symbol":"BAM","narrow":"KM"},"BAN":{"displayName":{"other":"BAN"},"symbol":"BAN","narrow":"BAN"},"BBD":{"displayName":{"other":"барбадосского доллара","one":"барбадосский доллар","few":"барбадосских доллара","many":"барбадосских долларов"},"symbol":"BBD","narrow":"$"},"BDT":{"displayName":{"other":"бангладешской таки","one":"бангладешская така","few":"бангладешские таки","many":"бангладешских так"},"symbol":"BDT","narrow":"৳"},"BEC":{"displayName":{"other":"Бельгийский франк (конвертируемый)"},"symbol":"BEC","narrow":"BEC"},"BEF":{"displayName":{"other":"Бельгийский франк"},"symbol":"BEF","narrow":"BEF"},"BEL":{"displayName":{"other":"Бельгийский франк (финансовый)"},"symbol":"BEL","narrow":"BEL"},"BGL":{"displayName":{"other":"Лев"},"symbol":"BGL","narrow":"BGL"},"BGM":{"displayName":{"other":"BGM"},"symbol":"BGM","narrow":"BGM"},"BGN":{"displayName":{"other":"болгарского лева","one":"болгарский лев","few":"болгарских лева","many":"болгарских левов"},"symbol":"BGN","narrow":"BGN"},"BGO":{"displayName":{"other":"BGO"},"symbol":"BGO","narrow":"BGO"},"BHD":{"displayName":{"other":"бахрейнского динара","one":"бахрейнский динар","few":"бахрейнских динара","many":"бахрейнских динаров"},"symbol":"BHD","narrow":"BHD"},"BIF":{"displayName":{"other":"бурундийского франка","one":"бурундийский франк","few":"бурундийских франка","many":"бурундийских франков"},"symbol":"BIF","narrow":"BIF"},"BMD":{"displayName":{"other":"бермудского доллара","one":"бермудский доллар","few":"бермудских доллара","many":"бермудских долларов"},"symbol":"BMD","narrow":"$"},"BND":{"displayName":{"other":"брунейского доллара","one":"брунейский доллар","few":"брунейских доллара","many":"брунейских долларов"},"symbol":"BND","narrow":"$"},"BOB":{"displayName":{"other":"боливийского боливиано","one":"боливийский боливиано","few":"боливийских боливиано","many":"боливийских боливиано"},"symbol":"BOB","narrow":"Bs"},"BOL":{"displayName":{"other":"BOL"},"symbol":"BOL","narrow":"BOL"},"BOP":{"displayName":{"other":"Боливийское песо"},"symbol":"BOP","narrow":"BOP"},"BOV":{"displayName":{"other":"Боливийский мвдол"},"symbol":"BOV","narrow":"BOV"},"BRB":{"displayName":{"other":"Бразильский новый крузейро (1967–1986)"},"symbol":"BRB","narrow":"BRB"},"BRC":{"displayName":{"other":"Бразильское крузадо"},"symbol":"BRC","narrow":"BRC"},"BRE":{"displayName":{"other":"Бразильский крузейро (1990–1993)"},"symbol":"BRE","narrow":"BRE"},"BRL":{"displayName":{"other":"бразильского реала","one":"бразильский реал","few":"бразильских реала","many":"бразильских реалов"},"symbol":"R$","narrow":"R$"},"BRN":{"displayName":{"other":"Бразильское новое крузадо"},"symbol":"BRN","narrow":"BRN"},"BRR":{"displayName":{"other":"Бразильский крузейро"},"symbol":"BRR","narrow":"BRR"},"BRZ":{"displayName":{"other":"BRZ"},"symbol":"BRZ","narrow":"BRZ"},"BSD":{"displayName":{"other":"багамского доллара","one":"багамский доллар","few":"багамских доллара","many":"багамских долларов"},"symbol":"BSD","narrow":"$"},"BTN":{"displayName":{"other":"бутанского нгултрума","one":"бутанский нгултрум","few":"бутанских нгултрума","many":"бутанских нгултрумов"},"symbol":"BTN","narrow":"BTN"},"BUK":{"displayName":{"other":"Джа"},"symbol":"BUK","narrow":"BUK"},"BWP":{"displayName":{"other":"ботсванской пулы","one":"ботсванская пула","few":"ботсванские пулы","many":"ботсванских пул"},"symbol":"BWP","narrow":"P"},"BYB":{"displayName":{"other":"Белорусский рубль (1994–1999)"},"symbol":"BYB","narrow":"BYB"},"BYN":{"displayName":{"other":"белорусского рубля","one":"белорусский рубль","few":"белорусских рубля","many":"белорусских рублей"},"symbol":"BYN","narrow":"р."},"BYR":{"displayName":{"other":"белорусского рубля (2000–2016)","one":"белорусский рубль (2000–2016)","few":"белорусских рубля (2000–2016)","many":"белорусских рублей (2000–2016)"},"symbol":"BYR","narrow":"BYR"},"BZD":{"displayName":{"other":"белизского доллара","one":"белизский доллар","few":"белизских доллара","many":"белизских долларов"},"symbol":"BZD","narrow":"$"},"CAD":{"displayName":{"other":"канадского доллара","one":"канадский доллар","few":"канадских доллара","many":"канадских долларов"},"symbol":"CA$","narrow":"$"},"CDF":{"displayName":{"other":"конголезского франка","one":"конголезский франк","few":"конголезских франка","many":"конголезских франков"},"symbol":"CDF","narrow":"CDF"},"CHE":{"displayName":{"other":"WIR евро"},"symbol":"CHE","narrow":"CHE"},"CHF":{"displayName":{"other":"швейцарского франка","one":"швейцарский франк","few":"швейцарских франка","many":"швейцарских франков"},"symbol":"CHF","narrow":"CHF"},"CHW":{"displayName":{"other":"WIR франк"},"symbol":"CHW","narrow":"CHW"},"CLE":{"displayName":{"other":"CLE"},"symbol":"CLE","narrow":"CLE"},"CLF":{"displayName":{"other":"Условная расчетная единица Чили"},"symbol":"CLF","narrow":"CLF"},"CLP":{"displayName":{"other":"чилийского песо","one":"чилийский песо","few":"чилийских песо","many":"чилийских песо"},"symbol":"CLP","narrow":"$"},"CNH":{"displayName":{"other":"китайского офшорного юаня","one":"китайский офшорный юань","few":"китайских офшорных юаня","many":"китайских офшорных юаней"},"symbol":"CNH","narrow":"CNH"},"CNX":{"displayName":{"other":"CNX"},"symbol":"CNX","narrow":"CNX"},"CNY":{"displayName":{"other":"китайского юаня","one":"китайский юань","few":"китайских юаня","many":"китайских юаней"},"symbol":"CN¥","narrow":"¥"},"COP":{"displayName":{"other":"колумбийского песо","one":"колумбийский песо","few":"колумбийских песо","many":"колумбийских песо"},"symbol":"COP","narrow":"$"},"COU":{"displayName":{"other":"Единица реальной стоимости Колумбии"},"symbol":"COU","narrow":"COU"},"CRC":{"displayName":{"other":"костариканского колона","one":"костариканский колон","few":"костариканских колона","many":"костариканских колонов"},"symbol":"CRC","narrow":"₡"},"CSD":{"displayName":{"other":"Старый Сербский динар"},"symbol":"CSD","narrow":"CSD"},"CSK":{"displayName":{"other":"Чехословацкая твердая крона"},"symbol":"CSK","narrow":"CSK"},"CUC":{"displayName":{"other":"кубинского конвертируемого песо","one":"кубинский конвертируемый песо","few":"кубинских конвертируемых песо","many":"кубинских конвертируемых песо"},"symbol":"CUC","narrow":"$"},"CUP":{"displayName":{"other":"кубинского песо","one":"кубинский песо","few":"кубинских песо","many":"кубинских песо"},"symbol":"CUP","narrow":"$"},"CVE":{"displayName":{"other":"эскудо Кабо-Верде"},"symbol":"CVE","narrow":"CVE"},"CYP":{"displayName":{"other":"Кипрский фунт"},"symbol":"CYP","narrow":"CYP"},"CZK":{"displayName":{"other":"чешской кроны","one":"чешская крона","few":"чешские кроны","many":"чешских крон"},"symbol":"CZK","narrow":"Kč"},"DDM":{"displayName":{"other":"Восточногерманская марка"},"symbol":"DDM","narrow":"DDM"},"DEM":{"displayName":{"other":"Немецкая марка"},"symbol":"DEM","narrow":"DEM"},"DJF":{"displayName":{"other":"франка Джибути","one":"франк Джибути","many":"франков Джибути"},"symbol":"DJF","narrow":"DJF"},"DKK":{"displayName":{"other":"датской кроны","one":"датская крона","few":"датские кроны","many":"датских крон"},"symbol":"DKK","narrow":"kr"},"DOP":{"displayName":{"other":"доминиканского песо","one":"доминиканский песо","few":"доминиканских песо","many":"доминиканских песо"},"symbol":"DOP","narrow":"$"},"DZD":{"displayName":{"other":"алжирского динара","one":"алжирский динар","few":"алжирских динара","many":"алжирских динаров"},"symbol":"DZD","narrow":"DZD"},"ECS":{"displayName":{"other":"Эквадорский сукре"},"symbol":"ECS","narrow":"ECS"},"ECV":{"displayName":{"other":"Постоянная единица стоимости Эквадора"},"symbol":"ECV","narrow":"ECV"},"EEK":{"displayName":{"other":"Эстонская крона"},"symbol":"EEK","narrow":"EEK"},"EGP":{"displayName":{"other":"египетского фунта","one":"египетский фунт","few":"египетских фунта","many":"египетских фунтов"},"symbol":"EGP","narrow":"E£"},"ERN":{"displayName":{"other":"эритрейской накфы","one":"эритрейская накфа","few":"эритрейские накфы","many":"эритрейских накф"},"symbol":"ERN","narrow":"ERN"},"ESA":{"displayName":{"other":"Испанская песета (А)"},"symbol":"ESA","narrow":"ESA"},"ESB":{"displayName":{"other":"Испанская песета (конвертируемая)"},"symbol":"ESB","narrow":"ESB"},"ESP":{"displayName":{"other":"Испанская песета"},"symbol":"ESP","narrow":"₧"},"ETB":{"displayName":{"other":"эфиопского быра","one":"эфиопский быр","few":"эфиопских быра","many":"эфиопских быров"},"symbol":"ETB","narrow":"ETB"},"EUR":{"displayName":{"other":"евро"},"symbol":"€","narrow":"€"},"FIM":{"displayName":{"other":"Финская марка"},"symbol":"FIM","narrow":"FIM"},"FJD":{"displayName":{"other":"доллара Фиджи","one":"доллар Фиджи","many":"долларов Фиджи"},"symbol":"FJD","narrow":"$"},"FKP":{"displayName":{"other":"фунта Фолклендских островов","one":"фунт Фолклендских островов","many":"фунтов Фолклендских островов"},"symbol":"FKP","narrow":"£"},"FRF":{"displayName":{"other":"Французский франк"},"symbol":"FRF","narrow":"FRF"},"GBP":{"displayName":{"other":"британского фунта стерлингов","one":"британский фунт стерлингов","few":"британских фунта стерлингов","many":"британских фунтов стерлингов"},"symbol":"£","narrow":"£"},"GEK":{"displayName":{"other":"Грузинский купон"},"symbol":"GEK","narrow":"GEK"},"GEL":{"displayName":{"other":"грузинского лари","one":"грузинский лари","few":"грузинских лари","many":"грузинских лари"},"symbol":"GEL","narrow":"ლ"},"GHC":{"displayName":{"other":"Ганский седи (1979–2007)"},"symbol":"GHC","narrow":"GHC"},"GHS":{"displayName":{"other":"ганского седи","one":"ганский седи","few":"ганских седи","many":"ганских седи"},"symbol":"GHS","narrow":"GHS"},"GIP":{"displayName":{"other":"гибралтарского фунта","one":"гибралтарский фунт","few":"гибралтарских фунта","many":"гибралтарских фунтов"},"symbol":"GIP","narrow":"£"},"GMD":{"displayName":{"other":"гамбийского даласи","one":"гамбийский даласи","few":"гамбийских даласи","many":"гамбийских даласи"},"symbol":"GMD","narrow":"GMD"},"GNF":{"displayName":{"other":"гвинейского франка","one":"гвинейский франк","few":"гвинейских франка","many":"гвинейских франков"},"symbol":"GNF","narrow":"FG"},"GNS":{"displayName":{"other":"Гвинейская сили"},"symbol":"GNS","narrow":"GNS"},"GQE":{"displayName":{"other":"Эквеле экваториальной Гвинеи"},"symbol":"GQE","narrow":"GQE"},"GRD":{"displayName":{"other":"Греческая драхма"},"symbol":"GRD","narrow":"GRD"},"GTQ":{"displayName":{"other":"гватемальского кетсаля","one":"гватемальский кетсаль","few":"гватемальских кетсаля","many":"гватемальских кетсалей"},"symbol":"GTQ","narrow":"Q"},"GWE":{"displayName":{"other":"Эскудо Португальской Гвинеи"},"symbol":"GWE","narrow":"GWE"},"GWP":{"displayName":{"other":"Песо Гвинеи-Бисау"},"symbol":"GWP","narrow":"GWP"},"GYD":{"displayName":{"other":"гайанского доллара","one":"гайанский доллар","few":"гайанских доллара","many":"гайанских долларов"},"symbol":"GYD","narrow":"$"},"HKD":{"displayName":{"other":"гонконгского доллара","one":"гонконгский доллар","few":"гонконгских доллара","many":"гонконгских долларов"},"symbol":"HK$","narrow":"$"},"HNL":{"displayName":{"other":"гондурасской лемпиры","one":"гондурасская лемпира","few":"гондурасские лемпиры","many":"гондурасских лемпир"},"symbol":"HNL","narrow":"L"},"HRD":{"displayName":{"other":"Хорватский динар"},"symbol":"HRD","narrow":"HRD"},"HRK":{"displayName":{"other":"хорватской куны","one":"хорватская куна","few":"хорватские куны","many":"хорватских кун"},"symbol":"HRK","narrow":"kn"},"HTG":{"displayName":{"other":"гаитянского гурда","one":"гаитянский гурд","few":"гаитянских гурда","many":"гаитянских гурдов"},"symbol":"HTG","narrow":"HTG"},"HUF":{"displayName":{"other":"венгерского форинта","one":"венгерский форинт","few":"венгерских форинта","many":"венгерских форинтов"},"symbol":"HUF","narrow":"Ft"},"IDR":{"displayName":{"other":"индонезийской рупии","one":"индонезийская рупия","few":"индонезийские рупии","many":"индонезийских рупий"},"symbol":"IDR","narrow":"Rp"},"IEP":{"displayName":{"other":"Ирландский фунт"},"symbol":"IEP","narrow":"IEP"},"ILP":{"displayName":{"other":"Израильский фунт"},"symbol":"ILP","narrow":"ILP"},"ILR":{"displayName":{"other":"ILR"},"symbol":"ILR","narrow":"ILR"},"ILS":{"displayName":{"other":"нового израильского шекеля","one":"новый израильский шекель","few":"новых израильских шекеля","many":"новых израильских шекелей"},"symbol":"₪","narrow":"₪"},"INR":{"displayName":{"other":"индийской рупии","one":"индийская рупия","few":"индийские рупии","many":"индийских рупий"},"symbol":"₹","narrow":"₹"},"IQD":{"displayName":{"other":"иракского динара","one":"иракский динар","few":"иракских динара","many":"иракских динаров"},"symbol":"IQD","narrow":"IQD"},"IRR":{"displayName":{"other":"иранского риала","one":"иранский риал","few":"иранских риала","many":"иранских риалов"},"symbol":"IRR","narrow":"IRR"},"ISJ":{"displayName":{"other":"ISJ"},"symbol":"ISJ","narrow":"ISJ"},"ISK":{"displayName":{"other":"исландской кроны","one":"исландская крона","few":"исландские кроны","many":"исландских крон"},"symbol":"ISK","narrow":"kr"},"ITL":{"displayName":{"other":"Итальянская лира"},"symbol":"ITL","narrow":"ITL"},"JMD":{"displayName":{"other":"ямайского доллара","one":"ямайский доллар","few":"ямайских доллара","many":"ямайских долларов"},"symbol":"JMD","narrow":"$"},"JOD":{"displayName":{"other":"иорданского динара","one":"иорданский динар","few":"иорданских динара","many":"иорданских динаров"},"symbol":"JOD","narrow":"JOD"},"JPY":{"displayName":{"other":"японской иены","one":"японская иена","few":"японские иены","many":"японских иен"},"symbol":"¥","narrow":"¥"},"KES":{"displayName":{"other":"кенийского шиллинга","one":"кенийский шиллинг","few":"кенийских шиллинга","many":"кенийских шиллингов"},"symbol":"KES","narrow":"KES"},"KGS":{"displayName":{"other":"киргизского сома","one":"киргизский сом","few":"киргизских сома","many":"киргизских сомов"},"symbol":"KGS","narrow":"KGS"},"KHR":{"displayName":{"other":"камбоджийского риеля","one":"камбоджийский риель","few":"камбоджийских риеля","many":"камбоджийских риелей"},"symbol":"KHR","narrow":"៛"},"KMF":{"displayName":{"other":"коморского франка","one":"коморский франк","few":"коморских франка","many":"коморских франков"},"symbol":"KMF","narrow":"CF"},"KPW":{"displayName":{"other":"северокорейской воны","one":"северокорейская вона","few":"северокорейские воны","many":"северокорейских вон"},"symbol":"KPW","narrow":"₩"},"KRH":{"displayName":{"other":"KRH"},"symbol":"KRH","narrow":"KRH"},"KRO":{"displayName":{"other":"KRO"},"symbol":"KRO","narrow":"KRO"},"KRW":{"displayName":{"other":"южнокорейской воны","one":"южнокорейская вона","few":"южнокорейские воны","many":"южнокорейских вон"},"symbol":"₩","narrow":"₩"},"KWD":{"displayName":{"other":"кувейтского динара","one":"кувейтский динар","few":"кувейтских динара","many":"кувейтских динаров"},"symbol":"KWD","narrow":"KWD"},"KYD":{"displayName":{"other":"доллара Островов Кайман","one":"доллар Островов Кайман","many":"долларов Островов Кайман"},"symbol":"KYD","narrow":"$"},"KZT":{"displayName":{"other":"казахского тенге","one":"казахский тенге","few":"казахских тенге","many":"казахских тенге"},"symbol":"KZT","narrow":"₸"},"LAK":{"displayName":{"other":"лаосского кипа","one":"лаосский кип","few":"лаосских кипа","many":"лаосских кипов"},"symbol":"LAK","narrow":"₭"},"LBP":{"displayName":{"other":"ливанского фунта","one":"ливанский фунт","few":"ливанских фунта","many":"ливанских фунтов"},"symbol":"LBP","narrow":"L£"},"LKR":{"displayName":{"other":"шри-ланкийской рупии","one":"шри-ланкийская рупия","few":"шри-ланкийские рупии","many":"шри-ланкийских рупий"},"symbol":"LKR","narrow":"Rs"},"LRD":{"displayName":{"other":"либерийского доллара","one":"либерийский доллар","few":"либерийских доллара","many":"либерийских долларов"},"symbol":"LRD","narrow":"$"},"LSL":{"displayName":{"other":"Лоти"},"symbol":"LSL","narrow":"LSL"},"LTL":{"displayName":{"other":"литовского лита","one":"литовский лит","few":"литовских лита","many":"литовских литов"},"symbol":"LTL","narrow":"Lt"},"LTT":{"displayName":{"other":"Литовский талон"},"symbol":"LTT","narrow":"LTT"},"LUC":{"displayName":{"other":"Конвертируемый франк Люксембурга"},"symbol":"LUC","narrow":"LUC"},"LUF":{"displayName":{"other":"Люксембургский франк"},"symbol":"LUF","narrow":"LUF"},"LUL":{"displayName":{"other":"Финансовый франк Люксембурга"},"symbol":"LUL","narrow":"LUL"},"LVL":{"displayName":{"other":"латвийского лата","one":"латвийский лат","few":"латвийских лата","many":"латвийских латов"},"symbol":"LVL","narrow":"Ls"},"LVR":{"displayName":{"other":"Латвийский рубль"},"symbol":"LVR","narrow":"LVR"},"LYD":{"displayName":{"other":"ливийского динара","one":"ливийский динар","few":"ливийских динара","many":"ливийских динаров"},"symbol":"LYD","narrow":"LYD"},"MAD":{"displayName":{"other":"марокканского дирхама","one":"марокканский дирхам","few":"марокканских дирхама","many":"марокканских дирхамов"},"symbol":"MAD","narrow":"MAD"},"MAF":{"displayName":{"other":"Марокканский франк"},"symbol":"MAF","narrow":"MAF"},"MCF":{"displayName":{"other":"MCF"},"symbol":"MCF","narrow":"MCF"},"MDC":{"displayName":{"other":"MDC"},"symbol":"MDC","narrow":"MDC"},"MDL":{"displayName":{"other":"молдавского лея","one":"молдавский лей","few":"молдавских лея","many":"молдавских леев"},"symbol":"MDL","narrow":"MDL"},"MGA":{"displayName":{"other":"малагасийского ариари","one":"малагасийский ариари","few":"малагасийских ариари","many":"малагасийских ариари"},"symbol":"MGA","narrow":"Ar"},"MGF":{"displayName":{"other":"Малагасийский франк"},"symbol":"MGF","narrow":"MGF"},"MKD":{"displayName":{"other":"македонского денара","one":"македонский денар","few":"македонских денара","many":"македонских денаров"},"symbol":"MKD","narrow":"MKD"},"MKN":{"displayName":{"other":"MKN"},"symbol":"MKN","narrow":"MKN"},"MLF":{"displayName":{"other":"Малийский франк"},"symbol":"MLF","narrow":"MLF"},"MMK":{"displayName":{"other":"мьянманского кьята","one":"мьянманский кьят","few":"мьянманских кьята","many":"мьянманских кьятов"},"symbol":"MMK","narrow":"K"},"MNT":{"displayName":{"other":"монгольского тугрика","one":"монгольский тугрик","few":"монгольских тугрика","many":"монгольских тугриков"},"symbol":"MNT","narrow":"₮"},"MOP":{"displayName":{"other":"патаки Макао","one":"патака Макао","many":"патак Макао"},"symbol":"MOP","narrow":"MOP"},"MRO":{"displayName":{"other":"мавританской угии (1973–2017)","one":"мавританская угия (1973–2017)","few":"мавританские угии (1973–2017)","many":"мавританских угий (1973–2017)"},"symbol":"MRO","narrow":"MRO"},"MRU":{"displayName":{"other":"мавританской угии","one":"мавританская угия","few":"мавританские угии","many":"мавританских угий"},"symbol":"MRU","narrow":"MRU"},"MTL":{"displayName":{"other":"Мальтийская лира"},"symbol":"MTL","narrow":"MTL"},"MTP":{"displayName":{"other":"Мальтийский фунт"},"symbol":"MTP","narrow":"MTP"},"MUR":{"displayName":{"other":"маврикийской рупии","one":"маврикийская рупия","few":"маврикийские рупии","many":"маврикийских рупий"},"symbol":"MUR","narrow":"Rs"},"MVP":{"displayName":{"other":"MVP"},"symbol":"MVP","narrow":"MVP"},"MVR":{"displayName":{"other":"мальдивской руфии","one":"мальдивская руфия","few":"мальдивские руфии","many":"мальдивских руфий"},"symbol":"MVR","narrow":"MVR"},"MWK":{"displayName":{"other":"малавийской квачи","one":"малавийская квача","few":"малавийские квачи","many":"малавийских квач"},"symbol":"MWK","narrow":"MWK"},"MXN":{"displayName":{"other":"мексиканского песо","one":"мексиканский песо","few":"мексиканских песо","many":"мексиканских песо"},"symbol":"MX$","narrow":"$"},"MXP":{"displayName":{"other":"Мексиканское серебряное песо (1861–1992)"},"symbol":"MXP","narrow":"MXP"},"MXV":{"displayName":{"other":"Мексиканская пересчетная единица (UDI)"},"symbol":"MXV","narrow":"MXV"},"MYR":{"displayName":{"other":"малайзийского ринггита","one":"малайзийский ринггит","few":"малайзийских ринггита","many":"малайзийских ринггитов"},"symbol":"MYR","narrow":"RM"},"MZE":{"displayName":{"other":"Мозамбикское эскудо"},"symbol":"MZE","narrow":"MZE"},"MZM":{"displayName":{"other":"Старый мозамбикский метикал"},"symbol":"MZM","narrow":"MZM"},"MZN":{"displayName":{"other":"мозамбикского метикала","one":"мозамбикский метикал","few":"мозамбикских метикала","many":"мозамбикских метикалов"},"symbol":"MZN","narrow":"MZN"},"NAD":{"displayName":{"other":"доллара Намибии","one":"доллар Намибии","many":"долларов Намибии"},"symbol":"NAD","narrow":"$"},"NGN":{"displayName":{"other":"нигерийской найры","one":"нигерийская найра","few":"нигерийские найры","many":"нигерийских найр"},"symbol":"NGN","narrow":"₦"},"NIC":{"displayName":{"other":"Никарагуанская кордоба (1988–1991)"},"symbol":"NIC","narrow":"NIC"},"NIO":{"displayName":{"other":"никарагуанской кордобы","one":"никарагуанская кордоба","few":"никарагуанские кордобы","many":"никарагуанских кордоб"},"symbol":"NIO","narrow":"C$"},"NLG":{"displayName":{"other":"Нидерландский гульден"},"symbol":"NLG","narrow":"NLG"},"NOK":{"displayName":{"other":"норвежской кроны","one":"норвежская крона","few":"норвежские кроны","many":"норвежских крон"},"symbol":"NOK","narrow":"kr"},"NPR":{"displayName":{"other":"непальской рупии","one":"непальская рупия","few":"непальские рупии","many":"непальских рупий"},"symbol":"NPR","narrow":"Rs"},"NZD":{"displayName":{"other":"новозеландского доллара","one":"новозеландский доллар","few":"новозеландских доллара","many":"новозеландских долларов"},"symbol":"NZ$","narrow":"$"},"OMR":{"displayName":{"other":"оманского риала","one":"оманский риал","few":"оманских риала","many":"оманских риалов"},"symbol":"OMR","narrow":"OMR"},"PAB":{"displayName":{"other":"панамского бальбоа","one":"панамский бальбоа","few":"панамских бальбоа","many":"панамских бальбоа"},"symbol":"PAB","narrow":"PAB"},"PEI":{"displayName":{"other":"Перуанское инти"},"symbol":"PEI","narrow":"PEI"},"PEN":{"displayName":{"other":"перуанского соля","one":"перуанский соль","few":"перуанских соля","many":"перуанских солей"},"symbol":"PEN","narrow":"PEN"},"PES":{"displayName":{"other":"перуанского соля (1863–1965)","one":"перуанский соль (1863–1965)","few":"перуанских соля (1863–1965)","many":"перуанских солей (1863–1965)"},"symbol":"PES","narrow":"PES"},"PGK":{"displayName":{"other":"кины Папуа – Новой Гвинеи","one":"кина Папуа – Новой Гвинеи","many":"кин Папуа – Новой Гвинеи"},"symbol":"PGK","narrow":"PGK"},"PHP":{"displayName":{"other":"филиппинского песо","one":"филиппинский песо","few":"филиппинских песо","many":"филиппинских песо"},"symbol":"PHP","narrow":"₱"},"PKR":{"displayName":{"other":"пакистанской рупии","one":"пакистанская рупия","few":"пакистанские рупии","many":"пакистанских рупий"},"symbol":"PKR","narrow":"Rs"},"PLN":{"displayName":{"other":"польского злотого","one":"польский злотый","few":"польских злотых","many":"польских злотых"},"symbol":"PLN","narrow":"zł"},"PLZ":{"displayName":{"other":"Злотый"},"symbol":"PLZ","narrow":"PLZ"},"PTE":{"displayName":{"other":"Португальское эскудо"},"symbol":"PTE","narrow":"PTE"},"PYG":{"displayName":{"other":"парагвайского гуарани","one":"парагвайский гуарани","few":"парагвайских гуарани","many":"парагвайских гуарани"},"symbol":"PYG","narrow":"₲"},"QAR":{"displayName":{"other":"катарского риала","one":"катарский риал","few":"катарских риала","many":"катарских риалов"},"symbol":"QAR","narrow":"QAR"},"RHD":{"displayName":{"other":"Родезийский доллар"},"symbol":"RHD","narrow":"RHD"},"ROL":{"displayName":{"other":"Старый Румынский лей"},"symbol":"ROL","narrow":"ROL"},"RON":{"displayName":{"other":"румынского лея","one":"румынский лей","few":"румынских лея","many":"румынских леев"},"symbol":"RON","narrow":"L"},"RSD":{"displayName":{"other":"сербского динара","one":"сербский динар","few":"сербских динара","many":"сербских динаров"},"symbol":"RSD","narrow":"RSD"},"RUB":{"displayName":{"other":"российского рубля","one":"российский рубль","few":"российских рубля","many":"российских рублей"},"symbol":"₽","narrow":"₽"},"RUR":{"displayName":{"other":"Российский рубль (1991–1998)"},"symbol":"р.","narrow":"р."},"RWF":{"displayName":{"other":"франка Руанды","one":"франк Руанды","many":"франков Руанды"},"symbol":"RWF","narrow":"RF"},"SAR":{"displayName":{"other":"саудовского рияла","one":"саудовский риял","few":"саудовских рияла","many":"саудовских риялов"},"symbol":"SAR","narrow":"SAR"},"SBD":{"displayName":{"other":"доллара Соломоновых Островов","one":"доллар Соломоновых Островов","many":"долларов Соломоновых Островов"},"symbol":"SBD","narrow":"$"},"SCR":{"displayName":{"other":"сейшельской рупии","one":"сейшельская рупия","few":"сейшельские рупии","many":"сейшельских рупий"},"symbol":"SCR","narrow":"SCR"},"SDD":{"displayName":{"other":"Суданский динар"},"symbol":"SDD","narrow":"SDD"},"SDG":{"displayName":{"other":"суданского фунта","one":"суданский фунт","few":"суданских фунта","many":"суданских фунтов"},"symbol":"SDG","narrow":"SDG"},"SDP":{"displayName":{"other":"Старый суданский фунт"},"symbol":"SDP","narrow":"SDP"},"SEK":{"displayName":{"other":"шведской кроны","one":"шведская крона","few":"шведские кроны","many":"шведских крон"},"symbol":"SEK","narrow":"kr"},"SGD":{"displayName":{"other":"сингапурского доллара","one":"сингапурский доллар","few":"сингапурских доллара","many":"сингапурских долларов"},"symbol":"SGD","narrow":"$"},"SHP":{"displayName":{"other":"фунта острова Святой Елены","one":"фунт острова Святой Елены","many":"фунтов острова Святой Елены"},"symbol":"SHP","narrow":"£"},"SIT":{"displayName":{"other":"Словенский толар"},"symbol":"SIT","narrow":"SIT"},"SKK":{"displayName":{"other":"Словацкая крона"},"symbol":"SKK","narrow":"SKK"},"SLL":{"displayName":{"other":"леоне"},"symbol":"SLL","narrow":"SLL"},"SOS":{"displayName":{"other":"сомалийского шиллинга","one":"сомалийский шиллинг","few":"сомалийских шиллинга","many":"сомалийских шиллингов"},"symbol":"SOS","narrow":"SOS"},"SRD":{"displayName":{"other":"суринамского доллара","one":"суринамский доллар","few":"суринамских доллара","many":"суринамских долларов"},"symbol":"SRD","narrow":"$"},"SRG":{"displayName":{"other":"Суринамский гульден"},"symbol":"SRG","narrow":"SRG"},"SSP":{"displayName":{"other":"южносуданского фунта","one":"южносуданский фунт","few":"южносуданских фунта","many":"южносуданских фунтов"},"symbol":"SSP","narrow":"£"},"STD":{"displayName":{"other":"добры Сан-Томе и Принсипи (1977–2017)","one":"добра Сан-Томе и Принсипи (1977–2017)","many":"добр Сан-Томе и Принсипи (1977–2017)"},"symbol":"STD","narrow":"STD"},"STN":{"displayName":{"other":"добры Сан-Томе и Принсипи","one":"добра Сан-Томе и Принсипи","many":"добр Сан-Томе и Принсипи"},"symbol":"STN","narrow":"Db"},"SUR":{"displayName":{"other":"Рубль СССР"},"symbol":"SUR","narrow":"SUR"},"SVC":{"displayName":{"other":"Сальвадорский колон"},"symbol":"SVC","narrow":"SVC"},"SYP":{"displayName":{"other":"сирийского фунта","one":"сирийский фунт","few":"сирийских фунта","many":"сирийских фунтов"},"symbol":"SYP","narrow":"£"},"SZL":{"displayName":{"other":"свазилендского лилангени","one":"свазилендский лилангени","few":"свазилендских лилангени","many":"свазилендских лилангени"},"symbol":"SZL","narrow":"SZL"},"THB":{"displayName":{"other":"таиландского бата","one":"таиландский бат","few":"таиландских бата","many":"таиландских батов"},"symbol":"฿","narrow":"฿"},"TJR":{"displayName":{"other":"Таджикский рубль"},"symbol":"TJR","narrow":"TJR"},"TJS":{"displayName":{"other":"таджикского сомони","one":"таджикский сомони","few":"таджикских сомони","many":"таджикских сомони"},"symbol":"TJS","narrow":"TJS"},"TMM":{"displayName":{"other":"Туркменский манат"},"symbol":"TMM","narrow":"TMM"},"TMT":{"displayName":{"other":"нового туркменского маната","one":"новый туркменский манат","few":"новых туркменских маната","many":"новых туркменских манатов"},"symbol":"ТМТ","narrow":"ТМТ"},"TND":{"displayName":{"other":"тунисского динара","one":"тунисский динар","few":"тунисских динара","many":"тунисских динаров"},"symbol":"TND","narrow":"TND"},"TOP":{"displayName":{"other":"тонганской паанги","one":"тонганская паанга","few":"тонганские паанги","many":"тонганских паанг"},"symbol":"TOP","narrow":"T$"},"TPE":{"displayName":{"other":"Тиморское эскудо"},"symbol":"TPE","narrow":"TPE"},"TRL":{"displayName":{"other":"турецкой лиры (1922–2005)","one":"турецкая лира (1922–2005)","few":"турецкие лиры (1922–2005)","many":"турецких лир (1922–2005)"},"symbol":"TRL","narrow":"TRL"},"TRY":{"displayName":{"other":"турецкой лиры","one":"турецкая лира","few":"турецкие лиры","many":"турецких лир"},"symbol":"TRY","narrow":"₺"},"TTD":{"displayName":{"other":"доллара Тринидада и Тобаго","one":"доллар Тринидада и Тобаго","many":"долларов Тринидада и Тобаго"},"symbol":"TTD","narrow":"$"},"TWD":{"displayName":{"other":"нового тайваньского доллара","one":"новый тайваньский доллар","few":"новых тайваньских доллара","many":"новых тайваньских долларов"},"symbol":"NT$","narrow":"NT$"},"TZS":{"displayName":{"other":"танзанийского шиллинга","one":"танзанийский шиллинг","few":"танзанийских шиллинга","many":"танзанийских шиллингов"},"symbol":"TZS","narrow":"TZS"},"UAH":{"displayName":{"other":"украинской гривны","one":"украинская гривна","few":"украинские гривны","many":"украинских гривен"},"symbol":"₴","narrow":"₴"},"UAK":{"displayName":{"other":"Карбованец (украинский)"},"symbol":"UAK","narrow":"UAK"},"UGS":{"displayName":{"other":"Старый угандийский шиллинг"},"symbol":"UGS","narrow":"UGS"},"UGX":{"displayName":{"other":"угандийского шиллинга","one":"угандийский шиллинг","few":"угандийских шиллинга","many":"угандийских шиллингов"},"symbol":"UGX","narrow":"UGX"},"USD":{"displayName":{"other":"доллара США","one":"доллар США","many":"долларов США"},"symbol":"$","narrow":"$"},"USN":{"displayName":{"other":"Доллар США следующего дня"},"symbol":"USN","narrow":"USN"},"USS":{"displayName":{"other":"Доллар США текущего дня"},"symbol":"USS","narrow":"USS"},"UYI":{"displayName":{"other":"Уругвайский песо (индекс инфляции)"},"symbol":"UYI","narrow":"UYI"},"UYP":{"displayName":{"other":"Уругвайское старое песо (1975–1993)"},"symbol":"UYP","narrow":"UYP"},"UYU":{"displayName":{"other":"уругвайского песо","one":"уругвайский песо","few":"уругвайских песо","many":"уругвайских песо"},"symbol":"UYU","narrow":"$"},"UYW":{"displayName":{"other":"UYW"},"symbol":"UYW","narrow":"UYW"},"UZS":{"displayName":{"other":"узбекского сума","one":"узбекский сум","few":"узбекских сума","many":"узбекских сумов"},"symbol":"UZS","narrow":"UZS"},"VEB":{"displayName":{"other":"Венесуэльский боливар (1871–2008)"},"symbol":"VEB","narrow":"VEB"},"VEF":{"displayName":{"other":"венесуэльского боливара (2008–2018)","one":"венесуэльский боливар (2008–2018)","few":"венесуэльских боливара (2008–2018)","many":"венесуэльских боливаров (2008–2018)"},"symbol":"VEF","narrow":"Bs"},"VES":{"displayName":{"other":"венесуэльского боливара","one":"венесуэльский боливар","few":"венесуэльских боливара","many":"венесуэльских боливаров"},"symbol":"VES","narrow":"VES"},"VND":{"displayName":{"other":"вьетнамского донга","one":"вьетнамский донг","few":"вьетнамских донга","many":"вьетнамских донгов"},"symbol":"₫","narrow":"₫"},"VNN":{"displayName":{"other":"VNN"},"symbol":"VNN","narrow":"VNN"},"VUV":{"displayName":{"other":"вату Вануату"},"symbol":"VUV","narrow":"VUV"},"WST":{"displayName":{"other":"самоанской талы","one":"самоанская тала","few":"самоанские талы","many":"самоанских тал"},"symbol":"WST","narrow":"WST"},"XAF":{"displayName":{"other":"франка КФА ВЕАС","one":"франк КФА ВЕАС","many":"франков КФА ВЕАС"},"symbol":"FCFA","narrow":"FCFA"},"XAG":{"displayName":{"other":"Серебро"},"symbol":"XAG","narrow":"XAG"},"XAU":{"displayName":{"other":"Золото"},"symbol":"XAU","narrow":"XAU"},"XBA":{"displayName":{"other":"Европейская составная единица"},"symbol":"XBA","narrow":"XBA"},"XBB":{"displayName":{"other":"Европейская денежная единица"},"symbol":"XBB","narrow":"XBB"},"XBC":{"displayName":{"other":"расчетная единица европейского валютного соглашения (XBC)"},"symbol":"XBC","narrow":"XBC"},"XBD":{"displayName":{"other":"расчетная единица европейского валютного соглашения (XBD)"},"symbol":"XBD","narrow":"XBD"},"XCD":{"displayName":{"other":"восточно-карибского доллара","one":"восточно-карибский доллар","few":"восточно-карибских доллара","many":"восточно-карибских долларов"},"symbol":"EC$","narrow":"$"},"XDR":{"displayName":{"other":"СДР (специальные права заимствования)"},"symbol":"XDR","narrow":"XDR"},"XEU":{"displayName":{"other":"ЭКЮ (единица европейской валюты)"},"symbol":"XEU","narrow":"XEU"},"XFO":{"displayName":{"other":"Французский золотой франк"},"symbol":"XFO","narrow":"XFO"},"XFU":{"displayName":{"other":"Французский UIC-франк"},"symbol":"XFU","narrow":"XFU"},"XOF":{"displayName":{"other":"франка КФА ВСЕАО","one":"франк КФА ВСЕАО","many":"франков КФА ВСЕАО"},"symbol":"CFA","narrow":"CFA"},"XPD":{"displayName":{"other":"Палладий"},"symbol":"XPD","narrow":"XPD"},"XPF":{"displayName":{"other":"французского тихоокеанского франка","one":"французский тихоокеанский франк","few":"французских тихоокеанских франка","many":"французских тихоокеанских франков"},"symbol":"CFPF","narrow":"CFPF"},"XPT":{"displayName":{"other":"Платина"},"symbol":"XPT","narrow":"XPT"},"XRE":{"displayName":{"other":"единица RINET-фондов"},"symbol":"XRE","narrow":"XRE"},"XSU":{"displayName":{"other":"XSU"},"symbol":"XSU","narrow":"XSU"},"XTS":{"displayName":{"other":"тестовый валютный код"},"symbol":"XTS","narrow":"XTS"},"XUA":{"displayName":{"other":"XUA"},"symbol":"XUA","narrow":"XUA"},"XXX":{"displayName":{"other":"единицы неизвестной валюты","one":"единица неизвестной валюты","many":"единиц неизвестной валюты"},"symbol":"XXXX","narrow":"XXXX"},"YDD":{"displayName":{"other":"Йеменский динар"},"symbol":"YDD","narrow":"YDD"},"YER":{"displayName":{"other":"йеменского риала","one":"йеменский риал","few":"йеменских риала","many":"йеменских риалов"},"symbol":"YER","narrow":"YER"},"YUD":{"displayName":{"other":"Югославский твердый динар"},"symbol":"YUD","narrow":"YUD"},"YUM":{"displayName":{"other":"Югославский новый динар"},"symbol":"YUM","narrow":"YUM"},"YUN":{"displayName":{"other":"Югославский динар"},"symbol":"YUN","narrow":"YUN"},"YUR":{"displayName":{"other":"YUR"},"symbol":"YUR","narrow":"YUR"},"ZAL":{"displayName":{"other":"Южноафриканский рэнд (финансовый)"},"symbol":"ZAL","narrow":"ZAL"},"ZAR":{"displayName":{"other":"южноафриканского рэнда","one":"южноафриканский рэнд","few":"южноафриканских рэнда","many":"южноафриканских рэндов"},"symbol":"ZAR","narrow":"R"},"ZMK":{"displayName":{"other":"замбийской квачи (1968–2012)","one":"замбийская квача (1968–2012)","few":"замбийские квачи (1968–2012)","many":"замбийских квач (1968–2012)"},"symbol":"ZMK","narrow":"ZMK"},"ZMW":{"displayName":{"other":"замбийской квачи","one":"замбийская квача","few":"замбийские квачи","many":"замбийских квач"},"symbol":"ZMW","narrow":"ZK"},"ZRN":{"displayName":{"other":"Новый заир"},"symbol":"ZRN","narrow":"ZRN"},"ZRZ":{"displayName":{"other":"Заир"},"symbol":"ZRZ","narrow":"ZRZ"},"ZWD":{"displayName":{"other":"Доллар Зимбабве"},"symbol":"ZWD","narrow":"ZWD"},"ZWL":{"displayName":{"other":"Доллар Зимбабве (2009)"},"symbol":"ZWL","narrow":"ZWL"},"ZWR":{"displayName":{"other":"ZWR"},"symbol":"ZWR","narrow":"ZWR"}},"numbers":{"nu":["latn"],"symbols":{"latn":{"decimal":",","group":" ","list":";","percentSign":"%","plusSign":"+","minusSign":"-","exponential":"E","superscriptingExponent":"×","perMille":"‰","infinity":"∞","nan":"не число","timeSeparator":":"}},"percent":{"latn":"#,##0 %"},"decimal":{"latn":{"long":{"1000":{"other":"0 тысячи","one":"0 тысяча","many":"0 тысяч"},"10000":{"other":"00 тысячи","one":"00 тысяча","many":"00 тысяч"},"100000":{"other":"000 тысячи","one":"000 тысяча","many":"000 тысяч"},"1000000":{"other":"0 миллиона","one":"0 миллион","many":"0 миллионов"},"10000000":{"other":"00 миллиона","one":"00 миллион","many":"00 миллионов"},"100000000":{"other":"000 миллиона","one":"000 миллион","many":"000 миллионов"},"1000000000":{"other":"0 миллиарда","one":"0 миллиард","many":"0 миллиардов"},"10000000000":{"other":"00 миллиарда","one":"00 миллиард","many":"00 миллиардов"},"100000000000":{"other":"000 миллиарда","one":"000 миллиард","many":"000 миллиардов"},"1000000000000":{"other":"0 триллиона","one":"0 триллион","many":"0 триллионов"},"10000000000000":{"other":"00 триллиона","one":"00 триллион","many":"00 триллионов"},"100000000000000":{"other":"000 триллиона","one":"000 триллион","many":"000 триллионов"}},"short":{"1000":{"other":"0 тыс'.'"},"10000":{"other":"00 тыс'.'"},"100000":{"other":"000 тыс'.'"},"1000000":{"other":"0 млн"},"10000000":{"other":"00 млн"},"100000000":{"other":"000 млн"},"1000000000":{"other":"0 млрд"},"10000000000":{"other":"00 млрд"},"100000000000":{"other":"000 млрд"},"1000000000000":{"other":"0 трлн"},"10000000000000":{"other":"00 трлн"},"100000000000000":{"other":"000 трлн"}}}},"currency":{"latn":{"currencySpacing":{"beforeInsertBetween":" ","afterInsertBetween":" "},"standard":"#,##0.00 ¤","accounting":"#,##0.00 ¤","unitPattern":"{0} {1}","short":{"1000":{"other":"0 тыс'.' ¤"},"10000":{"other":"00 тыс'.' ¤"},"100000":{"other":"000 тыс'.' ¤"},"1000000":{"other":"0 млн ¤"},"10000000":{"other":"00 млн ¤"},"100000000":{"other":"000 млн ¤"},"1000000000":{"other":"0 млрд ¤"},"10000000000":{"other":"00 млрд ¤"},"100000000000":{"other":"000 млрд ¤"},"1000000000000":{"other":"0 трлн ¤"},"10000000000000":{"other":"00 трлн ¤"},"100000000000000":{"other":"000 трлн ¤"}}}}},"nu":["latn"]}},"availableLocales":["ru"]})
}
// Intl.PluralRules.~locale.ru
/* @generated */
// prettier-ignore
if (Intl.PluralRules && typeof Intl.PluralRules.__addLocaleData === 'function') {
  Intl.PluralRules.__addLocaleData({"data":{"ru":{"categories":{"cardinal":["one","few","many","other"],"ordinal":["other"]},"fn":function(n, ord) {
  var s = String(n).split('.'), i = s[0], v0 = !s[1], i10 = i.slice(-1), i100 = i.slice(-2);
  if (ord) return 'other';
  return v0 && i10 == 1 && i100 != 11 ? 'one'
    : v0 && (i10 >= 2 && i10 <= 4) && (i100 < 12 || i100 > 14) ? 'few'
    : v0 && i10 == 0 || v0 && (i10 >= 5 && i10 <= 9) || v0 && (i100 >= 11 && i100 <= 14) ? 'many'
    : 'other';
}}},"availableLocales":["ru"]})
}
})
('object' === typeof window && window || 'object' === typeof self && self || 'object' === typeof global && global || {});
