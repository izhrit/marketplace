<?
require_once '../assets/config.php';
global $test_settings;
if (isset($test_settings) && isset($test_settings->use_test_time) && true==$test_settings->use_test_time && isset($_GET['use-test-time']))
{
	require_once '../assets/helpers/time.php';
	session_start();
	safe_store_test_time();
}
if (isset($_COOKIE[session_name()]) && !isset($_GET['start']))
{
	global $_SESSION, $auth_info;
	session_start();
	if (isset($_SESSION['auth_info']))
		$auth_info= $_SESSION['auth_info'];
}
?>
<html>
	<head>
		<title>Marketplace ПАУ</title>
		<meta http-equiv="X-UA-Compatible" content="IE=11" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />

		<link rel="icon" type="image/gif/png" href="img/pau-logo.png">
		<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/vendors/json2.js"></script>
		<script type="text/javascript" src="js/vendors/jszip.min.js"></script>

		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.custom.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/grid.locale-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.inputmask.js"></script>

		<link rel="stylesheet" type="text/css" href="css/vendors/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

		<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
		<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>

		<script type="text/javascript" src="js/vendors/typehead/handlebars.js"></script>
		<script type="text/javascript" src="js/vendors/typehead/typeahead.bundle.js"></script>

		<script>
		if("<?=isset($_GET['enable_viewers2'])?true:false?>"||'function'!=typeof (Intl.NumberFormat))
		{
			$.getScript('js/vendors/polyfill.js');
		}
		</script>  
		<link rel="stylesheet" type="text/css" href="css/vendors/fullcalendar/main.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/fullcalendar/tooltip.css" />
		<script type="text/javascript" src="js/vendors/fullcalendar/popper.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/tooltip.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/main.js"></script>
		<script type="text/javascript" src="js/vendors/fullcalendar/locales/ru.js"></script>
		<!-- вот это надо в extension ы! } -->

		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/marketplace.css" />

		<script type="text/javascript" src="js/ui-fixes.js"></script>

		<script type="text/javascript">
		window.onerror = function (message, source, lineno)
		{
			var msg = "Ошибка: <b>" + message + "</b><br/><br/>\n" +
				"файл: <b>" + source + "</b><br/>\n" +
				"строка: <b>" + lineno + '</b>';
			var div = $("#cpw-unhandled-error-message-form");
			div.html(msg);
			div.dialog({
				modal: true,
				width:800,height:"auto",
				buttons: { Ok: function () { $(this).dialog("close"); } }
			});
		}
		<? global $autologin_error_text; ?>
		app= { };
		function RegisterCpwFormsExtension(extension)
		{
			if ('mplace'==extension.key)
			{
				var base_url= '<?= $datamart_bck_url ?>';
				var sel= 'body.cpw-ama > div.cpw-marketplace-ui-content';
				var form_spec = extension.forms.mplace.CreateController({base_url:base_url});
				form_spec.CreateNew(sel);
			}
		}
		</script>

		<script type="text/javascript" src="js/ama-marketplace.js?2020_11_27_1614"></script>
	</head>

	<body class="cpw-ama">
		<div id="cpw-unhandled-error-message-form" title="Необработанная ошибка!"></div>
		<div class="cpw-marketplace-ui-content">

			<div class="ui-placeholder">
				<div>
					<small>
						<?= date_format(date_create(),'Y-m-d H:i:s') ?><br/>
						<div style="text-align:right">формируется html-страница на сервере.</div><br/>
						Здесь на html-странице должна отобразиться<br/>
					</small>
					<p style="text-align:center;">

						Форма входа на витрину ПАУ

					</p>
					<small>
						После загрузки и выполнения javascript.
						<br/><br/>
						<script>
							var t= new Date();
							var m= t.getMonth()+1;
							var d= t.getDate();
							if (d<10)
								d= '0' + d;
							if (m<10)
								m= '0' + m;
							document.write(t.getFullYear()+'-'+m+'-'+d+' '+t.toLocaleTimeString());
						</script>
						<div style="text-align:right">выполняется javascript в браузере.</div><br/>
					</small>
					<center><img src="img/loading-spinner.gif" /></center>
				</div>
			</div>

		</div>

		<div class="dm-footer">
			<div>
				
			</div>
		</div>
	</body>
</html>
