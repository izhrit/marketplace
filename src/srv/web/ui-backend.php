<?
require_once '../assets/config.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

global $trace_methods;
if ($trace_methods)
	require_once '../assets/helpers/log.php';

$possible_file_actions= array
(
	 'asset.jqgrid'=> 'asset/asset_jqgrid'
);

///////////////////////////////////////////////////////////////////////////////
if (!isset($_GET['action']))
{
	require '../assets/views/action-undefined.php';
}
else
{
	$action= $_GET['action'];

	global $trace_methods;
	if ($trace_methods)
		write_to_log('------'.$action.'--------------------------');

	if (!isset($possible_file_actions[$action]))
	{
		require_once '../assets/helpers/log.php';
		require_once '../assets/helpers/validate.php';
		exit_bad_request('unknown action!');
	}
	else
	{
		$subpath= $possible_file_actions[$action];
		if (''==$subpath)
			$subpath= $action;
		try
		{
			require_once '../assets/actions/backend/'.$subpath.'.php';
		}
		catch (Exception $exception)
		{
			require_once '../assets/helpers/log.php';
			require_once '../assets/helpers/validate.php';
			write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
			exit_internal_server_error('Unhandled exception!');
		}
	}
}
