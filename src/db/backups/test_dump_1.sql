-- MySQL dump 10.13  Distrib 5.7.28, for Win64 (x86_64)
--
-- Host: localhost    Database: marketplacedevel
-- ------------------------------------------------------
-- Server version	5.7.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ama`
--

DROP TABLE IF EXISTS `ama`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ama` (
  `id_AMA` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id_AMA`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ama`
--

LOCK TABLES `ama` WRITE;
/*!40000 ALTER TABLE `ama` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `ama` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset` (
  `id_Asset` int(11) NOT NULL AUTO_INCREMENT,
  `id_Debtor` int(11) NOT NULL,
  `id_Lot` int(11) DEFAULT NULL,
  `id_AMA` int(11) DEFAULT NULL,
  `Name` varchar(250) NOT NULL,
  `IsPublic` bit(1) NOT NULL DEFAULT b'0',
  `Body` longblob NOT NULL,
  PRIMARY KEY (`id_Asset`),
  KEY `refAsset_Debtor` (`id_Debtor`),
  KEY `refAsset_Lot` (`id_Lot`),
  KEY `refAsset_AMA` (`id_AMA`),
  CONSTRAINT `refAsset_AMA` FOREIGN KEY (`id_AMA`) REFERENCES `ama` (`id_AMA`),
  CONSTRAINT `refAsset_Debtor` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `refAsset_Lot` FOREIGN KEY (`id_Lot`) REFERENCES `lot` (`id_Lot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset`
--

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `bidding`
--

DROP TABLE IF EXISTS `bidding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bidding` (
  `id_Bidding` int(11) NOT NULL AUTO_INCREMENT,
  `id_Debtor` int(11) NOT NULL,
  `id_ETP` int(11) DEFAULT NULL,
  `id_Manager` int(11) NOT NULL,
  `id_User` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Bidding`),
  KEY `refBidding_Debtor` (`id_Debtor`),
  KEY `refBidding_ETP` (`id_ETP`),
  KEY `refBidding_Manager` (`id_Manager`),
  KEY `refBidding_User` (`id_User`),
  CONSTRAINT `refBidding_Debtor` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `refBidding_ETP` FOREIGN KEY (`id_ETP`) REFERENCES `etp` (`id_ETP`),
  CONSTRAINT `refBidding_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `refBidding_User` FOREIGN KEY (`id_User`) REFERENCES `user` (`id_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bidding`
--

LOCK TABLES `bidding` WRITE;
/*!40000 ALTER TABLE `bidding` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `bidding` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `debtor`
--

DROP TABLE IF EXISTS `debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor` (
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) NOT NULL,
  `id_Region` int(11) DEFAULT NULL,
  `Name` varchar(250) NOT NULL,
  `INN` char(13) DEFAULT NULL,
  `OGRN` char(10) DEFAULT NULL,
  `SNILS` char(10) DEFAULT NULL,
  `BankruptId` char(10) DEFAULT NULL,
  PRIMARY KEY (`id_Debtor`),
  KEY `refDebtor_Manager` (`id_Manager`),
  KEY `refDebtor_Region` (`id_Region`),
  CONSTRAINT `refDebtor_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `refDebtor_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtor`
--

LOCK TABLES `debtor` WRITE;
/*!40000 ALTER TABLE `debtor` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `debtor` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `efrsb`
--

DROP TABLE IF EXISTS `efrsb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `efrsb` (
  `id_EFRSB` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) NOT NULL,
  `Code` char(10) NOT NULL,
  `Level` int(11) NOT NULL,
  PRIMARY KEY (`id_EFRSB`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `efrsb`
--

LOCK TABLES `efrsb` WRITE;
/*!40000 ALTER TABLE `efrsb` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `efrsb` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `efrsb_asset`
--

DROP TABLE IF EXISTS `efrsb_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `efrsb_asset` (
  `id_EFRSB` int(11) NOT NULL,
  `id_Asset` int(11) NOT NULL,
  PRIMARY KEY (`id_EFRSB`,`id_Asset`),
  KEY `refEFRSB_Asset_Asset` (`id_Asset`),
  KEY `refEFRSB_Asset_EFRSB` (`id_EFRSB`),
  CONSTRAINT `refEFRSB_Asset_Asset` FOREIGN KEY (`id_Asset`) REFERENCES `asset` (`id_Asset`),
  CONSTRAINT `refEFRSB_Asset_EFRSB` FOREIGN KEY (`id_EFRSB`) REFERENCES `efrsb` (`id_EFRSB`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `efrsb_asset`
--

LOCK TABLES `efrsb_asset` WRITE;
/*!40000 ALTER TABLE `efrsb_asset` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `efrsb_asset` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `etp`
--

DROP TABLE IF EXISTS `etp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etp` (
  `id_ETP` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) DEFAULT NULL,
  `URL` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_ETP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etp`
--

LOCK TABLES `etp` WRITE;
/*!40000 ALTER TABLE `etp` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `etp` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `lot`
--

DROP TABLE IF EXISTS `lot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lot` (
  `id_Lot` int(11) NOT NULL AUTO_INCREMENT,
  `id_Bidding` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Lot`),
  KEY `refLot_Bidding` (`id_Bidding`),
  CONSTRAINT `refLot_Bidding` FOREIGN KEY (`id_Bidding`) REFERENCES `bidding` (`id_Bidding`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lot`
--

LOCK TABLES `lot` WRITE;
/*!40000 ALTER TABLE `lot` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `lot` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `efrsbNumber` varchar(10) DEFAULT NULL,
  `INN` char(13) DEFAULT NULL,
  `ArbitrManagerID` char(18) DEFAULT NULL,
  PRIMARY KEY (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager`
--

LOCK TABLES `manager` WRITE;
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `manager` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `okpd`
--

DROP TABLE IF EXISTS `okpd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `okpd` (
  `id_OKPD` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) NOT NULL,
  `Code` char(10) NOT NULL,
  `Level` int(11) NOT NULL,
  PRIMARY KEY (`id_OKPD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `okpd`
--

LOCK TABLES `okpd` WRITE;
/*!40000 ALTER TABLE `okpd` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `okpd` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `okpd_asset`
--

DROP TABLE IF EXISTS `okpd_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `okpd_asset` (
  `id_OKPD` int(11) NOT NULL,
  `id_Asset` int(11) NOT NULL,
  PRIMARY KEY (`id_OKPD`,`id_Asset`),
  KEY `refOKPD_Asset_OKPD` (`id_OKPD`),
  KEY `refOKPD_Asset_Asset` (`id_Asset`),
  CONSTRAINT `refOKPD_Asset_Asset` FOREIGN KEY (`id_Asset`) REFERENCES `asset` (`id_Asset`),
  CONSTRAINT `refOKPD_Asset_OKPD` FOREIGN KEY (`id_OKPD`) REFERENCES `okpd` (`id_OKPD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `okpd_asset`
--

LOCK TABLES `okpd_asset` WRITE;
/*!40000 ALTER TABLE `okpd_asset` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `okpd_asset` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `OKPO` char(10) DEFAULT NULL,
  PRIMARY KEY (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `MigrationNumber` varchar(250) NOT NULL,
  `MigrationName` varchar(250) NOT NULL,
  `MigrationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MigrationNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_migration`
--

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id_User` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `isOrganizer` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `user_asset`
--

DROP TABLE IF EXISTS `user_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_asset` (
  `id_User_asset` int(11) NOT NULL AUTO_INCREMENT,
  `id_User` int(11) NOT NULL,
  `id_Asset` int(11) NOT NULL,
  `IsOffer` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_User_asset`),
  KEY `refUser_Asset_User` (`id_User`),
  KEY `refUser_Asset_Asset` (`id_Asset`),
  CONSTRAINT `refUser_Asset_Asset` FOREIGN KEY (`id_Asset`) REFERENCES `asset` (`id_Asset`),
  CONSTRAINT `refUser_Asset_User` FOREIGN KEY (`id_User`) REFERENCES `user` (`id_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_asset`
--

LOCK TABLES `user_asset` WRITE;
/*!40000 ALTER TABLE `user_asset` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `user_asset` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping routines for database 'marketplacedevel'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-19 23:40:58
