﻿alter table efrsb add UNIQUE KEY `byCode` (`Code`);
alter table okpd add UNIQUE KEY `byCode` (`Code`);

insert into tbl_migration set MigrationNumber='m001', MigrationName='AddIndexes';
