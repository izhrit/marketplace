
CREATE TABLE ama(
    id_AMA    INT            AUTO_INCREMENT,
    Name      VARCHAR(80),
    PRIMARY KEY (id_AMA)
)ENGINE=INNODB
;



CREATE TABLE asset(
    id_Asset     INT             AUTO_INCREMENT,
    id_Debtor    INT             NOT NULL,
    id_Lot       INT,
    id_AMA       INT,
    Name         VARCHAR(250)    NOT NULL,
    IsPublic     BIT              DEFAULT 0 NOT NULL,
    Body         LONGBLOB        NOT NULL,
    PRIMARY KEY (id_Asset)
)ENGINE=INNODB
;



CREATE TABLE bidding(
    id_Bidding    INT    AUTO_INCREMENT,
    id_Debtor     INT    NOT NULL,
    id_ETP        INT,
    id_Manager    INT    NOT NULL,
    id_User       INT,
    PRIMARY KEY (id_Bidding)
)ENGINE=INNODB
;



CREATE TABLE debtor(
    id_Debtor     INT             AUTO_INCREMENT,
    id_Manager    INT             NOT NULL,
    id_Region     INT,
    Name          VARCHAR(250)    NOT NULL,
    INN           CHAR(13),
    OGRN          CHAR(10),
    SNILS         CHAR(10),
    BankruptId    CHAR(10),
    PRIMARY KEY (id_Debtor)
)ENGINE=INNODB
;



CREATE TABLE efrsb(
    id_EFRSB    INT             AUTO_INCREMENT,
    Name        VARCHAR(250)    NOT NULL,
    Code        CHAR(10)        NOT NULL,
    Level       INT             NOT NULL,
    PRIMARY KEY (id_EFRSB)
)ENGINE=INNODB
;



CREATE TABLE efrsb_asset(
    id_EFRSB    INT    NOT NULL,
    id_Asset    INT    NOT NULL,
    PRIMARY KEY (id_EFRSB, id_Asset)
)ENGINE=INNODB
;



CREATE TABLE etp(
    id_ETP    INT             AUTO_INCREMENT,
    Name      VARCHAR(250),
    URL       VARCHAR(250),
    PRIMARY KEY (id_ETP)
)ENGINE=INNODB
;



CREATE TABLE lot(
    id_Lot        INT    AUTO_INCREMENT,
    id_Bidding    INT,
    PRIMARY KEY (id_Lot)
)ENGINE=INNODB
;



CREATE TABLE manager(
    id_Manager         INT            AUTO_INCREMENT,
    FirstName          VARCHAR(50),
    LastName           VARCHAR(50),
    MiddleName         VARCHAR(50),
    efrsbNumber        VARCHAR(10),
    INN                CHAR(13),
    ArbitrManagerID    CHAR(18),
    PRIMARY KEY (id_Manager)
)ENGINE=INNODB
;



CREATE TABLE okpd(
    id_OKPD    INT             AUTO_INCREMENT,
    Name       VARCHAR(250)    NOT NULL,
    Code       CHAR(10)        NOT NULL,
    Level      INT             NOT NULL,
    PRIMARY KEY (id_OKPD)
)ENGINE=INNODB
;



CREATE TABLE okpd_asset(
    id_OKPD     INT    NOT NULL,
    id_Asset    INT    NOT NULL,
    PRIMARY KEY (id_OKPD, id_Asset)
)ENGINE=INNODB
;



CREATE TABLE region(
    id_Region    INT            AUTO_INCREMENT,
    Name         VARCHAR(50),
    OKPO         CHAR(10),
    PRIMARY KEY (id_Region)
)ENGINE=INNODB
;



CREATE TABLE tbl_migration(
    MigrationNumber    VARCHAR(250)    NOT NULL,
    MigrationName      VARCHAR(250)    NOT NULL,
    MigrationTime      TIMESTAMP       NOT NULL,
    PRIMARY KEY (MigrationNumber)
)ENGINE=INNODB
;



CREATE TABLE user(
    id_User        INT            AUTO_INCREMENT,
    Name           VARCHAR(50)    NOT NULL,
    Email          VARCHAR(50)    NOT NULL,
    Password       VARCHAR(20)    NOT NULL,
    isOrganizer    BIT             DEFAULT 0 NOT NULL,
    PRIMARY KEY (id_User)
)ENGINE=INNODB
;



CREATE TABLE user_asset(
    id_User_asset    INT    AUTO_INCREMENT,
    id_User          INT    NOT NULL,
    id_Asset         INT    NOT NULL,
    IsOffer          BIT     DEFAULT 1 NOT NULL,
    PRIMARY KEY (id_User_asset)
)ENGINE=INNODB
;



CREATE INDEX refAsset_Debtor ON asset(id_Debtor)
;
CREATE INDEX refAsset_Lot ON asset(id_Lot)
;
CREATE INDEX refAsset_AMA ON asset(id_AMA)
;
CREATE INDEX refBidding_Debtor ON bidding(id_Debtor)
;
CREATE INDEX refBidding_ETP ON bidding(id_ETP)
;
CREATE INDEX refBidding_Manager ON bidding(id_Manager)
;
CREATE INDEX refBidding_User ON bidding(id_User)
;
CREATE INDEX refDebtor_Manager ON debtor(id_Manager)
;
CREATE INDEX refDebtor_Region ON debtor(id_Region)
;
CREATE UNIQUE INDEX byCode ON efrsb(Code)
;
CREATE INDEX refEFRSB_Asset_Asset ON efrsb_asset(id_Asset)
;
CREATE INDEX refEFRSB_Asset_EFRSB ON efrsb_asset(id_EFRSB)
;
CREATE INDEX refLot_Bidding ON lot(id_Bidding)
;
CREATE UNIQUE INDEX byCode ON okpd(Code)
;
CREATE INDEX refOKPD_Asset_OKPD ON okpd_asset(id_OKPD)
;
CREATE INDEX refOKPD_Asset_Asset ON okpd_asset(id_Asset)
;
CREATE INDEX refUser_Asset_User ON user_asset(id_User)
;
CREATE INDEX refUser_Asset_Asset ON user_asset(id_Asset)
;
ALTER TABLE asset ADD CONSTRAINT refAsset_AMA 
    FOREIGN KEY (id_AMA)
    REFERENCES ama(id_AMA)
;

ALTER TABLE asset ADD CONSTRAINT refAsset_Debtor 
    FOREIGN KEY (id_Debtor)
    REFERENCES debtor(id_Debtor)
;

ALTER TABLE asset ADD CONSTRAINT refAsset_Lot 
    FOREIGN KEY (id_Lot)
    REFERENCES lot(id_Lot)
;


ALTER TABLE bidding ADD CONSTRAINT refBidding_Debtor 
    FOREIGN KEY (id_Debtor)
    REFERENCES debtor(id_Debtor)
;

ALTER TABLE bidding ADD CONSTRAINT refBidding_ETP 
    FOREIGN KEY (id_ETP)
    REFERENCES etp(id_ETP)
;

ALTER TABLE bidding ADD CONSTRAINT refBidding_Manager 
    FOREIGN KEY (id_Manager)
    REFERENCES manager(id_Manager)
;

ALTER TABLE bidding ADD CONSTRAINT refBidding_User 
    FOREIGN KEY (id_User)
    REFERENCES user(id_User)
;


ALTER TABLE debtor ADD CONSTRAINT refDebtor_Manager 
    FOREIGN KEY (id_Manager)
    REFERENCES manager(id_Manager)
;

ALTER TABLE debtor ADD CONSTRAINT refDebtor_Region 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;


ALTER TABLE efrsb_asset ADD CONSTRAINT refEFRSB_Asset_Asset 
    FOREIGN KEY (id_Asset)
    REFERENCES asset(id_Asset)
;

ALTER TABLE efrsb_asset ADD CONSTRAINT refEFRSB_Asset_EFRSB 
    FOREIGN KEY (id_EFRSB)
    REFERENCES efrsb(id_EFRSB)
;


ALTER TABLE lot ADD CONSTRAINT refLot_Bidding 
    FOREIGN KEY (id_Bidding)
    REFERENCES bidding(id_Bidding)
;


ALTER TABLE okpd_asset ADD CONSTRAINT refOKPD_Asset_Asset 
    FOREIGN KEY (id_Asset)
    REFERENCES asset(id_Asset)
;

ALTER TABLE okpd_asset ADD CONSTRAINT refOKPD_Asset_OKPD 
    FOREIGN KEY (id_OKPD)
    REFERENCES okpd(id_OKPD)
;


ALTER TABLE user_asset ADD CONSTRAINT refUser_Asset_Asset 
    FOREIGN KEY (id_Asset)
    REFERENCES asset(id_Asset)
;

ALTER TABLE user_asset ADD CONSTRAINT refUser_Asset_User 
    FOREIGN KEY (id_User)
    REFERENCES user(id_User)
;


