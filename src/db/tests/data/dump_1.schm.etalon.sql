--

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
--



--
--

DROP TABLE IF EXISTS `ama`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ama` (
  `Name` varchar(80) DEFAULT NULL,
  `id_AMA` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_AMA`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset` (
  `Body` longblob NOT NULL,
  `IsPublic` bit(1) NOT NULL DEFAULT b'0',
  `Name` varchar(250) NOT NULL,
  `id_AMA` int(11) DEFAULT NULL,
  `id_Asset` int(11) NOT NULL AUTO_INCREMENT,
  `id_Debtor` int(11) NOT NULL,
  `id_Lot` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Asset`),
  KEY `refAsset_AMA` (`id_AMA`),
  KEY `refAsset_Debtor` (`id_Debtor`),
  KEY `refAsset_Lot` (`id_Lot`)
  CONSTRAINT `refAsset_AMA` FOREIGN KEY (`id_AMA`) REFERENCES `ama` (`id_AMA`),
  CONSTRAINT `refAsset_Debtor` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `refAsset_Lot` FOREIGN KEY (`id_Lot`) REFERENCES `lot` (`id_Lot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `bidding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bidding` (
  `id_Bidding` int(11) NOT NULL AUTO_INCREMENT,
  `id_Debtor` int(11) NOT NULL,
  `id_ETP` int(11) DEFAULT NULL,
  `id_Manager` int(11) NOT NULL,
  `id_User` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Bidding`),
  KEY `refBidding_Debtor` (`id_Debtor`),
  KEY `refBidding_ETP` (`id_ETP`),
  KEY `refBidding_Manager` (`id_Manager`),
  KEY `refBidding_User` (`id_User`)
  CONSTRAINT `refBidding_Debtor` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `refBidding_ETP` FOREIGN KEY (`id_ETP`) REFERENCES `etp` (`id_ETP`),
  CONSTRAINT `refBidding_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `refBidding_User` FOREIGN KEY (`id_User`) REFERENCES `user` (`id_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor` (
  `BankruptId` char(10) DEFAULT NULL,
  `INN` char(13) DEFAULT NULL,
  `Name` varchar(250) NOT NULL,
  `OGRN` char(10) DEFAULT NULL,
  `SNILS` char(10) DEFAULT NULL,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  `id_Manager` int(11) NOT NULL,
  `id_Region` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Debtor`),
  KEY `refDebtor_Manager` (`id_Manager`),
  KEY `refDebtor_Region` (`id_Region`)
  CONSTRAINT `refDebtor_Manager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `refDebtor_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `efrsb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `efrsb` (
  `Code` char(10) NOT NULL,
  `Level` int(11) NOT NULL,
  `Name` varchar(250) NOT NULL,
  `id_EFRSB` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_EFRSB`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `efrsb_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `efrsb_asset` (
  `id_Asset` int(11) NOT NULL,
  `id_EFRSB` int(11) NOT NULL,
  PRIMARY KEY (`id_EFRSB`,`id_Asset`),
  KEY `refEFRSB_Asset_Asset` (`id_Asset`),
  KEY `refEFRSB_Asset_EFRSB` (`id_EFRSB`)
  CONSTRAINT `refEFRSB_Asset_Asset` FOREIGN KEY (`id_Asset`) REFERENCES `asset` (`id_Asset`),
  CONSTRAINT `refEFRSB_Asset_EFRSB` FOREIGN KEY (`id_EFRSB`) REFERENCES `efrsb` (`id_EFRSB`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `etp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etp` (
  `Name` varchar(250) DEFAULT NULL,
  `URL` varchar(250) DEFAULT NULL,
  `id_ETP` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_ETP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `lot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lot` (
  `id_Bidding` int(11) DEFAULT NULL,
  `id_Lot` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Lot`),
  KEY `refLot_Bidding` (`id_Bidding`)
  CONSTRAINT `refLot_Bidding` FOREIGN KEY (`id_Bidding`) REFERENCES `bidding` (`id_Bidding`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `ArbitrManagerID` char(18) DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `INN` char(13) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `efrsbNumber` varchar(10) DEFAULT NULL,
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `okpd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `okpd` (
  `Code` char(10) NOT NULL,
  `Level` int(11) NOT NULL,
  `Name` varchar(250) NOT NULL,
  `id_OKPD` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_OKPD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `okpd_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `okpd_asset` (
  `id_Asset` int(11) NOT NULL,
  `id_OKPD` int(11) NOT NULL,
  PRIMARY KEY (`id_OKPD`,`id_Asset`),
  KEY `refOKPD_Asset_Asset` (`id_Asset`),
  KEY `refOKPD_Asset_OKPD` (`id_OKPD`)
  CONSTRAINT `refOKPD_Asset_Asset` FOREIGN KEY (`id_Asset`) REFERENCES `asset` (`id_Asset`),
  CONSTRAINT `refOKPD_Asset_OKPD` FOREIGN KEY (`id_OKPD`) REFERENCES `okpd` (`id_OKPD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `Name` varchar(50) DEFAULT NULL,
  `OKPO` char(10) DEFAULT NULL,
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `MigrationName` varchar(250) NOT NULL,
  `MigrationNumber` varchar(250) NOT NULL,
  `MigrationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MigrationNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `Email` varchar(50) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `id_User` int(11) NOT NULL AUTO_INCREMENT,
  `isOrganizer` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `user_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_asset` (
  `IsOffer` bit(1) NOT NULL DEFAULT b'1',
  `id_Asset` int(11) NOT NULL,
  `id_User_asset` int(11) NOT NULL AUTO_INCREMENT,
  `id_User` int(11) NOT NULL,
  PRIMARY KEY (`id_User_asset`),
  KEY `refUser_Asset_Asset` (`id_Asset`),
  KEY `refUser_Asset_User` (`id_User`)
  CONSTRAINT `refUser_Asset_Asset` FOREIGN KEY (`id_Asset`) REFERENCES `asset` (`id_Asset`),
  CONSTRAINT `refUser_Asset_User` FOREIGN KEY (`id_User`) REFERENCES `user` (`id_User`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

