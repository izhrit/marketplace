
if (WScript.FullName.indexOf("cscript.exe")==-1)
{
  WScript.Echo("This script may be runned only with CScript..  Try to run create_sql.bat")
  WScript.Quit(1)
}

var fso = new ActiveXObject("Scripting.FileSystemObject");
var shell= new ActiveXObject("WScript.Shell");

function ReadSettings(filepath, settings)
{
	var f= fso.OpenTextFile(filepath, 1);
	var res= !settings ? {} : settings;
	var line_num= 1;
	while (!f.AtEndOfStream)
	{
		var line= f.ReadLine();
		while (0<line.length && (' '==line.charAt(0) || '\t'==line.charAt(0)))
			line= line.substr(1);
		if (0<line.length && '#'!=line.charAt(0))
		{
			var pos= line.indexOf('=');
			if (-1==pos)
			{
				WScript.Echo('wrong format of line ' + line_num + ': ' + line);
				WScript.Quit();
			}
			else
			{
				var name= line.substr(0,pos);
				var value= line.substr(pos+1);
				res[name]= value;
			}
		}
		line_num++;
	}
	return res;
}

function ProcessException(e)
{
  WScript.Echo(e);
  WScript.Echo(e.description);
  for (var prop in e)
  {
    WScript.Echo(prop + ":" + e[prop]);
  }
  WScript.Echo("}");
}

function WSHInclude(path)
{
  try
  {
    if (!fso)
      WScript.Echo('fso is null');
    var objStream = new ActiveXObject("ADODB.Stream");
    if (!objStream)
      WScript.Echo('can not create objStream');
    objStream.CharSet = "utf-8";
    objStream.Open();
    objStream.LoadFromFile(path);
    try
    {
      var txt= objStream.ReadText();
      if (!txt)
        WScript.Echo('ReadText for file returns null');
      try
      {
        var res= eval(txt);
      }
      catch (e)
      {
        WScript.Echo('eval throw exception!');
        throw e;
      }
      return res;
    }
    finally
    {
      objStream.Close();
      delete objStream;
    }
  }
  catch (e)
  {
    WScript.Echo("can not include path \"" + path + "\"");
    WScript.Echo("shell.CurrentDirectory=\"" + shell.CurrentDirectory + "\"");
    ProcessException(e);
    throw e;
  }
}

WSHInclude('..\\uc\\js\\vendors\\JSON2.js');
WSHInclude('..\\uc\\js\\vendors\\underscore.js');

function ReadAllText(fpath,funicode)
{
  try
  {
    var objStream = new ActiveXObject("ADODB.Stream");
    objStream.CharSet = "utf-8";
    objStream.Open();
    objStream.LoadFromFile(fpath);
    try
    {
      return objStream.ReadText();
    }
    finally
    {
      objStream.Close();
    }
  }
  catch (ex)
  {
    WScript.Echo("can not load file \"" + fpath + "\"");
    throw ex;
  }
}

function WriteAllTextUtf8(txt,fpath_out)
{
    var objStream = new ActiveXObject("ADODB.Stream");

    objStream.CharSet = "utf-8";
    objStream.Open();
    objStream.WriteText(txt);

    var binStream = new ActiveXObject("ADODB.Stream");
    binStream.Type = 1;
    binStream.Mode = 3;
    binStream.Open();

    objStream.Position= 3;
    objStream.CopyTo(binStream);
    objStream.Flush();
    objStream.Close();
    
    binStream.SaveToFile(fpath_out,2);
    binStream.Close();
}

function PrepareByTemplate(tpl_filename,settings)
{
	var built_filename= tpl_filename.substr(0,tpl_filename.length-('.tpl'.length));

	WScript.Echo('  build ' + built_filename);

	var tpl_txt= ReadAllText('tpl\\' + tpl_filename,1);
	var tpl= cpw_forms_tests_underscore.template(tpl_txt);
	var tpl_res= tpl(settings);
	
	WriteAllTextUtf8(tpl_res,'built-local\\' + built_filename);
}

WScript.Echo('read settings ..');
var settings= ReadSettings('settings.txt');
if (fso.FileExists('settings.local.txt'))
{
	WScript.Echo('read local settings ..');
	ReadSettings('settings.local.txt',settings);
}

var cur_dir_path= shell.CurrentDirectory;
settings.ROOT_PROJ_DIR= cur_dir_path.substr(0,cur_dir_path.length - ('src\\etc'.length));

// WScript.Echo(JSON.stringify(settings,null,'\t'));

var tpl_folder= fso.GetFolder('tpl');
var tpl_files= tpl_folder.Files;
for (var tpl_file_enum = new Enumerator(tpl_files); !tpl_file_enum.atEnd(); tpl_file_enum.moveNext())
{
	var tpl_file_name = tpl_file_enum.item();
	var tpl_file= fso.GetFile(tpl_file_name);
	PrepareByTemplate(tpl_file.Name,settings);
}

