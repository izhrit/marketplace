<?xml version="1.0" encoding="windows-1251"?>
<declarations>

	<params>
		<par><name>mysql_dir</name><value><%= MYSQL_DIR_BIN %></value></par>
		<par><name>mysql_conf</name><value><%= ROOT_PROJ_DIR %>src\etc\built-local\mysql.conf</value></par>
		<par><name>mysql_db</name><value><%= DBName %></value></par>
	</params>

</declarations>