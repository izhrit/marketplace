CREATE DATABASE `<%= DBName %>` DEFAULT CHARACTER SET utf8;

CREATE USER '<%= DBUser %>'@'%' IDENTIFIED BY '<%= DBPassword %>';
GRANT ALL ON <%= DBName %>.* TO '<%= DBUser %>'@'%';
