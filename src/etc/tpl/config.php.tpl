<?

global $default_db_options;
$default_db_options= (object)array(
	'host'=> 'localhost'
	,'user'=>'<%= DBUser %>'
	,'password'=> '<%= DBPassword %>'
	,'dbname'=> '<%= DBName %>'
	,'charset'=> 'utf8'
);

global $email_settings;
$email_settings= (object)array(
	 'smtp_server'=>     '?????'
	,'smtp_user'=>       '?????'
	,'smtp_password'=>   '?????'
	,'smtp_port'=>       25
	,'smtp_from_email'=> 'info@russianit.ru'
	,'smtp_from_name'=>  '���'
	,'enabled'=>false
);

global $sms_settings;
$sms_settings= (object)array(
	 'HTTPS_LOGIN'=>'???'
	,'HTTPS_PASSWORD'=>'???'
	,'HTTPS_ADDRESS'=>'???'

	,'FROM'=>'PAU'

	,'enabled'=>false
);


global $log_file_name, $trace_methods, $use_pretty_json_print;
$log_file_name= '<%= ROOT_PROJ_DIR %>src\srv\log.txt';
$trace_methods= true;
$use_pretty_json_print= true;

global $base_apps_url;
$base_apps_url= 'http://local.test/marketplace/';

global $datamart_api_url, $datamart_bck_url, $datamart_ui_url, $datamart_cabinetcc_url;
$datamart_api_url= $base_apps_url.'api.php';
$datamart_bck_url= $base_apps_url.'ui-backend.php';
$datamart_ui_url=  $base_apps_url.'ui.php';
