# 
# ВНИМАНИЕ!
# 
# В этом файле settings.txt собраны настройки по умолчанию
# Если настройки в конкретной рабочей папке отличаются,
# необходимо указать отличающиеся настройки в файле
#
# settings.local.txt 
#
# просто скопировать файл settings.txt в settings.local.txt и поменять
# нужные настройки, после чего выполнить prep.bat!
# 
# НЕ НАДО! менять сам файл settings.txt,
# чтобы просто настроиться на конкретную рабочую папку
# 

# папка, в которой расположены клиентские утилиты mysql 
#   (mysql.exe, mysqldump.exe)
# если эта папка есть в PATH, значение можно оставить пустым
# иначе там может быть путь типа 
# "C:\Program Files\MySQL\MySQL Server 5.7\bin" 
# в зависимости от того, где в действительности установлен mysql
MYSQL_DIR_BIN=

# host для mysql (опция --host)
MYSQL_HOST=localhost

# port для mysql
MYSQL_PORT=3306

# название базы данных
DBName=marketplacedevel

# пользователь базы данных
DBUser=marketplaceuser

# пароль к базе данных
DBPassword=marketplaceDevel7gh7
