require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	map:
	{
		'*':
		{
			txt: 'js/libs/txt'
		}
	}
});

require([
	  'forms/base/ajax/ajax-collector'
	, 'forms/test/test-ajax'
	, 'forms/mplace/tests/ajax/a_test_mplace'
], function (ajax_collector)
{
	var transports= Array.prototype.slice.call(arguments, 1);

	$.ajaxTransport
	(	
		'+*',
		function (options, originalOptions, jqXHR)
		{
			var send_abort = ajax_collector(transports)(options, originalOptions, jqXHR);
			if (send_abort)
			{
				console.log("text-ajax catch url " + options.url);
				return {
					send: function (headers, completeCallback)
					{
						try
						{
							return send_abort.send(headers, completeCallback);
						}
						catch (ex)
						{
							console.log("exception on test-ajax!");
							if (ex.description)
								console.log("description: " + ex.description);
							if (ex.stack)
								console.log("stack:\r\n" + ex.stack);
							throw ex;
						}
					},
					abort: send_abort.abort
				};
			}
		}
	);

	if (OnLoadTestTransports)
		OnLoadTestTransports();
});

