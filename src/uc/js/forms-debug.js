require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	paths:
	{
		styles: 'css',
		images: 'images'
	},
	map:
	{
		'*':
		{
			tpl:   'js/libs/tpl',
			css:   'js/libs/css',
			image: 'js/libs/image',
			txt:   'js/libs/txt'
		}
	}
});

var namespace = {
	  'mplace': 'forms/mplace/spec_ama_mplace'
	, 'test': 'forms/test/spec_test'
};

var namespaces_path = [];
var namespace_name = [];
for (var name in namespace)
{
	namespace_name.push(name);
	namespaces_path.push(namespace[name]);
}

require(namespaces_path, function ()
{
	aCPW_forms = {};
	var CPW_forms_specs = {};
	for (var i = 0; i < arguments.length; i++)
	{
		var spec = arguments[i];
		var forms = {};
		var specs = {};
		for (var name in spec.controller)
		{
			forms[name] = (function (path)
			{
				return function ()
				{
					var dfd = $.Deferred();
					require([path], function (controller_constructor)
					{
						dfd.resolve(controller_constructor);
					});
					return dfd.promise();
				};
			})(spec.controller[name].path);
			specs[name] = spec.controller[name];
		}
		aCPW_forms[namespace_name[i]] = forms;
		CPW_forms_specs[namespace_name[i]] = specs;
	}

	CPW_forms = aCPW_forms;

	$(function ()
	{
		var form_selector = $('#cpw-form-select');
		for (var form_namespace in CPW_forms)
		{
			var namespace_forms = CPW_forms[form_namespace];
			var namespace_specs = CPW_forms_specs[form_namespace];
			for (var form_name in namespace_forms)
			{
				var id_form = form_namespace + '.' + form_name;
				var option_attrs = {
					value: id_form,
					text: id_form
				};
				if (namespace_specs[form_name])
				{
					var spec = namespace_specs[form_name];
					if (spec.keywords)
					{
						option_attrs['data-keywords'] = spec.keywords;
						option_attrs.text += ' ' + spec.keywords;
					}
					if (spec.title)
					{
						option_attrs['data-title'] = spec.title;
						option_attrs.text += ' ' + spec.title;
					}
				}
				form_selector.append($('<option>', option_attrs));
			}
		}

		if (OnLoadForms)
			OnLoadForms();

		$('#forms_loaded_span').show();
	});
});
