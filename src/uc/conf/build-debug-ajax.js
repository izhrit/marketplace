({
    baseUrl: "..",
    include: ['js/ajax'],
    name: "optimizers/almond",
    out: "..\\built\\ajax.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})