call :prep-forms
call :build-forms
call :build-contents
call :build-wbt
call :build-test-ajax
exit /B

rem -----------------------------------------------------------
:prep-forms
call %~dp0forms\test\prep.bat 
call %~dp0forms\mplace\prep.bat 
exit /B

rem -----------------------------------------------------------
:build-forms
del built\forms.js
node optimizers\r.js -o conf\build-debug-forms.js
if NOT 0==%ERRORLEVEL% exit
@echo ok build-forms!
exit /B

rem -----------------------------------------------------------
:build-contents
del built\contents.js
node optimizers\r.js -o conf\build-debug-contents.js
if NOT 0==%ERRORLEVEL% exit
@echo ok build-contents!
exit /B

rem -----------------------------------------------------------
:build-wbt
del built\wbt.js
node optimizers\r.js -o conf\build-debug-wbt.js
if NOT 0==%ERRORLEVEL% exit
@echo ok build-wbt!
exit /B

rem -----------------------------------------------------------
:build-test-ajax
del built\test-ajax.js
node optimizers\r.js -o conf\build-debug-test-ajax.js
if NOT 0==%ERRORLEVEL% exit
@echo ok build-test-ajax!
exit /B
