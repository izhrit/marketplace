include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
shot_check_png ..\..\..\..\min\tests\shots\00new.png
wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\..\..\min\tests\shots\00new_invalid.png
wait_click_full_text "OK"
type_id test-test-form-edit 12
shot_check_png ..\..\..\..\min\tests\shots\01sav.png
wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\..\..\min\tests\contents\01new.result.txt
wait_click_full_text "Редактировать модель в элементе управления"
shot_check_png ..\..\..\..\min\tests\shots\01sav.png
exit