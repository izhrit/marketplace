define(function ()
{

	return {

		controller: {
		  "xml": { path: 'forms/test/core/xml/controller' }
		, "tpl": { path: 'forms/test/core/tpl/controller' }
		, "min": { path: 'forms/test/core/min/controller' }
		, "base": { path: 'forms/test/core/base/controller' }
		}

		, content: {
		  "min-01new": { path: 'txt!forms/test/core/min/tests/contents/01new.etalon.txt' }
		, "xml-01new": { path: 'txt!forms/test/core/xml/tests/contents/01new.etalon.xml' }
		}

	};

});