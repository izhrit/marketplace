include ..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\fastened\simple\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

wait_text "Правильное оформление ajax запроса"
shot_check_png ..\..\shots\00new.png

click_text "Форма с контроллером"
wait_text "форма с controller ом"
shot_check_png ..\..\shots\controller-new.png

play_stored_lines simple_fields_1
shot_check_png ..\..\shots\controller-1.png
wait_click_full_text "Сохранить"
wait_text_disappeared "форма с controller ом"

shot_check_png ..\..\shots\00new.png
click_text "Форма с контроллером"
wait_text "форма с controller ом"
shot_check_png ..\..\shots\controller-1.png

play_stored_lines simple_fields_2
shot_check_png ..\..\shots\controller-2.png
wait_click_full_text "Отмена"
wait_text_disappeared "форма с controller ом"

shot_check_png ..\..\shots\00new.png
click_text "Форма с контроллером"
wait_text "форма с controller ом"
shot_check_png ..\..\shots\controller-1.png

play_stored_lines simple_fields_2
shot_check_png ..\..\shots\controller-2.png
wait_click_full_text "Сохранить"
wait_text_disappeared "форма с controller ом"

shot_check_png ..\..\shots\00new.png
click_text "Форма с контроллером"
wait_text "форма с controller ом"
shot_check_png ..\..\shots\controller-2.png

exit