﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/test/collection/inline2/e_collection_inline2.html'
	, 'forms/base/b_collection'
	, 'txt!forms/test/collection/simple/tests/contents/test3.json.txt'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/h_msgbox'
	, 'forms/test/binded/simple/c_simple'
	, 'forms/base/h_times'
],
function (c_binded, tpl, b_collection, set, copy_codec, h_msgbox, c_simple, h_times)
{
	return function ()
	{
		var options=
			{
				  CreateBinding: b_collection
				, ccopy: copy_codec()
				, h_msgbox: h_msgbox
				, c_simple: c_simple
				, h_times: h_times
			};
		var controller = c_binded(tpl,options);
		return controller;
	}
});