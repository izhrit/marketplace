﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/collection/inline/e_collection_inline.html'
	, 'txt!forms/test/collection/simple/tests/contents/test3.json.txt'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/h_msgbox'
	, 'forms/test/fastened/simple/c_simple'
],
function (c_fastened, tpl, set, copy_codec, h_msgbox, c_simple)
{
	return function ()
	{
		var options=
			{
				  ccopy: copy_codec()
				, h_msgbox: h_msgbox
				, c_simple: c_simple
			};
		var controller = c_fastened(tpl, options);
		return controller;
	}
});