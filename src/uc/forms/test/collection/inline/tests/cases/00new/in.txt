include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Добавить"
shot_check_png ..\..\..\..\editable\tests\shots\00new.png
wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\..\..\simple\tests\contents\00new.json.result.txt
wait_click_full_text "Редактировать модель в элементе управления"
wait_text "Добавить"
shot_check_png ..\..\..\..\editable\tests\shots\00new.png
exit