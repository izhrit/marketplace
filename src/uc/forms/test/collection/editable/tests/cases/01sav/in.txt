include ..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\..\fastened\simple\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Добавить"
shot_check_png ..\..\shots\00new.png

click_text "Добавить"
shot_check_png ..\..\shots\01sav-add.png
play_stored_lines simple_fields_1
click_text "Отмена"

shot_check_png ..\..\shots\00new.png

click_text "Добавить"
shot_check_png ..\..\shots\01sav-add.png
play_stored_lines simple_fields_1
wait_click_full_text "Сохранить"

je $('div.cpw-collection [data-array-index="0"] .do_delete').click();
shot_check_png ..\..\shots\01sav-delete-confirm.png
wait_click_text "Нет"

click_text "Добавить"
play_stored_lines simple_fields_2
wait_click_full_text "Сохранить"

shot_check_png ..\..\shots\01sav.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\..\..\simple\tests\contents\01edt-2.json.result.txt
exit