include ..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\..\fastened\simple\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Добавить"

shot_check_png ..\..\shots\01sav.png

je $('div.cpw-collection [data-array-index="0"] .do_delete').click();
shot_check_png ..\..\shots\02edt-delete-confirm.png
wait_click_text "Нет"

shot_check_png ..\..\shots\01sav.png

je $('div.cpw-collection [data-array-index="0"] .do_delete').click();
shot_check_png ..\..\shots\02edt-delete-confirm.png
wait_click_text "Да"

shot_check_png ..\..\shots\02edt-deleted.png

je $('div.cpw-collection [data-array-index="0"]').click();
play_stored_lines simple_fields_1
shot_check_png ..\..\shots\02edt-edit.png
wait_click_text "Отмена"

shot_check_png ..\..\shots\02edt-deleted.png

je $('div.cpw-collection tr[data-array-index="0"]').click();
play_stored_lines simple_fields_1
shot_check_png ..\..\shots\02edt-edit.png
wait_click_full_text "Сохранить"

shot_check_png ..\..\shots\02edt.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\..\..\simple\tests\contents\01edt-1.json.result.txt
exit