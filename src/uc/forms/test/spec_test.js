define([
	  'forms/base/h_spec'
	, 'forms/test/core/spec_test_core'
	, 'forms/test/fastened/spec_test_fastened'
	, 'forms/test/collection/spec_test_collection'
]
, function (h_spec, core_spec, fastened_spec, collection_spec)
{
	var single_specs = {

		controller: {
		  "styles":       { path: 'forms/test/styles/c_styles' }
		, "msgbox":       { path: 'forms/test/msgbox/c_msgbox' }
		, "complex_view": { path: 'forms/test/complex/view/c_test_complex_view' }
		}

		, content: {
		  "complex-1": { path: 'txt!forms/test/complex/view/tests/contents/complex1.json.txt' }
		}

	};

	return h_spec.combine(core_spec, single_specs, fastened_spec, collection_spec);
});