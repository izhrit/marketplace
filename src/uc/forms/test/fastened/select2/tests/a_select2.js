﻿define
([
	'forms/test/fastened/select2/h_select2_variants'
	, 'forms/base/ajax/ajax-select2'
	, 'forms/base/codec/codec.copy'
]
, function (h_select2_variants, ajax_select2, codec_copy)
{
	var service = ajax_select2('test/select2/sixth');

	service.query= function(q)
	{
		var maxSize = 6;
		var items = [];
		var i = 0;
		for (; i < h_select2_variants.length && items.length<maxSize; i++)
		{
			var item= h_select2_variants[i];
			if (-1 != item.text.indexOf(q))
				items.push(h_select2_variants[i]);
		}
		var more = items.length == maxSize && i < h_select2_variants.length;
		return { results: items, pagination: { more: more } };
	}

	return service;
});