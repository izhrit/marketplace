﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/fastened/with/e_with.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		return controller;
	}
});