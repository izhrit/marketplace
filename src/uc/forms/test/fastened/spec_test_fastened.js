define(function ()
{

	return {

		controller: {
		  "fields":          { path: 'forms/test/fastened/fields/c_fastened_fields' }
		, "simple":          { path: 'forms/test/fastened/simple/c_simple' }
		, "containers":      { path: 'forms/test/fastened/containers/c_containers' }
		, "a_modal":         { path: 'forms/test/fastened/a_modal/c_a_modal' }
		, "select2":         { path: 'forms/test/fastened/select2/c_select2' }
		, "typeahead":       { path: 'forms/test/fastened/typeahead/c_typeahead' }
		, "with":            { path: 'forms/test/fastened/with/c_with' }
		, "with_collection": { path: 'forms/test/fastened/with_collection/c_with_collection' }
		}

		, content: {
		  "fields-01sav":          { path: 'txt!forms/test/fastened/fields/tests/contents/01sav.json.etalon.txt' }
		, "simple-01sav":          { path: 'txt!forms/test/fastened/simple/tests/contents/01sav.json.etalon.txt' }
		, "containers-01sav":      { path: 'txt!forms/test/fastened/containers/tests/contents/01sav.json.etalon.txt' }
		, "a_modal-01sav":         { path: 'txt!forms/test/fastened/a_modal/tests/contents/01sav.json.etalon.txt' }
		, "select2-01sav":         { path: 'txt!forms/test/fastened/select2/tests/contents/01sav.json.etalon.txt' }
		, 'typeahead-01sav':       { path: 'txt!forms/test/fastened/typeahead/tests/contents/01sav.json.etalon.txt' }
		, 'with-01sav':            { path: 'txt!forms/test/fastened/with/tests/contents/01sav.json.etalon.txt' }
		, 'with-collection-test1': { path: 'txt!forms/test/fastened/with_collection/tests/contents/test1.json.txt' }
		, 'with-collection-01sav': { path: 'txt!forms/test/fastened/with_collection/tests/contents/01edt-2.json.etalon.txt' }
		}

	};

});