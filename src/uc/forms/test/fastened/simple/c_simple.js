﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/fastened/simple/e_simple.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		return controller;
	}
});