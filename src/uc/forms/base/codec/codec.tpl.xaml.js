define([
	  'forms/base/codec/codec'
	, 'forms/base/log'
],
function (BaseCodec, GetLogger)
{
	var log = GetLogger('codec.tpl.xaml');
	return function ()
	{
		var codec_tpl_xaml = BaseCodec();

		var marker_comment_start = '<!-- ';
		var marker_comment_stop = '-->';

		var marker_tpl_cmd_skip_start= '-{';
		var marker_tpl_cmd_skip_stop = '-}';

		codec_tpl_xaml.Encode = function (data)
		{
			var result= '';
			var pos_start= 0;
			var pos_marker_comment_start = data.indexOf(marker_comment_start, pos_start);
			while (-1 != pos_marker_comment_start)
			{
				var part_before_comment = data.substring(pos_start, pos_marker_comment_start);
				result += part_before_comment;
				var pos_after_marker_comment_start = pos_marker_comment_start + marker_comment_start.length;
				var marker_cmd = data.substring(pos_after_marker_comment_start, pos_after_marker_comment_start + 2);
				switch (marker_cmd)
				{
					case marker_tpl_cmd_skip_start:
						var marker_skip_stop = marker_comment_start + marker_tpl_cmd_skip_stop;
						var pos_marker_skip_stop = data.indexOf(marker_skip_stop, pos_after_marker_comment_start + marker_cmd.length);
						pos_marker_comment_start = data.indexOf(marker_comment_stop, pos_marker_skip_stop) + marker_comment_stop.length - 1;
						break;
					case '+{':
						var pos_start_add = pos_after_marker_comment_start + marker_cmd.length;
						var marker_stop_add = "+} -->";
						var pos_stop_add = data.indexOf(marker_stop_add, pos_start_add);
						var part = data.substring(pos_start_add, pos_stop_add);
						result += part;
						pos_marker_comment_start = pos_stop_add + marker_stop_add.length - 1;
						break;
					default:
						var part = marker_comment_start;
						result += part;
						pos_marker_comment_start += marker_comment_start.length - 1;
						break;
				}
				pos_start = pos_marker_comment_start + 1;
				pos_marker_comment_start = data.indexOf(marker_comment_start, pos_start);
			}
			result += data.substring(pos_start);
			return result;
		}

		return codec_tpl_xaml;
	}
});
