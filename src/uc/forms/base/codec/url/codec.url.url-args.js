define([
	  'forms/base/codec/codec'
	, 'forms/base/codec/url/codec.url'
	, 'forms/base/codec/url/codec.url.args'
],
function (BaseCodec, codec_url, codec_url_args)
{
	return function ()
	{
		var codec = BaseCodec();

		var ccodec_url = codec_url();
		var ccodec_url_args = codec_url_args();

		codec.Encode= function(data)
		{
			var url= ccodec_url_args.Encode(data);
			return ccodec_url.Encode(url);
		}

		codec.Decode= function(txt)
		{
			var decoded_url = ccodec_url.Decode(txt);
			return ccodec_url_args.Decode(decoded_url);
		}

		return codec;
	}
});
