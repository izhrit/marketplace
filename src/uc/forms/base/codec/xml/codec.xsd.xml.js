define([
	  'forms/base/codec/xml/codec.xml'
	, 'forms/base/log'
],
function (BaseCodec, GetLogger)
{
	return function ()
	{
		var log = GetLogger('codec.xsd.xml');
		var res = BaseCodec();

		res.LoadXsd = function (xsd_text)
		{
			var xsd_doc = new ActiveXObject('MSXML2.DOMDocument.6.0');
			var res = xsd_doc.loadXML(xsd_text);
			if (res)
			{
				return xsd_doc;
			}
			else
			{
				log.Error('loading of xsd scheme is failed!');
				return null;
			}
		}

		res.GetScheme = function ()
		{
			log.Debug('GetScheme() {');
			log.Debug('unimplemented GetScheme !');
			alert('unimplemented GetScheme');
			log.Debug('GetScheme() }');
		};

		res.GetRootNamespaceURI = function ()
		{
			log.Debug('GetRootNamespaceURI() {');
			log.Debug('unimplemented GetRootNamespaceURI!');
			alert('unimplemented GetRootNamespaceURI!');
			log.Debug('GetRootNamespaceURI() }');
			return 'unspecified namespaceURI';
		};

		res.LoadXmlDocument = function (xml_string)
		{
			try
			{
				doc = new ActiveXObject('MSXML2.DOMDocument.6.0');
				doc.async = 'false';
				if (this.GetScheme && null != this.GetScheme)
				{
					doc.validateOnParse = 'true';
					var schemas = this.GetScheme();
					doc.schemas = schemas;
				}
				doc.loadXML(xml_string);
			}
			catch (ex)
			{
				log.Debug('can not prepare xml reader with scheme:\r\n' + ex.description);
				throw ex;
			}
			return (!doc) ? null : doc;
		};

		res.CheckLoadedXmlDocument = function (doc)
		{
			if (!doc)
			{
				log.Debug('!doc');
			}
			else if (doc.parseError && 0 != doc.parseError.errorCode)
			{
				var exmsg = "\nReason: " + doc.parseError.reason +
				"\nLine: " + doc.parseError.line + "\n";
				var err = { parseException: true, readablemsg: exmsg };
				log.Debug('throw parsing error..');
				throw err;
			}
			else if (!doc.documentElement)
			{
				log.Debug('!doc.documentElement');
			}
			else if (doc.getElementsByTagName('parsererror').length)
			{
				log.Debug('doc.getElementsByTagName("parsererror").length');
				log.Debug(doc.getElementsByTagName('parsererror'));
			}
			else
			{
				var namespaceURI = this.GetRootNamespaceURI();
				var namespaceURIEdit = !this.GetEditNamespaceURI ? '' : this.GetEditNamespaceURI();
				if (doc.documentElement.namespaceURI == namespaceURI || doc.documentElement.namespaceURI == namespaceURIEdit)
				{
					return true;
				}
				else
				{
					log.Debug('namespaceURI != doc.documentElement.namespaceURI');
					var err = { parseException: true, readablemsg: 'doc.documentElement.namespaceURI !=\r\n"' + namespaceURI + '"' };
					throw err;
				}
			}
			return false;
		};

		res.Decode = function (xml_string)
		{
			var doc = this.LoadXmlDocument(xml_string);
			if (!this.CheckLoadedXmlDocument(doc))
			{
				return null;
			}
			else
			{
				var res = this.DecodeXmlDocument(doc);
				return res;
			}
		};

		return res;
	}
});