define(function ()
{
	return function (scheme)
	{
		var endline= '\r\n';
		var res = '<?xml version="1.0" encoding="utf-8"?>' + endline;

		res += '<schema elementFormDefault="qualified">' + endline;
		res += '</schema>' + endline;

		return res;
	}
});