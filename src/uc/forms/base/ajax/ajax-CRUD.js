define([
	  'forms/base/ajax/ajax-service-base'
]
, function (ajax_service_base)
{
	return function(url_prefix)
	{
		var service_crud= ajax_service_base(url_prefix);

		service_crud.action= function (cx)
		{
			var args= cx.url_args();
			switch (args.cmd)
			{
				case 'add':
					var id = this.create(cx.data_args(), args);
					var res = { ok: true };
					if (id)
						res.id = id;
					cx.completeCallback(200, 'success', { text: JSON.stringify(res) });
					return;
				case 'get':
					var row = this.read(args.id);
					cx.completeCallback(200, 'success', { text: JSON.stringify(row) });
					return;
				case 'update':
					this.update(args.id, cx.data_args(), args);
					cx.completeCallback(200, 'success', { text: '{ "ok": true }' });
					return;
				case 'delete':
					this._delete(args.id);
					cx.completeCallback(200, 'success', { text: '{ "ok": true }' });
					return;
			}
			cx.completeCallback(400, 'bad request', { text: 'unknown cmd=' + args.cmd + '!' });
			
		}

		return service_crud;
	}
});
