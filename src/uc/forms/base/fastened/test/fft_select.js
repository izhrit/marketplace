﻿define([
	'forms/base/fastened/test/fft_abstract'
]
, function (fft_abstract)
{
	var default_fastened_field_tester = fft_abstract();

	default_fastened_field_tester.match = function (dom_item, tag_name, fc_type)
	{
		return 'SELECT'==tag_name;
	}

	default_fastened_field_tester.check_value = function (dom_item, value)
	{
		var options = dom_item.find('option');
		for (var i = 0; i < options.length; i++)
		{
			var option = $(options[i]);
			var text = option.text();
			if (-1 != text.indexOf(value))
			{
				return (option.attr('selected'))
					? null
					: ' text "' + text + '"! is WRONG!!!!!! should be "' + value + '"';
			}
		}
		return '  WRONG!!!!!! can not find option for text "' + value + '"';
	}

	default_fastened_field_tester.set_value = function (dom_item, value)
	{
		dom_item.val(value);
		var options = dom_item.find('option');
		for (var i = 0; i < options.length; i++)
		{
			var option = $(options[i]);
			var text = option.text();
			if (-1 != text.indexOf(value))
			{
				var option_value = option.attr('value');
				dom_item.val(option_value);
				break;
			}
		}
		dom_item.change();
		dom_item.trigger('focusout');
		return 'set value "' + value + '"';
	}

	return default_fastened_field_tester;
});