﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
	, 'forms/base/log'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/ql'
]
, function (fc_abstract, h_fastening_clip, GetLogger, codec_copy, ql)
{
	var log = GetLogger('fc_not_empty_array');
	var fc_not_empty_array = fc_abstract();
	var ccodec_copy = codec_copy();
	var fc_type_array_not_empty = 'array-not-empty';

	fc_not_empty_array.match = function (adom_item, tag_name, fc_type)
	{
		return fc_type_array_not_empty == fc_type;
	}

	fc_not_empty_array.save_to_model = function (model, model_selector, dom_item, not_empty_array_fc_data)
	{
		log.Debug('fc_not_empty_array.save_to_model {');
		if (not_empty_array_fc_data)
		{
			if (!model)
				model = [];
			var max_index = -1;
			for (var id in not_empty_array_fc_data.children)
			{
				if ('0' != id)
				{
					var fc_item_data = not_empty_array_fc_data.children[id];
					while (fc_item_data.cdata.index >= model.length)
						model.push(null);
					if (fc_item_data.cdata)
						model[fc_item_data.cdata.index] = fc_item_data.cdata.value;
					if (fc_item_data.cdata.index > max_index)
						max_index = fc_item_data.cdata.index;
				}
			}
			if (!not_empty_array_fc_data || !not_empty_array_fc_data.cdata || !not_empty_array_fc_data.cdata.f_filter)
			{
				while (max_index < (model.length - 1))
					model.pop();
			}
		}
		var h_fastening_clips = this.h_fastening_clips;
		h_fastening_clip.each_first_level_fastening(dom_item, function (child_dom_item)
		{
			var child_fc_model_selector = child_dom_item.attr('data-fc-selector');
			if ('seed' != child_fc_model_selector)
			{
				var child_fc_id = child_dom_item.attr('data-fc-id');
				var index = parseInt(child_dom_item.attr('data-array-index'));
				var child_fc_type = child_dom_item.attr('data-fc-type');
				var child_fc_methods = h_fastening_clips.find_methods_for(child_dom_item);
				model = child_fc_methods.save_to_model(model, index, child_dom_item, null);
			}
		});
		log.Debug('fc_not_empty_array.save_to_model }');
		return model;
	}

	var find_seed_dom_jitem= function(dom_item)
	{
		var res = null;
		dom_item.children().each(function (index)
		{
			var child_item = $(this);
			var fc_type = child_item.attr('data-fc-type');
			if ('array-item' == fc_type)
			{
				res = child_item;
				return false;
			}
			var seed = find_seed_dom_jitem(child_item);
			if (null != seed)
			{
				res = seed;
				return false;
			}
		});
		return res;
	}
	
	var collect_dom_items_info = function (seed_dom_jitem, not_empty_array_fc_data, old_array_info)
	{
		log.Debug('collect_dom_items_info {');
		var item_dom_jitem = seed_dom_jitem.next();
		var i = 0;
		var max_fc_id = 0;
		while (item_dom_jitem && null != item_dom_jitem && 1 == item_dom_jitem.length && i < 1000)
		{
			var array_index = parseInt(item_dom_jitem.attr('data-array-index'));
			var fc_id = parseInt(item_dom_jitem.attr('data-fc-id'));
			if (fc_id > max_fc_id)
				max_fc_id = fc_id;
			var item_info = {
				dom_item: item_dom_jitem
				, fc_id: fc_id
				, fc_data: not_empty_array_fc_data.children[fc_id]
			};
			for (var i = old_array_info.length; i <= array_index; i++)
			{
				if (i >= old_array_info.length)
					old_array_info.push(null);
			}
			old_array_info[array_index] = item_info;
			item_dom_jitem = item_dom_jitem.next();
			i++;
		}
		log.Debug('collect_dom_items_info }');
		return max_fc_id;
	}

	var find_index_in_old_array = function (/* [ {dom_item, fc_id, fc_data, index_in_new_array } ] */array_dom_items_info, array_item)
	{
		for (var i = 0; i < array_dom_items_info.length; i++)
		{
			var oai = array_dom_items_info[i];
			if (oai && null!=oai && !oai.index_in_new_array && 0 != oai.index_in_new_array && array_item == oai.fc_data.cdata.value)
			{
				return i;
			}
		}
		return null;
	}

	var fill_dom_item = function (dom_item, item_model, h_fastening_clips, index, fc_data)
	{
		log.Debug('fill_dom_item {');
		dom_item.attr('data-fc-selector', '[' + index + ']');
		dom_item.attr('data-array-index', index);

		h_fastening_clip.each_first_level_fastening(dom_item, function (child_dom_item)
		{
			var child_fc_id = child_dom_item.attr('data-fc-id');
			var child_fc_type = child_dom_item.attr('data-fc-type');
			var child_fc_model_selector = child_dom_item.attr('data-fc-selector');
			log.Debug('fill_dom_item child fc_id=' + child_fc_id + ' fc_type=' + child_fc_type + ' {');
			var child_fc_methods = h_fastening_clips.find_methods_for(child_dom_item);
			child_fc_methods.load_from_model(item_model, child_fc_model_selector, child_dom_item, fc_data.children[child_fc_id]);
			log.Debug('fill_dom_item child fc_id=' + child_fc_id + ' fc_type=' + child_fc_type + ' }');
		});
		log.Debug('fill_dom_item }');
	}

	var append_new_array_item = function (seed, last_loaded_item, fc_data, h_fastening_clips)
	{
		var new_dom_item = seed.clone();
		new_dom_item.removeClass('seed');
		new_dom_item.insertAfter(last_loaded_item);
		new_dom_item.attr('data-fc-id', fc_data.id);
		new_dom_item.show();

		fill_dom_item(new_dom_item, fc_data.cdata.value, h_fastening_clips, fc_data.cdata.index, fc_data);
		return new_dom_item;
	}

	
	var collect_new_array_indexes_in_old_array = function (new_array, /* [ {dom_item, fc_id, fc_data } ] */array_dom_items_info)
	{
		log.Debug('collect_new_array_indexes_in_old_array {');
		var new_array_info = [];
		for (var i = 0; i < new_array.length; i++)
		{
			var array_item = new_array[i];
			var index_in_old_array = find_index_in_old_array(array_dom_items_info, array_item);
			new_array_info[i] = { index_in_old_array: index_in_old_array };
			if (null != index_in_old_array)
				array_dom_items_info[index_in_old_array].index_in_new_array = i;
		}
		log.Debug('collect_new_array_indexes_in_old_array }');
		return new_array_info;
	}

	var drop_excess_dom_items_and_fc_data = function (/* [ {dom_item, fc_id, fc_data } ] */ array_dom_items_info, not_empty_array_fc_data)
	{
		log.Debug('drop_excess_dom_items_and_fc_data {');
		var last_index_in_new_array = 0;
		for (var i = 0; i < array_dom_items_info.length; i++)
		{
			var oai = array_dom_items_info[i];
			if (oai && null!=oai)
			{
				if (!oai.index_in_new_array && 0 != oai.index_in_new_array) // если нет в новом массиве
				{
					oai.dom_item.detach();                              // удаляем дом элемент
					delete not_empty_array_fc_data.children[oai.fc_id]; // удаляем крепёж
				}
				else
				{
					var index_in_new_array = oai.index_in_new_array;
					if (index_in_new_array >= last_index_in_new_array) // если идёт по старому порядку
					{
						last_index_in_new_array = index_in_new_array;  // ничего не удаляем
					}
					else
					{
						oai.dom_item.detach(); // удаляем дом элемент
						oai.detached = true;   // помечаем как удалённый, но не удаляем крепёж
					}
				}
			}
		}
		log.Debug('drop_excess_dom_items_and_fc_data }');
	}

	var fill_dom_items = function (self, seed_dom_jitem, array, new_array_indexes_in_old_array, array_dom_items_info, not_empty_array_fc_data, max_fc_id, filtered_array_indexes)
	{
		log.Debug('fill_dom_items {');
		var last_loaded_dom_jitem = seed_dom_jitem;
		for (var i = 0; i < array.length; i++)
		{
			log.Debug('fill_dom_items ' + i + ' {');
			var index_in_old_array = new_array_indexes_in_old_array[i].index_in_old_array;
			log.Debug('index_in_old_array= ' + index_in_old_array);
			if (null != index_in_old_array && array_dom_items_info[index_in_old_array].fc_data.cdata.value == array[i])
			{
				var oai = array_dom_items_info[index_in_old_array];
				not_empty_array_fc_data.children[oai.fc_id].cdata.index = !filtered_array_indexes ? i : filtered_array_indexes[i];
				if (true == oai.detached)
					oai.dom_item.insertAfter(last_loaded_dom_jitem);
				fill_dom_item(oai.dom_item, oai.fc_data.cdata.value, self.h_fastening_clips, !filtered_array_indexes ? i : filtered_array_indexes[i], oai.fc_data);
				last_loaded_dom_jitem = oai.dom_item;
			}
			else
			{
				max_fc_id++;
				var fc_id = max_fc_id;
				new_array_indexes_in_old_array[i].fc_id = fc_id;
				var new_array_item_fc_data = ccodec_copy.Copy(not_empty_array_fc_data.children['0']);
				new_array_item_fc_data.id = fc_id;
				new_array_item_fc_data.cdata = { index: !filtered_array_indexes ? i : filtered_array_indexes[i], value: array[i] };
				//var new_array_item_fc_data = { id: fc_id, afc_type: 'array-item', cdata: { index: i, value: array[i] } };
				if (!not_empty_array_fc_data.children)
					not_empty_array_fc_data.children = {};
				not_empty_array_fc_data.children[fc_id] = new_array_item_fc_data;
				last_loaded_dom_jitem = append_new_array_item(seed_dom_jitem, last_loaded_dom_jitem, new_array_item_fc_data, self.h_fastening_clips);
			}
			log.Debug('fill_dom_items ' + i + ' }');
		}
		log.Debug('fill_dom_items }');
	}

	var load_from_non_empty_array = function (self, filtered_array, not_empty_array_dom_item, not_empty_array_fc_data, filtered_array_indexes)
	{
		log.Debug('load_from_non_empty_array {');
		var seed_dom_jitem = find_seed_dom_jitem(not_empty_array_dom_item);

		var array_dom_items_info = []; // [ {dom_item, fc_id, fc_data } ]
		var max_fc_id = collect_dom_items_info(seed_dom_jitem, not_empty_array_fc_data, array_dom_items_info);

		var new_array_indexes_in_old_array = collect_new_array_indexes_in_old_array(filtered_array, array_dom_items_info); // [ { index_in_old_array } ]

		if (!not_empty_array_fc_data || null == not_empty_array_fc_data)
			not_empty_array_fc_data = { children: {} };

		drop_excess_dom_items_and_fc_data(array_dom_items_info, not_empty_array_fc_data);

		fill_dom_items(self, seed_dom_jitem, filtered_array, new_array_indexes_in_old_array, array_dom_items_info, not_empty_array_fc_data, max_fc_id, filtered_array_indexes);

		log.Debug('load_from_non_empty_array }');
		return not_empty_array_fc_data;
	}

	fc_not_empty_array.load_from_model = function (array, model_selector, dom_item, not_empty_array_fc_data)
	{
		log.Debug('load_from_model {');

		var filtered_array_indexes = null;
		var filtered_array = array;
		if (not_empty_array_fc_data && not_empty_array_fc_data.cdata && not_empty_array_fc_data.cdata.f_filter)
		{
			var f_filter = not_empty_array_fc_data.cdata.f_filter;
			filtered_array = [];
			filtered_array_indexes = [];
			for (var i = 0; i < array.length; i++)
			{
				var item = array[i];
				if (f_filter(item))
				{
					filtered_array_indexes.push(i);
					filtered_array.push(item);
				}
			}
		}

		if (filtered_array && null != filtered_array)
			not_empty_array_fc_data = load_from_non_empty_array(this, filtered_array, dom_item, not_empty_array_fc_data, filtered_array_indexes);
		if (!filtered_array || null == filtered_array || 0 == filtered_array.length)
		{
			dom_item.hide();
		}
		else
		{
			dom_item.show();
		}
		log.Debug('load_from_model }');
		return not_empty_array_fc_data;
	}

	fc_not_empty_array.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.not_empty_attrs = function (f_filter)
		{
			var res = this.fastening_attrs('', fc_type_array_not_empty);

			var array = this.current_context.model.value;

			if (!array || null == array || 0 == array.length)
			{
				res += ' style="display:none" ';
			}
			else
			{
				var fastening = this.current_context.fastening;
			}
			this.push_fc_context();
			if (f_filter)
				this.current_context.f_filter = f_filter;
			this.push_fc_context();
			return res;
		}

		_template_argument.iterate = function ()
		{
			var array_model = this.current_context.parent_context.model;
			if (!array_model.index && 0 != array_model.index)
			{
				array_model.index = -1;
				return true;
			}
			else
			{
				array_model.index++;
				var children = this.pop_fc_context();
				
				var array = array_model.value;
				var index = array_model.index - 1;
				var сdata = null;
				if (index >= 0)
					сdata = { index: index, value: array[index] };
				this.store_fc_data({ cdata: сdata, children: children, fc_type: 'array-item' });

				var array_len = !array || null == array ? 0 : array.length;

				var ok_iterate = array_model.index < array_len;
				if (this.current_context.f_filter)
				{
					while (ok_iterate && !this.current_context.f_filter(array[array_model.index]))
					{
						array_model.index++;
						ok_iterate = array_model.index < array_len;
					}
				}

				if (!ok_iterate)
				{
					var fc_data = { fc_type: fc_type_array_not_empty };
					if (this.current_context.f_filter)
						fc_data.cdata = { f_filter: this.current_context.f_filter };
					fc_data.children = this.pop_fc_context();
					this.store_fc_data(fc_data);
				}
				else
				{
					
					this.push_fc_context();
					var current_model = this.current_context.model;
					current_model.value = array_model.value[array_model.index];
					current_model.selector = null;
					current_model.index = null;
				}
				return ok_iterate;
			}
		}

		_template_argument.is_seed = function ()
		{
			var array_model = this.current_context.model;
			return -1 == array_model.index;
		}
	}

	return fc_not_empty_array;
});