﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_empty_array = fc_abstract();

	var fc_type_array_empty = 'array-empty';

	fc_empty_array.match = function (adom_item, tag_name, fc_type)
	{
		return fc_type_array_empty == fc_type;
	}

	fc_empty_array.save_to_model = function (model, model_selector, dom_item, empty_array_fc_data)
	{
		if (empty_array_fc_data && empty_array_fc_data.cdata.array && !empty_array_fc_data.cdata.f_filter)
			model = empty_array_fc_data.cdata.array;
		return model;
	}

	fc_empty_array.load_from_model = function (array, model_selector, dom_item, empty_array_fc_data)
	{
		var filtered_array = array;
		if (empty_array_fc_data && empty_array_fc_data.cdata && empty_array_fc_data.cdata.f_filter)
		{
			var f_filter = empty_array_fc_data.cdata.f_filter;
			filtered_array = [];
			for (var i = 0; i < array.length; i++)
			{
				var item = array[i];
				if (f_filter(item))
					filtered_array.push(item);
			}
		}

		if (filtered_array && null != filtered_array && 0 != filtered_array.length)
		{
			dom_item.hide();
			if (empty_array_fc_data && empty_array_fc_data.cdata && empty_array_fc_data.cdata.array)
				delete empty_array_fc_data.cdata.array;
		}
		else
		{
			dom_item.show();
			if (empty_array_fc_data && empty_array_fc_data.cdata)
				empty_array_fc_data.cdata.array = !array ? [] : array;
		}
		return empty_array_fc_data;
	}

	fc_empty_array.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.empty_attrs = function (f_filter)
		{
			var res = this.fastening_attrs('', 'array-empty');

			var array = this.current_context.model.value;

			if (array && null != array && 0 != array.length)
			{
				res += ' style="display:none" ';
			}
			else
			{
				var fastening = this.current_context.fastening;
				var fc_data = { cdata: { array: !array ? [] : array }, fc_type: fc_type_array_empty };
				if (f_filter)
					fc_data.cdata.f_filter = f_filter;
				this.store_fc_data(fc_data);
			}

			return res;
		}
	}

	return fc_empty_array;
});