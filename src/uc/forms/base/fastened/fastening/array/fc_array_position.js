﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_array_position = fc_abstract();

	fc_array_position.match = function (adom_item, tag_name, fc_type)
	{
		return 'position1-in-array' == fc_type;
	}

	fc_array_position.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		var array_item_dom_item = h_fastening_clip.get_parent_fc_of_type(dom_item, 'array-item');
		var array_index = array_item_dom_item.attr('data-array-index');
		array_index = parseInt(array_index);
		dom_item.text(array_index+1);
		return fc_data;
	}

	fc_array_position.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.item_position1 = function ()
		{
			var res = '<span ';
			res += this.fastening_attrs(null, 'position1-in-array');
			res += '>';

			if (!this.is_seed())
				res += (this.current_context.parent_context.model.index + 1);

			return res;
		}

		_template_argument.item_position1_end = function ()
		{
			return '</span>';
		}
	}

	return fc_array_position;
});