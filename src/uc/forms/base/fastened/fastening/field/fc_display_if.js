﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_display_if = fc_abstract();

	var fc_type_display_if = 'display_if';

	fc_display_if.match = function (adom_item, tag_name, fc_type)
	{
		return fc_type_display_if == fc_type;
	}

	var check_value= function(value)
	{
		return value && null != value;
	}

	fc_display_if.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		model_selector = dom_item.attr('data-fc-selector');

		//var value = h_fastening_clip.get_model_field_value(model, model_selector);

		var array_item_dom_item = h_fastening_clip.get_parent_fc_of_type(dom_item, 'array-item');
		/*var array_index = array_item_dom_item.attr('array-index');
		array_index = parseInt(array_index);*/
		var iterate_number= 0;
		var prev = array_item_dom_item.prev();
		while ('seed' != prev.attr('data-fc-selector'))
		{
			iterate_number++;
			array_item_dom_item = prev;
			prev = array_item_dom_item.prev();
		}

		var show = fc_data.cdata.check_if(model, iterate_number/*array_index*/);
		dom_item.css('display', show ? '' : 'none');
		return fc_data;
	}

	fc_display_if.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.display_if = function (model_selector_or_function)
		{
			/*if ('string' == typeof model_selector_or_function)
			{
				var model_selector = model_selector_or_function;
				var attrs = this.fastening_attrs(model_selector, fc_type_display_if);
				var value = this.value();

				var res = '';
				if (!check_value(value))
					res += 'display:none;';
				res += '"' + attrs.substring(0, attrs.length - 2);
				console.log('display_if } 1');
				return res;
			}
			else
			{*/
				var check_if_function = ('function' == typeof model_selector_or_function)
					? model_selector_or_function : check_value;
				var attrs = this.fastening_attrs(null, fc_type_display_if);

				this.store_fc_data({ cdata: { check_if: check_if_function }, fc_type: fc_type_display_if });

				var value = this.value();
				var res = '';
			//if (!check_if_function(value, this.current_context.parent_context.model.index))
				var index = -1;
				for (var name in this.current_context.parent_context.fastening.data)
					index++;
				if (!check_if_function(value, index))
					res += 'display:none;';
				res += '"' + attrs.substring(0, attrs.length - 2);
				return res;
			//}
		}
	}

	return fc_display_if;
});