﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_radio = fc_abstract();

	fc_radio.match = function (adom_item, tag_name, fc_type)
	{
		var dom_item = $(adom_item);
		return 'INPUT' == tag_name && 'radio' == dom_item.attr('type');
	}

	fc_radio.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		var value = h_fastening_clip.get_model_field_value(model, model_selector);
		if (value == $(dom_item).attr('value'))
		{
			$(dom_item).attr('checked', 'checked');
		}
		else
		{
			$(dom_item).attr('checked', null);
		}
		return fc_data;
	}

	fc_radio.save_to_model = function (model, model_selector, dom_item)
	{
		var value = dom_item.val();
		if (dom_item.prop('checked'))
			model = h_fastening_clip.set_model_field_value(model, model_selector, value);
		return model;
	}

	fc_radio.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.radio_attrs = function (model_selector, value_to_check)
		{
			var res = this.fastening_attrs(model_selector);
			res += ' name="' + model_selector + '"';
			res += ' value="' + value_to_check + '"';
			var actual_value = this.value();
			res += actual_value != value_to_check ? "" : " checked='checked'";
			return res;
		}
	}

	return fc_radio;
});