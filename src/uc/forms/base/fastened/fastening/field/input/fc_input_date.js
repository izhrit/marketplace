﻿define([
	  'forms/base/fastened/fastening/field/input/fc_input_text'
	, 'forms/base/h_times'
	, 'forms/base/codec/codec.copy'
]
, function (fc_input_text, h_times, codec_copy)
{
	var fc_date_field = codec_copy().Copy(fc_input_text);

	fc_date_field.match = function (dom_item, tag_name, fc_type)
	{
		return 'INPUT' == tag_name && 'date' == fc_type;
	}

	fc_date_field.render = function (options, model, model_selector, adom_item, fc_data)
	{
		var dom_item = $(adom_item);
		dom_item.datepicker(h_times.DatePickerOptions);
		return fc_data;
	}

	fc_date_field.destroy = function (adom_item, fc_data)
	{
		var dom_item = $(adom_item);
		dom_item.datepicker("destroy");
	}

	fc_date_field.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.date_attrs = function ()
		{
			if (_template_argument.options && _template_argument.options.readonly)
				return '';
			return this.fastening_attrs(null, 'date');
		}

		_template_argument.date = function (model_selector)
		{
			return this.txt(model_selector, 'date');
		}
	}

	return fc_date_field;
});