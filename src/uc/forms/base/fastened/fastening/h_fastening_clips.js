﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/extra/fc_typeahead'
	, 'forms/base/fastened/fastening/field/input/fc_input_checkbox'
	, 'forms/base/fastened/fastening/field/input/fc_input_radio'
	, 'forms/base/fastened/fastening/field/input/fc_input_date'
	, 'forms/base/fastened/fastening/field/input/fc_input_text'
	, 'forms/base/fastened/fastening/extra/fc_select2'
	, 'forms/base/fastened/fastening/field/fc_select'
	, 'forms/base/fastened/fastening/field/fc_textarea'
	, 'forms/base/fastened/fastening/control/fc_control'
	, 'forms/base/fastened/fastening/control/fc_fields'
	, 'forms/base/fastened/fastening/extra/fc_tabs'
	, 'forms/base/fastened/fastening/control/fc_a_modal'
	, 'forms/base/fastened/fastening/fc_with'
	, 'forms/base/fastened/fastening/field/fc_text'
	, 'forms/base/fastened/fastening/array/fc_empty_array'
	, 'forms/base/fastened/fastening/array/fc_not_empty_array'
	, 'forms/base/fastened/fastening/array/fc_array_position'
	, 'forms/base/fastened/fastening/array/fc_array_item'
	, 'forms/base/fastened/fastening/field/fc_display_if'
]
, function (fc_abstract)
{
	var fastenings = arguments;
	var fc_abstract_instance = fc_abstract();
	var h_fastening_clips = fc_abstract_instance.h_fastening_clips;

	h_fastening_clips.find_methods_for = function (adom_item)
	{
		var dom_item = $(adom_item);
		var tag_name = dom_item.prop("tagName").toUpperCase();
		var fc_type = dom_item.attr('data-fc-type');
		for (var i = 1; i < fastenings.length; i++)
		{
			var fastening = fastenings[i];

			if (fastening.match(dom_item, tag_name, fc_type))
				return fastening;
		}
		return fc_abstract();
	}

	h_fastening_clips.add_template_argument_methods = function (template_argument)
	{
		for (var i = 1; i < fastenings.length; i++)
		{
			var fastening = fastenings[i];
			fastening.add_template_argument_methods(template_argument);
		}
	}

	return h_fastening_clips;
});