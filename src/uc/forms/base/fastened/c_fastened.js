define([
	  'forms/base/controller'
	, 'forms/base/fastened/fastened_object'
	, 'forms/base/h_constraints'
	, 'forms/base/fastened/_template_argument'
],
function (BaseController, fastened_object, h_constraints, _template_argument)
{
	return function (view_template, options)
	{
		var controller = BaseController();

		controller.options = options;
		controller.GetViewTemplate = function () { return view_template; };

		if (options && options.constraints && !options.constraints_by_fields)
			options.constraints_by_fields = h_constraints.build_by_fields(options.constraints);

		controller.Render = function (selector)
		{
			var form_div = $(selector);

			var view_template = this.GetViewTemplate();

			this.fastening = fastened_object();
			this.fastening.controller = this;
			this.fastening.selector = selector;

			this.fastening.model = controller.model;
			this.fastening.options = options;
			if (options && options.foreign_data)
				this.fastening.foreign_data = options.foreign_data;

			var _tpl_arg = _template_argument(this.fastening.model);
			_tpl_arg.selector = selector;
			_tpl_arg.options = options;
			_tpl_arg.fastening = this.fastening;
			if (options && options.foreign_data)
				_tpl_arg.foreign_data = options.foreign_data;
			var html = view_template(_tpl_arg);
			if (html.charCodeAt(0) === 0xFEFF)
				html = html.substr(1);
			form_div.html(html);
			this.fastening.fc_data = _tpl_arg.current_context.fastening.data;
			this.fastening.on_after_update_dom = _tpl_arg.on_after_update_dom;

			this.fastening.render_fastened();

			if (_tpl_arg.on_after_render)
				_tpl_arg.on_after_render();
		};

		controller.GetFormContent = function ()
		{
			var res = this.fastening.SaveFieldsToModel(this.fastening.selector, this.model);
			return res;
		}

		controller.SetFormContent = function (content)
		{
			this.model =
				("string" != typeof content) ? content :
				('[' != content.charAt(0) && '{' != content.charAt(0)) ? null :
				JSON.parse(content);
		}

		controller.CreateNew = function (sel) { this.Render(sel); };
		controller.Edit = function (sel) { this.Render(sel); }

		controller.Validate = function ()
		{
			if (this.fastening.CurrentCollectionIndex >= 0)
			{
				return null;
			}
			else
			{
				return this.fastening.Validate(this.fastening.selector);
			}
		}

		var base_Destroy= controller.Destroy;
		controller.Destroy = function ()
		{
			base_Destroy.call(this);
			if (this.fastening && null!=this.fastening)
				this.fastening.Destroy(this.fastening.selector);
		}

		return controller;
	}
});
