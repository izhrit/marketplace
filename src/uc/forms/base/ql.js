define([
	'forms/base/codec/codec.copy'
]
, function (codec_copy)
{
	var ccodec_copy = codec_copy();

	var rowset = null;

	rowset = function (parent)
	{
		return {
			rows: []

			,exec: ((!parent || null==parent) ? null : parent.exec)

			,where: function(f)
			{
				var res = rowset(this);
				for (var i = 0; i < this.rows.length; i++)
				{
					var row = this.rows[i];
					if (f(row))
						res.rows.push(ccodec_copy.Copy(row));
				}
				return res;
			}

			,inner_join: function(arr,alias,fon)
			{
				var res = rowset(this);
				for (var i = 0; i < this.rows.length; i++)
				{
					var row = this.rows[i];
					for (var j= 0; j<arr.length; j++)
					{
						row[alias] = arr[j];
						if (fon(row))
							res.rows.push(ccodec_copy.Copy(row));
					}
					delete row[alias];
				}
				return res;
			}

			, left_join: function (arr, alias, fon)
			{
				var res = rowset(this);
				for (var i = 0; i < this.rows.length; i++)
				{
					var row = this.rows[i];
					var count = 0;
					for (var j = 0; j < arr.length; j++)
					{
						row[alias] = arr[j];
						if (fon(row))
						{
							res.rows.push(ccodec_copy.Copy(row));
							count++;
						}
					}
					if (0 == count)
					{
						row[alias] = null;
						res.rows.push(ccodec_copy.Copy(row));
					}
					delete row[alias];
				}
				return res;
			}

			,limit: function(maxSize)
			{
				var res = rowset(this);
				for (var i = 0; i < this.rows.length && i < maxSize; i++)
				{
					var row = this.rows[i];
					res.rows.push(ccodec_copy.Copy(row));
				}
				return res;
			}

			,map: function(f)
			{
				return ql_helper.map(this.rows,f);
			}

			,order: function(f)
			{
				this.rows.sort(f);
				return this;
			}

			,empty: function()
			{
				return 0==this.rows.length;
			}

			,from: function (arr, alias)
			{
				for (var i = 0; i < arr.length; i++)
				{
					var row = {};
					var fields = {}
					row[alias] = fields;
					ccodec_copy.CopyFieldsTo(arr[i], fields);
					this.rows.push(row);
				}
				return this;
			}
		}
	}

	var ql_helper = {
		select: function (f)
		{
			var res = rowset();
			res.exec = function ()
			{
				return this.map(f);
			};
			return res;
		}
		,from: function (arr, alias)
		{
			var res = rowset();
			for (var i = 0; i < arr.length; i++)
			{
				var row = {};
				var fields = {}
				row[alias] = fields;
				ccodec_copy.CopyFieldsTo(arr[i], fields);
				res.rows.push(row);
			}
			return res;
		}
		, map: function (arr,f)
		{
			var res = [];
			for (var i = 0; i < arr.length; i++)
			{
				res.push(f(arr[i]));
			}
			return res;
		}
		, find_first_or_null: function (arr, f)
		{
			if (arr)
			{
				for (var i = 0; i < arr.length; i++)
				{
					var row = arr[i];
					if (f(row))
						return row;
				}
			}
			return null;
		}
		, next_id: function (arr, id_name)
		{
			var res = 0;
			for (var i = 0; i < arr.length; i++)
			{
				var row = arr[i];
				var id = row[id_name];
				if (res <= id)
					res = id + 1;
			}
			return res;
		}
		, insert_with_next_ids: function (arr, id_name, new_rows)
		{
			for (var i= 0; i<new_rows.length; i++)
				this.insert_with_next_id(arr, id_name, new_rows[i]);
		}
		, insert_with_next_id: function (arr, id_name, new_row)
		{
			var id = this.next_id(arr, id_name);
			new_row[id_name] = id;
			arr.push(new_row);
			return new_row;
		}
		, _delete: function (arr, f)
		{
			var res = [];
			for (var i = 0; i < arr.length; i++)
			{
				var row = arr[i];
				if (!f(row))
					res.push(row);
			}
			return res;
		}
		, union: function(arr1,arr2)
		{
			if (arr1.rows)
			{
				var res = rowset();
				res.rows = this.union(arr1.rows,arr2.rows);
				return res;
			}
			else
			{
				var res = [];
				for (var i = 0; i < arr1.length; i++)
					res.push(arr1[i]);
				for (var i = 0; i < arr2.length; i++)
					res.push(arr2[i]);
				return res;
			}
		}
	}

	return ql_helper;
});
