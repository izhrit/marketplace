define(function ()
{
	var helper = {}

	helper.combine = function ()
	{
		var res = { controller: {}, content: {} };
		for (var i = 0; i < arguments.length; i++)
		{
			var spec = arguments[i];
			if (spec.controller)
			{
				for (var name in spec.controller)
				{
					if (res.controller[name])
						throw 'duplicate controller name "' + name + '"!';
					res.controller[name] = spec.controller[name];
				}
			}
			if (spec.content)
			{
				for (var name in spec.content)
				{
					if (res.content[name])
						throw 'duplicate content name "' + name + '"!';
					res.content[name] = spec.content[name];
				}
			}
		}
		return res;
	}

	return helper;
});