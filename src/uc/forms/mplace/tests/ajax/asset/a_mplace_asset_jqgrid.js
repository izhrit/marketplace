﻿define([
	  'forms/mplace/tests/d_marketplace'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/ql'
], function (db, ajax_jqGrid, ql)
{
	var service = ajax_jqGrid('ama/mplace?action=asset.jqgrid');

	service.rows_all = function ()
	{
		return ql.select(function (r) {return {
			id_Asset: r.a.id_Asset
			,Name: r.a.Name
		};})
		.from(db.asset, "a")
		.exec();

		return [];
	}

	return service;
});