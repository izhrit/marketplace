﻿define
(
	[
		  'forms/base/ajax/ajax-collector'

		, 'forms/mplace/tests/ajax/asset/a_mplace_asset_jqgrid'
	],
	function (ajax_collector)
	{
		return ajax_collector(Array.prototype.slice.call(arguments, 1));
	}
);