﻿define([
	  'txt!forms/mplace/tests/marketplace.json.txt'
	, 'forms/mplace/tests/d_marketplace_read'
	],
function (content_ini_txt, d_marketplace_read)
{
	d_marketplace_read.content= JSON.parse(content_ini_txt);
	return d_marketplace_read.content;
});