﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/mplace/biddings/e_mplace_biddings.html'
],
function (c_fastened, tpl)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		return controller;
	}
});