﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/mplace/assets/e_mplace_assets.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'ama/mplace');
		controller.base_grid_url = controller.base_url + '?action=asset.jqgrid';

		controller.colModel =
		[
			{ name: 'id_Asset', hidden: true }
			, { label: 'Объект', name: 'Name', width: 100 }
		];

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			return url;
		}

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var grid = $(sel + ' table.grid');
			var url = self.PrepareUrl();
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Показано объектов {1} из {2}'
				, emptyText: 'Нет объектов для отображения'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				, pager: '#cpw-cpw-ama-marketplace-assets-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: false
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true 
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка объектов конкурсной массы", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		return controller;
	}
});