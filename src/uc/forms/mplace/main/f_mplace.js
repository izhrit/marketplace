define(['forms/mplace/main/c_mplace'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'mplace'
		, Title: 'ПАУ.Marketplace'
	};
	return form_spec;
});
