﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/mplace/main/e_mplace.html'
	, 'forms/mplace/assets/c_mplace_assets'
	, 'forms/mplace/biddings/c_mplace_biddings'
],
function (c_fastened, tpl, c_mplace_assets, c_mplace_biddings)
{
	return function (options_arg)
	{
		var base_url = !options_arg ? 'ama/mplace' : options_arg.base_url;

		var options = {
			field_spec:
				{
					КМ: { controller: function () { return c_mplace_assets({ base_url: base_url }); }, render_on_activate:true }
					, Торги: { controller: function () { return c_mplace_biddings({ base_url: base_url }); }, render_on_activate:true }
				}
		};

		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' div > header button.logout').click(function () { if (self.OnLogout) self.OnLogout(); });

			$(sel + " .jquery-menu").menu();

			$(sel + " .profile-dropdown").click(function (e)
			{
				e.preventDefault();
				$(this).find("ul.jquery-menu").toggle();
			})

			//close menu
			$(document).on("click", function (e)
			{
				var target = $(e.target);
				/*menu buttons*/
			});

			$(sel + ' ul.ui-tabs-nav > li > a').click(function () { $(window).resize(); });
		}

		return controller;
	}
});