define(function ()
{

	return {

		controller: {
			"main": {
				path: 'forms/mplace/main/c_mplace'
				, title: 'главная форма marketplace а'
			}
			,"assets": {
				path: 'forms/mplace/assets/c_mplace_assets'
				, title: 'Все оБъекты конкурсной массы'
			}
			,"biddings": {
				path: 'forms/mplace/biddings/c_mplace_biddings'
				, title: 'Все торги'
			}
		}

		, content: {
		}

	};

});